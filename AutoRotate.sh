#!/bin/bash
# Bryant Hansen

GET_ORIENTATION=/projects/webpics/GetOrientation.sh
RIGHT_ROTATE=/projects/webpics/rrot.sh
LEFT_ROTATE=/projects/webpics/lrot.sh

O=$($GET_ORIENTATION "$1")

LEFT_OVER=6
RIGHT_OVER=8
RIGHT_UNKNOWN=3
UPRIGHT=1

case $O in
    ${LEFT_OVER})
        echo "${1}: rotate right" >&2
        "$RIGHT_ROTATE" "$1"
        ;;
    ${RIGHT_OVER}|${RIGHT_UNKNOWN})
        echo "${1}: rotate left" >&2
        "$LEFT_ROTATE" "$1"
        ;;
    ${UPRIGHT})
        echo "${1}: Already upright" >&2
        # DO NOTHING
        ;;
    *)
        echo "ERROR: $1 orientation value $O not known" >&2
        exit 2
esac
