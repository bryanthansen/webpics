#!/bin/bash
# Bryant Hansen

BUILD_SCRIPT=/projects/webpics/BuildAlbum.sh
LOGFILE="BA_$(date "+%Y%m%d_%H%M%S").log"

if [[ ! -f "$BUILD_SCRIPT" ]] && [[ ! "$(which "$BUILD_SCRIPT")" ]] ; then
	echo "$0 ERROR: build script not found!  Exiting abnormally!" >&2
	exit 2
fi

nice nohup "$BUILD_SCRIPT" >> "$LOGFILE" 2>&1 &

echo "Executing $BUILD_SCRIPT in $PWD and logging to $LOGFILE" >&2
