# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#         BuildAlbum     0.1  14 Sept 2006
#         by Bryant Hansen
#
#         updated 2008 02 18
#         updated 2010 12 12
#
#<!-- copyright (c) 2006-2008 by Bryant Hansen -->
#<!-- all rights reserved -->

# This should take over most of the current responsibility of MakePicindex.sh
# MakePicindex.sh should only create the indexed table of pics (or possibly the whole index.html too?)
# This function should process the rest of the directory and call the other scripts

######################################################
# Set global defaults
######################################################
THUMB_SIZE="$DEFAULT_THUMB_SIZE"
THUMB_DIR="$THUMB_DIR_BASENAME$THUMB_SIZE"
IMAGE_SIZE="$DEFAULT_IMAGE_SIZE"
IMAGE_DIR="$THUMB_DIR_BASENAME$IMAGE_SIZE"
SLIDESHOW=1  # variable to set whether the slide show is generates and a link to it below every picture
KB=1 # zoom & pan effects (currently terribly-slow)
FADE=1 # fade effects

INDEX_PROTOTYPE="index.html"
PICTURE_PROTOTYPE="picture.html"
SLIDE_PROTOTYPE="slide.html"
KB_PROTOTYPE="kb_1.html"

######################################################
# - Local Functions -
######################################################

WaitForKey()
{
    echo "Press Any Key"
    read -n 1 <&1
    return 0
}

DebugMessage()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "

    while [[ "$OPTION_FOUND" ]] ; do
        OPTION_FOUND=""
        if [[ "$1" = "-e" ]] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [[ "$1" = "-n" ]] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}

CreateBackups()
{
    local USAGE="CreateBackups [ -v ]"
    if [[ -d "${HTML_DIR}" ]] ; then
        (( "$VERBOSE" )) && DebugMessage "backing up $PWD/$HTML_DIR to \".${HTML_DIR}.`date +%Y%m%d`\""
        mv "$HTML_DIR" ".${HTML_DIR}.`date +%Y%m%d`"
    fi
    if [[ -f "$PICTURE_FILENAME" ]] ; then
        (( "$VERBOSE" )) && DebugMessage "backing up $PWD/$FILENAME to $FILENAME.`date +%Y%m%d`"
        cp "$PWD/$FILENAME" ".${FILENAME}.`date +%Y%m%d`"
    fi
    DebugMessage "backup done."
    return 0
}

FatalError()
{
    echo "$1" >&2
    exit 
}

VerifyRights()
{
    echo "test" > touch_test.tmp
    if [[ ! -f "touch_test.tmp" ]] ; then
        FatalError "$0 ERROR: no permissions to operate on $PWD.  Exiting abnormally!"
        exit -1
    fi
    rm "touch_test.tmp"
    return 0
}

ProcessDirectory() 
{
    DebugMessage "Processing directory \"$PWD\"..."

    # verify permissions
    VerifyRights

    if [[ "$BACKUP_ORIGINAL" ]] ; then
        CreateBackups
    fi

    VerifyTemplateFiles $VERBOSE_ARG "."
#    VerifyTemplateFiles -v "."

    /projects/webpics/ConvertRawImages.py

    /projects/webpics/MakeImageInfos.py

    if (( "$BLENDED_BORDER" )) ; then
        opts="--blended-border"
    fi

    local mth=""
    for resolution in $PICTURE_SIZES ; do
        nice MakeThumbnails $opts --size=$resolution &
        local mth="$mth $!"                              # append the process id
    done
    nice MakeThumbnails $opts --movie-mode --size=256 &
    local mth="$mth $!"                                                                         # get the process id

    DebugMessage "waiting for the thumbnail processes to complete ($mth)..."
    wait $mth  # wait for all to finish
    DebugMessage "thumbnail processes completed."

    nice MakePicturePages  $VERBOSE_ARG &
    local mpp=$!
    for resolution in $PICTURE_SIZES ; do
        nice MakePicturePages .thumbnails${resolution} &
        mpp="$mpp $!"
    done

    DebugMessage "waiting for the picturePage processes to complete ($mpp)..."
    wait $mpp
    DebugMessage "picturePage processes completed."

    if (( "$SLIDESHOW" )) ; then
        nice MakePicturePages $VERBOSE_ARG "--template=${SLIDE_PROTOTYPE}" ".thumbnails256" &
        mpps1=$!                                                                         # get the process id
        nice MakePicturePages $VERBOSE_ARG "--template=${SLIDE_PROTOTYPE}" ".thumbnails600" &
        local mpps2=$!                                                                         # get the process id
        nice MakePicturePages $VERBOSE_ARG "--template=${SLIDE_PROTOTYPE}" ".thumbnails1024" &
        local mpps3=$!                                                                         # get the process id
        MakePicturePages $VERBOSE_ARG "--template=${SLIDE_PROTOTYPE}" . &
        local mpps4=$!                                                                         # get the process id
        DebugMessage "waiting for the slideshow processes to complete..."
        wait "$mpps1" "$mpps2" "$mpps3" "$mpps4"            # wait for all to finish
        DebugMessage "slideshow processes completed."
    fi

    if (( "$KB" )) || (( "$FADE" )) ; then
        MakePicturePages $VERBOSE_ARG "--template=${KB_PROTOTYPE}" . &
        mpp2=$!                                                                         # get the process id
        wait "$mpp2" 
    fi

    # warning: have to put a wait here if the index will link to any pictures in the html directory, rather than just the ones in the .thumbnails
    MakePicindex
}

DirectoryIterator() 
{
    (( "$VERBOSE" )) && DebugMessage "DirectoryIterator: $*"
    until [[ -z "$1" ]] ; do
        # should look for more stuff in the command line, rather than just -v
        if [[ ! -z `expr "$1" : "\(-v\)"` ]] ; then
            VERBOSE=1
            VERBOSE_ARG="-v"
        fi
        shift
    done
    (( "$VERBOSE" )) && DebugMessage "DirectoryIterator: dirname=\"$PWD\""
    ProcessDirectory
    if (( "$RECURSE" )) ; then
        local subdir=""
        for subdir in * .* ; do
            if [[ -d "$subdir" ]] && [[ "$subdir" != "." ]] && [[ "$subdir" != ".." ]] ; then
                if [[ -L "$subdir" ]] ; then
                    (( "$VERBOSE" )) && DebugMessage "symbolic link: $subdir" `ls -l "$subdir" | sed 's/^.*'"$subdir"' //'`
                fi
                if (("`grep --files-with-matches -e "^[ ^t]*$subdir[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
                    (( "$VERBOSE" )) && DebugMessage "The directory \"$subdir\" is in the excludes list."
                else
                    cd "${subdir}"
                    ProcessDirectory
                    cd ..
                fi
            fi
        done
    fi
    (( "$VERBOSE" )) && DebugMessage "Exit DirectoryIterator()"
}


######################################################
# Setup Environment
######################################################
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}


######################################################
# - Main -
######################################################

DebugMessage "`basename "$0"` called"

until [ -z "$1" ] ; do
if [ ! -z `expr "$1" : "\(-\)"` ] ; then
    if [ ! -z `expr "$1" : "\(--size=\)"` ] ; then
        THUMB_SIZE_ARG=${1#--size=}
        if [ "$THUMB_SIZE_ARG" -gt "0" ] ; then
            THUMB_SIZE=$THUMB_SIZE_ARG
        else
            DebugMessage "INVALID Thumbnail size arg: $1"
        fi
    fi   
    if [ ! -z `expr "$1" : "\(-h\)"` ] ; then
        DebugMessage "TODO: must create help"
        DebugMessage "USAGE=$USAGE"
        exit 0
    fi
    if [ ! -z `expr "$1" : "\(-b\)"` ] \
    || [ ! -z `expr "$1" : "\(--backup-original\)"` ] ; then
        BACKUP_ORIGINAL=1
    fi
    if [ ! -z `expr "$1" : "\(-f\)"` ] \
    || [ ! -z `expr "$1" : "\(--refresh-template-files\)"` ] ; then
        BACKUP_ORIGINAL=1
    fi
    if [ ! -z `expr "$1" : "\(-r\)"` ] ; then
        RECURSE=1
    fi
    if [ ! -z `expr "$1" : "\(-ns\)"` ] ; then
        SLIDESHOW=0
    fi
    if [ ! -z `expr "$1" : "\(-nh\)"` ] ; then
        SHOWHEADING=0
    fi
    if [ ! -z `expr "$1" : "\(-v\)"` ] ; then
        VERBOSE=1
        VERBOSE_ARG="-v"
    fi
    if [ ! -z `expr "$1" : "\(--rn\)"` ] ; then
        REMOVE_NEW_TEMPLATES=1
    fi
    if [ ! -z `expr "$1" : "\(--remove-new-templates\)"` ] ; then
        REMOVE_NEW_TEMPLATES=1
    fi
fi
LAST_PARAM=$1
shift
done
if [[ -d "$LAST_PARAM" ]] && [[ ! -z "$LAST_PARAM" ]]; then
#   BASE_DIR="${LAST_PARAM%\/}"       # ==> Otherwise, move to indicated directory.
   BASE_DIR="${LAST_PARAM}"       # ==> Otherwise, move to indicated directory.
   DebugMessage "Warning: changing directory; local album.conf is not sourced - to avoid overriding command line options!"
else
   BASE_DIR="`pwd`"    # ==> No args to script, then use current working directory.
fi

if (( "$VERBOSE" )) ; then
   DebugMessage "Script Options:"
   DebugMessage "   VERBOSE is on.  VERBOSE=$VERBOSE"
   DebugMessage "   RECURSE=$RECURSE"
   DebugMessage "   THUMB_SIZE=$THUMB_SIZE"
   DebugMessage "   THUMB_DIR=$THUMB_DIR"
   DebugMessage "   IMAGE_SIZE=$IMAGE_SIZE"
   DebugMessage "   IMAGE_DIR=$IMAGE_DIR"
   DebugMessage "   TEMPLATE_DIR=$TEMPLATE_DIR"
   DebugMessage "   INDEX_PROTOTYPE=$INDEX_PROTOTYPE"
   DebugMessage "   SLIDE_PROTOTYPE=$SLIDE_PROTOTYPE"
   DebugMessage "   BASE_DIR=$BASE_DIR"
   DebugMessage "   SLIDESHOW=$SLIDESHOW"
   DebugMessage "   SHOWHEADING=$SHOWHEADING"
   DebugMessage "   FRESHEN_TEMPLATE_FILES=$FRESHEN_TEMPLATE_FILES"
fi

cd "$BASE_DIR"

DirectoryIterator "${VERBOSE_ARG}" "${BASE_DIR}"

DebugMessage "$0 complete, exiting normally..."
exit 0


