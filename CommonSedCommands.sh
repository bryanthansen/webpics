# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# CommonSedCommands.sh
# written by: Bryant Hansen
# last updated: 20080428
#
# copyright (c) 2003-2008 by Bryant Hansen
# all rights reserved
##################################################################
#!/bin/sh

# some needed SED commands
# some sed commands we'll need here; string them together to do something intelligent, but understandable (hopefully) in source
SED_COMMAND_DELETE_BLANK_LINES="/^[\t ]*$/d"
SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1="/^$/{;N;/^\n$/d;}"

# some sed commands we'll need here; string them together to do something intelligent, but understandable in source
SED_COMMAND_EXTRACT_ENDING="s/.*\.//g"
SED_COMMAND_REMOVE_LINK_INFO="s/ -> .*//g"
SED_COMMAND_TO_LOWER_CASE="y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
SED_COMMAND_PRINT_MOVIE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(mov\|avi\|mpi\|3gp\|mp4\)/p" 				# used with sed -n
SED_COMMAND_IS_CANON_RAW_FMT="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(cr2\)/p" 	# used with sed -n
SED_COMMAND_PRINT_IMAGE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)/p" 	# used with sed -n

