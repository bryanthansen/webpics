#!/usr/bin/python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# ConvertRawImage.py
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# created: 20080827
# last updated: 20080829
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

""" Program ConvertRawImage.py

This program will convert a raw image, a Canon .CR2 file to a .jpg file in a way that's compatible with the rest of the webpics programs.
"""

import sys
import getopt
import binascii
#import subprocess
from subprocess import Popen, PIPE
import os
import webpics
from webpics import *

def usage():
    print "USAGE: ", sys.argv[0], " [-d | --debug] [-f | --force] [-o output_file | --output=ouput_file] input_file"

ufraw = "ufraw-batch"
infile = ""
outfile = ""
#raw_settings = " --out-type=jpeg" + " --compression=100"
raw_settings = " --out-type=png"
raw_settings = " --out-type=jpeg" + " --compression=90"
conf_file  = ""
force = 0

deps = "ufraw-batch"

bash_script_output = False

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def check_dependencies(deps):
    print "TODO: check deps"


def is_excluded(file):
    excludes_file = "excludes.txt"
    exclude_match = `os.system('grep --files-with-matches -e "^[ ^t]*' + file + '[ ^t]*$" "' + excludes_file + '" 2>/dev/null | wc -l')`
    if exclude_match >= 1:
        return True
    else:
        return False


def read_config(varname):
    # TODO: finish and test
    # should accept a sequential list of config files
    conf_files = ["/projects/webpics/album.conf", "/etc/webpics/album.conf", os.environ['HOME'] + "/.webpics/album.conf", "album.conf"]
    ret_val = ""
    # read all conf files and all instances of the variable in a last-instance-wins fashion
    for file in conf_files:
        if os.path.exists(file):
            print file, " exists"
            # TODO: there's probably a python way of doing this, rather than this ugly cat/egrep/sed way
            command = 'cat ' + file + ' | egrep "^[ \t]*' + varname + '=.*" | sed "s/^[ \t]*' + varname + '=//"'
            ret = Popen(command, shell=True, stdout=PIPE)
            val = ret.stdout.readline()
            while val != "":
                ret_val = val.strip()
                val = ret.stdout.readline()
    debugMessage("returning " + ret_val + ", stripped = " + ret_val.strip(' \t\n'))
    return ret_val


def do_safe_command(ccmd, outfile):
    global bash_script_output
    if bash_script_output:
        print ccmd
    else:
        tempfile = outfile + "." + binascii.b2a_hex(os.urandom(6)) + ".tmp"
        if os.path.exists(outfile):
            os.rename(outfile, tempfile)
        debugMessage(" executing " + ccmd)
        os.system(ccmd)
        # if we've succeeded, remove the tempfile
        # if we've failed, restore the original outfile from the tempfile
        if os.path.exists(tempfile):
            if os.path.exists(outfile):
                os.remove(tempfile)
            else:
                os.rename(tempfile, outfile)


def is_convert_necessary(outfile, deps):
    global force
    if force:
        return 1
    
    if not os.path.exists(outfile):
        return 1

    for file in deps:
        if os.path.getmtime(file) > os.path.getmtime(outfile):
            return 1


def main(argv=None):

    global deps
    # check_dependencies(deps)

    # some code borrowed from http://docs.python.org/lib/module-getopt.html
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ho:vfc:", ["help", "outfile=", "conf-file=", "debug", "force"])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    infile = "".join(args)

    if infile == "":
        debugError(" ERROR: input file not specified.  This program requires at least 1 argument.  Exiting abnormally!")
        usage()
        sys.exit(3)
        
    if not os.path.exists(infile):
        debugError(" ERROR: input file, ", infile, " does not exist!  Exiting abnormally!")
        usage()
        sys.exit(4)

    # build a list of dependencies
    outfile_deps = [infile]
    
    # set default output
#    outfile = infile + ".jpg"
    outfile = infile + ".png"

    global conf_file
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif opt in ("-d", "--debug"):
            global _debug
            _debug = 1
        elif opt in ("-f", "--force"):
            global force
            force = 1
        elif opt in ("-o", "--outfile"):
            outfile = arg
        elif opt in ("-c", "--conf-file"):
            conf_file = arg

    CCMD = ufraw + raw_settings + " --output=" + outfile

    if conf_file == "":
        if os.path.exists(".ufrawrc"):
            debugMessage(".ufrawrc found in current directory")
            conf_file = ".ufrawrc"
        default_conf_file = infile + ".ufraw"
        if os.path.exists(infile + ".ufraw"):
            debugMessage(infile + ".ufraw" + " found in current directory")
            conf_file = infile + ".ufraw"
        default_conf_file = "." + infile + ".ufraw"
        if os.path.exists(default_conf_file):
            debugMessage(default_conf_file + " found in current directory")
            conf_file = default_conf_file
    if conf_file != "":
        debugMessage("conf_file = " + conf_file)
        if os.path.exists(conf_file):
            CCMD += " --conf=" + conf_file
            deps += conf_file
        else:
            debugError(" ERROR: ", conf_file, " does not exist.  Exiting abnormally!")
            sys.exit(5)

    CCMD += " " + infile

    if os.path.islink(infile):
        webpics.handle_link(infile, outfile, conf_file)

    if force or not webpics.is_fresh(outfile, outfile_deps):
        webpics.do_safe_command(CCMD, outfile)

if __name__=="__main__":
    main()

