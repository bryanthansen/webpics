#SED_COMMAND_IS_CANON_RAW_FMT="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(cr2\)/p" 	# used with sed -n



				# if the file is a raw format file, then convert to a jpeg first and then process that jpeg
				IsRawFormatFile="`echo "$file" | sed -n "$SED_COMMAND_IS_CANON_RAW_FMT"`"
				if [[ "$IsRawFormatFile" ]] ; then
					(( "$VERBOSE" )) && DebugMessage "$file is a raw format file (inferred by filename ending)!"
					# newfile="${file%.CR2}.jpg"  # this must match the naming conventions of ufraw-batch
					newfile="${file}.jpg"  # this must match the naming conventions of ufraw-batch

					# is this new file in the excludes list?
					if (("`grep --files-with-matches -e "^[ ^t]*$newfile[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
						(( "$VERBOSE" )) && DebugMessage "$file is not in the excludes list, but $newfile is.  Aborting thumb creation..."
						file="filename_that_will_not_be_processed"
					else
						# The thumbnail is already there; we're going to be processing it twice in our loops; don't count it twice though
						[[ -f "${newfile}" ]] && NumThumbs="`expr "$NumThumbs" - 1`"

						RAW_SETTINGS=""
						[[ -f "${HOME}/.ufrawrc" ]] && RAW_SETTINGS="${HOME}/.ufrawrc"
						[[ -f "./.ufrawrc" ]] 	&& RAW_SETTINGS="./.ufrawrc"
						[[ -f ".${file}.ufrawrc" ]] 	&& RAW_SETTINGS=".${file}.ufrawrc"
						if [[ "${RAW_SETTINGS}" ]] ; then
							(( "$VERBOSE" )) && DebugMessage "${file} ufraw conversion config = ${RAW_SETTINGS}"
						fi
						if [[ -f "${newfile}" ]] && [[ "${newfile}" -nt "${file}" ]] && [[ "${newfile}" -nt "${RAW_SETTINGS}" ]] ; then
							(( "$VERBOSE" )) && DebugMessage "the jpg version of the raw file \"${file}\" already exists and is up-to-date at ${newfile}!"
						else
                            tmpfile=".tmp.${newfile}"
							[[ -f "${tmpfile}" ]] && rm "${tmpfile}"
							DebugMessage -n "Creating jpeg version of ${file} at ${tmpfile}..."
							if [[ "${RAW_SETTINGS}" ]] ; then
								ufraw-batch "${file}" --conf="${RAW_SETTINGS}" --out-type=jpeg --compression=100 --output="${tmpfile}"
							else
								ufraw-batch "${file}" --out-type=jpeg --compression=100 --output="${tmpfile}"
							fi
							if [[ ! -f "${tmpfile}" ]] ; then
								DebugMessage "ERROR failed to create \"${tmpfile}\"!  Exiting abnormally!"
								exit -2
							fi
							mv "${tmpfile}" "${newfile}"
							# set the current file-under-operation to jpg
							file="${newfile}"
							DebugMessage "${file} created."
						fi
						# the new jpeg file is the master file that we'll use now
						file="${newfile}"
					fi
				fi

