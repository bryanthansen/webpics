# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) CopyNoOverwrite.sh     1.0     16 March 2003
#
#         by Bryant Hansen
#
#         Copies a file only if there is no file to overwrite

USAGE="CopyNoOverwrite [ -v ] SOURCE_FILE DEST_FILE"
VERBOSE=""

if [ "$1" = "-v" ] ; then
   VERBOSE=1
   echo "$0 \"$1\" \"$2\" \"$3\""
   shift # discard the verbose argument and continue
fi

if [ ! -n "$1" ] ; then
   echo "$0: arg #1 (\"$1\") is null."
   shift
fi

if [ $# -ne 2 ] ; then
   echo "$0 ERROR: invalid arguments.  SOURCE_FILENAME=\"$1\" DEST_FILE=\"$2\""
   echo "USAGE: $USAGE"
   exit -1
else
   SOURCE_FILENAME="$1"
   DEST_FILENAME="$2"
fi

if [ ! -f "$SOURCE_FILENAME" ] ; then
   echo "$0 ERROR: Source file \"$SOURCE_FILE\" does not exist."
   exit -2
fi

if [ ! -f "$DEST_FILENAME" ] ; then
   if [ $VERBOSE ] ; then
      echo "Copying \"$SOURCE_FILENAME\" to \"$DEST_FILENAME\"..."
   fi
   cp "$SOURCE_FILENAME" "$DEST_FILENAME"
else
   if [ $VERBOSE ] ; then
      echo "\"$DEST_FILENAME\" exists.  no file copied."
   fi
fi

