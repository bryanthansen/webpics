# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         DebugMessage.sh 		0.1  12.09.2006 	
#         by Bryant Hansen
#         prints a debug message to stderr 
#
#<!-- copyright (c) 2006 by Bryant Hansen -->
#<!-- all rights reserved -->

local NO_CR_ARG=""
local SPECIAL_CHAR_ARG=""

local ARGS=""

if [ "$1" = "-e" ] ; then
	ARGS="${ARGS} -e"
	shift
fi
if [ "$1" = "-n" ] ; then
	ARGS="${ARGS} -n"
	shift
fi
if [ "${ARGS}" = "" ] ; then
	echo  "$0: ${*/${ARGS}/}" 1>&2
else
	echo ${ARGS} "$0: ${*/${ARGS}/}" 1>&2
fi
exit 0

