#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# by Bryant Hansen <sourcecode@bryanthansen.net>
# Copyright (c) 2008 by Bryant Hansen

#         FormatTagRegexMatchString 	1.1.0  5 Sept 2006
#         by Bryant Hansen
#
#<!-- copyright (c) 2006 by Bryant Hansen -->
#<!-- all rights reserved -->

	my $USAGE="EscapeSpecialChars String";
	my $STRING1;
	$STRING1 = shift;
	# printing everything escaped, except vars
	print STDOUT "\Q$STRING1\E";
