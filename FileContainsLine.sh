# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) FileContainsLine.sh 	1.0 	16 Mar 2003
#
#         by Bryant Hansen
#
#         Searches a file for a line matching exactly our argument.
#

USAGE="FileContainsLine [ -v ] FILE SEARCH_STRING"

if [ -z "$1" ] ; then
   shift
fi

if [ $# -eq 3 ] ; then
   if [ "$1" = "-v" ] ; then
      VERBOSE=1
      shift # discard the verbose argument and continue
   else
      echo "$0 ERROR: if 3 arguments are provided, argument 1 must be \"-v\"!  Exiting abnormally."
      echo "USAGE: $USAGE"
      exit -1
   fi
fi

if [ $# -ne 2 ] ; then
   echo "ERROR: Function FileContainsLine called with $# parameters!  Should be 2 (or 3 with verbose mode)."
   exit -1
fi
if [ ! -f "$1" ] ; then
   if [ $VERBOSE ] ; then
      echo "function FileContainsLine called with missing file \"$1\"!"
   fi
   exit -2
fi
while read 'line'
do
{
   if [ "$line" = "$2" ] ; then
      # we found a matching line
      if [ $VERBOSE ] ; then
         echo "FileContainsLine: found exact match for string \"$2\" in file \"$1\""
      fi
      exit 1
   fi
}
done <"$1"
if [ $VERBOSE ] ; then
   echo "FileContainsLine: no match found for string \"$2\" in file \"$1\""
fi
# not found
exit 0
