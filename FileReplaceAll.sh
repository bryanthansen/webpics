# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) FileReplaceAll.sh 	1.0 	17 August, 2003
#
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         Replaces all instances of one string in a file with another string
#         Uses sed -e s/"$REPLACE_STRING"/"$WITH_STRING"/g 
#
#         copyright (c) 2006-2008 by Bryant Hansen

# Note: consider replacing this with a call to sed -i
#   the -i option means "in-place" which is essentially all that
#   we're doing here

USAGE="FileReplaceAll FILENAME REPLACE_STRING WITH_STRING"


# Sometimes we have a null arg to start, when a null arg was passed
# for verbose.  This is how to throw that away and continue
if [ -z "$1" ] ; then
	shift 
fi

if [ "$1" == "-v" ] ; then
	VERBOSE=1
	VERBOSE_ARG="-v"
	shift # discard the verbose argument and continue
fi

if [ $# -eq 3 ] ; then
   FILENAME="$1"
   REPLACE_STRING="$2"
   WITH_STRING="$3"
else
   echo "ERROR: FileReplaceAll called with $# arguments."
   echo "USAGE=$USAGE"
   echo "Press any key to continue"
   read -n 1
   exit -1
fi

if [ $VERBOSE ] ; then
	echo "$0: (FILENAME=$FILENAME, REPLACE_STRING=\"$REPLACE_STRING\", WITH_STRING=\"$WITH_STRING\")"
fi

if [ -f "$FILENAME" ] ; then
	if [ -f "$FILENAME.tmp" ] ; then
		echo "$0 WARNING: \"$FILENAME.tmp\" already exists.  Replacing...."
	fi
	cat "$FILENAME" | sed -e s/"$REPLACE_STRING"/"$WITH_STRING"/g > "$FILENAME.tmp"
	mv -f "$FILENAME.tmp" "$FILENAME"
else
	echo "ERROR: \"$FILENAME\" does not exist.  Exiting abnormally!"
	exit -3
fi
exit 1
