#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#         FormatTagRegexMatchString 	1.1.0  5 Sept 2006
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#<!-- copyright (c) 2006 by Bryant Hansen -->
#<!-- all rights reserved -->

	use strict;

	my $USAGE="$0 TagName";

#	my $ESCAPE_SPECIAL_CHARS=1;
	my $ESCAPE_SPECIAL_CHARS=0;
	my $USAGE="FormatTagRegexMatchString ObjectTag";
	my $OBJECT_TYPE_TAG="PhotoAlbumTag";
	my $TAG=shift;
#	my DescriptionObjectTag="<object[ \t]*id=\"${1}\"[ \t]*type=\"${OBJECT_TYPE_TAG}\"[^>]*>";
#	printf STDERR "${DescriptionObjectTag}";

	# using shortest possible match string for standard perl regex
	# my DescriptionObjectTag="<object[\ \t]*id=\"${1}\"[\ \t]*type=\"${OBJECT_TYPE_TAG}\".*?>";
	#
	# using shortest possible match string for standard sed regex
	if ($ESCAPE_SPECIAL_CHARS != 0)
	{
		# printing everything escaped, except vars
#		print STDOUT "\Q<object[ \t]*id=\"\E${TAG}\Q\"[ \t]*type=\"\E${OBJECT_TYPE_TAG}\Q\"[^>]*>\E\n\n";
#		print STDOUT "\Q<object[\t ]*id="${TAG}"[\t ]*type="${OBJECT_TYPE_TAG}"[^>]*>\E"; #try me?
		print STDOUT "\Q<object[\t ]*id=\"${TAG}\"[\t ]*type=\"${OBJECT_TYPE_TAG}\"[^>]*>\E";
	}
	else
	{
		print STDOUT "\<object\[\ \\t\]\*id\=\"${TAG}\"\[\ \\t\]\*type\=\"${OBJECT_TYPE_TAG}\"\[\^\>\]\*\>";
	}
