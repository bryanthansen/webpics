# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# GenMake.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# last updated: 20080426
#
# copyright (c) 2003-2008 by Bryant Hansen
# all rights reserved
##################################################################
#!/bin/sh

# one minor point of this script is to translate a list of files, 1 per line, to a list of space-separated files
# alternate possibilities for doing this:
#   perl chomp?
#   tr "\n" " " <input_file >output_file
#   awk '{ str1=str1 $0 " "}END{ print str1 }' some-file

HTML_DIR="html"
THUMB_DIR=""
THUMB_DIR_BASE=".thumbnails"

ValidateArgs() {
	if [[ -z "$MAKEFILE_OUT" ]] || [[ -z "$PICTURE_LIST" ]] || [[ -z "$THUMB_SIZE" ]] ; then
		echo "$0 requires 3 args!  \"$*\" is invalid!" 1>&2
		exit -4
	fi
	if [[ ! -f "$PICTURE_LIST" ]] ; then
		DebugMessage "ERROR: $PICTURE_LIST does not exist.  Exiting abnormally!"
		exit -2
	fi
	# allow a thumb size of 0 to indicate that we shouldn't create a thumbnail section
	if (( "$THUMB_SIZE" <= 0 )) ; then
#		echo "THUMB_SIZE=$THUMB_SIZE: use a sane value for thumb size!  Exiting abnormally!"
#		exit -3
		echo "THUMB_SIZE=$THUMB_SIZE: the thumbnail section will not be created"
	fi
}

DebugMessage()
{
	local NO_CR_ARG=""
	local SPECIAL_CHAR_ARG=""
	local ARGS=""
	local OPTION_FOUND="0"
	local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`($THUMB_SIZE): "

	while [[ "$OPTION_FOUND" ]] ; do
		OPTION_FOUND=""
		if [[ "$1" = "-e" ]] ; then
			OPTION_FOUND="-e"
			ARGS="${ARGS} -e"
			shift
		fi
		if [[ "$1" = "-n" ]] ; then
			OPTION_FOUND="-n"
			ARGS="${ARGS} -n"
			LOG_RECORD_HEADER=""
			shift
		fi
	done		

	echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

	return 0
}

AppendPicturePageProps() {
	local THIS_PIC="$1"
	local LAST_PIC="$2"
	local NEXT_PIC="$3"

	PROPS_FILE="${THUMB_DIR}/${HTML_DIR}/.${THIS_PIC}.properties"
	PROPS_TMP_FILE="`mktemp "${PROPS_FILE}.XXXXXX"`"

	if [[ "$THIS_PIC" ]] ; then
		[[ ! "$LAST_PIC" ]] && LAST_PIC=".."
		[[ ! "$NEXT_PIC" ]] && NEXT_PIC=".."
		echo "THIS_PIC=\"$THIS_PIC\"" >> "$PROPS_TMP_FILE"
		echo "LAST_PIC=\"$LAST_PIC\"" >> "$PROPS_TMP_FILE"
		echo "NEXT_PIC=\"$NEXT_PIC\"" >> "$PROPS_TMP_FILE"
		if [[ -f "$PROPS_FILE" ]] ; then
			if [[ "`diff "$PROPS_FILE" "$PROPS_TMP_FILE"`" ]] ; then
				DebugMessage "$PROPS_FILE changed!"
				cat "$PROPS_TMP_FILE" > "$PROPS_FILE"
			fi
		else
			cat "$PROPS_TMP_FILE" > "$PROPS_FILE"
			DebugMessage "created $PROPS_FILE"
		fi
	fi
	rm "$PROPS_TMP_FILE"
}


#######################################################
# MAIN
#######################################################


MAKEFILE="$1"
PICTURE_LIST="$2"
THUMB_SIZE="$3"

if (( "$THUMB_SIZE" > 0 )) ; then
	THUMB_DIR="${THUMB_DIR_BASE}${THUMB_SIZE}"
else
	THUMB_DIR="."
fi
[[ ! -d "$THUMB_DIR" ]] && mkdir "$THUMB_DIR"

[[ ! -d "${THUMB_DIR}/${HTML_DIR}" ]] && mkdir "${THUMB_DIR}/${HTML_DIR}"

MAKEFILE_OUT="`mktemp "${MAKEFILE}.tmp.XXXXXX"`"

ValidateArgs

SED_COMMAND_FILE="`dirname "$0"`/CommonSedCommands.sh"
[[ -f "./${SED_COMMAND_FILE}" ]]            && . ./${SED_COMMAND_FILE}

PICLIST="`mktemp "${pic}.tmp.XXXXXX"`"
# insure that we have at least 1 blank line at the end of the file
LAST_LINE="`cat "$PICTURE_LIST" | tail -n 1 | sed "s/\#.*$//"`"
echo "LAST_LINE=$LAST_LINE"
if [[ "$LAST_LINE" ]] ; then
	DebugMessage "ERROR: there appears to be no blank line at the end of ${PICTURE_LIST}.  This is required to process 1 last picture.  Exiting abnormally."
	exit -2
#	echo "" >> "$PICTURE_LIST"
fi

LAST_PIC=""
THIS_PIC=""
NEXT_PIC=""
cat "$PICTURE_LIST" | while read pic ; do

	if [[ "$pic" ]] ; then

		if [[ -f "$pic" ]] ; then
#			echo -e "\n$pic: ../$pic" >> "$MAKEFILE_OUT"
#			echo -e "\t@/projects/webpics/MakeThumbnail.sh --size=${THUMB_SIZE} \"../$pic\"" >> "$MAKEFILE_OUT"
			echo -n "$pic " >> "$PICLIST"
		fi
	
		# question: should this properties file creation be done here, or should it be done in the function that creates the picture list, MakeImageList
		#  					...or a new function, MakeImageProperties
		LAST_PIC="$THIS_PIC"
		THIS_PIC="$NEXT_PIC"
		NEXT_PIC="$pic"
		[[ -f "$THIS_PIC" ]] && AppendPicturePageProps "$THIS_PIC" "$LAST_PIC" "$NEXT_PIC"
	else
		# last iteration
		LAST_PIC="$THIS_PIC"
		THIS_PIC="$NEXT_PIC"
		NEXT_PIC=""
		[[ -f "$THIS_PIC" ]] && AppendPicturePageProps "$THIS_PIC" "$LAST_PIC" "$NEXT_PIC"
	fi

	# why doesn't this work; it's blank after the loop; I don't want this var (LIST) local to the loop!
	# LIST="$LIST \"$pic\""
	# must do a hack and pass the var via a file!

	# must determine a way to process the last pic, which needs to be done when the loop is finished!

done

THUMBNAILS="`cat "$PICLIST"`"
rm "$PICLIST"

echo -e "# Auto-Generated Makefile using $0\n"	  >> "$MAKEFILE_OUT"

if (( "$THUMB_SIZE" > 0 )) ; then
		echo -e "THUMBNAILS = $THUMBNAILS" >> "$MAKEFILE_OUT"
		echo -e "PAGES = \$(patsubst %,html/picture_%.html,\$(THUMBNAILS))"  >> "$MAKEFILE_OUT"
		echo -e "\n.PHONY: Thumbnails" 	>> "$MAKEFILE_OUT"
		echo -e "Thumbnails: \$(THUMBNAILS)" >> "$MAKEFILE_OUT"
fi
 
echo -e "\n.PHONY: PicturePages" >> "$MAKEFILE_OUT"
echo -e "PicturePages: \$(PAGES)" >> "$MAKEFILE_OUT"
echo -e "\t@echo \"PAGES = \$(PAGES)\"" >> "$MAKEFILE_OUT"

echo -e "\nhtml/picture_%.html: html/.%.properties" >> "$MAKEFILE_OUT"
echo -e "\t/projects/webpics/MakePicturePage.sh \"\$(subst html/picture_,,\$(subst .html,,\$@))\"" 	>> "$MAKEFILE_OUT"

#echo -e "\npictures = *.jpg *.JPG *.gif *.GIF *.png *.PNG *.tiff *.TIFF" >> "$MAKEFILE_OUT"

if (( "$THUMB_SIZE" > 0 )) ; then
	echo -e "\n%: ../%" >> "$MAKEFILE_OUT"
	echo -e "\t/projects/webpics/MakeThumbnail.sh \"\$(subst html/picture_,,\$(subst .html,,\$<))\"" >> "$MAKEFILE_OUT"
fi

if [[ -f "$MAKEFILE" ]] ; then
	if [[ "`diff "$MAKEFILE" "$MAKEFILE_OUT"`" ]] ; then
		DebugMessage "$MAKEFILE changed!"
		cat "$MAKEFILE_OUT" > "$MAKEFILE"
	fi
else
	cat "$MAKEFILE_OUT" > "$MAKEFILE"
	DebugMessage "Created $MAKEFILE"
fi
rm "$MAKEFILE_OUT"
