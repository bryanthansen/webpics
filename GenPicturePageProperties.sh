# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# GenPicturePageProperties.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# last updated: 20080925
#
# copyright (c) 2003-2008 by Bryant Hansen
# all rights reserved
##################################################################
#!/bin/sh

HTML_DIR="html"
THUMB_DIR=""
THUMB_DIR_BASE=".thumbnails"

DebugMessage()
{
	local NO_CR_ARG=""
	local SPECIAL_CHAR_ARG=""
	local ARGS=""
	local OPTION_FOUND="0"
	local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`($THUMB_SIZE): "

	while [[ "$OPTION_FOUND" ]] ; do
		OPTION_FOUND=""
		if [[ "$1" = "-e" ]] ; then
			OPTION_FOUND="-e"
			ARGS="${ARGS} -e"
			shift
		fi
		if [[ "$1" = "-n" ]] ; then
			OPTION_FOUND="-n"
			ARGS="${ARGS} -n"
			LOG_RECORD_HEADER=""
			shift
		fi
	done		

	echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

	return 0
}

UpdatePicturePageProps() {
		local THIS_PIC="$1"
		local LAST_PIC="$2"
		local NEXT_PIC="$3"

		PROPS_FILE="${THUMB_DIR}/${HTML_DIR}/.${THIS_PIC}.properties"
		PROPS_TMP_FILE="`mktemp "${PROPS_FILE}.XXXXXX"`"

		if [[ "$THIS_PIC" ]] ; then
			[[ ! "$LAST_PIC" ]] && LAST_PIC=""
			[[ ! "$NEXT_PIC" ]] && NEXT_PIC=""
			echo "THIS_PIC=\"$THIS_PIC\"" >> "$PROPS_TMP_FILE"
			echo "LAST_PIC=\"$LAST_PIC\"" >> "$PROPS_TMP_FILE"
			echo "NEXT_PIC=\"$NEXT_PIC\"" >> "$PROPS_TMP_FILE"
			if [[ -f "$PROPS_FILE" ]] ; then
				if [[ "`diff "$PROPS_FILE" "$PROPS_TMP_FILE"`" ]] ; then
					DebugMessage "$PROPS_FILE changed!"
					cat "$PROPS_TMP_FILE" > "$PROPS_FILE"
				fi
			else
				cat "$PROPS_TMP_FILE" > "$PROPS_FILE"
				DebugMessage "created $PROPS_FILE"
			fi
		fi
		rm "$PROPS_TMP_FILE"
}

AppendPicturePageProps() {
        local THIS_PIC="$1"
        local LAST_PIC="$2"
        local NEXT_PIC="$3"

        PROPS_FILE="${THUMB_DIR}/${HTML_DIR}/.${THIS_PIC}.properties"
        PROPS_TMP_FILE="`mktemp "${PROPS_FILE}.XXXXXX"`"

        if [[ "$THIS_PIC" ]] ; then
                [[ ! "$LAST_PIC" ]] && LAST_PIC=".."
                [[ ! "$NEXT_PIC" ]] && NEXT_PIC=".."
                echo "THIS_PIC=\"$THIS_PIC\"" >> "$PROPS_TMP_FILE"
                echo "LAST_PIC=\"$LAST_PIC\"" >> "$PROPS_TMP_FILE"
                echo "NEXT_PIC=\"$NEXT_PIC\"" >> "$PROPS_TMP_FILE"
                if [[ -f "$PROPS_FILE" ]] ; then
                        if [[ "`diff "$PROPS_FILE" "$PROPS_TMP_FILE"`" ]] ; then
                                DebugMessage "$PROPS_FILE changed!"
                                cat "$PROPS_TMP_FILE" > "$PROPS_FILE"
                        fi
                else
                        cat "$PROPS_TMP_FILE" > "$PROPS_FILE"
                        DebugMessage "created $PROPS_FILE"
                fi
        fi
        rm "$PROPS_TMP_FILE"
}



#######################################################
# MAIN
#######################################################

# TODO: un-thumbize this!

PICTURE_LIST="$1"
THUMB_SIZE="$2"

if (( "$THUMB_SIZE" > 0 )) ; then
        THUMB_DIR="${THUMB_DIR_BASE}${THUMB_SIZE}"
		[[ ! -d "$THUMB_DIR" ]] && mkdir "$THUMB_DIR"
else
        THUMB_DIR="."
fi
[[ ! -d "${THUMB_DIR}/${HTML_DIR}" ]] && mkdir "${THUMB_DIR}/${HTML_DIR}"

LAST_PIC=""
THIS_PIC=""
NEXT_PIC=""
cat "$PICTURE_LIST" | sed "s/\#.*//" | while read pic ; do
	LAST_PIC="$THIS_PIC"
	THIS_PIC="$NEXT_PIC"
	if [[ "$pic" ]] ; then
		NEXT_PIC="$pic"
		[[ -f "$THIS_PIC" ]] && UpdatePicturePageProps "$THIS_PIC" "$LAST_PIC" "$NEXT_PIC"
	else
		# last iteration
		NEXT_PIC=""
		[[ -f "$THIS_PIC" ]] && UpdatePicturePageProps "$THIS_PIC" "$LAST_PIC" "$NEXT_PIC"
	fi
done
