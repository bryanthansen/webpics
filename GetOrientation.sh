#!/bin/bash
# Bryant Hansen
# 20120917

exiv2 -pv pr "$1" \
| grep "Image[ \t][ \t]*Orientation[ \t]" \
| while read id category tag type size val
do
    echo "$val"
done






###   exiv2 -pv pr "DSC_0478.jpg"   ###
#############################
#0x010f Image        Make                        Ascii       5  Sony
#0x0110 Image        Model                       Ascii       6  C6603
#0x0112 Image        Orientation                 Short       1  6

