# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) MakeBasicPicindex.sh     1.0     17 Aug., 2003
#
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         Inserts a file into a file at each occurance of the specified string

USAGE="InsertFileIntoFile BASE_FILE INSERT_FILE FILE_INSERT_TAG"

TAGFOUND=0
if [ $# -eq 3 ] ; then
   BASE_FILE="$1"
   INSERT_FILE="$2"
   FILE_INSERT_TAG="$3"
else
   if [ $# -eq 0 ] ; then
      echo "InsertFileIntoFile called with 0 arguments.  This is the depricated usage (requiring global variables for passing data)."
   else
      echo "ERROR: InsertFileIntoFile called with $# arguments."
      echo "Press any key to continue"
      read -n 1
      exit -1
   fi
fi
if [ $VERBOSE ] ; then
   echo "Entering InsertFileIntoFile (BASE_FILE=$BASE_FILE, FILE_INSERT_TAG=$FILE_INSERT_TAG, INSERT_FILE=$INSERT_FILE)"
fi

if [ -f "$BASE_FILE.tmp" ] ; then
   echo "ERROR: \"$BASE_FILE.tmp\" already exists.  Can't use this as a temporary file!  Exiting abnormally!"
   exit -2
fi

if [ -f "$BASE_FILE" ] && [ -f "$INSERT_FILE" ] && [ ! -z "$FILE_INSERT_TAG" ] ; then
   rm -f "$BASE_FILE.tmp"
   while read 'line'
   do
   {
      if [ ! "$line" = "${line#*$FILE_INSERT_TAG*}" ] ; then
         TAGFOUND=1
         echo            "${line%<!--*$FILE_INSERT_TAG*}" | sed -e s/"<!--*$"// >> "$BASE_FILE.tmp"
         # tag found
         while read 'insertLine'
         do
         {
            echo $insertLine >> "$BASE_FILE.tmp"
         }
         done <"$INSERT_FILE"
         echo "${line#*$FILE_INSERT_TAG}" | sed -e s/"-->*"// >> "$BASE_FILE.tmp"
      else
         echo $line >> "$BASE_FILE.tmp"
      fi
   }
   done <"$BASE_FILE"
else
   echo "InsertFileIntoFile error: bad initial conditions!"
   echo "USAGE: $USAGE"
   echo "params: BASE_FILE=$BASE_FILE, FILE_INSERT_TAG=$FILE_INSERT_TAG, INSERT_FILE=$INSERT_FILE"
   echo "Press any key to continue (or ctrl-C to exit)"
   read -n 1
   exit -65
fi

if [ $TAGFOUND -eq 0 ] ; then
   echo "InsertFileIntoFile error: tag \"$FILE_INSERT_TAG\" not found!"
else
   mv -f "$BASE_FILE.tmp" "$BASE_FILE"
   exit 1
fi
