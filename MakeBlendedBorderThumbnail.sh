# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeBlendedBorderThumbnail.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# Adds a decorative border around an image using convert from the
# imagemagick package
#
# copyright (c) 2008-2013 by Bryant Hansen
##################################################################

#!/bin/bash

# interesting example:
# /projects/webpics/MakeBlendedBorderThumbnail.sh -v IMG_5269g2.png .blendedBorderTest/IMG_5269g2b.png 0 "rgb($(expr $RANDOM % 256),$(expr $RANDOM % 256),$(expr $RANDOM % 256))"


######################################################
# Constants and Global Defaults
######################################################

ME="$(basename "$0")"
USAGE="$ME [ -v ] input_image  output_image  [ size  [ background_color ] ]"

# defaults
ADD_DROPSHADOW=1
ADD_BLUR_BACKGROUND=1
OVERLAY_PICTURE=1
ENABLE_COPYRIGHT=1
ROUNDED_BLUR=1
COPYRIGHT_GRAVITY="SouthEast"

BLUR_BACKGROUND_WIDTH_PCNT=50
BLUR_IMAGE_EXPANSION_PCNT=95

COPYRIGHT_FONT="helvetica"
BLUR_STRENGTH="15"
SHADOW_GLOW_COLOR="rgb(0,0,0)"
SHADOW_GLOW_COLOR="rgb(255,255,255)"
TEXT_COLOR="rgb(0,0,0)"
TEXT_HIGHLIGHT_COLOR="rgb(255,255,255)"

FRAME_THICKNESS_PCNT=3
FRAME_THICKNESS_BASE=6

COPYRIGHT_PADDING_BOTTOM=1

CONVERT=`which convert`
[[ "$CONVERT" ]] || CONVERT=/usr/bin/convert
######################################################


######################################################
# Functions
######################################################

SourceFiles()
{
    CONF_FILE="album.conf"
    [[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
    [[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
    [[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
    [[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}

    if [[ -f "./${CONF_FILE}.${size}" ]] ; then
        echo "./${CONF_FILE}.${size} found" >&2
        . ./${CONF_FILE}.${size}
    else
        echo "./${CONF_FILE}.${size} NOT found" >&2
    fi
}

DebugMessage()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`($THUMB_SIZE): "

    while [[ "$OPTION_FOUND" ]] ; do
        OPTION_FOUND=""
        if [[ "$1" = "-e" ]] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [[ "$1" = "-n" ]] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}

function getCopyrightString() {
    local copyright_string="(c)2013 Hansen"

    # sed expressions
    local SED_STRIP_HTML="s/<[^>]*>//g"

    # last one wins
    [[ "$COPYRIGHT_STRING" ]] && copyright_string="$COPYRIGHT_STRING"

    copyright_file="templates/copyright.html"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$SED_STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    copyright_file="copyright.txt"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$SED_STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    copyright_file="${input_image}.copyright"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$SED_STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    DebugMessage "copyright=$copyright_string"

    # process possible html in copyright string
    if which lynx > /dev/null ; then
        # cat copyright.txt | lynx -stdin -dump | sed "/^[ \t]*$/d" ; echo "---"
        SED_REMOVE_BLANKS="/^[ \t]*$/d"
        copyright_string="$(echo "$copyright_string" | lynx -stdin -dump -width=512 | sed "$SED_REMOVE_BLANKS")"
        DebugMessage "copyright (post-html-dump)=$copyright_string"
    else
        DebugMessage "'lynx' not found in path; install the lynx utility to convert html notation in the copyright_string to text (for example '&amp;' to '&' or '&quot' to '\"')"
    fi

    echo "$copyright_string"
    return 0
}

getOriginalImageHeight() {
    ExifDir="`dirname "${input_image}"`/${IMAGE_INFO_DIR}"
    ExifFile="${ExifDir}/`basename "${input_image}"`.txt"
    if [[ -f "$ExifFile" ]] ; then
        #(( "$VERBOSE" )) && \
            DebugMessage "reading height from cache file: ${ExifFile}: $HEIGHT"
        HEIGHT="`cat "${ExifFile}" | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t].*x//;s/+.*//g"`"
        #(( "$VERBOSE" )) && \
            DebugMessage "${ExifFile}: HEIGHT=$HEIGHT"
    else
        DebugMessage "info file does not exist: ${ExifFile}."
    fi

    [[ ! "$HEIGHT" ]] && HEIGHT=0
    if (( "$HEIGHT" <= "0" )) ; then
        #(( "$VERBOSE" )) && \
            DebugMessage "Reading height from image manually via ImageMagick's identify utility...."
        HEIGHT="`identify -format "%h" "${input_image}"`"
        #(( "$VERBOSE" )) && \
            DebugMessage "${input_image}: HEIGHT=$HEIGHT"
    fi
    echo "$HEIGHT"
}

getOriginalImageWidth() {
    ExifDir="`dirname "${input_image}"`/${IMAGE_INFO_DIR}"
    ExifFile="${ExifDir}/`basename "${input_image}"`.txt"
    if [[ -f "$ExifFile" ]] ; then
        (( "$VERBOSE" )) && \
            DebugMessage "reading width from cache file: ${ExifFile}"
        WIDTH="`cat "${ExifFile}" | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t]*//;s/x.*//g"`"
        #(( "$VERBOSE" )) && \
        DebugMessage "${ExifFile}: WIDTH=$WIDTH"
    else
        DebugMessage "info file does not exist: ${ExifFile}."
    fi

    [[ ! "$WIDTH" ]] && WIDTH=0
    if (( "$WIDTH" <= "0" )) ; then
        (( "$VERBOSE" )) && \
            DebugMessage "Reading width from image manually via ImageMagick's identify utility...."
        WIDTH="`identify -format "%w" "${input_image}"`"
        #(( "$VERBOSE" )) && \
            DebugMessage "${input_image}: WIDTH=$WIDTH"
    fi
    echo "$WIDTH"
}

getScaledImageWidth() {
    ORIG_WIDTH=$1
    ORIG_HEIGHT=$2
    MAX_WIDTH_HEIGHT=$3
    TMP_OUTPUT="$4"

    # why such an inefficient system?  is it so hard to predict without making an image?
    if [[ ! -f "$TMP_OUTPUT" ]] ; then
        CONVERT_CMD1=" \
            $CONVERT \
                -size ${INPUT_WIDTH}x${INPUT_HEIGHT} \
                xc: none \
                -resize ${newMaxHeightWidth}x${newMaxHeightWidth}+0+0 \
                '${TEMP_OUTPUT}' 2> /dev/null   \
        "
        SED_FORMAT_CONVERT_COMMAND="s/[ \t][ \t][ \t]*/\ \t\\\\\n\ \ /g"
        echo "# CONVERT_CMD1 = ${CONVERT_CMD1}" | sed "$SED_FORMAT_CONVERT_COMMAND"
        echo "$CONVERT_CMD1" | /bin/bash
    fi

    SCALED_WIDTH="`identify -format "%w" "$TEMP_OUTPUT"`"
    echo "$SCALED_WIDTH"
    # TODO: remove tmp image
    rm "$TEMP_OUTPUT"
}

getScaledImageHeight() {
    ORIG_WIDTH=$1
    ORIG_HEIGHT=$2
    MAX_WIDTH_HEIGHT=$3
    TMP_OUTPUT="$4"

    if [[ ! -f "$TMP_OUTPUT" ]] ; then
        CONVERT_CMD1=" \
            $CONVERT \
                -size ${INPUT_WIDTH}x${INPUT_HEIGHT} \
                xc: none \
                -resize ${newMaxHeightWidth}x${newMaxHeightWidth}+0+0 \
                '${TEMP_OUTPUT}' 2> /dev/null   \
        "
        SED_FORMAT_CONVERT_COMMAND="s/[ \t][ \t][ \t]*/\ \t\\\\\n\ \ /g"
        echo "# CONVERT_CMD1 = ${CONVERT_CMD1}" | sed "$SED_FORMAT_CONVERT_COMMAND"
        echo "$CONVERT_CMD1" | /bin/bash
    fi

    SCALED_HEIGHT="`identify -format "%h" "$TEMP_OUTPUT"`"
    echo "$SCALED_HEIGHT"
}

calculateIdealFrameThickness() {
    local FRAME_THICKNESS=-1
    echo "$FRAME_THICKNESS"
    exit 1
}

makeThumbnail3() {

    local input_image="$1"
    local output_image="$2"

    CONVERT_BIN=" \
        $CONVERT \
    "

    CONVERT_STD_OPTS=" \
                -gravity center       \
                -size ${OUTPUT_WIDTH}x${OUTPUT_HEIGHT} \
        "

    CONVERT_NEW_FRAME=""
    if [[ "$BACKGROUND_COLOR" ]] ; then
        CONVERT_NEW_FRAME=" \
                xc:${BACKGROUND_COLOR} \
        "
        DebugMessage "  BACKGROUND_COLOR=${BACKGROUND_COLOR}" >&2
    else
        CONVERT_NEW_FRAME=" \
                xc:none \
        "
        DebugMessage "background is transparent."
    fi

    CONVERT_ADD_BLUR_BACKGROUND=""
    if [[ "$ADD_BLUR_BACKGROUND" ]] && (( "$ADD_BLUR_BACKGROUND" )) ; then

        BACKGROUND_BRIGHTNESS=100

        DEFAULT_BLUR_STRENGTH=`expr 20 - 15 \* $BACKGROUND_BRIGHTNESS \/ 100`
        BLUR_RADIUS="`expr "$BLUR_STRENGTH" \* 3`"
        echo "BLUR_BACKGROUND parameters: " >&2
        echo "  BACKGROUND_BRIGHTNESS=$BACKGROUND_BRIGHTNESS" >&2
        echo "  BLUR_STRENGTH=${BLUR_STRENGTH}" >&2
        echo "  BLUR_RADIUS=${BLUR_RADIUS}" >&2

        echo "  SCALED_WIDTH=${SCALED_WIDTH}" >&2
        echo "  SCALED_HEIGHT=${SCALED_HEIGHT}" >&2
        echo "  OUTPUT_WIDTH=${OUTPUT_WIDTH}" >&2
        echo "  OUTPUT_HEIGHT=${OUTPUT_HEIGHT}" >&2
        echo "  FRAME_THICKNESS=${FRAME_THICKNESS}" >&2

        [[ ! "$BLUR_IMAGE_EXPANSION_PCNT" ]] && BLUR_IMAGE_EXPANSION_PCNT="370"
        BLUR_IMAGE_WIDTH=`expr $OUTPUT_WIDTH \* $BLUR_IMAGE_EXPANSION_PCNT / 100`
        BLUR_IMAGE_HEIGHT=`expr $OUTPUT_HEIGHT \* $BLUR_IMAGE_EXPANSION_PCNT / 100`

        echo "  BLUR_IMAGE_EXPANSION_PCNT=${BLUR_IMAGE_EXPANSION_PCNT}" >&2
        echo "  BLUR_IMAGE_WIDTH=${BLUR_IMAGE_WIDTH}" >&2
        echo "  BLUR_IMAGE_HEIGHT=${BLUR_IMAGE_HEIGHT}" >&2

        BLUR_IMAGE_ROUNDED_CORNERS=0
        if [[ "$BLUR_IMAGE_ROUNDED_CORNERS" ]] && \
        (( "$BLUR_IMAGE_ROUNDED_CORNERS" )) ; then
            echo "# ERROR: BLUR_IMAGE_ROUNDED_CORNERS is not supported at the moment!  Option disabled" >&2
        fi

        CONVERT_ADD_BLUR_BACKGROUND=" \
            \( \
                '${input_image}' \
                -resize "${BLUR_IMAGE_WIDTH}x${BLUR_IMAGE_HEIGHT}\!" \
            \) \
            -channel RGBA \
            -blur ${BLUR_RADIUS},${BLUR_STRENGTH}    \
            -compose Over \
            -composite \
        "

    fi

    CONVERT_DRAW_DROPSHADOW=""
    if [[ "$ADD_DROPSHADOW" ]] && (( "$ADD_DROPSHADOW" )) ; then

        SHADOW_OFFSET_PERCENT=10
        SHADOW_ASIMUTH=315

        BACKGROUND_BRIGHTNESS=100

        IMAGE_MAX_HW=$SCALED_HEIGHT
        (( $SCALED_WIDTH > $IMAGE_MAX_HW )) && IMAGE_MAX_HW=$SCALED_WIDTH
        SHADOW_INDENT="`expr $IMAGE_MAX_HW \/ 125`"
        SHADOW_INDENT="`expr $IMAGE_MAX_HW \* 8 \/ 1000`"
        SHADOW_INDENT="`expr $FRAME_THICKNESS \/ 10`"
        SHADOW_TOP=`expr ${FRAME_THICKNESS} + ${SHADOW_INDENT}`
        SHADOW_LEFT=`expr ${FRAME_THICKNESS} + ${SHADOW_INDENT}`
        SHADOW_BOTTOM=`expr ${IMAGE_BOTTOM_X} + ${SHADOW_INDENT}`
        SHADOW_RIGHT=`expr ${IMAGE_BOTTOM_Y} + ${SHADOW_INDENT}`

        SHADOW_IMAGE_WIDTH=`expr "$OUTPUT_WIDTH"`
        SHADOW_IMAGE_HEIGHT=`expr "$OUTPUT_HEIGHT"`

        MARGIN_SIZE="`expr $FRAME_THICKNESS - $FONT_SIZE`"

        # observed behavior: dropshadow strength should be inversely-proportional
        # to brightness, since it's a far-more-pronounced effect on light
        # backgrounds
        # 20 seems good for reasonably-dark backgrounds
        # 5 is probably good for lighter
        # blur radius also plays an effect
        DEFAULT_SHADOW_BLUR_STRENGTH=`expr 20 - 10 \* $BACKGROUND_BRIGHTNESS \/ 100`
        [[ ! "$SHADOW_BLUR_STRENGTH" ]] && SHADOW_BLUR_STRENGTH="$BLUR_STRENGTH"

        [[ ! "$SHADOW_BLUR_RADIUS" ]] && SHADOW_BLUR_RADIUS="`expr "$SHADOW_BLUR_STRENGTH" \* 3`"
        # a couple of other SHADOW_BLUR_RADIUS algorithms I've tried:
        #    SHADOW_BLUR_RADIUS=`expr $SHADOW_INDENT \* \( 300 - $BACKGROUND_BRIGHTNESS \) \/ 100`
        #    SHADOW_BLUR_RADIUS=`expr $SHADOW_BLUR_STRENGTH \* 3`

        echo "  SHADOW_INDENT=${SHADOW_INDENT}" >&2
        echo "  SHADOW_TOPLEFT_XY=(${SHADOW_LEFT}, ${SHADOW_TOP})" >&2
        echo "  SHADOW_BOTTOMRIGHT_XY=(${SHADOW_RIGHT}, ${SHADOW_BOTTOM})" >&2
        echo "  CORNER_RADIUS=${CORNER_RADIUS}" >&2
        echo "  SHADOW_BLUR_RADIUS=${SHADOW_BLUR_RADIUS}" >&2
        echo "  SHADOW_BLUR_STRENGTH=${SHADOW_BLUR_STRENGTH}" >&2
        echo "  MARGIN_SIZE=${MARGIN_SIZE}" >&2

        shadow_rect="'roundrectangle ${SHADOW_LEFT},${SHADOW_TOP} ${SHADOW_RIGHT},${SHADOW_BOTTOM} ${CORNER_RADIUS} ${CORNER_RADIUS}'"

        CONVERT_DRAW_DROPSHADOW=" \
            \( \
                -size "${SHADOW_IMAGE_WIDTH}x${SHADOW_IMAGE_HEIGHT}\!" \
                xc:none   \
                -gravity "center"    \
                -fill "black"   \
                -draw $shadow_rect \
            \) \
            -channel RGBA \
            -blur ${SHADOW_BLUR_RADIUS},${SHADOW_BLUR_STRENGTH}    \
            -composite \
        "
    fi


    CONVERT_DRAW_OVERLAY_PICTURE=""
    if [[ "$OVERLAY_PICTURE" ]] && (( "$OVERLAY_PICTURE" )) ; then
        CONVERT_DRAW_OVERLAY_PICTURE=" \
            \(    \
                xc:none       \
                -draw 'roundrectangle ${FRAME_THICKNESS},${FRAME_THICKNESS} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X} ${CORNER_RADIUS},${CORNER_RADIUS}'      \
                \(    \
                    -gravity center       \
                    '${input_image}'       \
                    -resize "${SCALED_WIDTH}x${SCALED_HEIGHT}\!" \
                \)    \
                -compose SrcIn \
                -composite \
            \)    \
            -compose Over \
            -composite \
        "
    fi

    CONVERT_DRAW_COPYRIGHT=""
    if [[ "$ENABLE_COPYRIGHT" ]] && (( "$ENABLE_COPYRIGHT" )) ; then

        COPYRIGHT_X="`expr $FRAME_THICKNESS + 0`"
        COPYRIGHT_X="`expr $FRAME_THICKNESS \* 20 \/ 100 + 0`"

        [[ "$COPYRIGHT_PADDING_RIGHT" -ne 0 ]] && COPYRIGHT_X="`expr $COPYRIGHT_X + $COPYRIGHT_PADDING_RIGHT`"

        # Y position has caused lots of problems
    #    COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100 + \( $FRAME_THICKNESS - $FONT_SIZE \) \* 100 \/ 100 + 0`"
    #    COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100 + $MARGIN_SIZE`"
    #    COPYRIGHT_Y="`expr $MARGIN_SIZE \* 1 \/ 100`"
    #    COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100`"
    #    COPYRIGHT_Y="`expr \( $MARGIN_SIZE \* 1 \/ 100 \) - \( $FONT_SIZE \* 40 / 100 \)`"
        COPYRIGHT_Y="`expr \( $MARGIN_SIZE \* 100 \/ 100 \) - \( $FONT_SIZE \* 100 / 100 \) / 2`"
        [[ "$COPYRIGHT_PADDING_BOTTOM" -ne 0 ]] && COPYRIGHT_Y="`expr $COPYRIGHT_Y + $COPYRIGHT_PADDING_BOTTOM`"
        echo "COPYRIGHT_Y=$COPYRIGHT_Y"

        # allow any overrides
        SourceFiles

        [[ ! "$COPYRIGHT_STRING" ]] && COPYRIGHT_STRING="$(getCopyrightString)"
        #[[ "$copyright_string_arg" ]] && COPYRIGHT_STRING="$copyright_string_arg"
        echo "  COPYRIGHT_STRING=\"${COPYRIGHT_STRING}\"" >&2
        echo "  COPYRIGHT_FONT=${COPYRIGHT_FONT}" >&2
        echo "  FONT_SIZE=${FONT_SIZE}" >&2
        echo "  TEXT_COLOR=${TEXT_COLOR}" >&2
        echo "  TEXT_HIGHLIGHT_COLOR=${TEXT_HIGHLIGHT_COLOR}" >&2
        echo "  COPYRIGHT_X,Y=(${COPYRIGHT_X},${COPYRIGHT_Y})" >&2

        #    -fill \"rgb(0,0,0)\" \
        #    -draw 'text $(expr ${COPYRIGHT_X} + 2),$(expr ${COPYRIGHT_Y} + 2) \"${COPYRIGHT_STRING}\"' \
        #    -blur 1,1    \

        CONVERT_DRAW_COPYRIGHT=" \
            -gravity \"${COPYRIGHT_GRAVITY}\" \
            -font \"${COPYRIGHT_FONT}\" \
            -pointsize \"${FONT_SIZE}\" "
        if [[ "$DOUBLE_HIGHLIGHT" ]] && (( "$DOUBLE_HIGHLIGHT" )) ; then
            CONVERT_DRAW_COPYRIGHT="$CONVERT_DRAW_COPYRIGHT \
            -fill \"rgb(0,0,0)\" \
            -draw 'text $(expr ${COPYRIGHT_X} + 2),$(expr ${COPYRIGHT_Y} + 2) \"${COPYRIGHT_STRING}\"' \
            -blur 1,1 "
        fi
        CONVERT_DRAW_COPYRIGHT="$CONVERT_DRAW_COPYRIGHT \
            -fill \"${TEXT_HIGHLIGHT_COLOR}\" \
            -draw 'text ${COPYRIGHT_X},${COPYRIGHT_Y} \"${COPYRIGHT_STRING}\"' \
            -blur 1,1    \
            -fill \"${TEXT_COLOR}\" \
            -draw 'text $(expr ${COPYRIGHT_X} + 1),$(expr ${COPYRIGHT_Y} + 1) \"${COPYRIGHT_STRING}\"' \
        "

    fi

    # set output image - always done
    CONVERT_OUTPUT_IMAGE="\"${output_image}\""

    [[ ! "$CONVERT_BIN" ]] && DebugMessage "CONVERT_BIN not set.  Exiting abnormally!" && exit 12
    [[ ! "$CONVERT_OUTPUT_IMAGE" ]] && DebugMessage "CONVERT_OUTPUT_IMAGE not set.  Exiting abnormally!" && exit 13

    (
        echo -n "$CONVERT_BIN"
        [[ "$CONVERT_STD_OPTS" ]] &&                echo -n "$CONVERT_STD_OPTS"
        [[ "$CONVERT_NEW_FRAME" ]] &&               echo -n "$CONVERT_NEW_FRAME"
        [[ "$CONVERT_ADD_BLUR_BACKGROUND" ]] &&     echo -n "$CONVERT_ADD_BLUR_BACKGROUND"
        [[ "$CONVERT_DRAW_DROPSHADOW" ]] &&         echo -n "$CONVERT_DRAW_DROPSHADOW"
        [[ "$CONVERT_DRAW_OVERLAY_PICTURE" ]] &&    echo -n "$CONVERT_DRAW_OVERLAY_PICTURE"
        [[ "$CONVERT_DRAW_COPYRIGHT" ]] &&          echo -n "$CONVERT_DRAW_COPYRIGHT"
        echo "$CONVERT_OUTPUT_IMAGE"
    ) | bash

}


# main

######################################################
# Setup Environment
######################################################
ME="$(basename "$0")"
MYDIR="$(dirname "$0")"

MY_OPTS="${MY_OPTS} [ -v | --verbose ]"
MY_OPTS="${MY_OPTS} [ -s size | --size size | --size=size ] "
MY_OPTS="${MY_OPTS} [ --no-blended-border ]"
MY_OPTS="${MY_OPTS} [ --no-dropshadow ]"
MY_OPTS="${MY_OPTS} [ --no-overlay ]"
MY_OPTS="${MY_OPTS} [ --no-blended-border ]"
MY_OPTS="${MY_OPTS} [ -c copyright_string | --copyright-string copyright_string | --copyright-string=copyright_string ]"

USAGE="${ME} [ ${MY_OPTS} ]  \
             input_image  output_image  [ size  [ BACKGROUND_COLOR ] ]"

IMAGE_INFO_DIR=".image_info"
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}

DebugMessage -e "\nStarting ${ME}..."

######################################################
# process arguments
######################################################
while [[ ! "${1/-*/}" ]] ; do
    case "$1" in
        -v | --verbose)
            DebugMessage "VERBOSE is set"
            VERBOSE=1
            ;;
        -s | --size | --size=*)
            if [[ ! "${1/--size=*/}" ]] ; then
                size="${1/--size=/}"
            else
                shift
                size="$1"
            fi
            # validate
            if [[ ! "${size/-*/}" ]]     || \
               (( "${size//[0-9]/}" )) ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: size = $size  Must set a valid numerical size.  Exiting abnormally."
                ERROR_BAD_SIZE=10
                exit $ERROR_BAD_SIZE
            fi
            ;;
        -c | --copyright-string | --copyright-string=*)
            if [[ ! "${1/--copyright-string=*/}" ]] ; then
                COPYRIGHT_STRING="${1/--copyright-string=/}"
            else
                shift
                COPYRIGHT_STRING="$1"
            fi
            ENABLE_COPYRIGHT=1
            if [[ ! "${COPYRIGHT_STRING/-*/}" ]] ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: COPYRIGHT_STRING = $COPYRIGHT_STRING  Must set to something.  Exiting abnormally."
                ERROR_BAD_COPYRIGHT=11
                exit $ERROR_BAD_COPYRIGHT
            fi
            ;;
        -b | --blended-border)
            ADD_BLUR_BACKGROUND=1
            ;;
        -d | --dropshadow)
            ADD_DROPSHADOW=1
            ;;
        -o | --image-overlay)
            OVERLAY_PICTURE=1
            ;;
        --blur-strength | --blur-strength=*)
            if [[ ! "${1/--blur-strength=*/}" ]] ; then
                blur_strength="${1/--blur-strength=/}"
            else
                shift
                blur_strength="$1"
            fi
            # validate
            if [[ ! "${blur_strength/-*/}" ]]     || \
               (( "${blur_strength//[0-9]/}" )) ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: blur_strength = $blur_strength  Must set a valid numerical size.  Exiting abnormally."
                ERROR_blur_strength=11
                exit $ERROR_blur_strength
            fi
            BLUR_STRENGTH="$blur_strength"
            ;;
        --blur-radius | --blur-radius=*)
            if [[ ! "${1/--blur-radius=*/}" ]] ; then
                blur_radius="${1/--blur-radius=/}"
            else
                shift
                blur_radius="$1"
            fi
            # validate
            if [[ ! "${blur_radius/-*/}" ]]     || \
               (( "${blur_radius//[0-9]/}" )) ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: blur_radius = $blur_radius  Must set a valid numerical size.  Exiting abnormally."
                ERROR_blur_radius=11
                exit $ERROR_blur_radius
            fi
            BLUR_RADIUS="$blur_radius"
            ;;
        --blur-expansion | --blur-expansion=*)
            if [[ ! "${1/--blur-expansion=*/}" ]] ; then
                blur_expansion="${1/--blur-expansion=/}"
            else
                shift
                blur_expansion="$1"
            fi
            # validate
            if [[ ! "${blur_expansion/-*/}" ]]     || \
               (( "${blur_expansion//[0-9]/}" )) ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: blur_expansion = $blur_expansion  Must set a valid numerical size, as a percentage of the total resulting image size, including frame.  Exiting abnormally."
                ERROR_blur_expansion=12
                exit $ERROR_blur_expansion
            fi
            BLUR_IMAGE_EXPANSION_PCNT="$blur_expansion"
            ;;
        --no-blended-border)
            DebugMessage -e "Blended Border disabled on command line"
            unset ADD_BLUR_BACKGROUND
            ;;
        --no-dropshadow)
            DebugMessage -e "Dropshadow disabled on command line"
            unset ADD_DROPSHADOW
            ;;
        --no-overlay)
            DebugMessage -e "Overlay image disabled on command line"
            unset OVERLAY_PICTURE
            ;;
          *)
            DebugMessage -e "USAGE: \n    ${USAGE}"
            DebugMessage "ERROR: size = $size  Must set a valid numerical size.  Exiting abnormally."
            ERROR_BAD_SIZE=10
            exit $ERROR_BAD_SIZE
            ;;
    esac
    shift
done
input_image="$1"
[[ ! "$input_image" ]] && echo "not enough args, dude!  No input image.  Exiting abnormally." && exit -2
[[ "$3" ]] && size="$3"
output_image="$(dirname "$input_image")/.thumbnails${size}/$(basename "$input_image")"
[[ "$2" ]] && output_image="$2"
d="$(dirname "$output_image")"
[[ ! "$d" ]] && mkdir -p "$d"
[[ "$4" ]] && BACKGROUND_COLOR="$4"

######################################################

INPUT_HEIGHT=$(getOriginalImageHeight)
INPUT_WIDTH=$(getOriginalImageWidth)

RESIZE=1
RESIZE=0
[[ ! "$size" ]] && size=0
if (( "$size" < 1 )) ; then
    DebugMessage "Resize is not set; using original size of input image"
    RESIZE=0
    size="$INPUT_WIDTH"
    (( "$INPUT_HEIGHT" > "$size" )) && size="$INPUT_HEIGHT"
fi
echo "size=$size" >&2
[[ -f "./${CONF_FILE}.${size}" ]] && . ./${CONF_FILE}.${size}
        
#[[ ! "$FRAME_THICKNESS" ]] && FRAME_THICKNESS="`expr $size \/ 24 + 6`"
[[ ! "$FRAME_THICKNESS" ]] && FRAME_THICKNESS="`expr $FRAME_THICKNESS_PCNT \* $size / 100 + $FRAME_THICKNESS_BASE`"
(( "$FRAME_THICKNESS" < 0 )) && FRAME_THICKNESS="`expr $size \/ 24 + 6`"
#(( "$FRAME_THICKNESS" < 10 )) && FRAME_THICKNESS=10

#FONT_SIZE="`expr $size \/ 50 + 3`"
FONT_SIZE="`expr $FRAME_THICKNESS \/ 2 + 1`"
FONT_SIZE="`expr $FRAME_THICKNESS \* 70 / 100`"

if [[ "$FONT_SCALE_PERCENT" ]] ; then
    NEW_SIZE=$(expr $FONT_SIZE \* $FONT_SCALE_PERCENT / 100)
    echo "FONT_SCALE_PERCENT=${FONT_SCALE_PERCENT}%  FONT_SIZE: $FONT_SIZE -> $NEW_SIZE" >&2
    FONT_SIZE=$NEW_SIZE
fi

[[ ! "$CORNER_RADIUS" ]] && CORNER_RADIUS="`expr $FRAME_THICKNESS \/ 2`"

newMaxHeightWidth="`expr $size - $FRAME_THICKNESS \* 2`"
newMaxHeightWidth="`expr $size + $FRAME_THICKNESS \* 2`"
if (( $newMaxHeightWidth < 1 )) ; then
    DebugMessage "program error: invalid new image size: $newMaxHeightWidth"
    exit -2
fi

if (( $INPUT_HEIGHT > $INPUT_WIDTH )) ; then
    SCALED_HEIGHT=$size
    SCALED_WIDTH=$(expr $INPUT_WIDTH \* $size / $INPUT_HEIGHT)
else
    SCALED_WIDTH=$size
    SCALED_HEIGHT=$(expr $INPUT_HEIGHT \* $size / $INPUT_WIDTH)
fi
echo "  SCALED_WIDTH=$INPUT_WIDTH" >&2
echo "  SCALED_HEIGHT=$INPUT_HEIGHT" >&2

OUTPUT_HEIGHT="`expr $SCALED_HEIGHT + $FRAME_THICKNESS \* 200 \/ 100`"
OUTPUT_WIDTH="`expr $SCALED_WIDTH + $FRAME_THICKNESS \* 200 \/ 100`"

IMAGE_BOTTOM_X="`expr "$FRAME_THICKNESS" + ${SCALED_HEIGHT}`"
IMAGE_BOTTOM_Y="`expr "$FRAME_THICKNESS" + ${SCALED_WIDTH}`"

echo "  FRAME_THICKNESS=${FRAME_THICKNESS}" >&2
echo "  IMAGE_TOPLEFT_XY=(${FRAME_THICKNESS}, ${FRAME_THICKNESS})" >&2
echo "  IMAGE_BOTTOMRIGHT_XY=(${IMAGE_BOTTOM_X}, ${IMAGE_BOTTOM_Y})" >&2
echo "  CORNER_RADIUS=${CORNER_RADIUS}" >&2

SED_FORMAT_CONVERT_COMMAND="s/[ \t][ \t][ \t]*/\ \t\\\\\n\ \ /g"

CONVERT_CMD=$(makeThumbnail3 "$input_image" "$output_image")
echo "CONVERT_CMD = ${CONVERT_CMD}" | sed "$SED_FORMAT_CONVERT_COMMAND"
echo "$CONVERT_CMD" | /bin/bash

exit 0
