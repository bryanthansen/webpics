# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeBlendedBorderThumbnail.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# Adds a decorative border around an image using convert from the
# imagemagick package
#
# copyright (c) 2008-2011 by Bryant Hansen
##################################################################

#!/bin/bash

# interesting example:
# /projects/webpics/MakeBlendedBorderThumbnail.sh -v IMG_5269g2.png .blendedBorderTest/IMG_5269g2b.png 0 "rgb($(expr $RANDOM % 256),$(expr $RANDOM % 256),$(expr $RANDOM % 256))"


######################################################
# Constants and Global Defaults
######################################################

# defaults
ADD_DROPSHADOW=1
ADD_BLUR_BACKGROUND=1
OVERLAY_PICTURE=1
ENABLE_COPYRIGHT=1
ROUNDED_BLUR=1

BLUR_BACKGROUND_WIDTH_PCNT=50
BLUR_IMAGE_EXPANSION_PCNT=95

COPYRIGHT_FONT="helvetica"
BLUR_STRENGTH="15"
SHADOW_GLOW_COLOR="rgb(0,0,0)"
SHADOW_GLOW_COLOR="rgb(255,255,255)"
TEXT_COLOR="rgb(0,0,0)"
TEXT_HIGHLIGHT_COLOR="rgb(255,255,255)"

CONVERT=`which convert`
[[ "$CONVERT" ]] || CONVERT=/usr/bin/convert
######################################################


######################################################
# Functions
######################################################

DebugMessage()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`($THUMB_SIZE): "

    while [[ "$OPTION_FOUND" ]] ; do
        OPTION_FOUND=""
        if [[ "$1" = "-e" ]] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [[ "$1" = "-n" ]] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}

function getCopyrightString() {
    local copyright_string="(c)2011 Bryant Hansen"
    local STRIP_HTML="s/<[^>]*>//g"

    # last one wins
    [[ "$COPYRIGHT_STRING" ]] && copyright_string="$COPYRIGHT_STRING"

    copyright_file="templates/copyright.html"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    copyright_file="copyright.txt"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    copyright_file="${input_image}.copyright"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    DebugMessage "copyright=$copyright_string"

    echo "$copyright_string"
    return 0
}

######################################################
# Setup Environment
######################################################
ME="$(basename "$0")"
MYDIR="$(dirname "$0")"

MY_OPTS="${MY_OPTS} [ -v | --verbose ]"
MY_OPTS="${MY_OPTS} [ -s size | --size size | --size=size ] "
MY_OPTS="${MY_OPTS} [ --no-blended-border ]"
MY_OPTS="${MY_OPTS} [ --no-dropshadow ]"
MY_OPTS="${MY_OPTS} [ --no-overlay ]"
MY_OPTS="${MY_OPTS} [ --no-blended-border ]"
MY_OPTS="${MY_OPTS} [ -c copyright_string | --copyright-string copyright_string | --copyright-string=copyright_string ]"

USAGE="${ME} [ ${MY_OPTS} ]  \
             input_image  output_image  [ size  [ BACKGROUND_COLOR ] ]"

IMAGE_INFO_DIR=".image_info"
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}

DebugMessage -e "\nStarting ${ME}..."

######################################################
# process arguments
######################################################
while [[ ! "${1/-*/}" ]] ; do
    case "$1" in
        -v | --verbose)
            DebugMessage "VERBOSE is set"
            VERBOSE=1
            ;;
        -s | --size | --size=*)
            if [[ ! "${1/--size=*/}" ]] ; then
                size="${1/--size=/}"
            else
                shift
                size="$1"
            fi
            # validate
            if [[ ! "${size/-*/}" ]]     || \
               (( "${size//[0-9]/}" )) ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: size = $size  Must set a valid numerical size.  Exiting abnormally."
                ERROR_BAD_SIZE=10
                exit $ERROR_BAD_SIZE
            fi
            ;;
        -c | --copyright-string | --copyright-string=*)
            if [[ ! "${1/--copyright-string=*/}" ]] ; then
                COPYRIGHT_STRING="${1/--copyright-string=/}"
            else
                shift
                COPYRIGHT_STRING="$1"
            fi
            ENABLE_COPYRIGHT=1
            if [[ ! "${COPYRIGHT_STRING/-*/}" ]] ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: COPYRIGHT_STRING = $COPYRIGHT_STRING  Must set to something.  Exiting abnormally."
                ERROR_BAD_COPYRIGHT=11
                exit $ERROR_BAD_COPYRIGHT
            fi
            ;;
        -b | --blended-border)
            ADD_BLUR_BACKGROUND=1
            ;;
        -d | --dropshadow)
            ADD_DROPSHADOW=1
            ;;
        -o | --image-overlay)
            OVERLAY_PICTURE=1
            ;;
        --blur-strength | --blur-strength=*)
            if [[ ! "${1/--blur-strength=*/}" ]] ; then
                blur_strength="${1/--blur-strength=/}"
            else
                shift
                blur_strength="$1"
            fi
            # validate
            if [[ ! "${blur_strength/-*/}" ]]     || \
               (( "${blur_strength//[0-9]/}" )) ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: blur_strength = $blur_strength  Must set a valid numerical size.  Exiting abnormally."
                ERROR_blur_strength=11
                exit $ERROR_blur_strength
            fi
            BLUR_STRENGTH="$blur_strength"
            ;;
        --blur-radius | --blur-radius=*)
            if [[ ! "${1/--blur-radius=*/}" ]] ; then
                blur_radius="${1/--blur-radius=/}"
            else
                shift
                blur_radius="$1"
            fi
            # validate
            if [[ ! "${blur_radius/-*/}" ]]     || \
               (( "${blur_radius//[0-9]/}" )) ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: blur_radius = $blur_radius  Must set a valid numerical size.  Exiting abnormally."
                ERROR_blur_radius=11
                exit $ERROR_blur_radius
            fi
            BLUR_RADIUS="$blur_radius"
            ;;
        --blur-expansion | --blur-expansion=*)
            if [[ ! "${1/--blur-expansion=*/}" ]] ; then
                blur_expansion="${1/--blur-expansion=/}"
            else
                shift
                blur_expansion="$1"
            fi
            # validate
            if [[ ! "${blur_expansion/-*/}" ]]     || \
               (( "${blur_expansion//[0-9]/}" )) ; then
                DebugMessage -e "USAGE: \n    ${USAGE}"
                DebugMessage "ERROR: blur_expansion = $blur_expansion  Must set a valid numerical size, as a percentage of the total resulting image size, including frame.  Exiting abnormally."
                ERROR_blur_expansion=12
                exit $ERROR_blur_expansion
            fi
            BLUR_IMAGE_EXPANSION_PCNT="$blur_expansion"
            ;;
        --no-blended-border)
            DebugMessage -e "Blended Border disabled on command line"
            unset ADD_BLUR_BACKGROUND
            ;;
        --no-dropshadow)
            DebugMessage -e "Dropshadow disabled on command line"
            unset ADD_DROPSHADOW
            ;;
        --no-overlay)
            DebugMessage -e "Overlay image disabled on command line"
            unset OVERLAY_PICTURE
            ;;
          *)
            DebugMessage -e "USAGE: \n    ${USAGE}"
            DebugMessage "ERROR: size = $size  Must set a valid numerical size.  Exiting abnormally."
            ERROR_BAD_SIZE=10
            exit $ERROR_BAD_SIZE
            ;;
    esac
    shift
done
input_image="$1"
output_image="$2"
[[ "$3" ]] && size="$3"
[[ "$4" ]] && BACKGROUND_COLOR="$4"
######################################################


ExifDir="`dirname "${input_image}"`/${IMAGE_INFO_DIR}"
ExifFile="${ExifDir}/`basename "${input_image}"`.txt"
if [[ -f "$ExifFile" ]] ; then
    #(( "$VERBOSE" )) && \
        DebugMessage "reading dimensions from cache file: ${ExifFile}.  w=$WIDTH, h=$HEIGHT"
    WIDTH="`cat "${ExifFile}" | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t]*//;s/x.*//g"`"
    HEIGHT="`cat "${ExifFile}" | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t].*x//;s/+.*//g"`"
    #(( "$VERBOSE" )) && \
        DebugMessage "${ExifFile}: WIDTH=$WIDTH, HEIGHT=$HEIGHT"
else
    DebugMessage "info file does not exist: ${ExifFile}."
fi

[[ ! "$WIDTH" ]] && WIDTH=0
[[ ! "$HEIGHT" ]] && HEIGHT=0
if (( "$WIDTH" <= "0" )) || (( "$HEIGHT" <= "0" )) ; then
    #(( "$VERBOSE" )) && \
        DebugMessage "Reading width and height from image manually...."
    WIDTH="`identify -format "%w" "${input_image}"`"
    HEIGHT="`identify -format "%h" "${input_image}"`"
    #(( "$VERBOSE" )) && \
        DebugMessage "${input_image}: WIDTH=$WIDTH, HEIGHT=$HEIGHT"
fi

RESIZE=1
[[ ! "$size" ]] && size=0
if (( "$size" < 1 )) ; then
    DebugMessage "Resize is not set; using original size of input image"
    RESIZE=0
    size="$WIDTH"
    (( "$HEIGHT" > "$size" )) && size="$HEIGHT"
fi

[[ ! "$FRAME_THICKNESS" ]] && FRAME_THICKNESS="`expr $size \/ 24 + 6`"
(( "$FRAME_THICKNESS" < 0 )) && FRAME_THICKNESS="`expr $size \/ 24 + 6`"
#(( "$FRAME_THICKNESS" < 10 )) && FRAME_THICKNESS=10

#FONT_SIZE="`expr $size \/ 50 + 3`"
FONT_SIZE="`expr $FRAME_THICKNESS \/ 2 + 1`"

CORNER_RADIUS="`expr $FRAME_THICKNESS \/ 2`"

newsize="`expr $size - $FRAME_THICKNESS \* 2`"
newsize="`expr $size + $FRAME_THICKNESS \* 2`"
if (( $newsize < 1 )) ; then
	DebugMessage "program error: invalid new image size: $newsize"
	exit -2
fi


# TODO: determine how to get the new height & width (exactly!) without creating
#       a new scaled image
TEMP_SCALED_IMAGE="${output_image}.scaled.tmp.png"
if [[ "$RESIZE" ]] && (( "$RESIZE" )) ; then
    CONVERT_CMD1=" \
        $CONVERT \
            -size ${WIDTH}x${HEIGHT} \
            xc: none \
            -resize ${newsize}x${newsize}+0+0 \
            '${TEMP_SCALED_IMAGE}' 2> /dev/null   \
    "
else
    CONVERT_CMD1=" \
        cp \"$input_image\" \"$TEMP_SCALED_IMAGE\"
    "
fi


SED_FORMAT_CONVERT_COMMAND="s/[ \t][ \t][ \t]*/\ \t\\\\\n\ \ /g"
echo "# CONVERT_CMD1 = ${CONVERT_CMD1}" | sed "$SED_FORMAT_CONVERT_COMMAND"
echo "$CONVERT_CMD1" | /bin/bash

IMAGE_HEIGHT="`identify -format "%h" "$TEMP_SCALED_IMAGE"`"
IMAGE_WIDTH="`identify -format "%w" "$TEMP_SCALED_IMAGE"`"

FRAME_HEIGHT="`expr $IMAGE_HEIGHT + $FRAME_THICKNESS \* 200 \/ 100`"
FRAME_WIDTH="`expr $IMAGE_WIDTH + $FRAME_THICKNESS \* 200 \/ 100`"

IMAGE_BOTTOM_X="`expr "$FRAME_THICKNESS" + ${IMAGE_HEIGHT}`"
IMAGE_BOTTOM_Y="`expr "$FRAME_THICKNESS" + ${IMAGE_WIDTH}`"

echo "  FRAME_THICKNESS=${FRAME_THICKNESS}" >&2
echo "#  IMAGE_TOPLEFT_XY=(${FRAME_THICKNESS}, ${FRAME_THICKNESS})" >&2
echo "#  IMAGE_BOTTOMRIGHT_XY=(${IMAGE_BOTTOM_X}, ${IMAGE_BOTTOM_Y})" >&2
echo "  CORNER_RADIUS=${CORNER_RADIUS}" >&2

SED_FORMAT_CONVERT_COMMAND="s/[ \t][ \t][ \t]*/\ \t\\\\\n\ \ /g"
echo "# CONVERT_CMD = ${CONVERT_CMD}" | sed "$SED_FORMAT_CONVERT_COMMAND"
echo "$CONVERT_CMD" | /bin/bash

makeThumbnail3() {

    local input_image="$1"
    local output_image="$2"

    CONVERT_CMD=" \
        $CONVERT \
    "

    if [[ "$BACKGROUND_COLOR" ]] ; then
        local CONVERT_NEW_FRAME=" \
                -gravity center       \
                -size ${FRAME_WIDTH}x${FRAME_HEIGHT} \
                xc:${BACKGROUND_COLOR} \
        "
        echo "  BACKGROUND_COLOR=${BACKGROUND_COLOR}" >&2
    else
        local CONVERT_NEW_FRAME=" \
                -gravity center       \
                -size ${FRAME_WIDTH}x${FRAME_HEIGHT} \
                xc:none \
        "
        DebugMessage "background is transparent."
    fi
    local CONVERT_FILL_BACKGROUND_COLOR=" \
        -fill '$BACKGROUND_COLOR' \
        -draw 'rectangle 0,0 ${FRAME_WIDTH},${FRAME_HEIGHT}' \
    "

    if [[ "$ADD_BLUR_BACKGROUND" ]] && (( "$ADD_BLUR_BACKGROUND" )) ; then

        BACKGROUND_BRIGHTNESS=100

        DEFAULT_BLUR_STRENGTH=`expr 20 - 15 \* $BACKGROUND_BRIGHTNESS \/ 100`
        BLUR_RADIUS="`expr "$BLUR_STRENGTH" \* 3`"
        echo "  BACKGROUND_BRIGHTNESS=$BACKGROUND_BRIGHTNESS" >&2
        echo "  BLUR_STRENGTH=${BLUR_STRENGTH}" >&2
        echo "  BLUR_RADIUS=${BLUR_RADIUS}" >&2

        echo "  IMAGE_WIDTH=${IMAGE_WIDTH}" >&2
        echo "  IMAGE_HEIGHT=${IMAGE_HEIGHT}" >&2
        echo "  FRAME_WIDTH=${FRAME_WIDTH}" >&2
        echo "  FRAME_HEIGHT=${FRAME_HEIGHT}" >&2
        echo "  FRAME_THICKNESS=${FRAME_THICKNESS}" >&2

        [[ ! "$BLUR_IMAGE_EXPANSION_PCNT" ]] && BLUR_IMAGE_EXPANSION_PCNT="370"
        BLUR_IMAGE_WIDTH=`expr $FRAME_WIDTH \* $BLUR_IMAGE_EXPANSION_PCNT / 100`
        BLUR_IMAGE_HEIGHT=`expr $FRAME_HEIGHT \* $BLUR_IMAGE_EXPANSION_PCNT / 100`

        echo "  BLUR_IMAGE_EXPANSION_PCNT=${BLUR_IMAGE_EXPANSION_PCNT}" >&2
        echo "  BLUR_IMAGE_WIDTH=${BLUR_IMAGE_WIDTH}" >&2
        echo "  BLUR_IMAGE_HEIGHT=${BLUR_IMAGE_HEIGHT}" >&2

        BLUR_IMAGE_ROUNDED_CORNERS=0
        if [[ "$BLUR_IMAGE_ROUNDED_CORNERS" ]] && \
        (( "$BLUR_IMAGE_ROUNDED_CORNERS" )) ; then
            echo "# ERROR: BLUR_IMAGE_ROUNDED_CORNERS is not supported at the moment!  Option disabled" >&2
        fi

    fi

    local CONVERT_ADD_BLUR_BACKGROUND=" \
        \( \
            '${input_image}' \
            -resize "${BLUR_IMAGE_WIDTH}x${BLUR_IMAGE_HEIGHT}\!" \
        \) \
        -channel RGBA \
        -blur ${BLUR_RADIUS},${BLUR_STRENGTH}    \
        -composite \
    "

    if [[ "$ADD_DROPSHADOW" ]] && (( "$ADD_DROPSHADOW" )) ; then

        SHADOW_OFFSET_PERCENT=10
        SHADOW_ASIMUTH=315

        BACKGROUND_BRIGHTNESS=100

        IMAGE_MAX_HW=$IMAGE_HEIGHT
        (( $IMAGE_WIDTH > $IMAGE_MAX_HW )) && IMAGE_MAX_HW=$IMAGE_WIDTH
        SHADOW_INDENT="`expr $IMAGE_MAX_HW \/ 125`"
        SHADOW_INDENT="`expr $IMAGE_MAX_HW \* 8 \/ 1000`"
        SHADOW_INDENT="`expr $FRAME_THICKNESS \/ 10`"
        SHADOW_TOP=`expr ${FRAME_THICKNESS} + ${SHADOW_INDENT}`
        SHADOW_LEFT=`expr ${FRAME_THICKNESS} + ${SHADOW_INDENT}`
        SHADOW_BOTTOM=`expr ${IMAGE_BOTTOM_X} + ${SHADOW_INDENT}`
        SHADOW_RIGHT=`expr ${IMAGE_BOTTOM_Y} + ${SHADOW_INDENT}`

        SHADOW_IMAGE_WIDTH=`expr "$FRAME_WIDTH"`
        SHADOW_IMAGE_HEIGHT=`expr "$FRAME_HEIGHT"`


        # observed behavior: dropshadow strength should be inversely-proportional
        # to brightness, since it's a far-more-pronounced effect on light
        # backgrounds
        # 20 seems good for reasonably-dark backgrounds
        # 5 is probably good for lighter
        # blur radius also plays an effect
        DEFAULT_SHADOW_BLUR_STRENGTH=`expr 20 - 10 \* $BACKGROUND_BRIGHTNESS \/ 100`
        [[ ! "$SHADOW_BLUR_STRENGTH" ]] && SHADOW_BLUR_STRENGTH="$BLUR_STRENGTH"

        [[ ! "$SHADOW_BLUR_RADIUS" ]] && SHADOW_BLUR_RADIUS="`expr "$SHADOW_BLUR_STRENGTH" \* 3`"
    #    SHADOW_BLUR_RADIUS=`expr $SHADOW_INDENT \* \( 300 - $BACKGROUND_BRIGHTNESS \) \/ 100`
    #    SHADOW_BLUR_RADIUS=`expr $SHADOW_BLUR_STRENGTH \* 3`
    #    SHADOW_BLUR_RADIUS=0

        echo "  SHADOW_INDENT=${SHADOW_INDENT}" >&2
        echo "#  SHADOW_TOPLEFT_XY=(${SHADOW_LEFT}, ${SHADOW_TOP})" >&2
        echo "#  SHADOW_BOTTOMRIGHT_XY=(${SHADOW_RIGHT}, ${SHADOW_BOTTOM})" >&2
        echo "  CORNER_RADIUS=${CORNER_RADIUS}" >&2
        echo "  SHADOW_BLUR_RADIUS=${SHADOW_BLUR_RADIUS}" >&2
        echo "  SHADOW_BLUR_STRENGTH=${SHADOW_BLUR_STRENGTH}" >&2


        shadow_rect="'roundrectangle ${SHADOW_LEFT},${SHADOW_TOP} ${SHADOW_RIGHT},${SHADOW_BOTTOM} ${CORNER_RADIUS} ${CORNER_RADIUS}'"
    fi

    local CONVERT_DRAW_DROPSHADOW=" \
        \( \
            -size "${SHADOW_IMAGE_WIDTH}x${SHADOW_IMAGE_HEIGHT}\!" \
            xc:none   \
            -gravity "center"    \
            -fill "black"   \
            -draw $shadow_rect \
        \) \
        -channel RGBA \
        -blur ${SHADOW_BLUR_RADIUS},${SHADOW_BLUR_STRENGTH}    \
        -composite \
    "

#    if [[ "$OVERLAY_PICTURE" ]] && (( "$OVERLAY_PICTURE" )) ; then
#    fi

    local CONVERT_DRAW_OVERLAY_PICTURE=" \
        \(    \
            xc:none       \
            -draw 'roundrectangle ${FRAME_THICKNESS},${FRAME_THICKNESS} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X} ${CORNER_RADIUS},${CORNER_RADIUS}'      \
            \(    \
                -gravity center       \
                '${input_image}'       \
                -resize "${BLUR_IMAGE_WIDTH}x${BLUR_IMAGE_HEIGHT}\!" \
            \)    \
            -compose SrcIn \
            -composite \
        \)    \
        -compose Over \
        -composite \
    "

    if [[ "$ENABLE_COPYRIGHT" ]] && (( "$ENABLE_COPYRIGHT" )) ; then

        COPYRIGHT_X="`expr $FRAME_THICKNESS + 0`"
        COPYRIGHT_X="`expr $FRAME_THICKNESS \* 20 \/ 100 + 0`"

        # Y position has caused lots of problems
        MARGIN_SIZE="`expr $FRAME_THICKNESS - $FONT_SIZE`"
        COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100 + \( $FRAME_THICKNESS - $FONT_SIZE \) \* 100 \/ 100 + 0`"
        COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100 + $MARGIN_SIZE`"
        COPYRIGHT_Y="`expr $MARGIN_SIZE \/ 3`"
    #    COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100`"
    #    COPYRIGHT_Y="-5"

        COPYRIGHT_STRING="$(getCopyrightString)"
        echo "  COPYRIGHT_STRING=\"${COPYRIGHT_STRING}\"" >&2
        echo "  COPYRIGHT_FONT=${COPYRIGHT_FONT}" >&2
        echo "  FONT_SIZE=${FONT_SIZE}" >&2
        echo "  TEXT_COLOR=${TEXT_COLOR}" >&2
        echo "  TEXT_HIGHLIGHT_COLOR=${TEXT_HIGHLIGHT_COLOR}" >&2
        echo "  COPYRIGHT_X,Y=(${COPYRIGHT_X},${COPYRIGHT_Y})" >&2

    fi

    local CONVERT_DRAW_COPYRIGHT=" \
        -gravity \"SouthWest\" \
        -font \"${COPYRIGHT_FONT}\" \
        -pointsize \"${FONT_SIZE}\" \
        -fill \"${TEXT_HIGHLIGHT_COLOR}\" \
        -draw 'text ${COPYRIGHT_X},${COPYRIGHT_Y} \"${COPYRIGHT_STRING}\"' \
        -fill \"${TEXT_COLOR}\" \
        -draw 'text $(expr ${COPYRIGHT_X} + 1),$(expr ${COPYRIGHT_Y} + 1) \"${COPYRIGHT_STRING}\"' \
    "

    # set output image - always done
    local CONVERT_ADD_OUTPUT_IMAGE="\"${output_image}\""

    CONVERT_CMD="$CONVERT_CMD $CONVERT_NEW_FRAME $CONVERT_ADD_BLUR_BACKGROUND $CONVERT_DRAW_DROPSHADOW $CONVERT_DRAW_OVERLAY_PICTURE $CONVERT_DRAW_COPYRIGHT $CONVERT_ADD_OUTPUT_IMAGE"
    echo "$CONVERT_CMD"

}

makeThumbnail2() {

    local input_image="$1"
    local output_image="$2"


    CONVERT_CMD=" \
        $CONVERT \
            -gravity center       \
            -size ${FRAME_WIDTH}x${FRAME_HEIGHT} \
    "

    if [[ "$BACKGROUND_COLOR" ]] ; then
        CONVERT_CMD="${CONVERT_CMD} \
                xc:${BACKGROUND_COLOR} \
        "
        echo "  BACKGROUND_COLOR=${BACKGROUND_COLOR}" >&2
    else
        CONVERT_CMD="${CONVERT_CMD} \
                xc:none \
        "
        DebugMessage "background is transparent."
    fi

    if [[ "$ADD_BLUR_BACKGROUND" ]] && (( "$ADD_BLUR_BACKGROUND" )) ; then

        BACKGROUND_BRIGHTNESS=100

        DEFAULT_BLUR_STRENGTH=`expr 20 - 15 \* $BACKGROUND_BRIGHTNESS \/ 100`
        BLUR_RADIUS="`expr "$BLUR_STRENGTH" \* 3`"
        echo "  BACKGROUND_BRIGHTNESS=$BACKGROUND_BRIGHTNESS" >&2
        echo "  BLUR_STRENGTH=${BLUR_STRENGTH}" >&2
        echo "  BLUR_RADIUS=${BLUR_RADIUS}" >&2

        echo "  IMAGE_WIDTH=${IMAGE_WIDTH}" >&2
        echo "  IMAGE_HEIGHT=${IMAGE_HEIGHT}" >&2
        echo "  FRAME_WIDTH=${FRAME_WIDTH}" >&2
        echo "  FRAME_HEIGHT=${FRAME_HEIGHT}" >&2
        echo "  FRAME_THICKNESS=${FRAME_THICKNESS}" >&2

        [[ ! "$BLUR_IMAGE_EXPANSION_PCNT" ]] && BLUR_IMAGE_EXPANSION_PCNT="370"
        BLUR_IMAGE_WIDTH=`expr $FRAME_WIDTH \* $BLUR_IMAGE_EXPANSION_PCNT / 100`
        BLUR_IMAGE_HEIGHT=`expr $FRAME_HEIGHT \* $BLUR_IMAGE_EXPANSION_PCNT / 100`

        echo "  BLUR_IMAGE_EXPANSION_PCNT=${BLUR_IMAGE_EXPANSION_PCNT}" >&2
        echo "  BLUR_IMAGE_WIDTH=${BLUR_IMAGE_WIDTH}" >&2
        echo "  BLUR_IMAGE_HEIGHT=${BLUR_IMAGE_HEIGHT}" >&2

        BLUR_IMAGE_ROUNDED_CORNERS=0
        if [[ "$BLUR_IMAGE_ROUNDED_CORNERS" ]] && \
        (( "$BLUR_IMAGE_ROUNDED_CORNERS" )) ; then
            echo "# ERROR: BLUR_IMAGE_ROUNDED_CORNERS is not supported at the moment!  Option disabled.  Maybe not necessary; the effect is good" >&2
        fi

        CONVERT_CMD="${CONVERT_CMD} \
            \( \
                '${input_image}' \
                -resize "${BLUR_IMAGE_WIDTH}x${BLUR_IMAGE_HEIGHT}\!" \
            \) \
            -channel RGBA \
            -blur ${BLUR_RADIUS},${BLUR_STRENGTH}    \
            -composite \
        "
    fi

    if [[ "$ADD_DROPSHADOW" ]] && (( "$ADD_DROPSHADOW" )) ; then

        SHADOW_OFFSET_PERCENT=10
        SHADOW_ASIMUTH=315

        BACKGROUND_BRIGHTNESS=100

        IMAGE_MAX_HW=$IMAGE_HEIGHT
        (( $IMAGE_WIDTH > $IMAGE_MAX_HW )) && IMAGE_MAX_HW=$IMAGE_WIDTH
        SHADOW_INDENT="`expr $IMAGE_MAX_HW \/ 125`"
        SHADOW_INDENT="`expr $IMAGE_MAX_HW \* 8 \/ 1000`"
        SHADOW_INDENT="`expr $FRAME_THICKNESS \/ 10`"
        SHADOW_TOP=`expr ${FRAME_THICKNESS} + ${SHADOW_INDENT}`
        SHADOW_LEFT=`expr ${FRAME_THICKNESS} + ${SHADOW_INDENT}`
        SHADOW_BOTTOM=`expr ${IMAGE_BOTTOM_X} + ${SHADOW_INDENT}`
        SHADOW_RIGHT=`expr ${IMAGE_BOTTOM_Y} + ${SHADOW_INDENT}`

        SHADOW_IMAGE_WIDTH=`expr "$FRAME_WIDTH"`
        SHADOW_IMAGE_HEIGHT=`expr "$FRAME_HEIGHT"`


        # observed behavior: dropshadow strength should be inversely-proportional
        # to brightness, since it's a far-more-pronounced effect on light
        # backgrounds
        # 20 seems good for reasonably-dark backgrounds
        # 5 is probably good for lighter
        # blur radius also plays an effect
        DEFAULT_SHADOW_BLUR_STRENGTH=`expr 20 - 10 \* $BACKGROUND_BRIGHTNESS \/ 100`
        [[ ! "$SHADOW_BLUR_STRENGTH" ]] && SHADOW_BLUR_STRENGTH="$BLUR_STRENGTH"

        [[ ! "$SHADOW_BLUR_RADIUS" ]] && SHADOW_BLUR_RADIUS="`expr "$SHADOW_BLUR_STRENGTH" \* 3`"
    #    SHADOW_BLUR_RADIUS=`expr $SHADOW_INDENT \* \( 300 - $BACKGROUND_BRIGHTNESS \) \/ 100`
    #    SHADOW_BLUR_RADIUS=`expr $SHADOW_BLUR_STRENGTH \* 3`
    #    SHADOW_BLUR_RADIUS=0

        echo "  SHADOW_INDENT=${SHADOW_INDENT}" >&2
        echo "#  SHADOW_TOPLEFT_XY=(${SHADOW_LEFT}, ${SHADOW_TOP})" >&2
        echo "#  SHADOW_BOTTOMRIGHT_XY=(${SHADOW_RIGHT}, ${SHADOW_BOTTOM})" >&2
        echo "  CORNER_RADIUS=${CORNER_RADIUS}" >&2
        echo "  SHADOW_BLUR_RADIUS=${SHADOW_BLUR_RADIUS}" >&2
        echo "  SHADOW_BLUR_STRENGTH=${SHADOW_BLUR_STRENGTH}" >&2

        shadow_rect="'roundrectangle ${SHADOW_LEFT},${SHADOW_TOP} ${SHADOW_RIGHT},${SHADOW_BOTTOM} ${CORNER_RADIUS} ${CORNER_RADIUS}'"
        CONVERT_CMD="${CONVERT_CMD} \
            \( \
                -size "${SHADOW_IMAGE_WIDTH}x${SHADOW_IMAGE_HEIGHT}\!" \
                xc:none   \
                -gravity "center"    \
                -fill "black"   \
                -draw $shadow_rect \
            \) \
            -channel RGBA \
            -blur ${SHADOW_BLUR_RADIUS},${SHADOW_BLUR_STRENGTH}    \
            -composite \
            "

    fi

    if [[ "$OVERLAY_PICTURE" ]] && (( "$OVERLAY_PICTURE" )) ; then
        CONVERT_CMD="${CONVERT_CMD} \
            \(    \
                xc:none       \
                -draw 'roundrectangle ${FRAME_THICKNESS},${FRAME_THICKNESS} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X} ${CORNER_RADIUS},${CORNER_RADIUS}'      \
                \(    \
                    -gravity center       \
                    '${input_image}'       \
                    -resize "${BLUR_IMAGE_WIDTH}x${BLUR_IMAGE_HEIGHT}\!" \
                \)    \
                -compose SrcIn \
                -composite \
            \)    \
            -compose Over \
            -composite \
        "
    fi

    if [[ "$ENABLE_COPYRIGHT" ]] && (( "$ENABLE_COPYRIGHT" )) ; then

        COPYRIGHT_X="`expr $FRAME_THICKNESS + 0`"
        COPYRIGHT_X="`expr $FRAME_THICKNESS \* 20 \/ 100 + 0`"

        # Y position has caused lots of problems
        MARGIN_SIZE="`expr $FRAME_THICKNESS - $FONT_SIZE`"
        COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100 + \( $FRAME_THICKNESS - $FONT_SIZE \) \* 100 \/ 100 + 0`"
        COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100 + $MARGIN_SIZE`"
        COPYRIGHT_Y="`expr $MARGIN_SIZE \/ 3`"
    #    COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100`"
    #    COPYRIGHT_Y="-5"

        COPYRIGHT_STRING="$(getCopyrightString)"
        echo "  COPYRIGHT_STRING=\"${COPYRIGHT_STRING}\"" >&2
        echo "  COPYRIGHT_FONT=${COPYRIGHT_FONT}" >&2
        echo "  FONT_SIZE=${FONT_SIZE}" >&2
        echo "  TEXT_COLOR=${TEXT_COLOR}" >&2
        echo "  TEXT_HIGHLIGHT_COLOR=${TEXT_HIGHLIGHT_COLOR}" >&2
        echo "  COPYRIGHT_X,Y=(${COPYRIGHT_X},${COPYRIGHT_Y})" >&2

        CONVERT_CMD="${CONVERT_CMD} \
            -gravity \"SouthWest\" \
            -font \"${COPYRIGHT_FONT}\" \
            -pointsize \"${FONT_SIZE}\" \
            -fill \"${TEXT_HIGHLIGHT_COLOR}\" \
            -draw 'text ${COPYRIGHT_X},${COPYRIGHT_Y} \"${COPYRIGHT_STRING}\"' \
            -fill \"${TEXT_COLOR}\" \
            -draw 'text $(expr ${COPYRIGHT_X} + 1),$(expr ${COPYRIGHT_Y} + 1) \"${COPYRIGHT_STRING}\"' \
        "
    fi

    # set output image - always done
    CONVERT_CMD="${CONVERT_CMD} \
        \"${output_image}\"
    "

    echo "$CONVERT_CMD"

}

SED_FORMAT_CONVERT_COMMAND="s/[ \t][ \t][ \t]*/\ \t\\\\\n\ \ /g"

CONVERT_CMD=$(makeThumbnail2 "$input_image" "${output_image}")
echo "# CONVERT_CMD(3) = ${CONVERT_CMD}" | sed "$SED_FORMAT_CONVERT_COMMAND"
CONVERT_CMD=$(makeThumbnail3 "$input_image" "${output_image}")
echo "# CONVERT_CMD = ${CONVERT_CMD}" | sed "$SED_FORMAT_CONVERT_COMMAND"
echo "$CONVERT_CMD" | /bin/bash

# cleanup - remove temp files
rm "${TEMP_SCALED_IMAGE}"

exit 0