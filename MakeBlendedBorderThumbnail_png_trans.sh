# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeBlendedBorderThumbnail.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/bash

ENABLE_COPYRIGHT=1

# set -o verbose
# echo "running $0: $*..."

# various global settings
FONT="helvetica"
BLUR_STRENGTH="5"

BACKGROUND_COLOR="rgb(51,51,51)"
SHADOW_GLOW_COLOR="rgb(0,0,0)"
SHADOW_GLOW_COLOR="rgb(255,255,255)"
TEXT_COLOR="rgb(64,64,128)"

CONVERT=/usr/bin/convert

# sed expressions
STRIP_HTML="s/<[^>]*>//g"


DebugMessage()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`($THUMB_SIZE): "

    while [[ "$OPTION_FOUND" ]] ; do
        OPTION_FOUND=""
        if [[ "$1" = "-e" ]] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [[ "$1" = "-n" ]] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}


######################################################
# Setup Environment
######################################################
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}


# process arguments
input_image="$1"
output_image="$2"
size="$3"
[[ "$4" ]] && BACKGROUND_COLOR="$4"
(( "$#" < 3 )) && echo "not enough args, dude!" && exit -2
(( "$size" < 1 )) && echo "bad size, dude: $size" && exit -2

# this makes the shadow a bit large for my taste  on large pics
#SHADOW_SIZE="`expr $size \/ 16`"
# next try:
SHADOW_SIZE="`expr $size \/ 24 + 6`"
(( "$SHADOW_SIZE" < 10 )) && SHADOW_SIZE=10

FONT_SIZE="`expr $size \/ 25`"
(( "$FONT_SIZE" > 16 )) && FONT_SIZE=16

CORNER_RADIUS="`expr $SHADOW_SIZE \/ 2`"

BLUR_RADIUS="`expr "$SHADOW_SIZE" \/ 2`"
BLUR_RADIUS="$SHADOW_SIZE"

copyright_string=""
if (( "$ENABLE_COPYRIGHT" )) ; then
	[[ "$copyright_string" ]] || copyright_string="`cat "copyright.txt"`"
	[[ "$copyright_string" ]] || copyright_string="`cat "templates/copyright.html" | sed "$STRIP_HTML"`"
	[[ "$copyright_string" ]] || copyright_string="(c)2008 Bryant Hansen"
	echo "copyright=$copyright_string"
fi

IMAGE_INFO_DIR=".image_info"

temp_scaled_image="`mktemp "${output_image}.scaled.tiff.XXXXXX"`"
temp_rounded_image="`mktemp "${input_image}.mask.png.XXXXXX"`"

ExifFile="`dirname "${input_image}"`/${IMAGE_INFO_DIR}/`basename "${input_image}"`.txt"
if [[ -f "$ExifFile" ]] ; then
    WIDTH="`cat "${ExifFile}" | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t]*//;s/x.*//g"`"
    HEIGHT="`cat "${ExifFile}" | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t].*x//;s/+.*//g"`"
    (( "$VERBOSE" )) && DebugMessage "reading dimensions from info file: ${ExifFile}.  w=$WIDTH, h=$HEIGHT"
fi
if [[ ! "$WIDTH" ]] || [[ ! "$HEIGHT" ]] ; then
    DebugMessage "ERROR2: failed to read width/height from exit file.  Setting to 0 and continuing."
    WIDTH=0
    HEIGHT=0
fi
if (( "$WIDTH" <= "0" )) || (( "$HEIGHT" <= "0" )) ; then
    DebugMessage "Exif data not properly read from \"${ExifFile}\".  Reading manually...."
    WIDTH="`identify -format \"%w\" \"$file\"`"
    HEIGHT="`identify -format \"%h\" \"$file\"`"
fi

# TODO: read this from ImageInfo file first - 
h="`identify -format "%h" "$input_image"`"
w="`identify -format "%w" "$input_image"`"

echo "$0: WIDTH=$WIDTH, HEIGHT=$HEIGHT, w=$w, h=$h"

newsize="`expr $size - $SHADOW_SIZE \* 2`"
if (( $newsize < 1 )) ; then
	echo "program error: invalid new image size: $newsize"
	exit -2
fi

# TODO: get rid of this error: Unknown pseudo-tag 65537
$CONVERT -resize ${newsize}x${newsize}+0+0 "$input_image" "tiff:${temp_scaled_image}" 2> /dev/null

# duplicated settings?
h="`identify -format "%h" "$temp_scaled_image"`"
w="`identify -format "%w" "$temp_scaled_image"`"

frameheight="`expr $h + $SHADOW_SIZE \* 2`"
framewidth="`expr $w + $SHADOW_SIZE \* 2`"

# backwards from what I expect, but it works
IMAGE_BOTTOM_X="`expr "$SHADOW_SIZE" + ${h}`"
IMAGE_BOTTOM_Y="`expr "$SHADOW_SIZE" + ${w}`"

# echo "Image bottom: w=$IMAGE_BOTTOM_X h=$IMAGE_BOTTOM_Y"
# echo "$image: ${h}x${w} becomes ${frameheight}x${framewidth}"

$CONVERT \
	-size ${framewidth}x${frameheight} \
	xc:none \
	-fill "$BACKGROUND_COLOR" \
	-gravity "center" \
	-draw "roundrectangle ${SHADOW_SIZE},${SHADOW_SIZE} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X} $CORNER_RADIUS,$CORNER_RADIUS" \
	-draw "image In 0,0 0,0 \"${temp_scaled_image}\"" \
	"png:${temp_rounded_image}"

text_pos_x="`expr $SHADOW_SIZE + 0`"
text_pos_y="`expr $SHADOW_SIZE \* 3 \/ 2`"

# consider a resize & crop to bring out the border colors
$CONVERT \
	-size ${framewidth}x${frameheight} \
	xc:none \
	-fill "$BACKGROUND_COLOR" \
	-draw "rectangle 0,0 ${framewidth},${frameheight}" \
	-gravity "center" \
	-draw "image Over 0,0 0,0 \"${temp_rounded_image}\"" \
	-resize "`expr "$framewidth" + "$SHADOW_SIZE"`"x"`expr "$frameheight" + "$SHADOW_SIZE"`" \
	-blur "${BLUR_RADIUS},${BLUR_STRENGTH}" \
	-draw "image Over 0,0 0,0 \"${temp_rounded_image}\"" \
	-gravity "center" \
	-crop "${framewidth}x${frameheight}+0+0" \
	-gravity "SouthWest" \
	-font "$FONT" \
	-pointsize "$FONT_SIZE" \
	-fill "$TEXT_COLOR" \
	-draw "text `expr $SHADOW_SIZE + 0`,`expr $SHADOW_SIZE \* 3 \/ 2` \"${copyright_string}\"" \
	"png:${output_image}"

# cleanup - remove temp files
rm "${temp_rounded_image}"
rm "${temp_scaled_image}"

exit 0
