# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeDecoratedThumbnails.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/bash

set -o verbose

echo "running $0: $*..."

CONVERT=/usr/bin/convert

FONT="arial"

BACKGROUND_COLOR="rgb(0,0,0)"
SHADOW_GLOW_COLOR="rgb(0,0,0)"
SHADOW_GLOW_COLOR="rgb(255,255,255)"
TEXT_COLOR="rgb(128,32,32)"
MASK_COLOR="rgb(32,32,200)"

STRIP_HTML="s/<[^>]*>//g"

input_image="$1"
output_image="$2"
size="$3"
(( "$#" < 3 )) && echo "not enough args, dude!" && exit -2
(( "$size" < 1 )) && echo "bad size, dude: $size" && exit -2

FONT_SIZE="`expr $size \/ 18`"
(( "$FONT_SIZE" > 16 )) && FONT_SIZE=16

SHADOW_SIZE="`expr $size \/ 10`"
(( "$SHADOW_SIZE" < 10 )) && SHADOW_SIZE=10

BLUR_RADIUS="$SHADOW_SIZE"
BLUR_RADIUS="`expr "$SHADOW_SIZE" \/ 2`"
BLUR_STRENGTH="30"

copyright_string="`cat "templates/copyright.html" | sed "$STRIP_HTML"`"
echo "copyright=$copyright_string"

temp_scaled_image="${output_image}.image.tmp.tiff"

h="`identify -format "%h" "$input_image"`"
w="`identify -format "%w" "$input_image"`"

$CONVERT -resize ${size}x${size}+0+0 "$input_image" "$temp_scaled_image"

h="`identify -format "%h" "$temp_scaled_image"`"
w="`identify -format "%w" "$temp_scaled_image"`"

frameheight="`expr $h + $SHADOW_SIZE \* 2`"
framewidth="`expr $w + $SHADOW_SIZE \* 2`"

# backwards from what I expect, but it works
IMAGE_BOTTOM_X="`expr "$SHADOW_SIZE" + ${h}`"
IMAGE_BOTTOM_Y="`expr "$SHADOW_SIZE" + ${w}`"

echo "Image bottom: w=$IMAGE_BOTTOM_X h=$IMAGE_BOTTOM_Y"

echo "$image: ${h}x${w} becomes ${frameheight}x${framewidth}"

#TODO: create a mask and plot the image over it
#	-draw "roundRectangle  x0,y0 x1,y1 wc,hc
#	-draw "roundRectangle  x0,y0 x1,y1 wc,hc
#	-fill "$MASK_COLOR" \
#	-gravity "center" \
#	-draw "rectangle ${SHADOW_SIZE},${SHADOW_SIZE} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X}" "$SHADOW_SIZE","$SHADOW_SIZE"

#convert \
#	-size ${framewidth}x${frameheight} \
#	xc:none \
#	-fill "$BACKGROUND_COLOR" \
#	-draw "rectangle 0,0 ${framewidth},${frameheight}" \
#	-fill "$SHADOW_GLOW_COLOR" \
#	-gravity "center" \
#	-draw "rectangle ${SHADOW_SIZE},${SHADOW_SIZE} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X}" \
#	-blur "${BLUR_RADIUS},${BLUR_STRENGTH}" \
#	-gravity "center" \
#	-draw "image Over 0,0 0,0 \"${temp_scaled_image}\"" \
#	-gravity "SouthWest" \
#	-font helvetica \
#	-pointsize "$FONT_SIZE" \
#	-fill "$TEXT_COLOR" \
#	-draw "text 7,0 \"$copyright_string\"" \
#	-fill "$MASK_COLOR" \
#	-gravity "center" \
#	-draw "roundrectangle ${SHADOW_SIZE},${SHADOW_SIZE} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X} $SHADOW_SIZE,$SHADOW_SIZE" \
#	"${output_image}"

convert \
	-size ${framewidth}x${frameheight} \
	xc:none \
	-fill "$BACKGROUND_COLOR" \
	-draw "rectangle 0,0 ${framewidth},${frameheight}" \
	-fill "$SHADOW_GLOW_COLOR" \
	-gravity "center" \
	-draw "rectangle ${SHADOW_SIZE},${SHADOW_SIZE} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X}" \
	-blur "${BLUR_RADIUS},${BLUR_STRENGTH}" \
	-gravity "SouthWest" \
	-font helvetica \
	-pointsize "$FONT_SIZE" \
	-fill "$TEXT_COLOR" \
	-draw "text 7,0 \"$copyright_string\"" \
	-fill "none" \
	-gravity "center" \
	-draw "roundrectangle ${SHADOW_SIZE},${SHADOW_SIZE} ${IMAGE_BOTTOM_Y},${IMAGE_BOTTOM_X} $SHADOW_SIZE,$SHADOW_SIZE" \
	-gravity "center" \
	-fill "$MASK_COLOR" \
	-draw "image Under 0,0 0,0 \"${temp_scaled_image}\"" \
	"${output_image}"
