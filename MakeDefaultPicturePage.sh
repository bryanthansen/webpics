# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeDefaultPicturePage.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/sh

MASTER_INDEX_DIR="~/public_html"

FileContainsLine_old()  # this has been replaced by a stand-alone script
{
   if [ $# -ne 2 ] ; then
      echo "ERROR: Function FileContainsLine called with $# parameters!  Should be 2."
      return 254
   fi
   if [ ! -f "$1" ] ; then
      echo "warning: Function FileContainsLine called with missing file \"$1\"!"
      return 253
   fi
   while read 'line'
   do
   {
      if [ "$line" = "$2" ] ; then
         # we found a matching line
         return 1
         if [ ! -z $VERBOSE ] ; then
            echo "FileContainsLine: found exact match for string \"$2\" in file \"$1\""
         fi
      fi
   }
   done <"$1"
   # not found
   return 0
}

# Main

# start by making 600-pixel-res thumbnails, which we will use in our current dir
mth --size=600

# create full-size and move our current photos in there

# protect the full-size pictures
chmod 770 *.jpg *.JPG *.gif *.GIF

# make 256-pixel thumbnails last so they have the newest date
# if 600-pixel thumbnails are made first, MakePictureIndex.sh will try to make
# 256-pixel thumbnails from the 600-pixel ones since the 600-pixel thumbs
# would be newer
mth --size=800
mth --size=1024
mth --size=256

# create the picindex for this directory and full-size
mpi -r

echo "creating link to \"$PWD\" from the main picture page"
# avoid standard inaccesible directory shortcuts in our path

# first, remove the home shortcut
LINK_DIR="${PWD/$HOME//data}"

# then remove the main html page shortcut
LINK_DIR="${PWD/$HOME//data}"

if [ ! -z "$HTML_INDEX_DIR" ] ; then
   if [ -d "$HTML_INDEX_DIR" ] ; then
      MASTER_INDEX_DIR="$HTML_INDEX_DIR"
   fi
fi

# hopefully we have the absolute path now - too bad I can't find a function for this
echo "LINK_DIR=$LINK_DIR  MASTER_INDEX_DIR=$MASTER_INDEX_DIR"

ln -s "$LINK_DIR" "$MASTER_INDEX_DIR"

# do we already have a shortcut to our directory in the main web page?
CurrentDir=`basename "$PWD"`

# if sortorder.txt does not already contain our directory, then add it
FileContainsLine "$MASTER_INDEX_DIR/sortorder.txt" "$CurrentDir"
if [ "$?" -ne 1 ] ; then
   # insert the current directory into the top of the sort order
   # first copy the file to the new file in order to preserve the permissions
   cp -ra  "$MASTER_INDEX_DIR/sortorder.txt" "$MASTER_INDEX_DIR/sortorder.new.txt"
   echo `basename "$PWD"` > "$MASTER_INDEX_DIR/sortorder.new.txt"
   cat "$MASTER_INDEX_DIR/sortorder.txt" >> "$MASTER_INDEX_DIR/sortorder.new.txt"
   mv -f  "$MASTER_INDEX_DIR/sortorder.new.txt" "$MASTER_INDEX_DIR/sortorder.txt"
fi

# re-generate the main index of pictures (with our new page at the top of the list)
mpi "$MASTER_INDEX_DIR"
