# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeDropshadowThumbnails.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/bash

echo "running $0: $*..."

CONVERT=/usr/bin/convert

FONT="arial"

BACKGROUND_COLOR="rgb(215,215,215)"
BACKGROUND_COLOR="rgb(255,255,255)"
SHADOW_GLOW_COLOR="rgb(0,0,0)"
TEXT_COLOR="rgb(255,128,128)"
BLUR_INTENSITY="5"

STRIP_HTML="s/<[^>]*>//g"

input_image="$1"
output_image="$2"
size="$3"
(( "$#" < 3 )) && echo "not enough args, dude!" && exit -2
(( "$size" < 1 )) && echo "bad size, dude: $size" && exit -2

FONT_SIZE="`expr $size \/ 20`"
(( "$FONT_SIZE" > 16 )) && FONT_SIZE=16

SHADOW_SIZE="`expr $size \/ 15`"
(( "$SHADOW_SIZE" < 10 )) && SHADOW_SIZE=10

(( $size > 400 )) && SHADOW_SIZE="`expr $size \/ 30`"
(( $size > 400 )) && BLUR_INTENSITY="7"

SHADOW_INDENT="`expr $SHADOW_SIZE \/ 2`"

[ "$copyright_string" ] || copyright_string="`cat "copyright.txt"`"
[ "$copyright_string" ] || copyright_string="`cat "templates/copyright.html" | sed "$STRIP_HTML"`"
[ "$copyright_string" ] || copyright_string="copyright (c) 2006-2011 by Bryant Hansen"
echo "copyright=$copyright_string"

temp_frame_image="${output_image}.frame.tmp.tiff"
temp_scaled_image="${output_image}.image.tmp.tiff"

set -o verbose
h="`identify -format "%h" "$input_image"`"
w="`identify -format "%w" "$input_image"`"

$CONVERT -resize ${size}x${size}+0+0 "$input_image" "$temp_scaled_image"

h="`identify -format "%h" "$temp_scaled_image"`"
w="`identify -format "%w" "$temp_scaled_image"`"

frameheight="`expr $h + $SHADOW_SIZE`"
framewidth="`expr $w + $SHADOW_SIZE`"

echo "$image: ${h}x${w} becomes ${frameheight}x${framewidth}"

convert \
	-size ${framewidth}x${frameheight} \
	xc:none \
	-fill "$BACKGROUND_COLOR" \
	-draw "rectangle 0,0 ${framewidth},${frameheight}" \
	-fill "$SHADOW_GLOW_COLOR" \
	-gravity "SouthWest" \
	-draw "rectangle ${SHADOW_INDENT},${SHADOW_INDENT} ${w},${h}" \
	-blur "${SHADOW_SIZE},${BLUR_INTENSITY}" \
	-gravity "NorthWest" \
	-draw "image Over 0,0 0,0 \"${temp_scaled_image}\"" \
	-gravity "SouthWest" \
	-font helvetica \
	-pointsize "$FONT_SIZE" \
	-fill "$TEXT_COLOR" \
	-draw "text 7,0 \"$copyright_string\"" \
	"${output_image}"
