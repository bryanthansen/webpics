# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
##################################################################
# MakeImageinfo.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# last updated: 28 November 2006
#
# copyright (c) 2006 by Bryant Hansen
# all rights reserved
##################################################################

USAGE="MakeImageInfo.sh [ -v ] [ -r ] [ -m | --movie-mode ] [ directory ]"
HELP="  -v 	verbose"\
"\n -r	recurse subdirectories, making image info files for every photo"\
"\n -m | --movie-mode	create image info files of movies (avi, mov, mpg, 3gp, or mp4) instead of images"

IDENTIFY="/usr/bin/identify"
IMAGE_INFO_DIR=".image_info"
THUMB_DIR_BASE=".thumbnails"
VERBOSE=""
RECURSE=""

NumInfoFilesCreated=0
NumInfoFilesRemoved=0
NumInfoFiles=0

# some sed commands we'll need here; string them together to do something intelligent, but understandable in source
SED_COMMAND_EXTRACT_ENDING="s/.*\.//g"
SED_COMMAND_REMOVE_ENDING="s/\.[^\.]*$//"
SED_COMMAND_REMOVE_LINK_INFO="s/ -> .*//g"
SED_COMMAND_TO_LOWER_CASE="y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
SED_COMMAND_PRINT_IMAGE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)/p"
	
SED_COMMAND_PRINT_IMAGE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)/p" 	# used with sed -n
SED_COMMAND_PRINT_IF_IMAGE_INFO_TYPE="$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)\.txt$/p" 	# used with sed -n
SED_COMMAND_PRINT_MOVIE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(mov\|avi\|mpi\|3gp\|mp4\)/p" 				# used with sed -n
SED_COMMAND_PRINT_MOVIE_INFO_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(mov\|avi\|mpi\|3gp\|mp4\).txt/p" 				# used with sed -n

WaitForKey()
{
	local tmp
	echo "Press Any Key" 1>&2
	read -n 1 'tmp' <&1
#	read -n 1
	return 0
}

DebugMessage()
{
	local NO_CR_ARG=""
	local SPECIAL_CHAR_ARG=""
	local ARGS=""
	local OPTION_FOUND="0"
	local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "

	while [ "$OPTION_FOUND" ] ; do
		OPTION_FOUND=""
		if [ "$1" = "-e" ] ; then
			OPTION_FOUND="-e"
			ARGS="${ARGS} -e"
			shift
		fi
		if [ "$1" = "-n" ] ; then
			OPTION_FOUND="-n"
			ARGS="${ARGS} -n"
			LOG_RECORD_HEADER=""
			shift
		fi
	done		

	echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

	return 0
}

ProcessDirectory () {

#	local VERBOSE=1

	DebugMessage "Creating Image Information files for pictures in \"$PWD\"..."
	
	(( "$VERBOSE" )) && DebugMessage "searching \"$PWD\" for image files..."
	for dir in * ; do
		# if the file exists in excludes.txt...
		if (("`grep --files-with-matches -e "^[ ^t]*$dir[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
			(( "$VERBOSE" )) && DebugMessage "The file \"$dir\" is in the excludes list."
			local IMAGE_INFO_FILE="${IMAGE_INFO_DIR}/${dir}.txt"
			if [ -d "./$IMAGE_INFO_DIR" ] && [ -f "./$IMAGE_INFO_FILE" ] ; then
				IsPictureFile="`echo "$dir" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
				# if this is a picture file
				if [ $IsPictureFile ] ; then
					DebugMessage "DANGEROUS OPERATION!  (TODO: VERIFY ME!) \"$dir\" is in the excludes list, but an info file exists for it.  Removing info file (./$IMAGE_INFO_FILE)..."
					rm "./$IMAGE_INFO_FILE"
					NumInfoFilesRemoved="`expr "$NumInfoFilesRemoved" + 1`"
				fi
			fi
		else
			if [ -d "$dir" ] ; then   # ==> If it is a directory (-d)...
				if [ -L "$dir" ] ; then   # ==> If directory is a symbolic link...
					(( "$VERBOSE" )) && DebugMessage "symlink: $dir" `ls -l "$dir" | sed 's/^.*'"$dir"' //'`
				fi
				if [ $RECURSE ] ; then
					# if it's not a thumbnail directory or a reserved directory, then we can go into it and process it too
					if [ -z `expr "$dir" : "\($THUMB_DIR_BASE\)"` ] && [ "`basename "$dir"`" != "$TEMPLATE_DIR" ] && [ "`basename "$dir"`" != "$HTML_DIR" ] ; then
						if cd "$dir" ; then              # ==> If can move to subdirectory...
							ProcessDirectory                        # recursively
						fi
					else
						#display thumb dir warning only if we are verbose
						(( "$VERBOSE" )) && DebugMessage "Directory \"$dir\" is an excluded directory!  Skipping processing."
					fi
				fi
			fi
			if [ -f "$dir" ] ; then # or is it a file
				file="$dir"
				IsMovieFile="`echo "$file" | sed -n "$SED_COMMAND_PRINT_MOVIE_TYPE"`"
				if (( $MOVIE_MODE )) && [ $IsMovieFile ] ; then
					# Can we identify a movie file?
					# How should we do so?
					MovieThumb="./${file}.jpg"
				else
					IsPictureFile="`echo "$file" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
					# if this is a picture file
					if [ $IsPictureFile ] ; then
						NumInfoFiles="`expr $NumInfoFiles + 1`"
						[ ! -d "$IMAGE_INFO_DIR" ] && mkdir "$IMAGE_INFO_DIR"
						if [ ! -d "$IMAGE_INFO_DIR" ] ; then
							DebugMessage "ERROR: image info dir \"$IMAGE_INFO_DIR\" does not exist in $PWD.  Failed to create.  Skipping info!"
						else
							local IMAGE_INFO_FILE="${IMAGE_INFO_DIR}/${file}.txt"
							if [ -f "${IMAGE_INFO_FILE}" ] ; then
								if [ "$file" -nt "$IMAGE_INFO_FILE" ] ; then
									DebugMessage "the information for \"$file\" is outdated.  updating \"$IMAGE_INFO_FILE\"..."
									$IDENTIFY -verbose "$file" > "$IMAGE_INFO_FILE"
									NumInfoFilesCreated="`expr "$NumInfoFilesCreated" + 1`"
								else
									(( "$VERBOSE" )) && DebugMessage "the image information for \"$file\" already exists and is up to date."
								fi
							else
								DebugMessage "$PWD/$IMAGE_INFO_FILE"
								$IDENTIFY -verbose "$file" > "$IMAGE_INFO_FILE"
								NumInfoFilesCreated="`expr "$NumInfoFilesCreated" + 1`"
							fi
							EXIF_INFO_FILE="${IMAGE_INFO_FILE/.txt/}.exif.txt"
							if [ -f "${IMAGE_INFO_FILE}" ] && [ ! -f "${EXIF_INFO_FILE}" ] ; then
								# (( "$VERBOSE" )) && 
								DebugMessage "creating EXIF info file \"$EXIF_INFO_FILE\"..."
								cat "${IMAGE_INFO_FILE}" | sed -n "/^[ \t]*[Ee]xif/p" | sed "s/exif\:/Exif\:/" > "${EXIF_INFO_FILE}"
							fi
						fi
					fi
				fi
			fi
			if [ -L "$dir" ] && [ ! -f "$dir" ] && [ ! -d "$dir" ] ; then
				DebugMessage "$dir is a link to an invalid file or a non-file object."
			fi
		fi # if else in excludes.txt
	done
	
#	VERBOSE=1
	if [ -d "$IMAGE_INFO_DIR" ] ; then
		(( "$VERBOSE" )) && DebugMessage -n "   Removing unused image info files..."
		for file in "$IMAGE_INFO_DIR"/* ; do
			IsPictureFile="`echo "$file" | sed -n "$SED_COMMAND_PRINT_IF_IMAGE_INFO_TYPE"`"
			local OriginalImage="`echo "$file" | sed "$SED_COMMAND_REMOVE_ENDING"`"
			[ "$OriginalImage" ] && OriginalImage="`basename "$OriginalImage"`"
			# if this is a picture file
			if [ "$IsPictureFile" ] ; then
				(( "$VERBOSE" )) && DebugMessage "$file is an ImageInfoFile."
				# if there is not a corresponding image in the parent directory...
				if [ ! -f "$OriginalImage" ] ; then
					DebugMessage "DANGEROUS OPERATION (VERIFY ME!): \"$OriginalImage\" no longer exists in \"$PWD\", but the info file does.  Removing \"$file\"..."
					rm "./$file"
					NumInfoFilesRemoved="`expr "$NumInfoFilesRemoved" + 1`"
				fi
			else
				(( "$VERBOSE" )) && DebugMessage "$file is not an ImageInfoFile.  IsPictureFile=$IsPictureFile.  SED_COMMAND=$SED_COMMAND_PRINT_IF_IMAGE_INFO_TYPE"
			fi
		done
		(( "$VERBOSE" )) && DebugMessage "done."
	fi
	DebugMessage "$PWD/$IMAGE_INFO_DIR: $NumInfoFiles ImageInfoFiles, $NumInfoFilesCreated newly created, $NumInfoFilesRemoved removed."
	cd ..   # ==> Up one directory level.
}


######################################################
# Setup Environment
######################################################
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}


######################################################
# - Main -
######################################################

# is there a better way to do this that's non-destructive (unlike shift in a loop)?
if (( $# == 0 )) ; then
  (( "$VERBOSE" )) && DebugMessage "`basename "$0"` $* called"
fi

if [ -z "$1" ] ; then
   shift
fi

until [ -z "$1" ]  # Until all parameters used up...
do
   if [ ! -z `expr "$1" : "\(-\)"` ] ; then
      if [ ! -z `expr "$1" : "\(-r\)"` ] ; then
         RECURSE=1
      fi
      if [ ! -z `expr "$1" : "\(-v\)"` ] ; then
         VERBOSE=1
      fi
      if [ ! -z `expr "$1" : "\(-f\)"` ] ; then
         FORCE_CREATE=1
      fi
      if [ ! -z `expr "$1" : "\(--movie-mode\)"` ] \
      || [ ! -z `expr "$1" : "\(-m\)"` ] ; then
         DebugMessage "Movie mode enabled!"
         MOVIE_MODE=1
      fi   
   fi
   LAST_PARAM=$1
   shift
done
if [ -d "$LAST_PARAM" ] && [ ! -z "$LAST_PARAM" ]; then
   BASE_DIR="$LAST_PARAM"       # ==> Otherwise, move to indicated directory.
else
   BASE_DIR="$PWD"    # ==> No args to script, then use current working directory.
fi
if (( "$VERBOSE" )) ; then
   DebugMessage "Program Options:"
   DebugMessage "   VERBOSE is on."
   DebugMessage "   RECURSE=$RECURSE"
   DebugMessage "   BASE_DIR=$BASE_DIR"
fi
cd "$BASE_DIR"
ProcessDirectory
(( "$VERBOSE" )) && DebugMessage "`basename "$0"` complete.  Exiting normally."
exit 0

