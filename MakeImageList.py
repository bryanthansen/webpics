#!/usr/bin/python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeImageList.py
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# created: 20080925
# last updated: 20080925
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

""" Program MakeImageList.py

This program makes an ordered list of all image files in the directory that should be included in the photo album.  The
list file will not be updated unless it has changed.

Note: there will be no dependency checking in this module.  It would involve checking the entire directory contents,
which would take longer than the list operation that it's intended to do.  The resulting output file should be used
for dependency checking within other modules.

"""

import glob
import os
import fnmatch
import webpics
import binascii

def usage():
	print "USAGE: " + sys.argv[0] + " [-d | --debug]"

infile = "sortorder.txt"
outfile = ".webpics_imagelist.txt"

image_extensions = ["*.[Jj][Pp][Gg]", "*.[Gg][Ii][Ff]", "*.tiff", "*.[Pp][Nn][Gg]", "*.[Bb][Mm][Pp]"]
movie_extensions = ["*.3gp", "*.mov", "*.mpi", "*.avi", "*.mp4", "*.MP4"]

class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg


def extListMatch(name, extList):
	for ext in extList:
		if fnmatch.fnmatch(name, ext):
			return True
	return False


def main(argv=None):

	# first make a list from the sortorder.txt file
	# then continue the list with the remaining images
	outfile_tmp = outfile + "." + binascii.b2a_hex(os.urandom(6)) + ".tmp"
	fout = open(outfile_tmp, 'w')
	if os.path.exists(infile):
		fin = open(infile, 'r')
		try:
			for line in fin:
				pic = line.strip()
				if (os.path.exists(pic)):
					if extListMatch(pic, image_extensions):
						if webpics.isNameInFileList(pic, "excludes.txt"):
							webpics.DebugMessage ("WARNING: the file \"" + pic + "\" is in both the sortorder and the excludes " \
								+ "file.  Excluding file.")
						else:
							try:
								fout.write(pic + "\n")
							except:
								webpics.DebugMessage ("ERROR: cannot write to output file \"" \
									+ os.path.join(os.path.getcwd(), outfile_tmp) + "\"!  Exiting abnormally!")
								return -2
				else:
					webpics.DebugMessage ("WARNING: the file \"" + pic + "\" is in both the sortorder list, but does not " \
						+ "exist!.")
		finally:
			fin.close()

	for ext in image_extensions:
		# TODO: should use fnmatch.filter here instead of glob.glob, since we're using fnmatch above
		for pic in glob.glob(ext):
			if not webpics.isNameInFileList(pic, "excludes.txt"):
				if not webpics.isNameInFileList(pic, "sortorder.txt"):
					try:
						fout.writelines(pic + "\n")
					except:
						webpics.DebugMessage ("ERROR: cannot write to output file \"" \
							+ os.path.join(os.path.getcwd(), outfile_tmp) + "\"!  Exiting abnormally!")
						return -2

	fout.close()

	# if the file has changed, update it
	if not os.path.exists(outfile) or webpics.textdiff(outfile, outfile_tmp):
		# shutil.move(outfile_tmp, outfile)
		# os.rename(outfile_tmp, outfile)
		os.system("cat \"" + outfile_tmp + "\" > \"" + outfile + "\"")
	
	os.remove(outfile_tmp)
	
if __name__=="__main__":
	main()
