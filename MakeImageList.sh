# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeImageList.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

# builds a list of images to display
#   loops through all files in a directory
#   checks if the file is a known image type
#   if so, checks if it's in the excludes list
#   if it's not in the excludes list, it will be added to the images
#   2 loops
#     first with sortorder images
#     next with filename order and without sortorder
#     TODO: make a more effective sorting and/or layout feature (web-based interface?)

USAGE="$0 [ -v ] [ -r ] [ -f ] [ -m | --movie-mode ] [ --size=x ] directory"
HELP="  -v 	verbose"\
"\n -r	recurse subdirectories, making thumbnails for every photo"\
"\n -m | --movie-mode	create thumbnails of movies (avi, mov, mpg, 3gp, or mp4) instead of images"\
"\n -f 	force thumbnail creation, even if the resulting image is larger than the original" 

# set programs
CONVERT="/usr/bin/convert"


THUMB_DIR_BASE=".thumbnails"
THUMB_DIR="${THUMB_DIR_BASE}${THUMB_SIZE}"
VERBOSE=""
FORCE_CREATE=""

NumLinks=0

RESERVED_IMAGE=0
RESERVED_IMAGES="forward.gif backward.gif index.gif"

FILENAME_ARG=""

# some needed SED commands
# some sed commands we'll need here; string them together to do something intelligent, but understandable (hopefully) in source
SED_COMMAND_DELETE_BLANK_LINES="/^[\t ]*$/d"
SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1="/^$/{;N;/^\n$/d;}"

# some sed commands we'll need here; string them together to do something intelligent, but understandable in source
SED_COMMAND_EXTRACT_ENDING="s/.*\.//g"
SED_COMMAND_REMOVE_LINK_INFO="s/ -> .*//g"
SED_COMMAND_TO_LOWER_CASE="y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
SED_COMMAND_PRINT_MOVIE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(mov\|avi\|mpi\|3gp\|mp4\)/p" 				# used with sed -n
SED_COMMAND_IS_CANON_RAW_FMT="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(cr2\)/p" 	# used with sed -n
SED_COMMAND_PRINT_IMAGE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)/p" 	# used with sed -n

. /projects/webpics/CommonSedCommands.sh

DebugMessage()
{
	local NO_CR_ARG=""
	local SPECIAL_CHAR_ARG=""
	local ARGS=""
	local OPTION_FOUND="0"
	local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "

	while [ "$OPTION_FOUND" ] ; do
		OPTION_FOUND=""
		if [ "$1" = "-e" ] ; then
			OPTION_FOUND="-e"
			ARGS="${ARGS} -e"
			shift
		fi
		if [ "$1" = "-n" ] ; then
			OPTION_FOUND="-n"
			ARGS="${ARGS} -n"
			LOG_RECORD_HEADER=""
			shift
		fi
	done		

	echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

	return 0
}

BuildPictureList() 
{
	(( $VERBOSE )) && DebugMessage "template=\"$OBJECT_INDEX_PROTOTYPE\" directory=\"$PWD\"..."

	# TODO: if we're in a thumb dir, we don't have a sortorder.txt, and there's one in the parent dir, then link to it
#	local thumbextract="`expr match "$PWD" : ".*\(${THUMB_DIR}[0-9]*\)"`"
	if [[ "`echo "$PWD" | sed -e "s/${THUMB_DIR}[0-9]*$//"`" != "$PWD" ]] ; then 
	    # we're in a thumb dir
	    DebugMessage "This is a thumbnail directory, there is no sortorder.txt here, but there is one in the parent dir.  It would be a good idea to link to this."
	    [[ ! -f "sortorder.txt" ]] && [[ -f "../sortorder.txt" ]] && ln -s "../sortorder.txt" "./"
	fi
	
	(( $VERBOSE )) && DebugMessage "searching for all image files in \"$PWD\"..."
	
	local NUM_IMAGES_FOUND=0

	# add all of the image sets from sortorder.txt
	if [[ -f "sortorder.txt" ]] ; then
	    # DebugMessage "sortorder.txt found"
		cat "sortorder.txt" | while read "f1" ; do
			if [[ -f "$f1" ]] ; then
				IsPictureFile="`echo "$f1" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
				# if this is a picture file
				if [[ "$IsPictureFile" ]] ; then
					if (("`grep --files-with-matches -e "^[ ^t]*$f1[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
						(( $VERBOSE )) && DebugMessage "The file \"$f1\" is in both the excludes list and the sortorder list.  Excluding file."
					else
						echo "$f1"
						NUM_IMAGES_FOUND=`expr $NUM_IMAGES_FOUND + 1`
					fi
				fi
			fi
		done
	else
	    (( $VERBOSE )) && DebugMessage "sortorder.txt not found"
	fi
	
	# then go through all of the files
	for f1 in * ; do
		if [[ -f "$f1" ]] ; then
			IsPictureFile="`echo "$f1" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
			# if this is a picture file
			if [[ $IsPictureFile ]] ; then
				
				RESERVED_IMAGE=0
				for reserved_image in $RESERVED_IMAGES ; do
				if [[ "$reserved_image" = "$f1" ]] ; then
					RESERVED_IMAGE=1
				fi
				done
	
				local DoesSortOrderContainLine="`grep --files-with-matches -e "^[ ^t]*$f1[ ^t]*$" "sortorder.txt" 2>/dev/null | wc -l`"
				local DoesExcludeFileContainLine="`grep --files-with-matches -e "^[ ^t]*$f1[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`"
				if [[ $DoesExcludeFileContainLine -ge 1 || $DoesSortOrderContainLine -ge 1 ]] ; then
					if [[ $DoesExcludeFileContainLine -ge 1 ]] ; then
						(( $VERBOSE )) && DebugMessage "The file \"$f1\" is in the excludes list."
					fi
					if [[ $DoesSortOrderContainLine -ge 1 ]] ; then
						(( $VERBOSE )) && DebugMessage "The file \"$f1\" is in the sortorder list."
					fi
				else
					IsMovieFile="`echo "${f1%.jpg}" | sed -n "$SED_COMMAND_PRINT_MOVIE_TYPE;$SED_COMMAND_DELETE_BLANK_LINES"`"
					# if this is a movie file
					if [[ "${f1%.jpg}" != "${f1}" ]] && [[ "$IsMovieFile" ]] ; then
						# this matches the pattern of a movie frame
						# movie=some_movie.mov, first frame=some_movie.mov.jpg
						# don't add the info
						(( $VERBOSE )) && DebugMessage -e "$f1 appears to be a movie frame.  not adding to image list."
					else
						if (( $RESERVED_IMAGE == 0 )) ; then
							echo "$f1"
							NUM_IMAGES_FOUND=`expr $NUM_IMAGES_FOUND + 1`
						fi
					fi
				fi
			fi
		fi
	done

	(( "$VERBOSE" )) && DebugMessage -n -e "\n"

	DebugMessage "$PWD: created the ordered list of images.  $NUM_IMAGES_FOUND found."

	# return $NUM_IMAGES_FOUND  ???
}


######################################################
# Setup Environment
######################################################
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}


######################################################
# - Main -
######################################################

# if the VERBOSE arg is NULL, dump it
if [[ -z "$1" ]] ; then
	shift
fi

until [[ -z "$1" ]]  # Until all parameters used up...
do
	if [[ ! -z `expr "$1" : "\(-\)"` ]] ; then
		if [[ ! -z `expr "$1" : "\(-v\)"` ]] ; then
			VERBOSE=1
		fi
	fi
	LAST_PARAM=$1
	shift
done
if [[ ! -z "$LAST_PARAM" ]]; then
	FILENAME_ARG="$LAST_PARAM"       # ==> Otherwise, move to indicated directory.
fi



THUMB_DIR="${THUMB_DIR_BASE}${THUMB_SIZE}"

if (( "$VERBOSE" )) ; then
	DebugMessage -e "\n$0 called"
	DebugMessage "Program Options:"
	DebugMessage "   VERBOSE is on."
	DebugMessage "   FILENAME_ARG=$FILENAME_ARG"
	VERBOSE_ARG="-v"
fi

PICTURE_LIST="$FILENAME_ARG"
PICTURE_LIST_TMP="`mktemp "$PICTURE_LIST.tmp.XXXXXX"`"
BuildPictureList > "$PICTURE_LIST_TMP"
if [[ "`tail -n 1 "$PICTURE_LIST_TMP"`" ]] ; then
   	DebugMessage "There must be a blank line at the end of ${PICTURE_LIST}.  This is required to process 1 last picture in the MakePicturePages(?) script."
	echo "# blank line" >> "$PICTURE_LIST_TMP"
fi

if [[ -f "$PICTURE_LIST" ]] ; then
	if [[ "`diff "$PICTURE_LIST" "$PICTURE_LIST_TMP"`" ]] ; then
		echo "$PICTURE_LIST changed!"
		cat "$PICTURE_LIST_TMP" > "$PICTURE_LIST"
	fi
else
	echo "creating $PICTURE_LIST"
	cat "$PICTURE_LIST_TMP" > "$PICTURE_LIST"
fi

rm -f "$PICTURE_LIST_TMP"

(( "$VERBOSE" )) && DebugMessage "complete, exiting normally."
exit 0

