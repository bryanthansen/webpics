#!/usr/bin/python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeMovieImage.py
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# created: 20080829
# last updated: 20080829
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

""" Program MakeMovieImage.py

This program will extract the first frame from a movie file and create a jpg image from it.
"""

import os
import sys
import getopt
import binascii
import webpics

def usage():
	print "USAGE: ", os.path.basename(sys.argv[0]), " [-d | --debug] [-o output_file | --output=ouput_file] input_file"

mplayer = "mplayer"
infile = ""
outfile = ""
raw_settings1 = " -vo jpeg"
raw_settings2 = " -frames 1" + " -ao null"
conf_file  = ""
force = 0

deps = mplayer
# TODO: determine how to make mplayer put out a file with a specific name (-o does *not* work!)
# mplayer -vo jpeg "$file" -frames 1 -ao null

bash_script_output = 1

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def check_dependencies(deps):
	print "TODO: check deps"


def main(argv=None):

	global deps
	# check_dependencies(deps)

	# some code borrowed from http://docs.python.org/lib/module-getopt.html
	try:
		opts, args = getopt.getopt(sys.argv[1:], "ho:vf", ["help", "outfile=", "force", "debug"])
	except getopt.GetoptError, err:
		# print help information and exit:
 		print str(err) # will print something like "option -a not recognized"
 		usage()
		sys.exit(2)

	infile = "".join(args)

	if infile == "":
		print sys.argv[0], " ERROR: input file not specified.  This program requires at least 1 argument.  Exiting abnormally!"
		usage()
		sys.exit(-3)
		
	if not os.path.exists(infile):
		print sys.argv[0], " ERROR: input file, ", infile, " does not exist!  Exiting abnormally!"
		usage()
		sys.exit(-4)
	
	# build a list of dependencies
	outfile_deps = [ infile ]

	# set default output
	# outfile = infile[:-4] + ".jpg"
	outfile = infile + ".jpg"
	global conf_file
	for opt,arg in opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit(0)
		elif opt in ("-d", "--debug"):
			global _debug
			_debug = 1
		elif opt in ("-f", "--force"):
			global _force
			_force = 1
		elif opt in ("-o", "--outfile"):
			outfile = arg

	mplayer_outfile = "00000001.jpg"
	CCMD = "rm " + mplayer_outfile + " ; "
	CCMD += mplayer + raw_settings1 + " " + infile + raw_settings2 + " > /dev/null"
	CCMD += " && mv " + mplayer_outfile + " " + outfile
	CCMD += " || " + "echo \" ERROR: mplayer output file, " + mplayer_outfile + ", not created!\""

	if os.path.islink(infile):
		handle_link(infile, outfile, conf_file)
	
	if force or not webpics.is_fresh(outfile, outfile_deps):
		webpics.do_safe_command(CCMD, outfile)


if __name__=="__main__":
	main()

