#!/usr/bin/python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeMovieImages.py
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# created: 20080829
# last updated: 20080829
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

""" Program MakeMovieImages.py

This program loops through all movie images in a directory and calls another script to create a jpg image for each of them.
"""

import glob
import os

def usage():
	print "USAGE: " + sys.argv[0] + " [-d | --debug]"

makemovieimage = "/projects/webpics/MakeMovieImage.py"
deps = makemovieimage

movie_extensions = ["*.3[Gg][Pp]", "*.[Mm][Oo][Vv]", "*.[Mm][Pp][Ii]", "*.[Mm][Pp][Gg]", "*.[Aa][Vv][Ii]", "*.[Mm][Pp]4"]

class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg

def check_dependencies(deps):
	print "TODO: check deps"

def main(argv=None):
	for ext in movie_extensions:
		for f in glob.glob(ext):
			# os.spawnlp(os.P_WAIT, convertraw, convertraw, f)
			os.system(makemovieimage + " " + f)

if __name__=="__main__":
	main()

