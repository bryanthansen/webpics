# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) MakePicindex.sh   1.0     14 Dec  2002
#                                update:  4 June 2004
#                         latest update: 18 Okt  2011
#
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         Creates an HTML photo album which will layout pictures and/or image sets on a web page in a tabular form.
#
#<!-- copyright (c) 2002-2014 by Bryant Hansen -->
#<!-- all rights reserved -->

######################################################
# - Constant and variable declarations -
######################################################

USAGE="$0 [ -r ] [ -v ] [ -ns ] [ --template-name=html_filename ] \
[ -o | --output-file FILENAME ] [ DIRECTORY_NAME ]"

# TODO: consider bringing SSI-style comment tags back; object tags have problems (tags within quotes & comments)
#       comment tags could operate outside of the HTML layer
#               Don't make the same mistake as frames

# VERBOSE=""
VERBOSE_ARG=""

CONVERT=convert
BASE_DIR=""
THUMB_DIR_BASENAME=".thumbnails"
DEFAULT_THUMB_SIZE=256
THUMB_SIZE=256
THUMB_DIR="${THUMB_DIR_BASENAME}${DEFAULT_THUMB_SIZE}"
REMOVE_NEW_TEMPLATES=0
#DEFAULT_IMAGE_SIZE=600
DEFAULT_IMAGE_SIZE=1024
IMAGE_SIZE=0
SLIDESHOW=1  # variable to set whether the slide show is generates and a link to it below every picture
SHOWHEADING=1
BACKUP_ORIGINAL=0

# Overwrite template files with the defaults in the main template dir
FRESHEN_TEMPLATE_FILES=""

PICTURE_FILENAME="index.html"

INDEX_PROTOTYPE="index.html"
PICTURE_PROTOTYPE="picture.html"
SLIDE_PROTOTYPE="slide.html"

IMAGE_SUBSET_TABLENAME_TAG="Table:ImageSubsets"
CURRENT_SET_TABLENAME_TAG="Table:CurrentSet"
IMAGE_SET_TABLENAME_TAG="Table:Images"
MOVIE_TABLENAME_TAG="Table:Movies"

IMAGE_SET_FILENAME="ImageSetTable.html"
CURRENT_SET_FILENAME="CurrentSetTable.html"
IMAGE_SUBSET_FILENAME="ImageSubsetTable.html"
MOVIETABLE_FILENAME="MovieTable.html"

COPYRIGHT_FILENAME="copyright.html"

OBJECT_TYPE_TAG="PhotoAlbumTag"

IMAGE_TAG="ThumbnailN"
DESCRIPTION_TAG="DescriptionN"
FILENAME_TAG="FilenameN"
FILESIZE_TAG="FilesizeN"
FILEDATE_TAG="FiledateN"
MOVIE_TAG="MovieN"
MOVIE_THUMBNAIL_TAG="MovieThumbN"
MOVIE_DESCRIPTION_TAG="MovieDescriptionN"
CURRENT_SET_NAME_TAG="CurrentSetName"
CURRENT_SET_DESCRIPTION_TAG="CurrentSetDescription"

IMAGE_SUBSET_TAG="ImageSubsets"
IMAGE_SUBSET_DESCRIPTION="ImageSubsetDescription"
IMAGE_SUBSET_TAG="ImageSubsets"
IMAGE_SUBSET_DESCRIPTION="ImageSubsetDescription"
START_SLIDESHOW_TAG="SlideshowLink"

MOVIE_TAG="Movie"
MOVIE_DESCRIPTION="MovieDescription"

CURRENT_DIRECTORY_TAG="CurrentDirectoryName"

START_SLIDESHOW_TAG="SlideshowLink"

COPYRIGHT_TAG="Copyright"
CURRENT_DIRECTORY_TAG="CurrentDirectoryName"
LINK_PARENT_TAG="parent_url_rel"
SERIES_TAG="Series"

GENERATED_ON_TAG="GENERATION_NOTICE"

RESERVED_IMAGE=0
RESERVED_IMAGES="forward.gif backward.gif heading.jpg heading.gif play_green2.png"

TEMPLATE_DIR="templates"
HTML_DIR="html"
IMAGE_INFO_DIR=".image_info"

# REPLACE_OBJECT_TAGS_COMMAND="/projects/webpics/ReplaceObjectTags.sh"
REPLACE_OBJECT_TAGS_COMMAND="/projects/webpics/ReplaceObjectTags2.pl"

# some needed SED commands
# some sed commands we'll need here; string them together to do something intelligent, but understandable in source
SED_COMMAND_STRIP_HTML_COMMENTS="/<!--/!b;:a;/-->/!{N;ba};s/<!--.*-->//"
SED_COMMAND_DELETE_BLANK_LINES="/^[\t ]*$/d"
SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1="/^$/{;N;/^\n$/d;}"

SED_COMMAND_EXTRACT_ENDING="s/.*\.//g"
SED_COMMAND_REMOVE_LINK_INFO="s/ -> .*//g"
SED_COMMAND_TO_LOWER_CASE="y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
SED_COMMAND_PRINT_IMAGE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)/p"  # used with sed -n
SED_COMMAND_PRINT_MOVIE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(mov\|avi\|mpi\|3gp\|mp4\)/p"                 # used with sed -n


######################################################
# - Local Functions -
######################################################

WaitForKey()
{
    echo "Press Any Key"
    read -n 1
    return 0
}

DebugMessage()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "

    while [ "$OPTION_FOUND" ] ; do
        OPTION_FOUND=""
        if [ "$1" = "-e" ] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [ "$1" = "-n" ] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}

InvalidCharacterError()
{
    DebugMessage "${1}: Invalid Character in filename \"${2}\"!  Exiting abnormally."
    exit -1
}

FixThumbDirs()
{
    # FixThumbDirs: if there is already a directory that's not hidden (old style)
    #

    # if we're using hidden directories now
    if [ "${THUMB_DIR_BASENAME:0:1}" = "." ] ; then
        # but the non-hidden directory exists and the hidden one does not
        if [ ! -f "$1" ] && [ -f "{$1:1}" ] ; then
            mv "{$1:1}" "$1"
        fi
    fi
    DebugMessage "Exit FixThumbDirs()"
    #WaitForKey
    return 0
}

CreateBackupOfFile()
{
        local USAGE="CreateBackupOfFile [ -v ] FILENAME"
        local FILENAME="$1"
        DebugMessage -n "backing up $PWD/$FILENAME to $FILENAME.`date +%Y%m%d`..."
        if [ -f "$FILENAME" ] ; then
                if (( "$VERBOSE" )) ; then
                        DebugMessage "backing up $PWD/$FILENAME to $FILENAME.`date +%Y%m%d`"
                fi
                cp "$PWD/$FILENAME" "$FILENAME.`date +%Y%m%d`"
        DebugMessage "backup done."
        return 1
    fi
        return 0
}

PrintDirectoryDescription()
{
        local VERBOSE=""
        local VERBOSE_ARG=""
        if [ "$1" == "-v" ] ; then
                VERBOSE=1
                shift # discard the VERBOSE argument and continue
        fi
        VERBOSE=1
        (( "$VERBOSE" )) && VERBOSE_ARG="-v"

        d="$1"
        [[ ! "$d" ]] && d=.
        local CURRENT_DIRECTORY_NAME="$(basename "$(readlink -f "$d")")"
        if [[ ! "$CURRENT_DIRECTORY_NAME" ]] ; then
            DebugMessage "PrintDirectoryDescription ERROR: no directory found.  Exiting abnormally."
            return 1
        fi

        local DirectoryDescriptionFilename=""
        if [ -f "$CURRENT_DIRECTORY_NAME.txt" ] ; then
                DirectoryDescriptionFilename="$CURRENT_DIRECTORY_NAME.txt"
                (( "$VERBOSE" )) && DebugMessage "   found description for current image set (\"$CURRENT_DIRECTORY_NAME\") at: $DirectoryDescriptionFilename..."
        elif [ -f "$CURRENT_DIRECTORY_NAME"/"$CURRENT_DIRECTORY_NAME.txt" ] ; then
                        DirectoryDescriptionFilename="$CURRENT_DIRECTORY_NAME"/"$CURRENT_DIRECTORY_NAME.txt"
                        DebugMessage "   found description for current image set (\"$CURRENT_DIRECTORY_NAME\") at: $DirectoryDescriptionFilename..."
        else
                local numImages=0
                [ -f "$CURRENT_DIRECTORY_NAME"/".webpics_imagelist.txt" ] \
                        && numImages="`cat "$CURRENT_DIRECTORY_NAME"/.webpics_imagelist.txt | sed "$SED_COMMAND_DELETE_BLANK_LINES" | wc -l`"
                local numSubsets=0
                [ -f "$CURRENT_DIRECTORY_NAME"/".webpics_dirlist.txt" ]   \
                        && numSubsets="`cat "$CURRENT_DIRECTORY_NAME"/.webpics_dirlist.txt | sed "$SED_COMMAND_DELETE_BLANK_LINES" | wc -l`"
                local numMovies=0
                [ -f "$CURRENT_DIRECTORY_NAME"/".webpics_movielist.txt" ] \
                        && numMovies="`cat "$CURRENT_DIRECTORY_NAME"/.webpics_movielist.txt | sed "$SED_COMMAND_DELETE_BLANK_LINES" | wc -l`"

                (( "$numImages" > 0 )) && echo -n "$numImages image" && (( "$numImages" != 1 )) && echo "s"
                (( "$numImages" > 0 )) && (( "$numSubsets" > 0 )) && (( "$numMovies" > 0 )) && echo ", " || echo " "
                (( "$numImages" > 0 )) && (( "$numSubsets" > 0 )) && (( "$numMovies" < 1 )) && echo "and "
                (( "$numSubsets" > 0 )) && echo -n "$numSubsets image subset" && (( "$numSubsets" != 1 )) && echo "s " || echo " "
                (( "$numImages" > 0 )) || (( "$numSubsets" > 0 )) && (( "$numMovies" > 0 )) && echo "and "
                (( "$numMovies" > 0 )) && echo -n "$numMovies movie" && (( "$numMovies" != 1 )) && echo "s" || echo " "

                echo ""
                (( "$VERBOSE" )) && DebugMessage "no description found for \"$CURRENT_DIRECTORY_NAME\"."
        fi
        [ -f "$DirectoryDescriptionFilename" ] && cat "${DirectoryDescriptionFilename}"
}

BuildCurrentSetTable()
{
        USAGE="BuildCurrentSetTable [ -v ] TODO"

        local VERBOSE=""
        local VERBOSE_ARG=""
        if [ "$1" == "-v" ] ; then
                VERBOSE=1
                shift # discard the VERBOSE argument and continue
        fi
        # VERBOSE=1
        (( "$VERBOSE" )) && VERBOSE_ARG="-v"

        if [ -f "${TEMPLATE_DIR}/${CURRENT_SET_FILENAME}" ] ; then
                CurrentSetData="`cat "${TEMPLATE_DIR}/${CURRENT_SET_FILENAME}" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`"
        else
                DebugMessage "ERROR: CurrentSetTableTemplate file: \"${CURRENT_SET_FILENAME}\" not found"
        fi
        (( "$VERBOSE" )) && DebugMessage -e "Enter BuildCurrentSetTable().  CurrentSetData=\n${CurrentSetData}"
        local CURRENT_DIRECTORY_NAME=`basename "$PWD"`
        DirectoryDescriptionStr="$(PrintDirectoryDescription .)"
        DebugMessage "DirectoryDescriptionStr=$DirectoryDescriptionStr"
        # TODO: replace this horrible implementation of REPLACE_OBJECT_TAGS_COMMAND!!!
        CurrentSetData="`${REPLACE_OBJECT_TAGS_COMMAND} "${CurrentSetData}" "${CURRENT_SET_TAG}"                                "${CURRENT_DIRECTORY_NAME}"`"
        CurrentSetData="`${REPLACE_OBJECT_TAGS_COMMAND} "${CurrentSetData}" "${CURRENT_SET_DESCRIPTION_TAG}"    "${DirectoryDescriptionStr}"`"
        if (( "$VERBOSE" )) ; then
                DebugMessage -e "Exit BuildCurrentSetTable().  CurrentSetData=\n${CurrentSetData}"
                #WaitForKey
        fi
}

AppendImageSubsetInformation()
{
        USAGE="AppendImageSubsetInformation [ -v ] TODO"

        local VERBOSE=""
        local VERBOSE_ARG=""
        if [ "$1" == "-v" ] ; then
                VERBOSE=1
                shift # discard the VERBOSE argument and continue
        fi
        #VERBOSE=1
        (( "$VERBOSE" )) && VERBOSE_ARG="-v"

        if [[ -f "${TEMPLATE_DIR}/${IMAGE_SUBSET_FILENAME}" ]] ; then
                IMAGE_SUBSET_TABLE="$(
                    cat "${TEMPLATE_DIR}/${IMAGE_SUBSET_FILENAME}" | \
                    sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | \
                    sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"
                )"
        else
                DebugMessage "ERROR: ImageSubsetTableTemplate file: \"${IMAGE_SUBSET_FILENAME}\" not found"
        fi
        (( "$VERBOSE" )) && DebugMessage -e "Enter AppendImageSubsetInformation(arg=$1).  ImageSubsetData=\n${ImageSubsetData}"

        if [[ "$ImageSubsetData" == "${ImageSubsetData#*$IMAGE_TAG*}" ]] ; then
                (( "$VERBOSE" )) && DebugMessage "   adding new row / rowset to image table..."
                ImageSubsetData="${ImageSubsetData} $IMAGE_SUBSET_TABLE"
        fi

        local dir="`basename "$1"`"
        local HtmlImageSubsetLink="<A HREF=\"./${dir}/\">$dir</A>"
        ImageSubsetData="$(
                            ${REPLACE_OBJECT_TAGS_COMMAND} \
                                "${ImageSubsetData}" \
                                "${IMAGE_SUBSET_TAG}" \
                                "${HtmlImageSubsetLink}"
                        )"

        local HtmlImageSubsetDescription="$(PrintDirectoryDescription "${1}")"
        ImageSubsetData="$(
                            ${REPLACE_OBJECT_TAGS_COMMAND} \
                                "${ImageSubsetData}" \
                                "${IMAGE_SUBSET_DESCRIPTION}" \
                                "${HtmlImageSubsetDescription}"
                        )"

        echo "$dir" >> ".webpics_dirlist.txt"

        (( "$VERBOSE" )) && DebugMessage -e "Exit AppendImageSubsetInformation().  ImageSubsetData=\n\n${ImageSubsetData}\n\n"
        (( "$VERBOSE" )) && (( $interactive )) && WaitForKey
}

BuildImageSubsetTable()
{
    local VERBOSE=""
    local VERBOSE_ARG=""
    if [ "$1" == "-v" ] ; then
            VERBOSE=1
            shift # discard the VERBOSE argument and continue
    fi
    #VERBOSE=1
    (( "$VERBOSE" )) && VERBOSE_ARG="-v"

    local ImageSubsetTableHeader="${TEMPLATE_DIR}/${IMAGE_SUBSET_FILENAME/%.html/}_header.html"
    if [ -f "$ImageSubsetTableHeader" ] ; then
            ImageSubsetData="`cat "$ImageSubsetTableHeader" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`"
            (( "$VERBOSE" )) && DebugMessage -e "added: \n\n`cat "$ImageSubsetTableHeader" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`\n\n to the ImageSubsetTable at the header."
    else
            DebugMessage "$ImageSubsetTableHeader does not exist!"
    fi

    (( "$VERBOSE" )) && DebugMessage "BuildImageSubsetTable for \"$PWD\"..."
    # add all of the image sets from sortorder.txt
    if [[ -f "sortorder.txt" ]] ; then
        while read "dir" ; do
            if [ -d "$dir" ] ; then
                if [[ -L "$dir" ]] ; then   # ==> If directory is a symbolic link...
                    (( "$VERBOSE" )) && DebugMessage "Directory \"$dir\" is a symlink to `ls -l "$dir" | sed 's/^.*'"$dir"' //'`"
                fi
                # if we've somehow put a reserved directory in the sortorder, what do we do?
                #    reserved = thumb dirs, template dir, or html dir
                if [ "`expr match "$dir" "\($THUMB_DIR_BASENAME\)"`" ] || [ "`basename "$dir"`" = "$TEMPLATE_DIR" ] || [ "`basename "$dir"`" = "$HTML_DIR" ] ; then
                    DebugMessage "ERROR: the \"$PWD/sortorder.txt\" contains a reserved directory: \"$dir\".  Excluding file."
                    DebugMessage "thumbextract=$thumbextract, THUMB_DIR_BASENAME=$THUMB_DIR_BASENAME, basename(dir)=`basename "$dir"`, TEMPLATE_DIR=$TEMPLATE_DIR, HTML_DIR=$HTML_DIR"
                else
                    # use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
                    if (("`grep -e "^[ ^t]*$dir[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
                        (( "$VERBOSE" )) && DebugMessage "The directory \"$dir\" is in both the excludes list and the sortorder list.  Excluding file."
                    else
                        AppendImageSubsetInformation "$PWD/$dir"
                    fi
                fi
            fi
        done <"sortorder.txt"
    fi

    # add all of the rest of the images (those that were not in the sortorder.txt file)
    (( "$VERBOSE" )) && DebugMessage "searching for all subdirectories in \"$PWD\"..."
    for dir in * ; do
        if [ -d "$dir" ] ; then
            if [[ -L "$dir" ]] ; then   # ==> If directory is a symbolic link...
                (( "$VERBOSE" )) && DebugMessage "symlink: $dir" `ls -l "$dir" | sed 's/^.*'"$dir"' //'`
            fi
            local DoesSortOrderContainLine="`grep -e "^[ ^t]*$dir[ ^t]*$" "sortorder.txt" 2>/dev/null | wc -l`"
            local DoesExcludeFileContainLine="`grep -e "^[ ^t]*$dir[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`"
            if [[ "$DoesExcludeFileContainLine" -ge 1 || $DoesSortOrderContainLine -ge 1 ]] ; then
                if (( $DoesExcludeFileContainLine >= 1 )) ; then
                    (( "$VERBOSE" )) && DebugMessage "The directory \"$dir\" is in the excludes list."
                fi
            else
                # we don't want to descend into thumbnail directories
                thumbextract=`expr "$dir" : "\($THUMB_DIR_BASENAME\)"`
                if [[ ! "$thumbextract" ]] && [[ ! "`basename "$dir"`" = "$TEMPLATE_DIR" ]] && [[ ! "`basename "$dir"`" = "$HTML_DIR" ]] ; then
                    AppendImageSubsetInformation "$PWD/$dir"
                else
                    if [[ "$thumbextract" ]] ; then
                        (( "$VERBOSE" )) && DebugMessage "Thumb dir found: \"$dir\"."
                        if [ -f "./sortorder.txt" ] ; then
                            cp -f "./sortorder.txt" "./$dir/sortorder.txt"
                        fi
                        THUMBDIRS="$THUMBDIRS $dir"
                    fi
                fi
            fi
        fi
    done
    local ImageSubsetTableFooter="${TEMPLATE_DIR}/${IMAGE_SUBSET_FILENAME/%.html/}_footer.html"
    if (( `cat ".webpics_dirlist.txt" | sed "$SED_COMMAND_DELETE_BLANK_LINES" | wc -l` )) ; then
        if [[ -f "$ImageSubsetTableFooter" ]] ; then
            ImageSubsetData="${ImageSubsetData}  `cat "$ImageSubsetTableFooter" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`"
        else
            DebugMessage "$ImageSubsetTableFooter does not exist!"
        fi
    else
        (( "$VERBOSE" )) && DebugMessage "No subsets in the current directory!  Wiping out ImageSubsetData!"
        ImageSubsetData=""
    fi
    (( "$VERBOSE" )) && DebugMessage "Exit BuildImageSubsetTable(THUMBDIRS=$THUMBDIRS)"
}

AppendImageInformation()
{

    # TODO: optimize this to not do search & replaces on the entire structure!  Exponentially slows as records are added.
    # Note: This function modifies the global variable "CURRENT_TABLE"; consider making this a filter

    local VERBOSE=""
    local VERBOSE_ARG=""
    if [[ "$1" == "-v" ]] ; then
        VERBOSE=1
        shift # discard the VERBOSE argument and continue
    fi
    (( "$VERBOSE" )) && VERBOSE_ARG="-v"

    local ImageFilename="$1"
    (( "$VERBOSE" )) && DebugMessage "Enter AppendImageInformation.  ImageFilename=\"$ImageFilename\""
    (( "$VERBOSE" )) && [ $interactive ] && WaitForKey
    if [ -f "$ImageFilename" ] ; then
        # TODO: sanity-check image filename
        #               can't use anything with % or & chars
        if [[ "${ImageFilename}" != "${ImageFilename/%/}" ]] \
        || [[ "${ImageFilename}" != "${ImageFilename/&/}" ]] ; then
            DebugMessage "AppendImageInformation: Invalid Character in filename \"${ImageFilename}\"!  Exiting abnormally."
            exit -1
        else
            (( "$VERBOSE" )) && DebugMessage "AppendImageInformation: \"`/projects/webpics/EscapeSpecialCharsHtml.pl "${ImageFilename}"`\": filename ok."
        fi
        # if this is a picture file
        if [ "`echo "$ImageFilename" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`" ] ; then
            RESERVED_IMAGE=0
            for reserved_image in $RESERVED_IMAGES ;  do
                if [[ "$reserved_image" = "$ImageFilename" ]] ; then
                    RESERVED_IMAGE=1
                fi
                if (("`grep --files-with-matches -e "^[ ^t]*$ImageFilename[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
                    RESERVED_IMAGE=1
                fi
            done
            if [ $RESERVED_IMAGE -eq 0 ] ; then
                IMAGE_FOUND=`expr $IMAGE_FOUND + 1`  # ==> Increment image count.
                #insure that we have an appropriate thumbnail subdirectory first
                if [[ ! -d "./$THUMB_DIR" ]] ; then
                    DebugMessage "ERROR: thumbnail directory: \"./$THUMB_DIR\" does not exist!  pwd=\"$PWD\".  Exiting..."
                    ERROR_NO_THUMB_DIR=102
                    exit $ERROR_NO_THUMB_DIR
                fi
                if [[ ! -d "./$IMAGE_DIR_NAME" ]] ; then
                    DebugMessage "ERROR: default image directory: \"./$IMAGE_DIR_NAME\" does not exist!  pwd=\"$PWD\".  Exiting..."
                    ERROR_NO_IMAGE_DIR=103
                    exit $ERROR_NO_IMAGE_DIR
                fi

                if [[ "$CURRENT_TABLE" = "${CURRENT_TABLE#*$IMAGE_TAG*}" ]] ; then
                    (( "$VERBOSE" )) && DebugMessage "adding new row / rowset to the processed image table..."
                    PROCESSED_IMAGE_TABLE="${PROCESSED_IMAGE_TABLE} $CURRENT_TABLE"
                    CURRENT_TABLE="$ImageData"
                fi

                local PICTURE_LINK="${PICTURE_PROTOTYPE/%.html/}_$ImageFilename"
                if [[ -f "$IMAGE_DIR_NAME/$HTML_DIR/$PICTURE_LINK.html" ]] ; then
                    PICTURE_LINK="$IMAGE_DIR_NAME/$HTML_DIR/$PICTURE_LINK.html"
                else
                    if [[ -f "$HTML_DIR/$ImageFilename.html" ]] ; then
                        PICTURE_LINK="$HTML_DIR/$ImageFilename.html"
                    else
                        DebugMessage "AppendImageInformation ERROR: page cannot be found: \"$PWD/$HTML_DIR/$ImageFilename.html\".  PICTURE_PROTOTYPE=$PICTURE_PROTOTYPE. PICTURE_LINK=$PICTURE_LINK.  Exiting abnormally."
                    fi
                fi

                local SLIDE_LINK="${SLIDE_PROTOTYPE/%.html/}_${ImageFilename}"
                if [[ -f "$IMAGE_DIR_NAME/$HTML_DIR/$SLIDE_LINK.html" ]] ; then
                    SLIDE_LINK="$IMAGE_DIR_NAME/$HTML_DIR/$SLIDE_LINK.html"
                else
                    if [[ -f "$HTML_DIR/$ImageFilename.html" ]] ; then
                        SLIDE_LINK="$HTML_DIR/$ImageFilename.html"
                    else
                        DebugMessage "No slide link is available for $ImageFilename.  Searched in ./$IMAGE_DIR_NAME/$HTML_DIR and ./$HTML_DIR"
                    fi
                fi
                if [ "${SlideshowLink}" = "" ] ; then
                    SlideshowLink="${SLIDE_LINK}" # The first image in the series will be considered the slideshow link
                    # (( "$VERBOSE" )) && \
                        DebugMessage "Slideshow pic for index is $SlideshowLink"
                fi

                (( "$VERBOSE" )) && DebugMessage -e "CURRENT_TABLE=\n\n${CURRENT_TABLE}\n\n"

                local HTML_PICTURE_LINK="<a HREF=\"./${PICTURE_LINK}\"><img class="picture_link" src=\"./${THUMB_DIR}/${ImageFilename}\" alt=\"./${THUMB_DIR}/${ImageFilename}\"></a>"
                local HTML_FILENAME_LINK="<a HREF=\"./${PICTURE_LINK}\">`basename "${ImageFilename}"`</a>"
                # replace the image tag with an html image link

                # TODO: stop doing search & replaces on the whole table; use a working row; this becomes exponentially slower with big tables
                CURRENT_TABLE="`${REPLACE_OBJECT_TAGS_COMMAND}  "${CURRENT_TABLE}"      "${IMAGE_TAG}"                  "${HTML_PICTURE_LINK}"  `"
                CURRENT_TABLE="`${REPLACE_OBJECT_TAGS_COMMAND}  "${CURRENT_TABLE}"      "${FILENAME_TAG}"               "${HTML_FILENAME_LINK}" `"

                if [[ -f "${ImageFilename}.txt" ]] ; then
                    local ImageDescription="`cat "${ImageFilename}.txt"`"
                    CURRENT_TABLE="`${REPLACE_OBJECT_TAGS_COMMAND} "${CURRENT_TABLE}"       "${DESCRIPTION_TAG}"    "${ImageDescription}"`"
                    DebugMessage "\"${ImageFilename}\" description: \"${ImageDescription}\""
                    (( "$VERBOSE" )) && DebugMessage -e "CURRENT_TABLE=${CURRENT_TABLE}"
                    (( "$VERBOSE" )) && WaitForKey
                else
                    CURRENT_TABLE="`${REPLACE_OBJECT_TAGS_COMMAND} "${CURRENT_TABLE}"       "${DESCRIPTION_TAG}"    ""`"
                fi
                echo "$ImageFilename" >> ".webpics_imagelist.txt"
            fi
        fi
    fi
    (( "$VERBOSE" )) && DebugMessage "Exit AppendImageInformation()" || DebugMessage -n -e "."
    (( "$VERBOSE" )) && WaitForKey
}

BuildImageTable()
{
    local VERBOSE=""
    local VERBOSE_ARG=""
    if [ "$1" == "-v" ] ; then
        VERBOSE=1
        shift # discard the VERBOSE argument and continue
    fi
    # VERBOSE=1
    (( "$VERBOSE" )) && VERBOSE_ARG="-v"

    (( "$VERBOSE" )) && DebugMessage "Enter BuildImageTable: searching for all image files in \"$PWD\""
    (( "$VERBOSE" )) && [ $interactive ] && WaitForKey

    IMAGE_FOUND=0
    CURRENT_TABLE=""
    PROCESSED_IMAGE_TABLE=""

    ImageData="" # not local!
    local ImageTableHeader="${TEMPLATE_DIR}/${IMAGE_SET_FILENAME/%.html/}_header.html"
    if [ -f "$ImageTableHeader" ] ; then
        CURRENT_TABLE="`cat "$ImageTableHeader" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`"
        (( "$VERBOSE" )) && DebugMessage -e "added: \n\n`cat "$ImageTableHeader" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`\n\n to the ImageTable at the header."
    else
        DebugMessage "$ImageTableHeader does not exist."
    fi
    if [ -f "${TEMPLATE_DIR}/${IMAGE_SET_FILENAME}" ] ; then
        ImageData="`cat "${TEMPLATE_DIR}/${IMAGE_SET_FILENAME}" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`" # working
    else
        DebugMessage "ERROR: ImageTableTemplate file: \"${IMAGE_SET_FILENAME}\" not found.  Exiting abnormally!"
        exit -1
    fi

    # add all of the image sets from sortorder.txt
    # however, exludes.txt will override what is in sortorder.txt
    if [ -f "sortorder.txt" ] ; then
        exec 3<> "sortorder.txt"  # Open "sortorder.txt" and assign fd 3 to it.
        (( "$VERBOSE" )) && DebugMessage -e "BuildImageTable: entering loop..."
        (( "$VERBOSE" )) && [ $interactive ] && WaitForKey
        while read 'SortOrderItem' ; do
            if [ -f "${SortOrderItem}" ] ; then
                f1="${SortOrderItem}"
                if [[ -L "$f1" ]] ; then
                    (( "$VERBOSE" )) && DebugMessage "symlink: $f1" `ls -l "$f1" | sed 's/^.*'"$f1"' //'`
                fi
                # use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
                if (("`grep -e "^[ ^t]*$f1[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
                    (( "$VERBOSE" )) && DebugMessage "The file \"$f1\" is in both the excludes list and the sortorder list.  Excluding file."
                else
                    (( "$VERBOSE" )) && DebugMessage -e "Calling AppendImageInformation with $f1"
                    (( "$VERBOSE" )) && [ $interactive ] && WaitForKey
                    AppendImageInformation "$f1"
                fi
            fi
        done <&3
        (( "$VERBOSE" )) && DebugMessage "BuildImageTable: exited loop."
        (( "$VERBOSE" )) && [ $interactive ] && WaitForKey
        exec 3>&-                 # Close fd 3.
    fi

    # then go through all of the files
    for f1 in * ; do
        if [[ -f "$f1" ]] ; then
            if [[ -L "$f1" ]] ; then
                (( "$VERBOSE" )) && DebugMessage "symlink: $f1" `ls -l "$f1" | sed 's/^.*'"$f1"' //'`
            fi
            # use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
            local DoesSortOrderContainLine="`grep -e "^[ ^t]*$f1[ ^t]*$" "sortorder.txt" 2>/dev/null | wc -l`"
            # use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
            local DoesExcludeFileContainLine="`grep -e "^[ ^t]*$f1[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`"
            if [[ $DoesExcludeFileContainLine -ge 1 || $DoesSortOrderContainLine -ge 1 ]] ; then
                if ((${DoesExcludeFileContainLine} >= 1 )) ; then
                    (( "$VERBOSE" )) && DebugMessage "The file \"$f1\" is in the excludes list."
                fi
            else
                # check if the basename of the thumbnail matches a known movie type
                # if [[ "${f1%.jpg}" != "${f1}" ]] && [[ -f "${f1%.jpg}" ]] ; then
                IsMovieFile="`echo "${f1%.jpg}" | sed -n "$SED_COMMAND_PRINT_MOVIE_TYPE;$SED_COMMAND_DELETE_BLANK_LINES"`"
                if [[ "$IsMovieFile" ]] ; then
                    # this matches the pattern of a movie frame
                    # movie=some_movie.mov, first frame=some_movie.mov.jpg
                    # don't add the info
                    DebugMessage -e "$f1 appears to be a movie frame.  not adding to image list."

                    # what the hell do we do now!?!? we need to add raw image information that matches this pattern, but not movie information
                else
                    if (( "$VERBOSE" )) ; then
                        DebugMessage -e "Calling AppendImageInformation with $f1"
                        WaitForKey
                    fi
                    AppendImageInformation "$f1"
                fi
            fi
        fi
    done

    # get rid of the rest of the unused filename and image tags
    CURRENT_TABLE="`${REPLACE_OBJECT_TAGS_COMMAND} -g       "${CURRENT_TABLE}"      "${IMAGE_TAG}"                  ""                                      `"
    CURRENT_TABLE="`${REPLACE_OBJECT_TAGS_COMMAND} -g       "${CURRENT_TABLE}"      "${FILENAME_TAG}"               ""                                      `"
    CURRENT_TABLE="`${REPLACE_OBJECT_TAGS_COMMAND} -g       "${CURRENT_TABLE}"      "${DESCRIPTION_TAG}"            ""                                      `"

    (( "$VERBOSE" )) && DebugMessage "adding the remaining portion of the current row/table to the processed image table..."
    PROCESSED_IMAGE_TABLE="${PROCESSED_IMAGE_TABLE} $CURRENT_TABLE"

    ImageData="${PROCESSED_IMAGE_TABLE}"

    local ImageTableFooter="${TEMPLATE_DIR}/${IMAGE_SET_FILENAME/%.html/}_footer.html"
    if (( `cat ".webpics_imagelist.txt" | sed "$SED_COMMAND_DELETE_BLANK_LINES" | wc -l` )) ; then
        if [[ -f "$ImageTableFooter" ]] ; then
            ImageData="${ImageData}  `cat "$ImageTableFooter" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`"
        else
            DebugMessage "$ImageTableFooter does not exist!"
        fi
    else
        ImageData=""
    fi
}

AppendMovieInformation()
{
    USAGE="AppendMovieInformation [ -v ] TODO"

    local VERBOSE
    local VERBOSE_ARG=""
    if [ "$1" == "-v" ] ; then
        VERBOSE=1
        shift # discard the VERBOSE argument and continue
    fi
    #VERBOSE=1
    (( "$VERBOSE" )) && VERBOSE_ARG="-v"
    (( "$VERBOSE" )) && DebugMessage -e "Enter AppendMovieInformation().  MovieData=\n${MovieData}"

    if [ "$MovieData" = "${MovieData#*id=\"$MOVIE_TAG\"*}" ] ; then
        (( "$VERBOSE" )) && DebugMessage "adding new row / rowset to movie table..."
        MovieData="${MovieData} $MOVIE_TABLE"
    fi
    local thumbnail="${THUMB_DIR}/${file1}.jpg"
    local thumbnailhtml=""
    if [[ -f "$thumbnail" ]] ; then
        thumbnailhtml="<img src='${thumbnail}' /><br />"
        (( "$VERBOSE" )) && DebugMessage "thumbnail found at ${thumbnail}.  setting thumbnailhtml to $thumbnailhtml"
    else
        (( "$VERBOSE" )) && DebugMessage "no thumbnail found at ${thumbnail}.  PWD=$PWD"
    fi
    local file1="$1"
    local HtmlMovieLink
    local HtmlMovieDescription
    HtmlMovieLink="<a href=\"./${file1}\">${thumbnailhtml}${file1}</a>"
    MovieData="`${REPLACE_OBJECT_TAGS_COMMAND} "${MovieData}" "${MOVIE_TAG}" "${HtmlMovieLink}"`"

    HtmlMovieThumbnailLink="<a href=\"./${file1}\"><img class="movie_link" src=\"${THUMB_DIR}/${file1}.jpg\" alt=\"${file1}.jpg\"></a>"

    MovieData="`${REPLACE_OBJECT_TAGS_COMMAND} "${MovieData}" "${MOVIE_THUMBNAIL_TAG}" "$HtmlMovieThumbnailLink"`"

    MOVIE_DESCRIPTION_FILENAME="${file1}.txt"
    local HtmlMovieDescription
    if [ -f "$MOVIE_DESCRIPTION_FILENAME" ] ; then
        (( "$VERBOSE" )) && DebugMessage "found text comment for movie \"$file1\".  Adding to output..."
        HtmlMovieDescription="`cat "${MOVIE_DESCRIPTION_FILENAME}"`"
    else
        local f2="`readlink -f "$file1"`"
        HtmlMovieDescription="size: `ls -lh "$f2" | awk '{ print $5 }'`"
    fi

    (( "$VERBOSE" )) && DebugMessage "AppendMovieInformation: replacing movie tags \"$MOVIE_DESCRIPTION_TAG\" with $HtmlMovieDescription..."
    MovieData="`${REPLACE_OBJECT_TAGS_COMMAND} "${MovieData}" "${MOVIE_DESCRIPTION_TAG}" "${HtmlMovieDescription}"`"

    echo "$file1" >> ".webpics_movielist.txt"

    (( "$VERBOSE" )) && DebugMessage -e "Exit AppendMovieInformation().  MovieData=\n\n${MovieData}\n\n"
    (( "$VERBOSE" )) && (( "$interactive" )) && WaitForKey

}

BuildMovieTable()
{
    local dir="."
    THUMB_DIR="${THUMB_DIR_BASENAME}600"

    #local VERBOSE=1
    (( "$VERBOSE" )) && DebugMessage "calling BuildMovieTable on directory \"$dir\"..."
    local MovietableHeader="${TEMPLATE_DIR}/${MOVIETABLE_FILENAME/%.html/}_header.html"
    if [ -f "$MovietableHeader" ] ; then
        MovieData="`cat "$MovietableHeader" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`"
    fi
    if [ -f "${TEMPLATE_DIR}/${MOVIETABLE_FILENAME}" ] ; then
        MOVIE_TABLE="${MOVIE_TABLE} `cat "${TEMPLATE_DIR}/${MOVIETABLE_FILENAME}" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`"
    else
        DebugMessage "ERROR: MovieTableTemplate file: \"${MOVIETABLE_FILENAME}\" not found"
    fi
    # add all of the image sets from sortorder.txt
    if [ -f "sortorder.txt" ] ; then
        while read "file1" ; do
            if [ -f "$file1" ] ; then
                IsMovieFile="`echo "$file1" | sed -n "$SED_COMMAND_PRINT_MOVIE_TYPE;$SED_COMMAND_DELETE_BLANK_LINES"`"
                # if this is a movie file
                if [[ "$IsMovieFile" ]] ; then
                    (( "$VERBOSE" )) && DebugMessage -e "BuildMovieTable: filetype of \"file1\"=\"$IsMovieFile\""
                    if [[ -L "$file1" ]] ; then   # ==> If directory is a symbolic link...
                        (( "$VERBOSE" )) && DebugMessage "symlink: $file1" `ls -l "$file1" | sed 's/^.*'"$file1"' //'`
                    fi
                    file1="`/projects/webpics/EscapeSpecialCharsHtml.pl "${file1}"`"
                    # use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
                    if (("`grep -e "^[ ^t]*$file1[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
                        (( "$VERBOSE" )) && DebugMessage "The directory \"$file1\" is in both the excludes list and the sortorder list.  Excluding file1."
                    else
                        AppendMovieInformation "$file1"
                    fi
                    (( "$VERBOSE" )) && DebugMessage "processed directory \"$file1\".  "
                fi
            fi
        done <"sortorder.txt"
    fi

    # add all of the rest of the images (those that were not in the sortorder.txt file)
    (( "$VERBOSE" )) && DebugMessage "searching for all movies in \"$PWD\"..."

    # first go through all of the subdirectories
    # we will create a new IMAGE SUBSET for each subdirectory
    for file1 in * ; do
        if [ -f "$file1" ] ; then
            IsMovieFile="`echo "$file1" | sed -n "$SED_COMMAND_PRINT_MOVIE_TYPE"`"
            # if this is a movie file
            if [[ "$IsMovieFile" ]] ; then
                (( "$VERBOSE" )) && DebugMessage -e "BuildMovieTable: filetype of \"$file1\"=$IsMovieFile"
                if [[ -L "$file1" ]] ; then   # ==> If directory is a symbolic link...
                    (( "$VERBOSE" )) && DebugMessage "symlink: $file1" `ls -l "$file1" | sed 's/^.*'"$file1"' //'`
                fi
                local DoesSortOrderContainLine="`grep -e "^[ \t]*$file1[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`"
                local DoesExcludeFileContainLine="`grep -e "^[ \t]*$file1[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`"
                if [[ "$DoesExcludeFileContainLine" -ge 1 || $DoesSortOrderContainLine -ge 1 ]] ; then
                    if [ $DoesExcludeFileContainLine -ge 1 ] ; then
                        (( "$VERBOSE" )) && DebugMessage "The directory \"$file1\" is in the excludes list."
                    fi
                else
                    AppendMovieInformation "$file1"
                fi
            fi
        fi
    done
    if (( `cat ".webpics_movielist.txt" | sed "$SED_COMMAND_DELETE_BLANK_LINES" | wc -l` )) ; then
        local MovietableFooter="${TEMPLATE_DIR}/${MOVIETABLE_FILENAME/%.html/}_footer.html"
        if [ -f "$MovietableFooter" ] ; then
            MovieData="${MovieData} `cat "$MovietableFooter" | sed "$SED_COMMAND_STRIP_HTML_COMMENTS" | sed "$SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1"`"
        fi
    else
        MovieData=""
    fi
    (( "$VERBOSE" )) && DebugMessage -e "Exit BuildMovieTable().  MovieData: \n\n$MovieData\n\n"
}

BuildPage()
{
    local TEMP_FILENAME=".${NEW_FILENAME}.`basename "$0"`.tmp"

    if [ "$1" == "-v" ] ; then
        VERBOSE=1
        VERBOSE_ARG="-v"
        shift # discard the verbose argument and continue
    fi
    #VERBOSE=1

    # TODO: validate arguments and files

    TEMPLATE_FILENAME="$1"
    NEW_FILENAME="$2"

    if [ -f "${TEMPLATE_FILENAME}" ] ; then
        cp -f "${TEMPLATE_FILENAME}" "${NEW_FILENAME}"
    else
        DebugMessage "ERROR: BuildPage: template file \"${TEMPLATE_FILENAME}\" not found."
        return -1
    fi

    local CURRENT_DIRECTORY="$PWD"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*pictures}"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*private_pics}"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*public_html}"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*newpics}"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY/\//}" # replace the first with null, and the rest delimited by spaces
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY//\// }"

    # format the display name of the directory that contains all of the images
    TOPIC_DIR="${PWD/%$THUMB_DIR_BASENAME*/}"
    TOPIC_DIR="${TOPIC_DIR##*pictures}"
    TOPIC_DIR="${TOPIC_DIR##*private_pics}"
    TOPIC_DIR="${TOPIC_DIR##*public_html}"
    TOPIC_DIR="${TOPIC_DIR##*newpics}"
    TOPIC_DIR="${TOPIC_DIR//\// - }"
    TOPIC_DIR="${TOPIC_DIR% - }"
    TOPIC_DIR="${TOPIC_DIR# - }"

    local NUM_PARENTS=0
    local DIR="`dirname "${PWD/%$THUMB_DIR_BASENAME*/}"`"
    local DIR_LINK=""
    local FINISHED=""
    while [ "$DIR" ]                                                                \
        && [ "$DIR" != "`basename "$DIR"`" ]                 \
        && [ ! $FINISHED ]                                           \
        && [ "`basename "$DIR"`" != "pictures" ]     \
        && [ "`basename "$DIR"`" != "private_pics" ] \
        && [ "`basename "$DIR"`" != "newpics" ]      \
        && [ "`basename "$DIR"`" != "public_html" ]
    do
            DIR_LINK="../${DIR_LINK}"
            DIR_NAME="`basename "$DIR"`"
            SERIES_LINK="<a href=\"$DIR_LINK\">${DIR_NAME}</a> - $SERIES_LINK"
            NUM_PARENTS="`expr $NUM_PARENTS + 1`"
            (( "$NUM_PARENTS" > 3 )) && FINISHED=1
            DIR="`dirname "$DIR"`"
    done
    SERIES_LINK="$SERIES_LINK `basename "$PWD"`"

    local GeneratedDate="`date | sed "s/\ /\\\ /g"`"
    local GeneratedOn="<br><br>HTML File generated on ${GeneratedDate} by ${0}"
    local IndexHtmlString="`cat "${NEW_FILENAME}"`" # start with the file that we're about to replace
    local CopyrightString
    local CopyrightFilename="`dirname "${TEMPLATE_FILENAME}"`/${COPYRIGHT_FILENAME}"
    if [ -f "$CopyrightFilename" ] ; then
            CopyrightString="`cat "$CopyrightFilename"`"
            DebugMessage -e "adding copyright string from file $CopyrightFilename: \n\n$CopyrightString \n\n to output."
    else
            CopyrightString="Copyright (c) 2002-2014 by Bryant Hansen <br> all rights reserved"
            DebugMessage "copyright file: \"$CopyrightFilename\" not found.  Using default."
    fi

    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${IMAGE_SUBSET_TABLENAME_TAG}"        "${ImageSubsetData}"                    `"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${MOVIE_TABLENAME_TAG}"                       "${MovieData}"                  `"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${CURRENT_SET_TABLENAME_TAG}"         "${CurrentSetData}"                     `"

    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${CURRENT_DIRECTORY_TAG}"             "${CURRENT_DIRECTORY}"                  `"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${CURRENT_SET_NAME_TAG}"                      "${CURRENT_DIRECTORY}"                  `"
    DebugMessage "BuildPage: DirectoryDescriptionStr=$DirectoryDescriptionStr"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${CURRENT_SET_DESCRIPTION_TAG}"       "${DirectoryDescriptionStr}"    `"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${IMAGE_SUBSET_DESCRIPTION}"         "${ImageSubsetDescription}"        `"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${IMAGE_SUBSET_TAG}"                 "${HtmlImageSubsetLink}"        `"

    DebugMessage "replacing $START_SLIDESHOW_TAG with $SlideshowLink"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${START_SLIDESHOW_TAG}"                       "${SlideshowLink}"                              `"

    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${LINK_PARENT_TAG}"                           "."                                                             `"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${GENERATED_ON_TAG}"                          "${GeneratedOn}"                                `"
    DebugMessage "replacing $COPYRIGHT_TAG with $CopyrightString"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${COPYRIGHT_TAG}"                             "${CopyrightString}"                    `"

    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${SERIES_TAG}"                        "${TOPIC_DIR}"                          `"
    IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "SeriesLink"                           "${SERIES_LINK}"                        `"

#       IndexHtmlString="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${IndexHtmlString}" "${IMAGE_SET_TABLENAME_TAG}"           "${ImageData}"                          `"
    tempFile1=".`basename "${TEMPLATE_FILENAME}"`1.tmp"
    tempFile2=".`basename "${TEMPLATE_FILENAME}"`2.tmp"
    (( "$VERBOSE" )) && echo -e "\nIMAGE_DATA\n\n${ImageData}\n\n"

    echo "${IndexHtmlString}" > "$tempFile1"

    echo "${ImageData}" > "$tempFile2"
    /projects/webpics/ReplaceObjectTagsWithFile.pl -g "${tempFile1}" "${IMAGE_SET_TABLENAME_TAG}" "${tempFile2}"

    # TODO: this should be a ReplaceObjectTagsWithString function
    echo "${TOPIC_DIR}" > "$tempFile2"
    /projects/webpics/ReplaceObjectTagsWithFile.pl -g "${tempFile1}" "${SERIES_TAG}" "${tempFile2}"

    IndexHtmlString="`cat "$tempFile1"`"

    rm "$tempFile1"
    rm "$tempFile2"

    echo "${IndexHtmlString}" > "${NEW_FILENAME}"

    if (( "$VERBOSE" )) ; then
            DebugMessage -e "IndexHtmlString=\n***********\n${IndexHtmlString}\n**********\n"
            DebugMessage "Exit BuildPage"
#               WaitForKey   # this one is causing problems for some reason
    fi
}

DirectoryIterator()
{
    DebugMessage "DirectoryIterator: $*"

    SlideshowLink=""

    until [ -z "$1" ] ; do
        if [ ! -z `expr "$1" : "\(-v\)"` ] ; then
            VERBOSE=1
            VERBOSE_ARG="-v"
        fi
        shift
    done

    if (( "$VERBOSE" )) ; then
        DebugMessage "DirectoryIterator: dirname=\"`dirname "$0"`\""
    fi

# TODO: use umask
    echo "" > ".webpics_imagelist.txt" && chmod o-rwx ".webpics_imagelist.txt"
    echo "" > ".webpics_dirlist.txt" && chmod o-rwx ".webpics_dirlist.txt"
    echo "" > ".webpics_movielist.txt" && chmod o-rwx ".webpics_movielist.txt"

    (( "$VERBOSE" )) && DebugMessage "calling VerifyTemplateFiles on directory ./$subdir..."
    VerifyTemplateFiles $VERBOSE_ARG "./"
#    VerifyTemplateFiles -v "./"

    if [[ ! -f "${TEMPLATE_DIR}/${INDEX_PROTOTYPE}" ]] ; then
        DebugMessage "ERROR: ${TEMPLATE_DIR}/${INDEX_PROTOTYPE} does not exist!  Exiting abnormally!"
        exit -2
    fi

    BuildMovieTable
    BuildCurrentSetTable
    BuildImageSubsetTable
    BuildImageTable

    BuildPage "${TEMPLATE_DIR}/${INDEX_PROTOTYPE}" "$PICTURE_FILENAME"

    CHANGE_MOD=0
    if [ $CHANGE_MOD -ne 0 ] ; then
            chmod 644 "$PICTURE_FILENAME"
    fi

    if [[ "$RECURSE" ]] && (( "$RECURSE" )) ; then
        local subdir=""
        for subdir in * .* ; do
            if [[ -d "$subdir" ]] && \
               [[ "$subdir" != "." ]] && \
               [[ "$subdir" != ".." ]]
            then
                if [[ -L "$subdir" ]] ; then
                    (( "$VERBOSE" )) && DebugMessage  "symbolic link: $subdir" `ls -l "$subdir" | sed 's/^.*'"$subdir"' //'`
                fi
                # check if this directory is in our excludes list
                local grepResult="`grep -e "^[ ^t]*$subdir[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`"
                if (("${grepResult}" >= 1)) ; then
                    (( "$VERBOSE" )) && DebugMessage "The directory \"$subdir\" is in the excludes list."
                else
                    # we don't want to descend into thumbnail directories
                    thumbextract=`expr "$subdir" : "\($THUMB_DIR_BASENAME\)"`
                    if  [[ -z $thumbextract ]] && \
                        [[ ! "`basename "$subdir"`" = "$TEMPLATE_DIR" ]] && \
                        [[ ! "`basename "$subdir"`" = "$HTML_DIR" ]]
                    then
                        if cd "$subdir" ; then              # ==> If can move to subdirectory...
                            DirectoryIterator "$subdir"
                            cd ..
                        fi
                    fi
                fi
            fi
        done
    fi
    (( "$VERBOSE" )) && DebugMessage "Exit DirectoryIterator()"
}


######################################################
# Setup Environment
######################################################
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}


######################################################
# - Main -
######################################################

# Set defaults
THUMB_SIZE="$DEFAULT_THUMB_SIZE"
THUMB_DIR="$THUMB_DIR_BASENAME$THUMB_SIZE"
IMAGE_SIZE="$DEFAULT_IMAGE_SIZE"
IMAGE_DIR_NAME="$THUMB_DIR_BASENAME$IMAGE_SIZE"

(( "$VERBOSE" )) && DebugMessage "`basename "$0"` $* called.  Verbose messaging enabled."
until [ -z "$1" ] ; do
if [ ! -z `expr "$1" : "\(-\)"` ] ; then
    if [ ! -z `expr "$1" : "\(--size=\)"` ] ; then
        THUMB_SIZE_ARG=${1#--size=}
        if [ "$THUMB_SIZE_ARG" -gt "0" ] ; then
            THUMB_SIZE=$THUMB_SIZE_ARG
        else
            DebugMessage "INVALID Thumbnail size arg: $1"
        fi
    fi
    if [ ! -z `expr "$1" : "\(-h\)"` ] ; then
        DebugMessage "TODO: must create help"
        DebugMessage "USAGE=$USAGE"
        exit 0
    fi
    if [ ! -z `expr "$1" : "\(-b\)"` ] \
        || [ ! -z `expr "$1" : "\(--backup-original\)"` ] ; then
                BACKUP_ORIGINAL=1
        fi
        if [ ! -z `expr "$1" : "\(-o\)"` ] \
        || [ ! -z `expr "$1" : "\(--output-file\)"` ] ; then
                shift
                PICTURE_FILENAME="$1"
        fi
        if [ ! -z `expr "$1" : "\(-r\)"` ] ; then
                RECURSE=1
    fi
    if [ ! -z `expr "$1" : "\(-ns\)"` ] ; then
        SLIDESHOW=0
    fi
    if [ ! -z `expr "$1" : "\(-nh\)"` ] ; then
        SHOWHEADING=0
    fi
    if [ ! -z `expr "$1" : "\(-v\)"` ] ; then
                VERBOSE=1
                VERBOSE_ARG="-v"
        fi
fi
LAST_PARAM=$1
shift
done
if [ -d "$LAST_PARAM" ] && [ ! -z "$LAST_PARAM" ]; then
#   BASE_DIR="${LAST_PARAM%\/}"       # ==> Otherwise, move to indicated directory.
   BASE_DIR="${LAST_PARAM}"       # ==> Otherwise, move to indicated directory.
else
   BASE_DIR="$PWD"    # ==> No args to script, then use current working directory.
fi

if (( "$VERBOSE" )) ; then
   DebugMessage "Script Options:"
   DebugMessage "   VERBOSE is on.  VERBOSE=$VERBOSE"
   DebugMessage "   RECURSE=$RECURSE"
   DebugMessage "   THUMB_SIZE=$THUMB_SIZE"
   DebugMessage "   THUMB_DIR=$THUMB_DIR"
   DebugMessage "   IMAGE_SIZE=$IMAGE_SIZE"
   DebugMessage "   IMAGE_DIR_NAME=$IMAGE_DIR_NAME"
   DebugMessage "   TEMPLATE_DIR=$TEMPLATE_DIR"
   DebugMessage "   INDEX_PROTOTYPE=$INDEX_PROTOTYPE"
   DebugMessage "   SLIDE_PROTOTYPE=$SLIDE_PROTOTYPE"
   DebugMessage "   BASE_DIR=$BASE_DIR"
   DebugMessage "   SLIDESHOW=$SLIDESHOW"
   DebugMessage "   SHOWHEADING=$SHOWHEADING"
   DebugMessage "   FRESHEN_TEMPLATE_FILES=$FRESHEN_TEMPLATE_FILES"
fi

cd "$BASE_DIR"

DirectoryIterator "${VERBOSE_ARG}" "${BASE_DIR}"

DebugMessage "$0 complete, exiting normally."
exit 0

