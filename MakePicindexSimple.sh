#!/bin/bash
# Bryant Hansen
# 20100307

THUMB_DIR="."
FULL_SIZE_DIR=".."
TEMPLATE_DIR="./templates"

cat "${TEMPLATE_DIR}/simple/header.html"

echo "<body>"
for f in "$THUMB_DIR"/*.JPG "$THUMB_DIR"/*.jpg "$THUMB_DIR"/*.png ; do 
    echo "<div>"
    # get the largest thumbnail dir
    # ls -1d .thumbnails*  | sed "s/.thumbnails//" | sort -n | tail -1
    linkAddress="${FULL_SIZE_DIR}/${f}"
    if [[ -f "${FULL_SIZE_DIR}/.thumbnails1024/html/${f}.html" ]] ; then
        linkAddress="${FULL_SIZE_DIR}/.thumbnails1024/html/${f}.html"
    fi
    if [[ -f "${FULL_SIZE_DIR}/html/picture_${f}.html" ]] ; then
        linkAddress="${FULL_SIZE_DIR}/.thumbnails1024/html/picture_${f}.html"
    fi
    echo "<a href=\"${linkAddress}\" ><img src=\"${f}\"></a>"
    [[ -f ../"${f}.txt" ]] && echo "<br>" && cat ../"${f}.txt"
    echo "</div>"
done
echo "</body>"

cat "${TEMPLATE_DIR}/simple/footer.html"
