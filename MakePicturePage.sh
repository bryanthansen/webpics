# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
#         MakePicturePages.sh
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#         Create an HTML page to display a picture
#                     - link to next pic, previous pic, and parent folder
#                        - takes an argument for a properties file
#                        - can specify alternate templates
#
# copyright (c) 2003-2014 by Bryant Hansen
#
# all local links are relative (all _LINK vars are relative links to local - and therefore, dirspecs are . or .. and no dir assumes file is in .
#

CONVERT=convert
BASE_DIR=""

IMAGE_INFO_DIR=".image_info"
TEMPLATE_DIR="templates"
HTML_DIR="html"
DEFAULT_PROTOTYPE="picture.html"
OBJECT_INDEX_PROTOTYPE="picture.html"
PICTURE_LINK=""
PICTURE_FILENAME=""
THUMB_DIR_BASENAME=".thumbnails"

IMAGE_TAG="Thumbnail n"
DESCRIPTION_TAG="Description n"

TAG_SPEC="photo album tag:"
IMAGE_FILENAME_TAG="image filename"
HEADER_TABLE_TAG="forward / backward table"
NEXT_IMAGE_FILE_TAG="next_picture_file"
IMAGE_INDEX_TAG="index"
CURRENT_IMAGE_TAG="picture_filename"
CURRENT_IMAGE_REL_URL_TAG="picture_url_rel"
SERIES_TAG="Series"
SERIES_LINK_TAG="SeriesLink"
LINK_PARENT_TAG="parent_url_rel"
CAPTION_TAG="caption"

NEXT_IMAGE_TAG="next-image"
FORWARD_ARROW_TAG="forward_arrow"
IMAGE_FORWARD_TAG="forward"
IMAGE_FORWARD_HTML_URL_TAG="next_image_html_url_rel"
IMAGE_FORWARD_URL_TAG="next_image_url_rel"

PREVIOUS_IMAGE_TAG="previous-image"
BACWARD_ARROW_TAG="backward_arrow"
IMAGE_BACKWARD_TAG="backward"
IMAGE_BACKWARD_HTML_URL_TAG="last_image_html_url_rel"
IMAGE_BACKWARD_URL_TAG="last_image_url_rel"

EXIF_APERATURE_TAG="Exif:ApertureValue"
EXIF_DATETIME_TAG="Exif:DateTime"
EXIF_EXPOSURE_TIME_TAG="Exif:ExposureTime"
EXIF_FLASH_TAG="Exif:Flash"
EXIF_FOCAL_LENGTH_TAG="Exif:FocalLength"
EXIF_ISO_SPEED_TAG="Exif:ISOSpeedRatings"
EXIF_MODEL_TAG="Exif:Model"
EXIF_WHITE_BALANCE_TAG="Exif:WhiteBalance"

QUALITY_TAG="Quality"
DIMENSIONS_TAG="Dimensions"

AVAILABLE_RESOLUTIONS_TAG="available_resolutions"
AVAILABLE_RESOLUTIONS_DELIMITER=" <br />    "
IMAGE_DESCRIPTION_TAG="caption"

CURRENT_DIRECTORY_TAG="CurrentDirectoryName"
GENERATED_ON_TAG="GENERATION_NOTICE"

START_SLIDESHOW_TAG="StartSlideshowLink"
STOP_SLIDESHOW_TAG="StopSlideshowLink"

COPYRIGHT_TAG="Copyright"
COPYRIGHT_FILENAME="copyright.html"

LINK_TO_FULL_SIZE=1

RESERVED_IMAGE=0
RESERVED_IMAGES="forward.png backward.png index.gif"

# REPLACE_COMMENT_TAGS_COMMAND="/projects/webpics/ReplaceTags.sh" # todo: bring this back; it has a precedence in specifications (HTML server-side includes)
REPLACE_OBJECT_TAGS_COMMAND="/projects/webpics/ReplaceObjectTags2.pl"
REPLACE_OBJECT_EXIF_TAGS_COMMAND="/projects/webpics/ReplaceObjectExifTags.pl"

# some needed SED commands
# some sed commands we'll need here; string them together to do something intelligent, but understandable (hopefully) in source
SED_COMMAND_STRIP_HTML_COMMENTS="/<!--/!b;:a;/-->/!{N;ba};s/<!--.*-->//"
SED_COMMAND_DELETE_BLANK_LINES="/^[\t ]*$/d"
SED_COMMAND_MULTIPLE_BLANK_LINES_TO_1="/^$/{;N;/^\n$/d;}"
SED_COMMAND_EXTRACT_ENDING="s/.*\.//g"
SED_COMMAND_REMOVE_LINK_INFO="s/ -> .*//g"
SED_COMMAND_TO_LOWER_CASE="y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
SED_COMMAND_PRINT_IMAGE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)/p"  # used with sed -n
SED_COMMAND_PRINT_MOVIE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(mov\|avi\|mpi\|3gp\|mp4\)/p" # used with sed -n

WaitForKey()
{
    local tmp
    echo "Press Any Key" 1>&2
    read -n 1 'tmp' <&1
#    read -n 1
    return 0
}

DebugMessage()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "

    while [ "$OPTION_FOUND" ] ; do
        OPTION_FOUND=""
        if [ "$1" = "-e" ] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [ "$1" = "-n" ] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}

function getCopyrightString() {

    local copyright_string="(c)2014 Hansen"

    # sed expressions
    local SED_STRIP_HTML="s/<[^>]*>//g"

    # last one wins
    [[ "$COPYRIGHT_STRING" ]] && copyright_string="$COPYRIGHT_STRING"

    copyright_file="templates/copyright.html"
    if [[ -f "$copyright_file" ]] ; then
        copyright_string="`cat "$copyright_file" | sed "$SED_STRIP_HTML"`"
    fi

    copyright_file="copyright.txt"
    if [[ -f "$copyright_file" ]] ; then
        copyright_string="`cat "$copyright_file" | sed "$SED_STRIP_HTML"`"
    fi

    copyright_file="../copyright.txt"
    if [[ -f "$copyright_file" ]] ; then
        copyright_string="`cat "$copyright_file" | sed "$SED_STRIP_HTML"`"
    fi

    copyright_file="${input_image}.copyright"
    if [[ -f "$copyright_file" ]] ; then
        copyright_string="`cat "$copyright_file" | sed "$SED_STRIP_HTML"`"
    fi

    echo "$copyright_string"
    return 0
}

BuildPage()
{
    # WARNING: at the moment HTML links are different than relative links from the current directory

    USAGE="BuildPage [ -v ] PrototypeFilename NewFilename LastImage CurrentImage NextImage"

    local VERBOSE="0"
    local VERBOSE_ARG=""

    if [ "$1" == "-v" ] ; then
        VERBOSE=1
        VERBOSE_ARG="-v"
        shift # discard the verbose argument and continue
    fi

    if (( $# != 5 )) ; then
        DebugMessage "ERROR: BuildPage requires 5 arguments!  Got $#.  Args=$*"
        DebugMessage "Usage=$USAGE"
        WaitForKey
        exit -1
    fi

    local PROTOTYPE_FILENAME="${1}"
    local NEW_FILENAME="${2}"
    local LAST_IMAGE="${3}"
    local CURRENT_IMAGE_FILENAME="${4}"
    local NEXT_IMAGE="${5}"

    if (( "$VERBOSE" )) ; then 
        DebugMessage "Enter BuildPage  PrototypeFilename=$1, NewFilename=$2, CurrentImageFilename=$3"
    fi

    # TODO: validate arguments and files

    local DIRNAME="`basename "$PWD"`"
    IMAGE_IS_FULL_SIZE=""
    # a difficult-to-understand way of matching a partial string.
    [[ "`echo "$PWD" | sed -e "s/${THUMB_DIR}[0-9]*$//"`" == "$PWD" ]] && IMAGE_IS_FULL_SIZE=1

    local IMAGE_DIR=".."
    local ALBUM_BASE_DIR="../.."
    if [[ "$IMAGE_IS_FULL_SIZE" ]] ; then
        IMAGE_DIR="."
        ALBUM_BASE_DIR=".."
    fi

    local CURRENT_IMAGE_REL_URL="../${CURRENT_IMAGE_FILENAME}"

    if [[ ! -f "${PROTOTYPE_FILENAME}" ]] ; then
        DebugMessage "ERROR: BuildPage: template file \"${PROTOTYPE_FILENAME}\" not found."
        ERROR_PROTOTYPE_NOT_FOUND=10
        return $ERROR_PROTOTYPE_NOT_FOUND
    fi

    local PICTURE_LINK="./$HTML_DIR/${OBJECT_INDEX_PROTOTYPE/%.html/}_$CURRENT_IMAGE_FILENAME.html"

    local CURRENT_DIRECTORY="$PWD"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*pictures}"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*private_pics}"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*public_html}"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*newpics}"
#    CURRENT_DIRECTORY="${CURRENT_DIRECTORY//.thumbnails[0-9].*\//}"
#    CURRENT_DIRECTORY="`echo ${CURRENT_DIRECTORY} | sed "s/.thumbnails[0-9].*//g"`"
    CURRENT_DIRECTORY="${CURRENT_DIRECTORY/\//}" # replace the first with null, and the rest delimited by spaces
#    CURRENT_DIRECTORY="${CURRENT_DIRECTORY//\// }"
#    CURRENT_DIRECTORY="${CURRENT_DIRECTORY//\'''/\\'''}"

    # format the display name of the directory that contains all of the images
    SERIES_NAME="${CURRENT_DIRECTORY//\// - }"
    SERIES_NAME="`echo ${SERIES_NAME} | sed "s/.thumbnails[0-9].*//g"`"
    SERIES_NAME="${SERIES_NAME/%\//}" # replace the last with null, and the rest delimited by spaces
#    SERIES_NAME="${SERIES_NAME/%$THUMB_DIR_BASENAME*/}"
    SERIES_NAME="${SERIES_NAME% - }"
    SERIES_NAME="${SERIES_NAME# - }"

    if (( "$VERBOSE" )) ; then
        DebugMessage "BuildPage  CURRENT_DIRECTORY=$CURRENT_DIRECTORY, SERIES_NAME=$SERIES_NAME"
    fi

    local SERIES_LINK=""
    local NUM_PARENTS=0
    local DIR="${PWD/%$THUMB_DIR_BASENAME*/}"
    local DIR_LINK="../"
    local FINISHED=""
    while [[ "$DIR" ]]                                     \
       && [[ "$DIR" != "`basename "$DIR"`" ]]             \
       && [[ ! $FINISHED ]]                             \
       && [[ "`basename "$DIR"`" != "pictures" ]]        \
       && [[ "`basename "$DIR"`" != "private_pics" ]]    \
       && [[ "`basename "$DIR"`" != "newpics" ]]        \
       && [[ "`basename "$DIR"`" != "public_html" ]]
    do
        DIR_LINK="../${DIR_LINK}"
        DIR_NAME="`basename "$DIR"`"
        SERIES_LINK="<a href=\"$DIR_LINK\">${DIR_NAME}</a> - $SERIES_LINK"
        NUM_PARENTS="`expr $NUM_PARENTS + 1`"
        (( "$NUM_PARENTS" > 3 )) && FINISHED=1
        DIR="`dirname "$DIR"`"
    done
    SERIES_LINK="`echo "${SERIES_LINK}" | sed 's/ - $//'`"

    local GeneratedDate="`date | sed "s/\ /\\\ /g"`"
    local GeneratedOn="HTML File generated on ${GeneratedDate} by ${0}"
    local CopyrightString
    CopyrightString="$(getCopyrightString)"
    #[[ "$copyright_string_arg" ]] && CopyrightString="$copyright_string_arg"
    echo "  CopyrightString=\"${CopyrightString}\"" >&2
    
    local LAST_IMAGE_URL=""
    local NEXT_IMAGE_URL=""

    local LAST_IMAGE_HTML_URL="${ALBUM_BASE_DIR}/"
    local NEXT_IMAGE_HTML_URL="${ALBUM_BASE_DIR}/"

    # default these to the parent directory, unless last and/or next images are found
    local LAST_IMAGE_LINK_HTML="<A id=\"previousImage\" HREF=\"${ALBUM_BASE_DIR}/\"><IMG border=0 src=\"backward.png\" alt=\"back to the index\"></A>"
    local NEXT_IMAGE_LINK_HTML="<A id=\"nextImage\" HREF=\"${ALBUM_BASE_DIR}/\"><IMG border=0 src=\"forward.png\" alt=\"(back to the index)\"></A>"

    local StartSlideshowLink="slide_${CURRENT_IMAGE_FILENAME}.html"
    local StopSlideshowLink="picture_${CURRENT_IMAGE_FILENAME}.html"

    if [ ! -z "$LAST_IMAGE" ] ; then
        LAST_IMAGE_URL="../$LAST_IMAGE"
        LAST_IMAGE_HTML_URL="${OBJECT_INDEX_PROTOTYPE/%.html/}_$LAST_IMAGE.html"
        LAST_IMAGE_LINK_HTML="<A id=\"previousImage\" HREF=\"${OBJECT_INDEX_PROTOTYPE/%.html/}_$LAST_IMAGE.html\"><IMG border=0 src=\"backward.png\" alt=\"Back to ${OBJECT_INDEX_PROTOTYPE/%.html/}_$LAST_IMAGE.html\"></A>"
    fi

    if [ ! -z "$NEXT_IMAGE" ] ; then
        NEXT_IMAGE_URL="../$NEXT_IMAGE"
        NEXT_IMAGE_HTML_URL="${OBJECT_INDEX_PROTOTYPE/%.html/}_$NEXT_IMAGE.html"
        NEXT_IMAGE_LINK_HTML="<A id=\"nextImage\" HREF=\"${OBJECT_INDEX_PROTOTYPE/%.html/}_$NEXT_IMAGE.html\"><IMG border=0 src=\"forward.png\" alt=\"ahead to ${OBJECT_INDEX_PROTOTYPE/%.html/}_$NEXT_IMAGE.html\"></A>"
    fi

    local THUMBNAIL_DIR=".."
    if [ $IMAGE_IS_FULL_SIZE ] ; then
        THUMBNAIL_DIR="."
    fi
    local AVAILABLE_RESOLUTIONS_STRING=AVAILABLE_RESOLUTIONS_TAG="available-resolutions"
    AVAILABLE_RESOLUTIONS="`ls -1a "${THUMBNAIL_DIR}" | grep "\${THUMB_DIR_BASENAME}[0-9].*$" | sed -e "s/${THUMB_DIR_BASENAME}//" | sort -n`"
    (( "$VERBOSE" )) &&  echo "available resolutions=${AVAILABLE_RESOLUTIONS}"

    AVAILABLE_RESOLUTIONS_STRING=""
    LOCAL_DELIMITER=""
    for ThumbDirSize in $AVAILABLE_RESOLUTIONS ; do
        # TODO: consider adding better information about the thumbnail size (600xNNN, or NNNx1024), rather than just 600 and 1024
        local ResolutionName="${ThumbDirSize}"
        local DIMENSIONS
        local ThumbFilename="${IMAGE_DIR}${THUMB_DIR_BASENAME}${ThumbDirSize}/${CURRENT_IMAGE_FILENAME}"
        DIMENSIONS="`identify -format "%wx%h" "$ThumbFilename"`"  
        [[ "$DIMENSIONS" ]] && ResolutionName="${DIMENSIONS}"
        AVAILABLE_RESOLUTIONS_STRING="${AVAILABLE_RESOLUTIONS_STRING}${LOCAL_DELIMITER}<a href=\"${ALBUM_BASE_DIR}${THUMB_DIR_BASENAME}${ThumbDirSize}/html/${OBJECT_INDEX_PROTOTYPE/%.html/}_${CURRENT_IMAGE_FILENAME}.html\">${ResolutionName}</a>"        LOCAL_DELIMITER="${AVAILABLE_RESOLUTIONS_DELIMITER}"
    done
    if [[ "$LINK_TO_FULL_SIZE" ]] && (( "$LINK_TO_FULL_SIZE" )) ; then
        local ORIGINAL_IMAGE_FILENAME="${IMAGE_DIR}${CURRENT_IMAGE_FILENAME}"
        (( "$VERBOSE" )) && DebugMessage "identifying the original dimensions of $ORIGINAL_IMAGE_FILENAME"
        if [[ -f "$ORIGINAL_IMAGE_FILENAME" ]] ; then
            local DIMENSIONS
            local ImageInfoFile="${IMAGE_DIR}/${IMAGE_INFO_DIR}/${CURRENT_IMAGE_FILENAME}.txt"
            local ExifFile="${IMAGE_DIR}/${IMAGE_INFO_DIR}/${CURRENT_IMAGE_FILENAME}.exif.txt"
            if [[ ! -f "$ExifFile" ]] ; then
                ExifFile="$ImageInfoFile"
            fi
            if [[ -f "$ImageInfoFile" ]] ; then

                # working, but too long and in a bad order
                EXIF_INFO_LONG="`cat "$ImageInfoFile" | sed "s/exif\:/Exif\:/" | sed "/^0x.*$/d" | sed "s/$/<br>/" | sed "s/\ \ /\ \&nbsp\;\&nbsp\;/g" | sed "s/\t/\&nbsp\;\&nbsp\;\&nbsp\;\&nbsp\;/g"`"

                # not working; shows almost all, not just the bottom half (the EXIF info)
#                EXIF_INFO_LONG="`cat "$ImageInfoFile" | sed "N;s/.*Image Description:/Image Description:/" | sed "/^0x.*$/d" | sed "s/$/<br>/" | sed "s/\ \ /\ \&nbsp\;\&nbsp\;/g" | sed "s/\t/\&nbsp\;\&nbsp\;\&nbsp\;\&nbsp\;/g"`"


#                EXIF_INFO_LONG="`cat "$ImageInfoFile" | sed "N!/.*Image Description/D" | sed "/^0x.*$/d" | sed "s/$/<br>/" | sed "s/\ \ /\ \&nbsp\;\&nbsp\;/g" | sed "s/\t/\&nbsp\;\&nbsp\;\&nbsp\;\&nbsp\;/g"`"

                local EXIF_INFO_SHORT=""
                EXIF_INFO_SHORT="$EXIF_INFO_SHORT `echo -n "$EXIF_INFO_LONG" | sed -n "/Geometry:/p" | sed "s/ *Geometry: */Original Resolution: /"`"
                SED_COMMAND_PRINT_SELECTIONS="/Make:/p;/Model:/p;/Orientation:/p;/Exposure Time:/p;/F Number:/p;/ISO Speed Ratings:/p;/F Number:/p;/Compressed Bits Per Pixel:/p;/Focal Length:/p"
                EXIF_INFO_SHORT="$EXIF_INFO_SHORT `echo -n "$EXIF_INFO_LONG" | sed -n "$SED_COMMAND_PRINT_SELECTIONS"`"
                EXIF_INFO_SHORT="`echo "$EXIF_INFO_SHORT" | sed -e "s/<br>/ -- /g;s/\&nbsp\;/ /g"`"

                EXIF_APERATURE="`grep "Exif:ApertureValue:" "$ExifFile" | sed "s/^.*Exif\:ApertureValue\:\ //"`"
                # Aperture value is in the form of N/N
                # let's evaluate it mathematically, by expr N / N
                EXIF_APERATURE2=""
                [[ "$EXIF_APERATURE" ]] && EXIF_APERATURE2="`expr ${EXIF_APERATURE/\//\ \/\ }` (${EXIF_APERATURE})"

                EXIF_DATETIME="`grep "[Ee]xif:DateTime:" "$ExifFile" | sed "s/^.*[Ee]xif\:DateTime\://"`"
                EXIF_EXPOSURE_TIME="`grep "[Ee]xif:ExposureTime:" "$ExifFile" | sed "s/^.*[Ee]xif\:ExposureTime\://"`"
                EXIF_FLASH="`grep "[Ee]xif:Flash:" "$ExifFile"  | sed "s/^.*[Ee]xif\:Flash\://"`"
                EXIF_FOCAL_LENGTH="`grep "[Ee]xif:FocalLength:" "$ExifFile" | sed "s/^.*[Ee]xif\:FocalLength\://"`"
                EXIF_ISO_SPEED="`grep "[Ee]xif:ISOSpeedRatings:" "$ExifFile" | sed "s/^.*[Ee]xif\:ISOSpeedRatings\://"`"
                EXIF_MODEL="`grep "[Ee]xif:Model:" "$ExifFile" | sed "s/^.*[Ee]xif\:Model\://"`"
                EXIF_WHITE_BALANCE="`grep "[Ee]xif:WhiteBalance:" "$ExifFile" | sed "s/^.*[Ee]xif\:WhiteBalance\://"`"

                QUALITY="`cat "$ImageInfoFile"   | sed -n "/^[ \t]*Quality:/p" | sed "s/[ \t]*Quality:[ \t]*//"`"
                DIMENSIONS="`cat "$ImageInfoFile"   | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t]*//"`"
                DIMENSIONS="`echo "$DIMENSIONS" | sed "s/+0+0//"`"
                (( "$VERBOSE" )) && DebugMessage "reading dimensions from info file: $ExifFile is a $DIMENSIONS-pixel image."
            else
                DebugMessage "ORIGINAL_IMAGE_FILENAME=$ORIGINAL_IMAGE_FILENAME"
                DIMENSIONS="`identify -format \"%wx%h\" \"$ORIGINAL_IMAGE_FILENAME\"`"
                # strip the trailing +0+0
                DIMENSIONS="`echo "$DIMENSIONS" | sed "s/+0+0//"`"
#                (( "$VERBOSE" )) && 
                DebugMessage "$ORIGINAL_IMAGE_FILENAME: failed to read EXIF information from ($ExifFile).  slow call to identify show that it is a ${DIMENSIONS}-pixel image."
            fi
        else
            DebugMessage "ERROR: Original Image ($ORIGINAL_IMAGE_FILENAME) does not exist!  File $NEW_FILENAME will be corrupt!"
        fi
        AVAILABLE_RESOLUTIONS_STRING="${AVAILABLE_RESOLUTIONS_STRING}${AVAILABLE_RESOLUTIONS_DELIMITER}<a href=\"${ALBUM_BASE_DIR}/html/${OBJECT_INDEX_PROTOTYPE/%.html/}_${CURRENT_IMAGE_FILENAME}.html\">${DIMENSIONS} (full-size)</a>"
    fi
    (( "$VERBOSE" )) && DebugMessage "available resolutions string=${AVAILABLE_RESOLUTIONS_STRING}"

    local ImageDescriptionFilename="${CURRENT_IMAGE_FILENAME}.txt"
    if [[ ! -f "$ImageDescriptionFilename" ]] ; then
        ImageDescriptionFilename="${IMAGE_BASE_DIR}/${CURRENT_IMAGE_FILENAME}.txt"
        if [[ ! -f "$ImageDescriptionFilename" ]] ; then
            ImageDescriptionFilename="${ALBUM_BASE_DIR}/${CURRENT_IMAGE_FILENAME}.txt"
            if [[ ! -f "$ImageDescriptionFilename" ]] ; then
                ImageDescriptionFilename=""
            fi
        fi
    fi
    local ImageDescription=""
    if [[ -f "$ImageDescriptionFilename" ]] ; then
        ImageDescription="`cat "$ImageDescriptionFilename"`"
        DebugMessage "\"${ImageFile}\" description: \"${ImageDescription}\""
    fi

    local FileData="`cat "${PROTOTYPE_FILENAME}"`" # start with the file that we're about to replace

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${CURRENT_IMAGE_TAG}"                 "${CURRENT_IMAGE_FILENAME}"            `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${CURRENT_IMAGE_REL_URL_TAG}"         "${CURRENT_IMAGE_REL_URL}"            `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_BACKWARD_URL_TAG}"             "${LAST_IMAGE_URL}"                    `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_BACKWARD_HTML_URL_TAG}"     "${LAST_IMAGE_HTML_URL}"            `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_BACKWARD_TAG}"                 "${LAST_IMAGE_LINK_HTML}"            `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_FORWARD_URL_TAG}"             "${NEXT_IMAGE_URL}"                    `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_FORWARD_HTML_URL_TAG}"         "${NEXT_IMAGE_HTML_URL}"            `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_FORWARD_TAG}"                 "${NEXT_IMAGE_LINK_HTML}"            `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${AVAILABLE_RESOLUTIONS_TAG}"         "${AVAILABLE_RESOLUTIONS_STRING}"    `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${CURRENT_SET_DESCRIPTION_TAG}"     "${DirectoryDescriptionStr}"        `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_SUBSET_DESCRIPTION}"         "${ImageSubsetDescription}"            `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_SUBSET_TAG}"                 "${HtmlImageSubsetLink}"            `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_DESCRIPTION_TAG}"             "${ImageDescription}"                `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${CURRENT_DIRECTORY_TAG}"             "${CURRENT_DIRECTORY}"                `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${START_SLIDESHOW_TAG}"             "${StartSlideshowLink}"                `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${STOP_SLIDESHOW_TAG}"                 "${StopSlideshowLink}"                `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${LINK_PARENT_TAG}"                 "${ALBUM_BASE_DIR}"                    `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${SERIES_TAG}"                         "${SERIES_NAME}"                    `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${SERIES_LINK_TAG}"                         "${SERIES_LINK}"                    `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${GENERATED_ON_TAG}"                 "${GeneratedOn}"                    `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${COPYRIGHT_TAG}"                     "${CopyrightString}"                `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${CURRENT_SET_TABLENAME_TAG}"         "${CurrentSetData}"                    `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_SET_TABLENAME_TAG}"         "${ImageData}"                        `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${IMAGE_SUBSET_TABLENAME_TAG}"         "${ImageSubsetData}"                `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${EXIF_APERATURE_TAG}" "${EXIF_APERATURE2}"        `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${EXIF_DATETIME_TAG}" "${EXIF_DATETIME}"             `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${EXIF_EXPOSURE_TIME_TAG}" "${EXIF_EXPOSURE_TIME}"        `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${EXIF_FLASH_TAG}" "${EXIF_FLASH}"               `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${EXIF_FOCAL_LENGTH_TAG}" "${EXIF_FOCAL_LENGTH}"         `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${EXIF_ISO_SPEED_TAG}" "${EXIF_ISO_SPEED}"            `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${EXIF_MODEL_TAG}" "${EXIF_MODEL}"        `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${EXIF_WHITE_BALANCE_TAG}" "${EXIF_WHITE_BALANCE}"        `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${QUALITY_TAG}" "${QUALITY}"        `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "${DIMENSIONS_TAG}" "${DIMENSIONS}"        `"

    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "ExifInfoMultilineLong"                 "${EXIF_INFO_LONG}"                `"
    FileData="`${REPLACE_OBJECT_TAGS_COMMAND} -g "${FileData}" "ExifInfoShort"                         "${EXIF_INFO_SHORT}"                `"
#    FileData="`${REPLACE_OBJECT_EXIF_TAGS_COMMAND} -g "${FileData}" "ExifInfoShort"                 "${EXIF_INFO_SHORT}"                `"
#    FileData="`${REPLACE_OBJECT_EXIF_TAGS_COMMAND} -g "${FileData}" "ExifInfoShort"                 "${EXIF_INFO_SHORT}"                `"

    echo "${FileData}" > "${NEW_FILENAME}"

    if (( "$VERBOSE" )) ; then
        DebugMessage "Exiting BuildPage (pwd=$PWD, newfile=\"`basename "${NEW_FILENAME}"`\"."
    else
        DebugMessage -n -e "."
    fi
}

ProcessPictureFile() 
{
    (( "$VERBOSE" )) && DebugMessage "template=\"$OBJECT_INDEX_PROTOTYPE\" directory=\"$PWD\"..."

    VerifyTemplateFiles $VERBOSE_ARG "."

    if [[ ! -d "./$HTML_DIR" ]] ; then
        mkdir -p "./$HTML_DIR"
    fi

    # if images and style sheets are linked, the links should be preserved
    # however, consider whether the links should point to an HTML dir, rather than a template dir
    # cp -af "${TEMPLATE_DIR}"/* "${HTML_DIR}"/ # copy the style sheet, the images, and whatever else we need from the template directory
    rsync -ar "${TEMPLATE_DIR}"/* "${HTML_DIR}"/ # copy the style sheet, the images, and whatever else we need from the template directory

    local CAPTION=""
    local CAPTION_FILE=""

    local CURRENT_IMAGE_FILENAME="$1"
    local f1="$1"
    # local NEW_PAGE_FILENAME="./$HTML_DIR/${OBJECT_INDEX_PROTOTYPE/%.html/}_$CURRENT_IMAGE_FILENAME.html"
    local NEW_PAGE_FILENAME="$2"
    local PROPERTIES_FILENAME="$3"
    local Ending

    IsPictureFile="`echo "$f1" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
    # if this is a picture file
    if [[ ! "$IsPictureFile" ]] ; then
        echo "$f1 is not a picture file!  Exiting abnormally!" 1>&2
    else
        if [[ -L "$f1" && ! -z $VERBOSE ]] ; then   # ==> If file is a symbolic link...
            DebugMessage "symlink: $f1" `ls -l "$f1" | sed 's/^.*'"$f1"' //'`
        fi
        if (("`grep --files-with-matches -e "^[ ^t]*$f1[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
            # (( "$VERBOSE" )) && 
            DebugMessage "The file \"$f1\" is in the excludes list.  Excluding file."
        else
            IsMovieFile="`echo "${f1%.jpg}" | sed -n "$SED_COMMAND_PRINT_MOVIE_TYPE;$SED_COMMAND_DELETE_BLANK_LINES"`"
            # if this is a movie file
            if [[ "${f1%.jpg}" != "${f1}" ]] && [[ "$IsMovieFile" ]] ; then
                # this matches the pattern of a movie frame
                # movie=some_movie.mov, first frame=some_movie.mov.jpg
                # don't add the info
                # (( "$VERBOSE" )) && 
                DebugMessage -e "$f1 appears to be a movie frame.  not adding to image list."
            else
                if [[ $RESERVED_IMAGE -eq 0 ]] ; then
                    # get the settings from the properties file via the environment
                    . "$PROPERTIES_FILENAME"
                    LAST_IMAGE="$LAST_PIC"
                    CURRENT_IMAGE_FILENAME="$THIS_PIC"
                    NEXT_IMAGE="$NEXT_PIC"
                    # try to do a proper design pattern Builder class
                    if [[ ! -f "$CURRENT_IMAGE_FILENAME" ]] ; then
                        DebugMessage -e "ERROR: $CURRENT_IMAGE_FILENAME does not exist.  Exiting abnormally!"
                        exit -6
                    else
                        BuildPage $VERBOSE_ARG "./$TEMPLATE_DIR/$OBJECT_INDEX_PROTOTYPE" "$NEW_PAGE_FILENAME" "$LAST_IMAGE" "$CURRENT_IMAGE_FILENAME" "$NEXT_IMAGE"
                    fi
                fi
            fi
        fi
    fi
}


######################################################
# Setup Environment
######################################################
TEMPLATE_FILE="$DEFAULT_PROTOTYPE"
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}


######################################################
# - Main -
######################################################

# if the VERBOSE arg is NULL, dump it
if [ -z "$1" ] ; then
    shift
fi

# Until all parameters used up...
until [ -z "$1" ] ; do
    if [ ! -z `expr "$1" : "\(-\)"` ] ; then
        if [ ! -z `expr "$1" : "\(-t\)"` ] ; then
            shift
            TEMPLATE_FILE="$1"
            if [ -f "$TEMPLATE_DIR/$TEMPLATE_FILE" ] ; then
                OBJECT_INDEX_PROTOTYPE="$TEMPLATE_FILE"
            else
                DebugMessage "INVALID Picture Index file: \"$TEMPLATE_DIR/$TEMPLATE_FILE\"!  Exiting abnormally!"
                exit -1
            fi
        fi
        if [ ! -z `expr "$1" : "\(--template=\)"` ] ; then
            TEMPLATE_FILE="${1#--template=}"
            if [ -f "$TEMPLATE_DIR/$TEMPLATE_FILE" ] ; then
                OBJECT_INDEX_PROTOTYPE="$TEMPLATE_FILE"
            else
                DebugMessage "INVALID Picture Index file: \"$TEMPLATE_DIR/$TEMPLATE_FILE\"!  Exiting abnormally!"
                exit -1
            fi
        fi
        if [ ! -z `expr "$1" : "\(-v\)"` ] ; then
            VERBOSE=1
        fi
    fi
    LAST_PARAM=$1
    shift
done
if [[ ! -z "$LAST_PARAM" ]] ; then
    PICTURE_FILENAME="$LAST_PARAM"       # ==> Otherwise, move to indicated directory.
else
    DebugMessage "ERROR: the last argument must the the name of the picture to create a page for!  Exiting abnormally!"
    exit -1
fi
THUMB_DIR="$THUMB_DIR_BASENAME$THUMB_SIZE"
NEW_PAGE_FILENAME="./$HTML_DIR/${OBJECT_INDEX_PROTOTYPE/%.html/}_$PICTURE_FILENAME.html"
PROPERTIES_FILENAME="./$HTML_DIR/.`basename "$PICTURE_FILENAME"`.properties"

if (( "$VERBOSE" )) ; then
    DebugMessage -e "\n$0 called"
    DebugMessage "Program Options:"
    DebugMessage "   VERBOSE is on."
    DebugMessage "   PICTURE_FILENAME=$PICTURE_FILENAME"
    DebugMessage "   PROPERTIES_FILENAME=$PROPERTIES_FILENAME"
    DebugMessage "   NEW_PAGE_FILENAME=$NEW_PAGE_FILENAME"
    DebugMessage "   TEMPLATE_FILE=$TEMPLATE_FILE"
    DebugMessage "   Base HTML template=\"${PWD}/${TEMPLATE_DIR}/${OBJECT_INDEX_PROTOTYPE}\""
    DebugMessage "   OBJECT_INDEX_PROTOTYPE=${PWD}/${OBJECT_INDEX_PROTOTYPE}"
    DebugMessage "   AVAILABLE_RESOLUTIONS_DELIMITER=$AVAILABLE_RESOLUTIONS_DELIMITER"
    VERBOSE_ARG="-v"
fi

ProcessPictureFile "$PICTURE_FILENAME" "$NEW_PAGE_FILENAME" "$PROPERTIES_FILENAME"

(( "$VERBOSE" )) && DebugMessage "complete, exiting normally."
exit 0

