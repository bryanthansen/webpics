#!/bin/bash
# Bryant Hansen
# 20121020

USAGE="$0 picture_filename"

###############################################################################
# Constants
###############################################################################

IMAGE_SUFFIXES="jpg JPG png"
IMAGE_REGEX="${IMAGE_SUFFIXES// /\$\\|.*\\.}"
IMAGE_REGEX=".*\.${IMAGE_REGEX}\$"
SED_IMAGE_PRINT_REGEX="/${IMAGE_REGEX}/p"
SED_INCREASE_INDENT="s/^/\ \ /"
SED_TABS_TO_2_SPACES_REGEX="s/\t/\ \ /g"


###############################################################################
# Sanity-checking
###############################################################################
if [[ ! "$1" ]] ; then
        echo "$0 ERROR: this script requires a picture filename argument." >&2
        echo "USAGE: $USAGE" >&2
        echo "Exiting abnormally" >&2
        exit 1
fi


###############################################################################
# Functions
###############################################################################

dump_header() {
    title="Photo Album"
    series="$(dirname "$PWD")"
    dir="$(basename "$PWD")"
    [[ -f "./${dir}".txt ]] && title="$(cat "./${dir}".txt | head -n 1)"
    [[ -f "./title.txt" ]] && title="$(cat "./title.txt".txt | head -n 1)"
    [[ -f "./series.txt" ]] && title="$(cat "./series.txt".txt | head -n 1)"

    echo -e "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
<!-- produced by $0 on $(date) -->
<album>
  <title>${title}</title>
  <series>${series}</series>
  <pictures>"
}

dump_picture() {
    echo -e "<picture>"
    /projects/webpics/MakePictureXmlRecord.sh "$1" | sed "$SED_INCREASE_INDENT"
    echo -e "</picture>"
}

dump_footer() {
    echo -e "  </pictures>"
    echo -e "</album>"
}

dump_picture_dir() {
    echo "processing album of images matching regex: $IMAGE_REGEX" >&2
    echo "SED_IMAGE_PRINT_REGEX: $SED_IMAGE_PRINT_REGEX" >&2
    ls -1 "$1" | \
    sed -n "$SED_IMAGE_PRINT_REGEX" | \
    sort | \
    while read f ; do
        dump_picture "$f"
    done | \
    sed "$SED_INCREASE_INDENT" | \
    sed "$SED_TABS_TO_2_SPACES_REGEX"
}


###############################################################################
# Main
###############################################################################

dump_header

# if a directory argument is provided
if [[ -d "$1" ]] ; then
    dump_picture_dir "$1"
else
    while [[ "$1" ]] ; do
        dump_picture "$1"
        shift
    done
fi

dump_footer

exit 0
