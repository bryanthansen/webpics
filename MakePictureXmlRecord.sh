#!/bin/bash
# Bryant Hansen
# 20121020

USAGE="$0 picture_filename"


###############################################################################
# Constants
###############################################################################

EXIF_2_XML=/projects/webpics/exif2xml.sh
SED_INCREASE_INDENT="s/^/\ \ /"


###############################################################################
# Functions
###############################################################################

dump_picture() {
    echo -e "<filename>$1</filename>"
    filedate="$(stat -c "%y" "$1")"
    echo -e "<filedate>${filedate}</filedate>"
    filedateshort="${filedate/.* / GMT}"
    echo -e "<filedateshort>${filedateshort}</filedateshort>"
    if [[ -f "copyright.txt" ]] ; then
        echo -e "<copyright>$(cat copyright.txt)</copyright>"
    fi
    if [[ -f "$1".txt ]] ; then
        echo -e "<description>$(cat "$1".txt)</description>"
    else
        echo -e "<description></description>"
    fi
    if ls .thumbnails* > /dev/null 2>&1 ; then
        echo -e "<thumbnails>"
        for f in .thumbnails*/"${1}" ; do
            dir="$(dirname "$f")"
            size="${dir#.thumbnails}"
            if [[ "$size" -gt 0 ]] ; then
                echo "thumbnail ${f}, size=${size}" >&2
                echo -e "<thumbnail size=\"${size}\">"
            else
                echo "thumbnail ${f}, failed to get size" >&2
                echo -e "<thumbnail>"
            fi
            echo -e "\t<filename>${f}</filename>"
            echo -e "\t<filedate>${filedate}</filedate>"
            echo -e "\t<filedateshort>${filedateshort}</filedateshort>"
            echo -e "</thumbnail>"
        done | \
        sed "$SED_INCREASE_INDENT"
        echo -e "</thumbnails>"
    fi

    echo -e -n "<imagemagick_identify>"
    $EXIF_2_XML ./.image_info/"$1".txt 2>/dev/null | sed "$SED_INCREASE_INDENT"
    echo -e "</imagemagick_identify>"
}


###############################################################################
# Main
###############################################################################

if [[ ! "$1" ]] ; then
    echo "$0 ERROR: this script requires a picture filename argument." >&2
    echo "USAGE: $USAGE" >&2
    echo "Exiting abnormally" >&2
    exit 1
fi

dump_picture "$1"

exit 0
