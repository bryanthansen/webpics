#!/usr/bin/python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeMovieList.py
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# created: 20080925
# last updated: 20080925
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

""" Program MakeSubsetList.py

This program makes an ordered list of all image subset files in the directory that should be included in the photo
album.  The list file will not be updated unless it has changed.

Note: there will be no dependency checking in this module.  It would involve checking the entire directory contents,
which would take longer than the list operation that it's intended to do.  The resulting output file should be used
for dependency checking within other modules.

"""

import glob
import os
import fnmatch
import webpics
import binascii

def usage():
	print "USAGE: " + sys.argv[0] + " [-d | --debug]"

infile = "sortorder.txt"
outfile = ".webpics_subsetlist.txt"


class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg


def extListMatch(name, extList):
	for ext in extList:
		if fnmatch.fnmatch(name, ext):
			return True
	return False


def main(argv=None):

	# first make a list from the sortorder.txt file
	# then continue the list with the remaining images
	outfile_tmp = outfile + "." + binascii.b2a_hex(os.urandom(6)) + ".tmp"
	fout = open(outfile_tmp, 'w')
	if os.path.exists(infile):
		fin = open(infile, 'r')
		try:
			for line in fin:
				dir = line.strip()
				if (os.path.isdir(dir)):
					if webpics.isNameInFileList(dir, "excludes.txt"):
						webpics.DebugMessage ("WARNING: the file \"" + dir + "\" is in both the sortorder and the " \
							+ "excludes file.  Excluding file.")
					else:
						try:
							fout.write(dir + "\n")
						except:
							webpics.DebugMessage ("ERROR: cannot write to output file \"" \
								+ os.path.join(os.path.getcwd(), outfile_tmp) + "\"!  Exiting abnormally!")
							return -2
		finally:
			fin.close()

	# FIXME: verify that ordering is alphabetical
	for dir in os.listdir(os.getcwd()):
		if os.path.isdir(dir) and not dir[0:1] == ".":
			if not webpics.isNameInFileList(dir, "excludes.txt"):
				if not webpics.isNameInFileList(dir, "sortorder.txt"):
					try:
						fout.writelines(dir + "\n")
					except:
						webpics.DebugMessage ("ERROR: cannot write to output file \"" \
							+ os.path.join(os.path.getcwd(), outfile_tmp) + "\"!  Exiting abnormally!")
						return -2

	fout.close()

	# if the file has changed, update it
	if not os.path.exists(outfile) or webpics.textdiff(outfile, outfile_tmp):
		# shutil.move(outfile_tmp, outfile)
		# os.rename(outfile_tmp, outfile)
		os.system("cat \"" + outfile_tmp + "\" > \"" + outfile + "\"")
	
	os.remove(outfile_tmp)
	
if __name__=="__main__":
	main()
