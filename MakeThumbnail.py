#!/usr/bin/python                                                                                                                                                             
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeThumbnail.py
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# last updated: 20080907
# last updated: 20080907
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

""" Program MakeImageInfo.py

This program will create a thumbnail version of an image.
"""
import os
import sys
import subprocess
import re
import getopt
import webpics

USAGE=os.path.basename(sys.argv[0]) + " [ -v | --verbose ] [ -b | --blended-border ] [ -f | --force-create] [ -s x | --size=x ] directory"
HELP=" -v | --verbose 			verbose output 																	\
\n -s n | --size=n 	set the size of the thumbnail.  n is the maximum dimension (height or width); the smaller dimension is scaled proportionally to the aspect ratio \
\n -b | --blended-border 	add a border to every thumbnail that blends into the background 					\
\n -f | --force 			force thumbnail creation, even if the resulting image is larger than the original" 

# CONF_FILE="album.conf"
# see CONF_FILE for additional variables that are used as options

# set programs
CONVERT="convert"

thumb_dir_base=".thumbnails"

# default value
thumbsize="0"
thumb_dir=""

picture_dir=""
verbose=""
force=""
allow_image_growth="N"
blended_border=0

force=""

# TODO: set this somewhere more logical...or, better yet, use a transparent background when making a blended border; it had this working once before, but not time to debug the script right now
background_color="black"

SET_EXCLUDES_PERMISSIONS=1

def usage():
    print ("USAGE: %s" % USAGE)

def WaitForKey():
    raw_input("Press Enter to continue")
    return 0

def DebugMessage(message):
    print (message)
    return 0

def FixThumbDirVisibleToHidden(thumb_dir):

    # FixThumbDirs: if there is already a directory that's not hidden (old style)
    # 

    # if we're using hidden directories now
    if thumb_dir[0:1] == ".":
        # but the non-hidden directory exists and the hidden one does not
        if not os.path.exists(thumb_dir) and os.path.exists(thumb_dir[1:]):
            os.path.create_link(thumb_dir[1:], thumb_dir)


def IsThumbnailLargerThanPic(pic, size):
    # this should actually be name "IsThumbnailEqualToOrLargerThanPic", however, that's too long
    # if the thumbnail is to be of equal size of the picture, it should normally not need to be generated; it would be sufficient to link to the picture

    w = webpics.getPicWidth(pic)
    h = webpics.getPicHeight(pic)
    if verbose: DebugMessage ("file=" + pic + ", WIDTH=" + str(w) + ", HEIGHT=" + str(h) + ", thumbsize=" + str(size))

    # if either the width or the height are greater than the thumbnail size, the image must be scaled down to that size
    s = int(size)
    if (s >= w) and (s >= h):
        # the thumbnail image would be smaller than the original - scaled to the maximum dimension
        return True
    else:
        return False


######################################################
# - Main -
######################################################

def main(argv=None):

    global deps
    global force

    thumbsize=0

    # check_dependencies(deps)

    # some code borrowed from http://docs.python.org/lib/module-getopt.html
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ho:s:vfd", ["help", "outfile=", "size=", "verbose", "force", "debug"])
    except:
        err = sys.exc_info()[0]
        # print help information and exit:
        print (str(err)) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    PICTURE = "".join(args)
    if PICTURE == "":
            print ("%s ERROR: input file not specified.  This program requires at least 1 argument.  Exiting abnormally!" % sys.argv[0])
            usage()
            sys.exit(-3)

    if not os.path.exists(PICTURE):
        if os.path.exists(os.path.join("..", PICTURE)):
            PICTURE=os.path.join("..", PICTURE)
        else:
            print ("%s ERROR: input file, %s does not exist!  Exiting abnormally!" % (sys.argv[0], PICTURE))
            usage()
            sys.exit(-4)

    # build a list of dependencies
    outfile_deps = [ PICTURE ]
    global conf_file
    for opt,arg in opts:
            if opt in ("-h", "--help"):
                    usage()
                    sys.exit(0)
            elif opt in ("-d", "--debug"):
                    global debug
                    debug = 1
            elif opt in ("-v", "--verbose"):
                    global verbose
                    verbose = 1
            elif opt in ("-b", "--blended-border"):
                    global blended_border
                    blended_border = 1
            elif opt in ("-f", "--force"):
                    global force
                    force = 1
            elif opt in ("-o", "--outfile"):
                    outfile = arg
            elif opt in ("-s", "--size"):
                    thumbsize = arg


    if blended_border:
        if not force:
            DebugMessage ("Auto-Enabling ForceCreate feature.  BlendedBorder is enabled, and ForceCreate will prevent a link being created to the original image - not having a blended border - which is done in certain circumstancces.")
            force=1
        else:
            DebugMessage ("BlendedBorder feature is enabled")

    picture_dir=os.path.dirname(PICTURE)

    # if thumbsize is not valid/unspecified, we might be executing from within a thumbnail directory
    if thumbsize <= 0:
        # in this case, we can extract the thumb size from the directory name
        DIR_NAME=os.path.basename(os.getcwd())
        # does it match .thumbnailsNNNN?
        pattern = "^" + thumb_dir_base + "[0-9]*$"
        if not re.compile(pattern).match(DIR_NAME):
            DebugMessage ("Warning: the thumbnail size is not specified, either on the command line or by the current directory name, " + os.path.basename(os.getcwd()))
            DebugMessage ("Exiting abnormally.");
            exit (-5)
        else:
            # for now, use absolute paths
            thumb_dir=os.getcwd()
            thumbsize=int(DIR_NAME.replace(thumb_dir_base,""))
            if thumbsize > 0:
                if verbose: 
                    DebugMessage ("Extracted the thumbnail size from the present working dir, " \
                        + os.getcwd() + ".  Size = " + str(thumbsize))
    else:
        thumb_dir = os.path.join(picture_dir, thumb_dir_base + thumbsize)


    if not os.path.isdir(thumb_dir): os.mkdir(thumb_dir)

    if thumbsize <= 0:
        DebugMessage ("thumbsize is invalid: " + thumbsize + "!  Exiting abnormally!")
        exit (-5)

    if verbose:
        DebugMessage ("Program Options:")
        DebugMessage ("   verbose is on.")
        DebugMessage ("   PWD=" + os.getcwd())
        DebugMessage ("   thumbsize=" + str(thumbsize))
        DebugMessage ("   PICTURE=" + PICTURE)
        DebugMessage ("   picture_dir=" + picture_dir)
        DebugMessage ("   blended_border=" + str(blended_border))
        DebugMessage ("   force=" + str(force))
        DebugMessage ("   thumb_dir=" + thumb_dir)

    # PICTURE will be an absolute path; no need to join
    # infile = os.path.join(picture_dir, PICTURE)
    infile = PICTURE
    if not os.path.exists(infile):
        DebugMessage ("infile \"" + infile + "\" not found.  Exiting abnormally!")
        exit (-4)

    EXCLUDES_FILE="excludes.txt"
    if webpics.isNameInFileList (os.path.basename(infile), EXCLUDES_FILE):
        return 0
    elif webpics.isNameInFileList (os.path.basename(infile), os.path.join(thumb_dir, EXCLUDES_FILE)):
        return 0

    outfile = os.path.join(thumb_dir, os.path.basename(PICTURE))
    if not os.path.isdir(thumb_dir):
        os.makedirs(thumb_dir)

    raw_settings = "-resize " + str(thumbsize) + "x" + str(thumbsize) + "+0+0"
    ccmd = CONVERT + " " + raw_settings + " " + infile + " " + outfile

    if verbose: DebugMessage ("ccmd = " + ccmd)

    thumbnail_script="/projects/webpics/MakeBlendedBorderThumbnail.sh"

    # first check if we should just create a link to the original picture
    if IsThumbnailLargerThanPic(infile, thumbsize) and (allow_image_growth != "Y") and not force:
        # determine the relative link to infile from outfile
        outfile_dir = webpics.make_rel_path(os.path.dirname(infile), os.path.dirname(outfile))
        outfile_link = os.path.join(outfile_dir, os.path.basename(infile))
        if os.path.exists(outfile):
            DebugMessage (outfile + " already exists, but is larger than " + infile + "!  Removing and replacing with link.  " + outfile + " -> " + outfile_link)
        os.symlink(outfile_link, outfile)
    else:
        if os.path.islink(infile):
            webpics.handle_link(infile, outfile)
        if force or not webpics.is_fresh(outfile, outfile_deps):
            DebugMessage ("Making " + thumbsize + "-pixel thumbnail for " + PICTURE)
            if blended_border:
                ccmd = CONVERT + " " + raw_settings + " " + infile + " " + outfile 
                ccmd = "%s %s %s %s\n" % (
                            thumbnail_script,
                            infile,
                            outfile,
                            thumbsize,
                            background_color
                        )
            webpics.do_safe_command(ccmd, outfile)

#	ProcessFile PICTURE

#	CreateThumbnail (PICTURE, os.join(thumb_dir, os.path.basename(PICTURE)))

    if verbose: DebugMessage ("basename " + os.path.basename(sys.argv[0]) + " complete.  Exiting normally.")

if __name__=="__main__":
    main()

