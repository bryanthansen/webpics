# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeThumbnail.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# last updated: 20080426
#
# copyright (c) 2003-2008 by Bryant Hansen
# all rights reserved
##################################################################
#!/bin/sh

USAGE="$0 [ -v ] [ -b ] [ -f ] [ --size=x ] directory"
HELP=" -v | --verbose 			verbose output 																	\
\n -s n | --size=n 	set the size of the thumbnail.  n is the maximum dimension (height or width); the smaller dimension is scaled proportionally to the aspect ratio \
\n -b | --blended-border 	add a border to every thumbnail that blends into the background 					\
\n -f | --force 			force thumbnail creation, even if the resulting image is larger than the original" 

# CONF_FILE="album.conf"
# see CONF_FILE for additional variables that are used as options

# set programs
CONVERT="/usr/bin/convert"

THUMB_DIR_BASE=".thumbnails"
IMAGE_INFO_DIR=".image_info"

# default value
THUMB_SIZE="0"
THUMB_DIR=""

PICTURE_DIR=""
TEMPLATE_DIR="templates"
HTML_DIR="html"
VERBOSE=""
RECURSE=""
FORCE_CREATE=""
ALLOW_IMAGE_GROWTH="N"
MOVIE_MODE=0
BLENDED_BORDER=0

LAUNCH_DIR="$PWD" # maintain the value if something changes

FORCE_CREATE=""

SET_EXCLUDES_PERMISSIONS=1

# some sed commands we'll need here; string them together to do something intelligent, but understandable in source
SED_COMMAND_EXTRACT_ENDING="s/.*\.//g"
SED_COMMAND_REMOVE_LINK_INFO="s/ -> .*//g"
SED_COMMAND_TO_LOWER_CASE="y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
SED_COMMAND_PRINT_IMAGE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)/p" 	# used with sed -n
SED_COMMAND_PRINT_MOVIE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(mov\|avi\|mpi\|3gp\|mp4\)/p" 				# used with sed -n

WaitForKey()
{
	local tmp
	echo "Press Any Key" 1>&2
	read -n 1 'tmp' <&1
#	read -n 1
	return 0
}

DebugMessage()
{
	local NO_CR_ARG=""
	local SPECIAL_CHAR_ARG=""
	local ARGS=""
	local OPTION_FOUND="0"
	local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`($THUMB_SIZE): "

	while [[ "$OPTION_FOUND" ]] ; do
		OPTION_FOUND=""
		if [[ "$1" = "-e" ]] ; then
			OPTION_FOUND="-e"
			ARGS="${ARGS} -e"
			shift
		fi
		if [[ "$1" = "-n" ]] ; then
			OPTION_FOUND="-n"
			ARGS="${ARGS} -n"
			LOG_RECORD_HEADER=""
			shift
		fi
	done		

	echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

	return 0
}

CreateThumbnail () {
	#	file="$1"
	local file="$1"
	local thumbnail="$2"

	(( "$VERBOSE" )) && DebugMessage "Enter CreateThumbnail: file=$1, thumbnail=$2"

	if [[ -f "$thumbnail" ]] ; then
		if [[ "$thumbnail" -nt "$file" ]] && [[ ! "$FORCE_CREATE" ]] ; then
			(( "$VERBOSE" )) && DebugMessage "the thumbnail (\"$thumbnail\") for \"$file\" already exists and is up to date."
			exit 0
		fi
	fi

	(( "$VERBOSE" )) && DebugMessage "creating \"$thumbnail\""
	# don't allow a thumbnail to be larger than the original image
	# if this condition is true, create a shortcut instead
	local WIDTH
	local HEIGHT
	local ExifFile="`dirname "${file}"`/${IMAGE_INFO_DIR}/`basename "${file}"`.txt"
	if [[ -f "$ExifFile" ]] ; then
		WIDTH="`cat "${ExifFile}" | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t]*//;s/x.*//g"`"
		HEIGHT="`cat "${ExifFile}" | sed -n "/^[ \t]*Geometry:/p" | sed "s/[ \t]*Geometry:[ \t].*x//;s/+.*//g"`"
#		(( "$VERBOSE" )) && 
            DebugMessage "reading dimensions from info file: ${ExifFile}.  w=$WIDTH, h=$HEIGHT"
	fi
	if [[ ! "$WIDTH" ]] || [[ ! "$HEIGHT" ]] ; then
		DebugMessage "ERROR2: failed to read width/height from exif file.  Setting to 0 and continuing."
		WIDTH=0
		HEIGHT=0
	fi
	if (( "$WIDTH" <= "0" )) || (( "$HEIGHT" <= "0" )) ; then
		# (( "$VERBOSE" )) &&
		DebugMessage "Exif data not properly read from \"${ExifFile}\".  Reading manually...."
		WIDTH="`identify -format \"%w\" \"$file\"`"
		if [[ "$?" -ne "0" ]] ; then
			FORCE_CREATE=1 # can't id anything on the image; make sure we don't link to something invalid
			WIDTH=0
		fi
		HEIGHT="`identify -format \"%h\" \"$file\"`"
		if [[ "$?" -ne "0" ]] ; then
			FORCE_CREATE=1 # can't id the image; make thumb for sure
			HEIGHT=0
		fi
	fi

	(( "$VERBOSE" )) && DebugMessage "file=$file, WIDTH=$WIDTH, HEIGHT=$HEIGHT, THUMB_SIZE=$THUMB_SIZE"
	if [[ "$WIDTH" -gt "$THUMB_SIZE" ]] 	\
	|| [[ "$HEIGHT" -gt "$THUMB_SIZE" ]] 	\
	|| [[ "$ALLOW_IMAGE_GROWTH" = "Y" ]] 	\
	|| [[ "$FORCE_CREATE" ]] ; then
		ThumbFilename="${thumbnail}.tmp.${thumbnail//*./}"
		DebugMessage "Creating thumbnail for \"$THUMB_DIR/$file\".  original size (WxH) = (${WIDTH}x${HEIGHT})"
		[[ -f "$ThumbFilename" ]] && rm "$ThumbFilename"
		if (( "$BLENDED_BORDER" )) ; then
			DebugMessage "Experimental feature: MakeBlendedBorderThumbnail \"$file\" \"${thumbnail}.tmp\" \"${THUMB_SIZE}\""
			MakeBlendedBorderThumbnail "$file" "$ThumbFilename" "${THUMB_SIZE}"
		else
			(( "$VERBOSE" )) && DebugMessage "executing $CONVERT -resize ${THUMB_SIZE}x${THUMB_SIZE}+0+0 \"$file\" \"${ThumbFilename}\""
			$CONVERT -resize ${THUMB_SIZE}x${THUMB_SIZE}+0+0 "$file" "$ThumbFilename"
		fi
		if [[ ! -f "${ThumbFilename}" ]] ; then
			DebugMessage "ERROR failed to create \"${ThumbFilename}\"!  Exiting abnormally!"
			exit -2
		fi
		mv "${ThumbFilename}" "${thumbnail}"
	else
#		DebugMessage "$file: size = ${WIDTH}x${HEIGHT}`, both dimensions are less than requested thumbnail size of $THUMB_SIZE.  "
		DebugMessage "  \"$file\" is smaller that the requested thumbnail size of $THUMB_SIZE.  Creating a link instead of a thumbnail."
		ln -s "$file" "$THUMB_DIR/"
	fi
}

CreateMovieThumbnail () {

	local file="$1"
	local thumbnail="$2"

	if [[ ! -f "$file" ]] ; then
		DebugMessage "CreateMovieThumbnail ERROR: the file \"$file\" does not exist.  Exiting abnormally!"
		exit -3
	fi

	if [[ -f "$thumbnail" ]] ; then
		if [[ "$thumbnail" -nt "$file" ]] && [[ ! "$FORCE_CREATE" ]] ; then
			(( "$VERBOSE" )) && DebugMessage "the thumbnail for the movie \"$file\" already exists and is up to date."
			exit 0
		fi
	fi

	(( "$VERBOSE" )) && DebugMessage "creating \"$thumbnail\""
	mplayer -vo jpeg "$file" -frames 1 -ao null
	# TODO: how to have mplayer put out a filename other than 00000001.jpg (-o didn't work, with mencoder neither!)
	mv 00000001.jpg "$thumbnail"
	(( "$VERBOSE" )) && DebugMessage "executing \"mplayer -vo jpeg "$file" -frames 1\""

}

CreateJPGfromRaw () {

	local file="$1"
	if [[ ! -f "$file" ]] ; then
		DebugMessage "CreateJPGfromRaw ERROR: the file \"$file\" does not exist.  Exiting abnormally!"
		exit -3
	fi

	local newfile="$2"
	if [[ -z "$newfile" ]] ; then
		DebugMessage "CreateJPGfromRaw ERROR: This function requires a second argument.  Exiting abnormally!"
		exit -4
	fi

	RAW_SETTINGS=""
	[[ -f "${HOME}/.ufrawrc" ]] && RAW_SETTINGS="${HOME}.ufrawrc"
	[[ -f "./.ufrawrc" ]] 	&& RAW_SETTINGS="./.ufrawrc"
	[[ -f ".${file}.ufrawrc" ]] 	&& RAW_SETTINGS=".${file}.ufrawrc"
	if [[ "${RAW_SETTINGS}" ]] ; then
		(( "$VERBOSE" )) && DebugMessage "${file} ufraw conversion config = ${RAW_SETTINGS}"
	fi

	if [[ -f "${newfile}" ]] && [[ "${newfile}" -nt "${file}" ]] && [[ "${newfile}" -nt "${RAW_SETTINGS}" ]] ; then
		(( "$VERBOSE" )) && DebugMessage "The jpg version of the raw file \"${newfile}\" already exists and is up-to-date!"
	else
		[[ -f "${newfile}.tmp" ]] && rm "${newfile}.tmp"
		DebugMessage -n "Creating jpeg version of ${file}..."
		if [[ "${RAW_SETTINGS}" ]] ; then
			ufraw-batch "${file}" --conf="${RAW_SETTINGS}" --out-type=jpeg --compression=100 --output="${newfile}.tmp"
		else
			ufraw-batch "${file}" --out-type=jpeg --compression=100 --output="${newfile}.tmp"
		fi
		if [[ ! -f "${newfile}.tmp" ]] ; then
			DebugMessage "ERROR failed to create \"${newfile}.tmp\"!  Exiting abnormally!"
			exit -2
		fi
		mv "${newfile}.tmp" "${newfile}"
		DebugMessage "${file} created."
	fi
}

ProcessFile () {

	(( "$VERBOSE" )) && DebugMessage "Enter ProcessFile: Creating ${THUMB_SIZE} pixel thumbnail for \"$1\" (thumb dir=$THUMB_DIR)..."

	local file="$1"
	if [[ ! -f "$file" ]] ; then
		DebugMessage "ProcessFile ERROR: the file \"$file\" does not exist.  Exiting abnormally!"
		exit -3
	fi

	if [[ -f "$file" ]] ; then # or is it a file

		IsMovieFile="`echo "$file" | sed -n "$SED_COMMAND_PRINT_MOVIE_TYPE"`"
		if [[ $IsMovieFile ]] ; then
			CreateMovieThumbnail "$file" "${file}.jpg"
			# set the current file-under-operation to jpg
			file="${file}.jpg"
		fi

#		IsRawFormatFile="`echo "$file" | sed -n "$SED_COMMAND_IS_CANON_RAW_FMT"`"
#		if [[ "$IsRawFormatFile" ]] ; then
#			(( "$VERBOSE" )) && DebugMessage "$file is a raw format file (inferred by filename ending)!"
#			newfile="${file%.CR2}.jpg"  # this must match the naming conventions of ufraw-batch
#			CreateJPGfromRaw "$file" "$newfile"
#			# the new jpeg file is the master file that we'll use now
#			file="${newfile}"
#		fi

		IsPictureFile="`echo "$file" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
		if [[ $IsPictureFile ]] ; then
			CreateThumbnail "$file" "$THUMB_DIR/`basename "$file"`"
		fi

	else
		echo "ERROR: file does not exist!"
	fi
}


######################################################
# Setup Environment
######################################################

# process the conf file
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}

SED_COMMAND_FILE="`dirname "$0"`/CommonSedCommands.sh"
[[ -f "./${SED_COMMAND_FILE}" ]]            && . ./${SED_COMMAND_FILE}


######################################################
# - Main -
######################################################

# is there a better way to do this that's non-destructive (unlike shift in a loop)?
#if (( $# == 0 )) ; then
  (( "$VERBOSE" )) && echo "`basename "$0"` $* called" 1>&2
#fi

if [[ -z "$1" ]] ; then
   shift
fi

# process command-line args
until [[ -z "$1" ]]  # Until all parameters used up...
do
   DebugMessage "arg=$1"
   if [[ ! -z `expr "$1" : "\(-\)"` ]] ; then
      if [[ ! -z `expr "$1" : "\(--size=\)"` ]] ; then
         THUMB_SIZE_ARG=${1#--size=}
         if [[ "$THUMB_SIZE_ARG" -gt "0" ]] ; then
            THUMB_SIZE=$THUMB_SIZE_ARG
         else
            DebugMessage "INVALID Thumbnail size arg: $1"
         fi
      fi
      if [[ ! -z `expr "$1" : "\(-s\)"` ]] ; then
         shift
         THUMB_SIZE_ARG="$1"
         if [[ "$THUMB_SIZE_ARG" -gt "0" ]] ; then
            THUMB_SIZE=$THUMB_SIZE_ARG
         else
            DebugMessage "INVALID Thumbnail size arg: $1"
         fi
      fi
      if [[ ! -z `expr "$1" : "\(--verbose\)"` ]] \
      || [[ ! -z `expr "$1" : "\(-v\)"` ]] ; then
         VERBOSE=1
      fi
      if [[ ! -z `expr "$1" : "\(--blended-border\)"` ]] \
      || [[ ! -z `expr "$1" : "\(-b\)"` ]] ; then
         BLENDED_BORDER=1
      fi
      if [[ ! -z `expr "$1" : "\(--force\)"` ]] \
      || [[ ! -z `expr "$1" : "\(-f\)"` ]] ; then
         FORCE_CREATE=1
      fi
   fi
   LAST_PARAM=$1
   shift
done
DebugMessage "LAST_PARAM=$LAST_PARAM"

if (( "$BLENDED_BORDER" )) ; then
	if ! (( "$FORCE_CREATE" )) ; then
        	 DebugMessage "Auto-Enabling ForceCreate feature.  BlendedBorder is enabled, and ForceCreate will prevent a link being created to the original image - not having a blended border - which is done in certain circumstancces."
	         FORCE_CREATE=1
	else
        	 DebugMessage "BlendedBorder feature is enabled"
	fi
fi

if [[ ! -z "$LAST_PARAM" ]]; then
	if [[ -f "$LAST_PARAM" ]]; then
		PICTURE="$LAST_PARAM"
	else
		DebugMessage "ERROR: input picture \"$PICTURE\" not found!  Exiting abnormally!"
		exit -2
	fi
else
   DebugMessage "USAGE: $USAGE"
   DebugMessage "ERROR: picture not specified!  Exiting abnormally!"
   exit -1
fi

PICTURE_DIR="`dirname "$PICTURE"`"
[[ -f "$PICTURE_DIR/${CONF_FILE}" ]]  && . "$PICTURE_DIR/${CONF_FILE}"

if (( "$THUMB_SIZE" <= "0" )) ; then
	DIR_NAME="`basename "$PWD"`"
	if [[ ! "${DIR_NAME##.thumbnails[0-9]*}" ]] ; then 	# does it match .thumbnailsNNNN?
		# for now, use absolute paths
		THUMB_DIR="$PWD"
		THUMB_SIZE="${DIR_NAME/*[!0-9]/}"
		if (( "$THUMB_SIZE" > "0" )) ; then
			(( "$VERBOSE" )) && DebugMessage "Extracted the thumbnail size from the present working dir, $PWD.  Size = $THUMB_SIZE"
		fi
	fi
fi

if (( "$THUMB_SIZE" <= "0" )) ; then
	DebugMessage "THUMB_SIZE is invalid: ${THUMB_SIZE}!  Exiting abnormally!"
	# maybe this is contained within the picture dir
	exit -5
fi

[[ ! "$THUMB_DIR" ]] && THUMB_DIR="${THUMB_DIR_BASE}${THUMB_SIZE}"
[[ ! -d "$THUMB_DIR" ]] && mkdir "$THUMB_DIR"

if (( "$VERBOSE" )) ; then
   DebugMessage "Program Options:"
   DebugMessage "   VERBOSE is on."
   DebugMessage "   PWD=$PWD"
   DebugMessage "   THUMB_SIZE=$THUMB_SIZE"
   DebugMessage "   PICTURE=$PICTURE"
   DebugMessage "   PICTURE_DIR=$PICTURE_DIR"
   DebugMessage "   BLENDED_BORDER=$BLENDED_BORDER"
   DebugMessage "   FORCE_CREATE=$FORCE_CREATE"
   DebugMessage "   THUMB_DIR=$THUMB_DIR"
fi

ProcessFile "$PICTURE"

(( "$VERBOSE" )) && DebugMessage "`basename "$0"` complete.  Exiting normally."
exit 0

