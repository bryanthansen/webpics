#!/usr/bin/python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeThumbnails.py
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# created: 20080922
# last updated: 20080922
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

""" Program MakeThumbnails.py

This program loops through all image files in a directory and calls another script to create thumbnails for each of them.
"""

import os
import sys
import glob
import getopt

thumbsize="0"

def usage():
	print "USAGE: " + sys.argv[0] + " [-d | --debug]"

makethumbnail = "/projects/webpics/MakeThumbnail.py"
deps = [makethumbnail]

image_extensions = ["*.[Jj][Pp][Gg]", "*.[Gg][Ii][Ff]", "*.tiff", "*.[Pp][Nn][Gg]", "*.[Bb][Mm][Pp]"]

class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg

def check_dependencies(deps):
	print "TODO: check deps"

def main(argv=None):

		# some code borrowed from http://docs.python.org/lib/module-getopt.html
	try:
		opts, args = getopt.getopt(sys.argv[1:], "h:s:vfd", ["help", "size=", "force", "verbose", "debug"])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	extra = "".join(args)
	if extra != "":
			print sys.argv[0], " ERROR: no additional arguments are accepted beyond the basic options!  Remove \"" + extra + "\".  Exiting abnormally!"
			usage()
			sys.exit(-3)

	# build a list of dependencies
	global conf_file
	settings = ""
	for opt,arg in opts:
			if opt in ("-h", "--help"):
					usage()
					sys.exit(0)
			elif opt in ("-d", "--debug"):
					global debug
					debug = 1
					settings += "--debug "
			elif opt in ("-v", "--verbose"):
					global verbose
					verbose = 1
					settings += "--verbose "
			elif opt in ("-b", "--blended-border"):
					global blended_border
					blended_border = 1
					settings += "--blended_border "
			elif opt in ("-f", "--force"):
					global force
					force = 1
					settings += "--force "
			elif opt in ("-s", "--size"):
					thumbsize = arg
					settings += "--size=" + arg + " "

	for ext in image_extensions:
		for f in glob.glob(ext):
			# os.spawnlp(os.P_WAIT, convertraw, convertraw, f)
			# print "making imageinfo for " + f
			ccmd = makethumbnail + " " + settings + " " + f
			os.system(ccmd)

if __name__=="__main__":
	main()

