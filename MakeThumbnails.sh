# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# MakeThumbnails.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# last updated: 20080426
# last updated: 20121025
#
# copyright (c) 2003-2012 by Bryant Hansen
# all rights reserved
##################################################################
#!/bin/sh

USAGE="$0 [ -v | --verbose ] [ -r | --recursive ] [ -b | --blended-border ] [ -f | --force ] [ -m | --movie-mode ] [ --size=x ] directory"
HELP=" -v | --verbose         verbose output      \
\n -s n | --size=n     set the size of the thumbnail.  n is the maximum dimension (height or width); the smaller dimension is scaled proportionally to the aspect ratio \
\n -r | --recursive     recurse subdirectories, making thumbnails for every photo                             \
\n -m | --movie-mode     create thumbnails of movies (avi, mov, mpg, or 3gp) instead of images                 \
\n -b | --blended-border     add a border to every thumbnail that blends into the background                 \
\n -f | --force         force thumbnail creation, even if the resulting image is larger than the original"

# CONF_FILE="album.conf"
# see CONF_FILE for additional variables that are used as options

# set programs
CONVERT="/usr/bin/convert"

THUMB_DIR_BASE=".thumbnails"
IMAGE_INFO_DIR=".image_info"
THUMB_SIZE=256
THUMB_DIR="$THUMB_DIR_BASE$THUMB_SIZE"
TEMPLATE_DIR="templates"
HTML_DIR="html"
VERBOSE=""
RECURSE=""
FORCE_CREATE=""
ALLOW_IMAGE_GROWTH="N"
MOVIE_MODE=0
BLENDED_BORDER=0

NumThumbsRemoved=0
NumThumbs=0
NumCreated=0
NumLinks=0

SET_EXCLUDES_PERMISSIONS=1

# some sed commands we'll need here; string them together to do something intelligent, but understandable in source
SED_COMMAND_EXTRACT_ENDING="s/.*\.//g"
SED_COMMAND_REMOVE_LINK_INFO="s/ -> .*//g"
SED_COMMAND_TO_LOWER_CASE="y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
SED_COMMAND_PRINT_IMAGE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(jpg\|jpeg\|gif\|tiff\|png\)/p"     # used with sed -n
SED_COMMAND_PRINT_MOVIE_TYPE="$SED_COMMAND_EXTRACT_ENDING;$SED_COMMAND_REMOVE_LINK_INFO;$SED_COMMAND_TO_LOWER_CASE;/.*\(mov\|avi\|mpi\|3gp\|mp4\)/p"                 # used with sed -n

WaitForKey() {
    local tmp
    echo "Press Any Key" 1>&2
    read -n 1 'tmp' <&1
#    read -n 1
    return 0
}

DebugMessage() {
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`($THUMB_SIZE): "

    while [[ "$OPTION_FOUND" ]] ; do
        OPTION_FOUND=""
        if [[ "$1" = "-e" ]] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [[ "$1" = "-n" ]] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}

MakeThumbnail() {
    /projects/webpics/MakeThumbnail.sh $*
}

CreateThumbnail () {
    file="$1"
    (( "$VERBOSE" )) && DebugMessage "Enter CreateThumbnail: file=$1, PWD=$PWD"
    if [[ -f "./$THUMB_DIR/$file" ]] ; then
        if [[ "$file" -nt "./$THUMB_DIR/$file" ]] ; then
            DebugMessage "the thumbnail for \"$file\" is outdated.  updating..."
            rm -f "./$THUMB_DIR/$file"
        else
            (( "$VERBOSE" )) && DebugMessage "the thumbnail for \"$file\" already exists and is up to date."
            return 0
        fi
    fi
    if [[ ! -f "./$THUMB_DIR/$file" ]] ; then
        (( "$VERBOSE" )) && DebugMessage "creating \"$PWD/$THUMB_DIR/$file\""
        # if it's allowed to produce a "thumbnail" larger than the original...
        if [[ "$ALLOW_IMAGE_GROWTH" = "Y" ]] ; then
            [[ -f "$PWD/$THUMB_DIR/.tmp.${file}" ]] && rm "$PWD/$THUMB_DIR/.tmp.${file}"
            if (( "$BLENDED_BORDER" )) ; then
                (( "$VERBOSE" )) && DebugMessage "MakeBlendedBorderThumbnail \"$file\" \"$PWD/$THUMB_DIR/.tmp.${file}\" \"${THUMB_SIZE}\""
                MakeBlendedBorderThumbnail "$file" "$PWD/$THUMB_DIR/.tmp.${file}" "${THUMB_SIZE}"
            else
                (( "$VERBOSE" )) && DebugMessage "executing $CONVERT -resize ${THUMB_SIZE}x${THUMB_SIZE}+0+0 \"$file\" \"$PWD/$THUMB_DIR/.tmp.${file}\""
                $CONVERT -resize ${THUMB_SIZE}x${THUMB_SIZE}+0+0 "$file" "$PWD/$THUMB_DIR/.tmp.${file}"
            fi
            if [[ ! -f "$PWD/$THUMB_DIR/.tmp.${file}" ]] ; then
                DebugMessage "ERROR failed to create \"$PWD/$THUMB_DIR/.tmp.${file}\"!  Exiting abnormally!"
                exit -2
            fi
            mv "$PWD/$THUMB_DIR/.tmp.${file}" "$PWD/$THUMB_DIR/$file"
            NumCreated="`expr $NumCreated + 1`"
        else
            # don't allow a thumbnail to be larger than the original image
            # if this condition is true, create a shortcut instead
            local WIDTH
            local HEIGHT
            local ExifFile="${IMAGE_INFO_DIR}/${file}.txt"
            if [[ -f "$ExifFile" ]] ; then
                WIDTH="$(
                            cat "${ExifFile}" | \
                            sed -n "/^[ \t]*Geometry:/p" | \
                            sed "s/[ \t]*Geometry:[ \t]*//;s/x.*//g"
                        )"
                HEIGHT="$(
                            cat "${ExifFile}" | \
                            sed -n "/^[ \t]*Geometry:/p" | \
                            sed "s/[ \t]*Geometry:[ \t].*x//;s/+.*//g"
                        )"
                (( "$VERBOSE" )) && \
                    DebugMessage "reading dimensions from info file: ${ExifFile}.  w=$WIDTH, h=$HEIGHT"
            fi
            if [[ ! "$WIDTH" ]] || [[ ! "$HEIGHT" ]] ; then
                DebugMessage "ERROR2: failed to read width/height from exit file.  Setting to 0 and continuing."
                WIDTH=0
                HEIGHT=0
            fi
            if (( "$WIDTH" <= "0" )) || (( "$HEIGHT" <= "0" )) ; then
                # (( "$VERBOSE" )) &&
                DebugMessage "Exif data not properly read from \"${PWD}/${ExifFile}\".  Reading manually...."
                WIDTH="`identify -format \"%w\" \"$file\"`"
                if [[ "$?" -ne "0" ]] ; then
                    FORCE_CREATE=1 # can't id anything on the image; make sure we don't link to something invalid
                    WIDTH=0
                    HEIGHT=0
                fi
                HEIGHT="`identify -format \"%h\" \"$file\"`"
                if [[ "$?" -ne "0" ]] ; then
                    FORCE_CREATE=1 # can't id the image; make thumb for sure
                    WIDTH=0
                    HEIGHT=0
                fi
            fi
            (( "$VERBOSE" )) && DebugMessage "file=$file, WIDTH=$WIDTH, HEIGHT=$HEIGHT, THUMB_SIZE=$THUMB_SIZE"
            if [[ "$WIDTH" -gt "$THUMB_SIZE" ]] || [[ "$HEIGHT" -gt "$THUMB_SIZE" ]] || [[ "$FORCE_CREATE" ]] ; then
                DebugMessage "Creating thumbnail for \"$PWD/$THUMB_DIR/$file\".  original size (WxH) = (${WIDTH}x${HEIGHT})"
                [[ -f "$PWD/$THUMB_DIR/.tmp.${file}" ]] && rm "$PWD/$THUMB_DIR/.tmp.${file}"
                if (( "$BLENDED_BORDER" )) ; then
                    DebugMessage "BLENDED_BORDER is set.  Make thumbnail that's gone through the MakeBlendedBorderThumbnail process."
                    DebugMessage "MakeBlendedBorderThumbnail \"$file\" \"$PWD/$THUMB_DIR/.tmp.${file}\" \"${THUMB_SIZE}\""
                    MakeBlendedBorderThumbnail "$file" "$PWD/$THUMB_DIR/.tmp.${file}" "${THUMB_SIZE}"
                else
                    (( "$VERBOSE" )) && DebugMessage "executing $CONVERT -resize ${THUMB_SIZE}x${THUMB_SIZE}+0+0 \"$file\" \"$PWD/$THUMB_DIR/.tmp.${file}\""
                    $CONVERT -resize ${THUMB_SIZE}x${THUMB_SIZE}+0+0 "$file" "$PWD/$THUMB_DIR/.tmp.${file}"
                fi
                if [[ ! -f "$PWD/$THUMB_DIR/.tmp.${file}" ]] ; then
                    DebugMessage "ERROR failed to create \"$PWD/$THUMB_DIR/.tmp.${file}\"!  Exiting abnormally!"
                    exit -2
                fi
                mv "$PWD/$THUMB_DIR/.tmp.${file}" "$PWD/$THUMB_DIR/$file"
                NumCreated="`expr $NumCreated + 1`"
            else
                DebugMessage "  \"$file\" is smaller that the requested thumbnail size of $THUMB_SIZE.  Creating a link instead of a thumbnail."
                # make a shortcut to the original instead
                ln -s "$PWD/$file" "$PWD/$THUMB_DIR/"
                NumLinks="`expr $NumLinks + 1`"
            fi
        fi
    fi
}

ProcessDirectory() {

    DebugMessage "Creating ${THUMB_SIZE} pixel thumbnails in \"$PWD\" (thumb dir=$THUMB_DIR)..."

    # if we're using hidden directories now
    if [[ "${THUMB_DIR_BASE:0:1}" = "." ]] ; then
        local THUMB_DIR_NON_HIDDEN=${THUMB_DIR#.}
        if [[ -d "$THUMB_DIR_NON_HIDDEN" ]] && [[ ! -d "$THUMB_DIR" ]] ; then
            DebugMessage "non-hidden thumb dir exists ($THUMB_DIR_NON_HIDDEN), but hidden ($THUMB_DIR) does not.  Moving to new location..."
            mv "$THUMB_DIR_NON_HIDDEN" "$THUMB_DIR"
        fi
    fi

    if [[ -f "./excludes.txt" ]] ; then
        # we have an excludes.txt; we must either copy or merge it
        if [[ -f "$THUMB_DIR/excludes.txt" ]] ; then
            MergeExcludes "./excludes.txt" "$THUMB_DIR/excludes.txt" "$THUMB_DIR/excludes.txt.tmp"
            (( "$VERBOSE" )) && DebugMessage "MergeExcludes complete"
            cp -f "$THUMB_DIR/excludes.txt.tmp" "./$THUMB_DIR/excludes.txt"
            rm -f "$THUMB_DIR/excludes.txt.tmp"
        else
            if [[ -d "$THUMB_DIR/" ]] ; then
                cp "./excludes.txt" "$THUMB_DIR/excludes.txt"
                DebugMessage "overwriting \"$THUMB_DIR/excludes.txt\" with \"./excludes.txt\""
            else
                DebugMessage "\"$THUMB_DIR\" does not exist!"
            fi
        fi
    else
        DebugMessage "the excludes.txt file does not exist in \"$PWD\""
    fi

    # should do this only if we have ownership
    if [[ $SET_EXCLUDES_PERMISSIONS ]] ; then
        (( "$VERBOSE" )) && DebugMessage "setting permissions on excludes.txt files..."
        if [[ -f "./excludes.txt" ]] ; then
            chmod o-rwx "./excludes.txt" 2> /dev/null
        fi
        if [[ -f "./$THUMB_DIR/excludes.txt" ]] ; then
            chmod o-rwx "./$THUMB_DIR/excludes.txt" 2> /dev/null
        fi
    fi

    (( "$VERBOSE" )) && DebugMessage "searching \"$PWD\" for image files..."
    for dir in * ; do
        # if the item (file, dir, link, etc) is in our excludes.txt list
        if (("`grep --files-with-matches -e "^[ ^t]*$dir[ ^t]*$" "excludes.txt" 2>/dev/null | wc -l`" >= 1)) ; then
            (( "$VERBOSE" )) && \
                DebugMessage "The file \"$dir\" is in the excludes list."
            if [[ -d "./$THUMB_DIR" ]] && [[ -f "./$THUMB_DIR/$dir" ]] ; then
                IsPictureFile="`echo "$dir" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
                # if this is a picture file
                if [[ $IsPictureFile ]] ; then
                    DebugMessage "\"$dir\" is in the excludes list, but a thumbnail exists for it.  Removing thumbnail (./$THUMB_DIR/$dir)..."
                    rm "./$THUMB_DIR/$dir"
                    NumThumbsRemoved="`expr "$NumThumbsRemoved" + 1`"
                fi
            fi
        else
            # the item is not in our excludes.txt list; first check to see if it's a directory to process, or something else
            if [[ -d "$dir" ]] ; then   # ==> If it is a directory (-d)...
                if [[ -L "$dir" ]] ; then   # ==> If directory is a symbolic link...
                    DebugMessage "symlink: $dir" `ls -l "$dir" | sed 's/^.*'"$dir"' //'`
                fi
                if [[ $RECURSE ]] ; then
                    # if it's not a thumbnail directory or a reserved directory, then we can go into it and process it too
                    if [[ -z `expr "$dir" : "\($THUMB_DIR_BASE\)"` ]] \
                    && [[ "`basename "$dir"`" != "$TEMPLATE_DIR" ]] \
                    && [[ "`basename "$dir"`" != "$HTML_DIR" ]]
                    then
                        if cd "$dir" ; then              # ==> If can move to subdirectory...
                            ProcessDirectory                        # recursively
                        fi
                    else
                        #display thumb dir warning only if we are verbose
                        (( "$VERBOSE" )) && \
                            DebugMessage "Directory \"$dir\" is an excluded directory!  Skipping processing."
                    fi
                fi
            fi
            if [[ -f "$dir" ]] ; then # or is it a file
                file="$dir"
                IsMovieFile="`echo "$file" | sed -n "$SED_COMMAND_PRINT_MOVIE_TYPE"`"
                # if this is a picture file
                if [[ $IsMovieFile ]] ; then
                    NumThumbs="`expr $NumThumbs + 1`"
                    MovieThumb="./${file}.jpg"
                    if [[ -f "$MovieThumb" ]] ; then
                        if [[ "$file" -nt "$MovieThumb" ]] ; then
                            DebugMessage "the thumbnail for \"$file\" is outdated.  updating..."
                            rm -f "$MovieThumb"
                        else
                            (( "$VERBOSE" )) && \
                                DebugMessage "the thumbnail for the movie \"$file\" already exists and is up to date."
                            return 0
                        fi
                    fi
                    if [[ ! -f "$MovieThumb" ]] ; then
                        (( "$VERBOSE" )) && \
                            DebugMessage "creating \"$PWD/$MovieThumb\""
                        mplayer -vo jpeg "$file" -frames 1 -ao null
                        # dumpFrame: mplayer -start-frame=$frameNum -vo jpeg "$file" -frames 1 -ao null ; mv 00000001.jpg "$PWD/$MovieThumb"
                        # t = getPlayingTime "$file"
                        # ffmpeg -i inputfile.avi -r 1 -vframes 1 -ss  01:30:14 image-%d.jpeg
                        # numFrames = getNumFrames "$file"
                        # numExtracted = x
                        # for n in numExtracted ; do
                        #    frameNum = n * numExtracted
                        #    dumpFrame "$file" "frame".$n $n
                        # TODO: how to have mplayer put out a filename other than 00000001.jpg (-o didn't work, with mencoder neither!)
                        mv 00000001.jpg "$PWD/$MovieThumb"
                        (( "$VERBOSE" )) && \
                            DebugMessage "executing \"mplayer -vo jpeg "$file" -frames 1\""
                        NumCreated="`expr $NumCreated + 1`"
                    fi
                fi
                IsPictureFile="`echo "$file" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
                # if this is a picture file
                if [[ $IsPictureFile ]] ; then
                    NumThumbs="`expr "$NumThumbs" + 1`"
                    #insure that we have an appropriate thumbnail subdirectory first
                    if [[ ! -d "./$THUMB_DIR" ]] ; then
                        DebugMessage "Creating a thumbnail directory: mkdir \"$PWD/$THUMB_DIR\""
                        mkdir "$PWD/$THUMB_DIR"
                    fi
                    [[ ! -d "$IMAGE_INFO_DIR" ]] && mkdir "$IMAGE_INFO_DIR"
                    if [[ ! -d "$IMAGE_INFO_DIR" ]] ; then
                        DebugMessage "ERROR: image info dir \"$IMAGE_INFO_DIR\" does not exist in $PWD.  Failed to create.  Skipping info!"
                    else
                        local IMAGE_INFO_FILE="${IMAGE_INFO_DIR}/${file}.txt"
                        if [[ -f "${IMAGE_INFO_FILE}" ]] ; then
                            if [[ "$file" -nt "$IMAGE_INFO_FILE" ]] ; then
                                DebugMessage "the information for \"$file\" is outdated.  updating \"$IMAGE_INFO_FILE\"..."
                                identify -verbose "$file" > "$IMAGE_INFO_FILE"
                            else
                                (( "$VERBOSE" )) && DebugMessage "the image information for \"$file\" already exists and is up to date."
                            fi
                        else
                            DebugMessage "\"$file\": no image information exists for.  recording in \"$PWD/$IMAGE_INFO_FILE\"..."
                            identify -verbose "$file" > "$IMAGE_INFO_FILE"
                        fi
                    fi
                    LINK_FILE=""
                    LINK_THUMBNAIL=""
                    if [[ -L "$file" ]] ; then   # ==> If file is a symbolic link...
                        LINK_FILE=`ls -l "$file" | sed 's/^.*'"$file"' //'`
                        LINK_FILE="${LINK_FILE#->\ }"
                        LINK_THUMBNAIL="`dirname "$LINK_FILE"`/$THUMB_DIR/`basename "$LINK_FILE"`"
                        (( "$VERBOSE" )) && DebugMessage "picture \"$file\" is linked to $LINK_FILE.  Does $LINK_THUMBNAIL exist?"
                        if [[ -f "$LINK_THUMBNAIL" ]] ; then
                            (( "$VERBOSE" )) && DebugMessage "there is a thumbnail to link to, \"$LINK_THUMBNAIL\"; we don't have to create one"
                            if [[ -f "./$THUMB_DIR/$file" ]] ; then
                                if [[ -L "./$THUMB_DIR/$file" ]] ; then   # ==> If file is a symbolic link...
                                    (( "$VERBOSE" )) && DebugMessage "\"./$THUMB_DIR/$file\" is a link.  Insure that it's up-to-date."
                                    if [[ -L "$LINK_THUMBNAIL" ]] ; then
                                        # can't link to a link; copy the link instead!  make it absolute?
                                        cp -a "$LINK_THUMBNAIL" "./$THUMB_DIR/$file"
                                        (( "$VERBOSE" )) && DebugMessage "cp -a \"$LINK_THUMBNAIL\" \"./$THUMB_DIR/$file\""
                                    else
                                        if [[ ${LINK_THUMBNAIL:0:1} = "/" ]] ; then
                                            ln -fs "$LINK_THUMBNAIL" "./$THUMB_DIR/$file"
                                            (( "$VERBOSE" )) && DebugMessage "ln -fs \"$LINK_THUMBNAIL\" \"./$THUMB_DIR/$file\""
                                        else
                                            ln -fs "../$LINK_THUMBNAIL" "./$THUMB_DIR/$file"
                                            (( "$VERBOSE" )) && DebugMessage "ln -fs \"../$LINK_THUMBNAIL\" \"./$THUMB_DIR/$file\""
                                        fi
                                    fi
                                    NumLinks="`expr $NumLinks + 1`"
                                else
                                    DebugMessage "./$THUMB_DIR/$file link exists as a file.  removing and replacing with link..."
                                    rm -f "./$THUMB_DIR/$file"
                                    # NumThumbsRemoved="`expr "$NumThumbsRemoved" + 1`"
                                    # is this link absolute?
                                    if [[ ${LINK_THUMBNAIL:0:1} = "/" ]] ; then
                                        ln -fs "$LINK_THUMBNAIL" "./$THUMB_DIR/$file"
                                        (( "$VERBOSE" )) && DebugMessage "ln -fs \"$LINK_THUMBNAIL\" \"./$THUMB_DIR/$file\""
                                    else
                                        ln -fs "../$LINK_THUMBNAIL" "./$THUMB_DIR/$file"
                                        (( "$VERBOSE" )) && DebugMessage "ln -fs \"../$LINK_THUMBNAIL\" \"./$THUMB_DIR/$file\""
                                    fi
                                    NumLinks="`expr $NumLinks + 1`"
                                fi
                            else
                                if [[ -L "$LINK_THUMBNAIL" ]] ; then
                                    # can't link to a link; copy the link instead!  make it absolute?
                                    cp -a "$LINK_THUMBNAIL" "./$THUMB_DIR/$file"
                                    (( "$VERBOSE" )) && DebugMessage "cp -a \"$LINK_THUMBNAIL\" \"./$THUMB_DIR/$file\""
                                else
                                    if [[ ${LINK_THUMBNAIL:0:1} = "/" ]] ; then
                                        ln -fs "$LINK_THUMBNAIL" "./$THUMB_DIR/$file"
                                        (( "$VERBOSE" )) && DebugMessage "ln -fs \"$LINK_THUMBNAIL\" \"./$THUMB_DIR/$file\""
                                    else
                                        ln -fs "../$LINK_THUMBNAIL" "./$THUMB_DIR/$file"
                                        (( "$VERBOSE" )) && DebugMessage "ln -fs \"../$LINK_THUMBNAIL\" \"./$THUMB_DIR/$file\""
                                    fi
                                fi
                                NumLinks="`expr $NumLinks + 1`"
                            fi
                        else
                            echo "MakeThumbnail --size=${THUMB_SIZE} '${file}'"
                        fi
                    else
                        (( "$VERBOSE" )) && DebugMessage "$file is a not a link.  Call MakeThumbnail."
                        echo "MakeThumbnail --size=${THUMB_SIZE} '${file}'"
                    fi
                fi
            fi
            if [[ -L "$dir" ]] && [[ ! -f "$dir" ]] && [[ ! -d "$dir" ]] ; then
                DebugMessage "$dir is a link to an invalid file or a non-file object."
            fi
        fi # if else in excludes.txt
    done

    if [[ -d "$THUMB_DIR" ]] ; then
        PARENT_DIR="$PWD"
        cd "$THUMB_DIR"
        (( "$VERBOSE" )) && DebugMessage -n "   Removing unused thumbnails..."
        # remove thumbnails
        for file in * ; do
            # TODO: detect if the parent is a movie file and check it specially!
            # if the file ends in .jpg (like a movie thumbnail), and the file *without* the .jpg ending is in the parent, don't remove the thumbnail
            # why was this previously commented?
            if [[ "${file%.jpg}" != "${file}" ]] && [[ -f "$PARENT_DIR/${file%.jpg}" ]] ; then
                DebugMessage "\"$file\" has a parent named \"${file%.jpg}\" in the parent directory \"$PARENT_DIR\".  Don't remove thumb."
            else
                IsPictureFile="`echo "$file" | sed -n "$SED_COMMAND_PRINT_IMAGE_TYPE"`"
                # if this is a picture file
                if [[ $IsPictureFile ]] ; then
                    # if there is not a corresponding thumbnail in the parent directory...
                    if [[ ! -f "$PARENT_DIR/$file" ]] ; then
                        DebugMessage "DANGEROUS OPERATION (verify my code!): \"$file\" no longer exists in \"$PWD\".  Removing corresponding thumbnail in \"$PWD\"..."
                        echo "rm './${file}/'"
                        NumThumbsRemoved="`expr "$NumThumbsRemoved" + 1`"
                    fi
                fi
            fi
        done
        (( "$VERBOSE" )) && DebugMessage "done."
        cd "$PARENT_DIR"
    fi
    DebugMessage "$PWD/$THUMB_DIR: $NumThumbs thumbnails, $NumCreated newly created, $NumThumbsRemoved removed, $NumLinks thumbnails are links"
    cd ..   # ==> Up one directory level.
}


######################################################
# Setup Environment
######################################################
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . "/etc/webpics/${CONF_FILE}"
[[ -f "${HOME}/templates/${CONF_FILE}" ]]  && . "${HOME}/templates/${CONF_FILE}"
[[ -f "./templates/${CONF_FILE}" ]]  && . "./templates/${CONF_FILE}"
[[ -f "./${CONF_FILE}" ]]            && . "./${CONF_FILE}"



######################################################
# - Main -
######################################################

# is there a better way to do this that's non-destructive (unlike shift in a loop)?
if (( $# == 0 )) ; then
  (( "$VERBOSE" )) && DebugMessage "`basename "$0"` $* called"
fi

if [[ -z "$1" ]] ; then
   shift
fi

# process command-line args
until [[ -z "$1" ]]  # Until all parameters used up...
do
   if [[ ! -z `expr "$1" : "\(-\)"` ]] ; then
      if [[ ! -z `expr "$1" : "\(--size=\)"` ]] ; then
         THUMB_SIZE_ARG=${1#--size=}
         if [[ "$THUMB_SIZE_ARG" -gt "0" ]] ; then
            THUMB_SIZE=$THUMB_SIZE_ARG
         else
            DebugMessage "INVALID Thumbnail size arg: $1"
         fi
      fi
      if [[ ! -z `expr "$1" : "\(-s\)"` ]] ; then
         shift
         THUMB_SIZE="$1"
      fi
      if [[ ! -z `expr "$1" : "\(--recursive\)"` ]] \
      || [[ ! -z `expr "$1" : "\(-r\)"` ]] ; then
         RECURSE=1
      fi
      if [[ ! -z `expr "$1" : "\(--verbose\)"` ]] \
      || [[ ! -z `expr "$1" : "\(-v\)"` ]] ; then
         VERBOSE=1
      fi
      if [[ ! -z `expr "$1" : "\(--blended-border\)"` ]] \
      || [[ ! -z `expr "$1" : "\(-b\)"` ]] ; then
         BLENDED_BORDER=1
      fi
      if [[ ! -z `expr "$1" : "\(--force\)"` ]] \
      || [[ ! -z `expr "$1" : "\(-f\)"` ]] ; then
         FORCE_CREATE=1
      fi
      if [[ ! -z `expr "$1" : "\(--movie-mode\)"` ]] \
      || [[ ! -z `expr "$1" : "\(-m\)"` ]] ; then
         MOVIE_MODE=1
      fi
   fi
   LAST_PARAM=$1
   shift
done

if (( "$BLENDED_BORDER" )) ; then
    if ! (( "$FORCE_CREATE" )) ; then
             DebugMessage "Consider enabling ForceCreate feature (-f).  BlendedBorder is enabled, and ForceCreate will prevent a link being created to 
the original image - not having a blended border - which is done in certain circumstancces."
             #FORCE_CREATE=1
    else
             DebugMessage "BlendedBorder feature is enabled"
    fi
fi
if (( "$MOVIE_MODE" )) ; then
    (( "$VERBOSE" )) && DebugMessage "Movie mode enabled!"
fi

if [[ -d "$LAST_PARAM" ]] && [[ ! -z "$LAST_PARAM" ]]; then
   BASE_DIR="$LAST_PARAM"       # ==> Otherwise, move to indicated directory.
else
   BASE_DIR="$PWD"    # ==> No args to script, then use current working directory.
fi
#THUMB_DIR="${BASE_DIR}/${THUMB_DIR_BASE}${THUMB_SIZE}"
THUMB_DIR="${THUMB_DIR_BASE}${THUMB_SIZE}"
if (( "$VERBOSE" )) ; then
   DebugMessage "Program Options:"
   DebugMessage "   VERBOSE is on."
   DebugMessage "   RECURSE=$RECURSE"
   DebugMessage "   THUMB_SIZE=$THUMB_SIZE"
   DebugMessage "   THUMB_DIR=$THUMB_DIR"
   DebugMessage "   BASE_DIR=$BASE_DIR"
fi
cd "$BASE_DIR"
ProcessDirectory
(( "$VERBOSE" )) && DebugMessage "`basename "$0"` complete.  Exiting normally."
exit 0

