#!/bin/bash
# Bryant Hansen
# 20101220

# example
# ./MakeWatermarkImage.sh "copyright(c)2010 by Bryant Hansen\nall rights reserved" /usr/share/fonts/corefonts/arial.ttf ./bryant-watermark2.png 12

USAGE="$0  copyright_string  font_filename  output_filename  [ size ]"

size=48

function errorExit() {
    msg="$1"
    errCode="$2"
    echo -e "$msg"
    echo -e "USAGE=$USAGE"
    exit $errCode
}

# sanity checks
(( $# < 3 )) && errExit "ERROR: need at least 3 args! ($# provided)" 253

#COPYRIGHT="2010 Hansen"
copyright_string="$1"
font_filename="$2"
outfile="$3"
(( $# >= 4 )) && size="$4"

[[ ! -f "$font_filename" ]] && errExit "ERROR: font file must exist!" 252

echo "font_filename=$font_filename  size=$size  copyright_string=$copyright_string  outfile=$outfile"

# execute the command
convert \
    -background transparent \
    -fill "#808080" \
	-colors 256 \
    -quality 90 \
    -depth 8 \
    -font "$font_filename" \
    -pointsize $size \
    -gravity center \
    label:"${copyright_string}" \
    -rotate 45 \
    "$outfile"
