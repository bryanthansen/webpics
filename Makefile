# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# by Bryant Hansen <sourcecode@bryanthansen.net>
# copyright 2008
# last major update: 2008 07 05
#!/bin/bash

# Makefile
# Copyright (c) 2008 by Bryant Hansen

# dependent programs
IDENTIFY="/usr/bin/identify"
UFRAW_BATCH="ufraw-batch"

# file and directory names used
IMAGE_INFO_DIR=".image_info"
THUMB_DIR_BASE=".thumbnails"
INDEX_PAGE=index.html
IMAGE_LIST_FILE=./imagelist.txt

# dependent scripts
MAKE_PICINDEX_SCRIPT="/projects/webpics/MakePicindex.sh"
MAKE_IMAGELIST_SCRIPT="/projects/webpics/MakeImageList.sh"
MAKE_IMAGEINFO_SCRIPT="/projects/webpics/MakeImageInfo.sh"
MAKE_PICTURE_PROPS_SCRIPT="/projects/webpics/GenPicturePageProperties.sh"

# default lists
THUMBNAIL_SIZES=256 600 1024
PICTURE_TYPES = jpg JPG gif GIF png PNG tiff TIFF
PICTURE_PAGE_TEMPLATES = picture.html slide.html

all: no_args

# aggregations of targets

# The original list is carriage-return-delimited and allows comments; change this to a format the makefile can use
IMAGE_LIST = $(shell [[ -f $(IMAGE_LIST_FILE) ]] && cat $(IMAGE_LIST_FILE) | sed "s/\#.*//" | tr "\n" " ")

THUMB_DIRS = $(patsubst %,.thumbnails%,$(THUMBNAIL_SIZES))

# A list of every thumbnail that should be created/updated in every thumb dir
THUMBNAILS_LIST = $(foreach thumbDir,$(THUMB_DIRS),$(patsubst %,$(thumbDir)/%,$(IMAGE_LIST)))

# A make wildcard to specify all thumbnail types in the current directory with a variable basename
# Example: THUMBNAIL_WILDCARD=%.jpg %.JPG %.gif %.GIF %.png %.PNG %.tiff %.TIFF
# problem: not so useful - we should support the same basename for multiple image types
THUMBNAIL_WILDCARD = $(PICTURE_TYPES:%=\%.%)

# A make target that is a list of all thumbnail directories
# Example: THUMBNAILS_TARGET=.thumbnails256/% .thumbnails600/% .thumbnails1024/%
# TODO: how to filter out image types without losing the .imagetype extension in the "%" target wildcard? - see problem with THUMBNAIL_WILDCARD
THUMBNAILS_TARGET = $(patsubst %,.thumbnails%,$(patsubst %,%/%,$(THUMBNAIL_SIZES)))

IMAGE_INFO_LIST=$(patsubst %,.image_info/%.txt,$(IMAGE_LIST))

# a list of raw pictures - always a dependency
RAW_PICS := $(wildcard *.CR2)

# a list of jpeg images that should exist (and be updated) from the raw source image
RAW_CONVERTED_JPGS := $(RAW_PICS:%.CR2=%.jpg)

# the list of templates basenames (html ending stripped)
PIC_TEMPLATE_BASENAMES = $(patsubst %.html,%,$(PICTURE_PAGE_TEMPLATES))

# a bit much duplicate code
ALL_PICTURE_PAGES = $(foreach thumbnail,$(THUMBNAIL_SIZES), $(patsubst %,.thumbnails$(thumbnail)/html/%.html,$(IMAGE_LIST)))

# All picture pages to be created - an aggregation of thumbnails and picture page templates
PICTURE_PAGE_LIST = 	$(foreach picturePage,$(ALL_PICTURE_PAGES),\
							$(foreach picturePageTemplate,$(PIC_TEMPLATE_BASENAMES),\
								$(dir $(picturePage))$(picturePageTemplate)_$(notdir $(picturePage)))\
							)\
						)

PICTURE_PAGE_DEPS = "$(PICTURE_PAGE_PROPERTY_FILE) $(PICTURE_TEMPLATE)"

#$(patsubst %.ufraw,%.jpg,$(wildcard *.ufraw)): %.ufraw %.CR2
#	$(UFRAW_BATCH) "$<" --out-type=jpeg --compression=100 --output="$@" --conf=@<

%.png: %.CR2
	$(UFRAW_BATCH) "$<" --out-type=png --output="$@"

%.jpg: %.CR2
#	$(shell set -o verbose ; [[ -f $(@:%.jpg=%.ufraw) ]] && arg="--conf=$(@:%.jpg=%.ufraw)" || arg="" ; $(UFRAW_BATCH) "$<" --out-type=jpeg --compression=100 --output="$@" $arg)
#	$(shell set -o verbose ; [[ -f $(@:%.jpg=%.ufraw) ]] && arg="--conf=$(@:%.jpg=%.ufraw)" || arg="" ; $(UFRAW_BATCH) "$<" --out-type=jpeg --compression=100 --output="$@" \$arg)
	$(UFRAW_BATCH) "$<" --out-type=jpeg --compression=100 --output="$@"

$(IMAGE_LIST_FILE): FORCE
	$(MAKE_IMAGELIST_SCRIPT) $@

sortorder.txt:
	echo "# the list of files here will appear in order in the index page and in the individual picture pages" >> "$@"
	echo "# pictures, movies, etc. in the current directory that are not listed here will appear in alphabetical order following this list" >> "$@"

album.conf:
	echo "# place configuration settings here; it will be loaded into the shell environment" >> "$@"

$(INDEX_PAGE): album.conf $(IMAGE_LIST_FILE)
	$(MAKE_PICINDEX_SCRIPT) -o $@

.thumbnails%/html/Makefile:
	$(shell [[ ! -d $(dir $@) ]] &&  mkdir $(dir $@) || touch $(dir $@))
	$(shell [[ ! -f $@ ]] && ln -s /projects/webpics/Makefile.picturePages $@ || touch $@)
	@echo "Created $@"

html/Makefile: FORCE
	$(shell $(MAKE_PICTURE_PROPS_SCRIPT) $(IMAGE_LIST_FILE) 0)
	$(shell [[ ! -f $@ ]] && ln -s /projects/webpics/Makefile.picturePages $@ || touch $@)
	cd $(dir $@) && $(MAKE)
	@echo "Created $@"

# TODO: remove this redundancy - with the Makefile rule, .thumbnails%/html/Makefile, below:
.thumbnails%/html: FORCE
	$(shell $(MAKE_PICTURE_PROPS_SCRIPT) $(IMAGE_LIST_FILE) $(patsubst .thumbnails%,%,$(patsubst %/html,%,$@)))
	$(shell [[ ! -d $@ ]] &&  mkdir $@ || touch $@)
	$(shell [[ ! -f $@/Makefile ]] && ln -s /projects/webpics/Makefile.picturePages $@/Makefile || touch $@)
	cd $@ && $(MAKE)

MANIFEST_FILE = templates/manifest.txt
TEMPLATE_FILES = $(shell cat $(MANIFEST_FILE) | sed "s/\#.*//" | tr "\n" " ")

$(TEMPLATE_FILES):

.PHONY: PicturePageProperties

.PHONY: PicturePages
#PicturePages: $(patsubst %,%/html/Makefile,$(THUMB_DIRS)) $(patsubst %,%/html,$(THUMB_DIRS)) $(IMAGE_INFO_LIST) $(TEMPLATE_FILES) FORCE
PicturePages: $(patsubst %,%/html/Makefile,$(THUMB_DIRS)) $(patsubst %,%/html,$(THUMB_DIRS)) $(IMAGE_INFO_LIST) $(TEMPLATE_FILES) html/Makefile FORCE

.PHONY: ImageInfo
ImageInfo:
	# TODO: MakeImageInfo should process one picture at a time, with specified input and output files
	$(MAKE_IMAGEINFO_SCRIPT)

.PHONY: FORCE
FORCE:

.thumbnails%/Makefile: /projects/webpics/Makefile.thumbnails
	$(shell [[ ! -d $(dir $@) ]] &&  mkdir $(dir $@) || touch $(dir $@))
	$(shell [[ ! -f $@ ]] && ln -s $< $@ || touch $@)
	@echo "Created $@"

.thumbnails%/album.conf: album.conf
	$(shell [[ ! -f $@ ]] && ln -s ../album.conf $@ || touch $@)
	@echo "Created $@"

.thumbnails%/$(IMAGE_LIST_FILE): 
	$(shell [[ ! -f $@ ]] && ln -s ../$(IMAGE_LIST_FILE) $@ || touch $@)
	@echo "Created $@"

# for some strange reason, the dependency list didn't work :(
#.thumbnails%: $(IMAGE_LIST) $(IMAGE_INFO_LIST)
.thumbnails%: .thumbnails%/$(IMAGE_LIST_FILE) .thumbnails%/album.conf FORCE
	$(shell [[ ! -d $@/templates ]] && [[ ! -L $@/templates ]] && ln -s ../templates $@/)
	$(shell [[ ! -d $@ ]] &&  mkdir $@ || touch $@)
	cd $@ && $(MAKE)

.PHONY: Thumbnails
Thumbnails: $(patsubst %,%/Makefile,$(THUMB_DIRS)) $(patsubst %,%/$(IMAGE_LIST_FILE),$(THUMB_DIRS)) $(patsubst %,%/album.conf,$(THUMB_DIRS)) $(THUMB_DIRS) $(IMAGE_LIST) $(IMAGE_INFO_LIST) FORCE

# must do this every time
#no_args: $(RAW_CONVERTED_JPGS) $(IMAGE_LIST_FILE) ImageInfo $(THUMBNAILS_LIST) $(INDEX_PAGE)
no_args: $(RAW_CONVERTED_JPGS) $(IMAGE_LIST_FILE) ImageInfo Thumbnails PicturePages $(INDEX_PAGE)
	@echo "Warning: at the moment, space are not allowed in picture filenames.  Other special characters may also be problematic.  This is a side-effect of building using GNU Make.  TODO: consider pymake" 1>&2

.PHONY: clean
clean:
	-rm .thumbnails*/*.jpg
	-rm .thumbnails*/*.gif 2>/dev/null
	-rm .thumbnails*/*.png 2>/dev/null
	-rm -r .thumbnails* 2>/dev/null
	-rm -r .image_info 2>/dev/null
	-rm -r html 2>/dev/null
	-rm $(INDEX_PAGE) 2>/dev/null
	-rm $(IMAGE_LIST_FILE) 2>/dev/null
	-rm Makefile.auto 2>/dev/null
	-rm $(RAW_CONVERTED_JPGS) 2>/dev/null
