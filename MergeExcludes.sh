# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) MergeExcludes.sh 	1.0 	11 Dec 2003
#
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         This file merges 2 excludes.txt files that are used in the picture scripts
#

USAGE="$0 [ -v ] EXCLUDES_FILE_1 EXCLUDES_FILE_2 OUTPUT_FILE"

VERBOSE=""
if [ -z "$1" ] ; then
	shift
fi

if [ $# -eq 4 ] ; then
	if [ "$1" = "-v" ] ; then
		VERBOSE=1
		shift # discard the verbose argument and continue
	else
		echo "$0 ERROR: if 4 arguments are provided, argument 1 must be \"-v\"!  Exiting abnormally."
		echo "USAGE: $USAGE"
		exit -1
	fi
fi

if [ $# -ne 3 ] ; then
	echo "$0 ERROR: with $# parameters!  Should be 3 (or 4 with verbose mode)."
	exit -1
fi

if [ ! -f "$1" ] ; then
	if [ $VERBOSE ] ; then
		echo "$0 ERROR:  called with missing file \"$1\"!"
	fi
	exit -2
fi

if [ ! -f "$2" ] ; then
	if [ $VERBOSE ] ; then
		echo "$0 ERROR:  called with missing file \"$2\"!"
	fi
	exit -3
fi

if [ -f "$3" ] ; then
	echo "$0 WARNING: file \"$3\" exists.  Overwriting..."
#      echo "$0 WARNING: file \"$3\" exists.  Overwrite feature not implemented.  Exiting."
#      exit -4
fi

FILE1="$1"
FILE2="$2"
OUTPUT_FILE="$3"

if [ $VERBOSE ] ; then
	echo "$0 called with $*"
fi

# copy the file to preserve the permissions
#cp -af "$FILE2" "$OUTPUT_FILE"
cp -f "$FILE2" "$OUTPUT_FILE"

# replace the contents of the new file with FILE1
# cat "$FILE1" | sed '/^$/d' > "$OUTPUT_FILE"

# echo "pwd=$PWD, FILE2=$FILE2"
while read 'file1_line'
do
{
#   echo "file1_line = $file1_line"

	ContainsLine=0
	line1="$file1_line"
	line1="${line1%%*[ \t]}"
	line1="${line1%%[ \t]*}"

	if [ ! "$line1" = "" ] ; then

		line1="`/projects/webpics/EscapeSpecialChars.pl "${file1_line}"`"

		while read 'file2_line'
		do
		{
			line2="$file2_line"
			line2="${line2%%*[ \t]}"
			line2="${line2%%[ \t]*}"
	#         echo "comparing \"$line1\" to \"$line2\"..."
			line2="`/projects/webpics/EscapeSpecialChars.pl "${file2_line}"`"
			if [ "$line1" = "$line2" ] ; then
				ContainsLine=1
				break
			fi
		}
		done <"$FILE2"

		if [ $ContainsLine -eq 0 ] ; then
			if [ $VERBOSE ] ; then
				echo "\"$FILE2\" does not contain \"$line1\".  Adding to \"$OUTPUT_FILE\"..."
			fi
			echo "$file1_line" >> "$OUTPUT_FILE"
		else
			if [ $VERBOSE ] ; then
				echo "\"$FILE2\" already contains \"$line2\"."
			fi
		fi

	fi
}
done <"$FILE1"

exit 0
