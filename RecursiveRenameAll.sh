# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# RecursiveRenameAll.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/sh

USAGE="$0 {from} {to}"

OperateOnPWD () 
{

   echo "Renaming files in \"`pwd`\" from \"$FROM\" to \"$TO\".."
   rename "$FROM" "$TO" "$MASK"

   for dir in *
   do
      if [ -d "$dir" ] ; then   # ==> If it is a directory (-d)...
         if [ -L "$dir" ] ; then   # ==> If directory is a symbolic link...
            if [ "$VERBOSE" ] ; then
               echo "symlink: $dir" `ls -l "$dir" | sed 's/^.*'"$dir"' //'`
            fi
         fi


         if cd "$dir" ; then              # ==> If can move to subdirectory...
            OperateOnPWD                        # recursively
            cd ..   # ==> Up one directory level.
         fi
      fi
   done

}

######################################################
# - Main -
######################################################

echo
echo "***************************************************************************"
echo "*** `basename "$0"` called with $# arguments ***"

if [ $# -eq 3 ] ; then
   FROM=$1
   TO=$2
   MASK=$3
   echo "command= \"$0  FROM=$FROM  TO=$TO  MASK=$MASK"\"
else
   echo "ERROR: wrong number of arguments: $#"
   echo "USAGE: $USAGE"
   echo "Exiting abnormally!"
   exit 65   
fi

OperateOnPWD

echo "*** `basename "$0"` complete.  Exiting normally...  ***"
echo "***************************************************************************"
exit 0

