# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# RecursivelyDeleteFilesMatchingPattern.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/bash

IDENTIFYING_STRING=".html.200"

# Update 2013: new preferred method
# find ./ -name "*${IDENTIFYING_STRING}*" -exec f="{}" ; echo "rm -f \"${f}\"" \; | /bin/bash
#   (without the pipe to /bin/bash for simulating)

if [ ! -z "#1" ] ; then
   cd "$1"
fi

echo "deleting all backup files in \"$PWD\"..."

# TODO: determine how to operate on files and directories with spaces in the name
# script used to delete backup files

echo "Are you sure you want to recursively delete all files containing the string \"$IDENTIFYING_STRING\" from the directory \"$PWD\"? (y/n)"
read -n 1 ANSWER
echo
if [[ ! $ANSWER = "y" ]] ; then
   echo "exiting without performing operation"
   exit -2
fi

find "$PWD" -iname "*$IDENTIFYING_STRING*" -exec rm -f "{}" \;
echo "file deletion complete"
exit 0

# with quotes around each line and 2 spaces at the end
dfiles=$(find "$PWD" -iname "*.html.200*" | sed 's/^/\"/;s/$/\ \"\ \ /')

# with backslashes leading each space and quotes around each line
# dfiles=$(find "$PWD" -iname "*.html.200*" | sed 's/\ /\\\ /g;s/^/\"/;s/$/\"\ /')

# with backslashes leading each space
# dfiles=$(find "$PWD" -iname "*.html.200*" | sed 's/\ /\\\ /g')

echo "dfiles=$dfiles"
echo "press any key to continue"
read -n 1
rm -i `echo $dfiles`
exit -1

echo $dfiles | while read f1 ; do
   echo "rm '$f1'"
done
exit -1

# for f1 in `find "$PWD" -iname "*.html.200*"` ; do 
for f1 in `echo "$dfiles"` ; do 
#while read 'f1' ; do
#   rm "$f1"
   echo "rm '$f1'"
done

for f1 in "test 1" "test 2" "test 3" ; do
   echo "rm '$f1'"
done
# done < $(find "$PWD" -iname "*.html.200*")

