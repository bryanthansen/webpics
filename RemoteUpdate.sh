# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# RemoteUpdate.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/bash

echo "*********************************************"
echo "***      Bryant's Photo Album Generator   ***"
echo "*********************************************"

Usage="UpdateDirectory.sh directory_name"
prompt="directory>"
base_dir="$HOME/public_html"

dir=""

while [ ! -d "$base_dir/$dir" ] || [ -z "$dir" ] ; do
 
   echo -e "\n\nPlease specify the name of the directory that you with to refresh.  (? for help, q for quit)"
   echo -n "$prompt"
   read dir

   if [ "$dir" = "q" ] ; then
      echo -e "\n\nexiting."
      exit -3
   elif [ "$dir" = "l" ] ; then
      echo -e "\nListing of \"$base_dir\": \n"
      ls "$base_dir"
      echo ""
   elif [ "$dir" = "?" ] ; then
      echo -e "\n\n  Help Instructions"
      echo -e "\n    Usage=$Usage"
      echo -e "\n    Publicly, directory names are relative to the home page"
      echo -e "    Privately, most directory names are relative to the public_html directory under your personal home directory"
      echo -e "    Directory names are case-sensitive; may attention to upper- and lower-case characters!"
      echo -e "    Use forward slashes (/), rather than backward slashes (\\) between directories"
      echo -e "    If any directory name contains a space, the entire directory name must be enclosed in double-quotes (\")"
      echo -e "    Directory names that do not contain a space may or may not be enclosed in double quotes (\")"
      echo -e "    If you are updating your home directory, just enter a single forward slash (/)"
      
      echo -e "    "
      echo -e "    Command list:\n"
      echo -e "      q - quit the application without doing anything"
      echo -e "      l - list the files in the current directory ($base_dir)"
      echo -e "      ? - show this menu"
      echo -e "    "
      echo -e "    Examples:"
      echo -e "    $0 yesterday"
      echo -e "    $0 Yesterday"
      echo -e "    $0 \"Yesterday\""
      echo -e "    $0 \"Yesterday/Walk in the Woods\""
      echo -e "    $0 /"
      echo -e "    "
   else
      if [ ! -d "$base_dir/$dir" ] ; then
         echo -e "\n\nError: the directory \"$base_dir/$dir\" does not exist.  try again."
      fi
   fi

done

if [ "$dir" = "/" ] ; then
   dir=""
fi

mpi "$base_dir/$dir"

echo -e "\npress any key"
read -n 1
