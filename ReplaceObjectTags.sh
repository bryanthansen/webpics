# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) ReplaceObjectTags.sh 	13.9.2006
#
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         Replaces object tags with a string.  Does necessary formatting and escaping of string
#
#<!-- copyright (c) 2006 by Bryant Hansen -->
#<!-- all rights reserved -->

USAGE="ReplaceObjectTags [ -v ] [ -g ] OriginalString ObjectTag ReplaceString"

REPLACE_ALL=""
IS_OPTION_ARG=1
# VERBOSE=1
VERBOSE_ARG=""

DebugMessage()
{
	local NO_CR_ARG=""
	local SPECIAL_CHAR_ARG=""
	local ARGS=""
	local OPTION_FOUND="0"
	local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "

	while [ "$OPTION_FOUND" ] ; do
		OPTION_FOUND=""
		if [ "$1" = "-e" ] ; then
			OPTION_FOUND="-e"
			ARGS="${ARGS} -e"
			shift
		fi
		if [ "$1" = "-n" ] ; then
			OPTION_FOUND="-n"
			ARGS="${ARGS} -n"
			LOG_RECORD_HEADER=""
			shift
		fi
	done		

	echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

	return 0
}

WaitForKey()
{
	DebugMessage "Press Any Key"
#	read -n 1 <&1
	read -n 1
	return 0
}

# process the command line
while (($IS_OPTION_ARG)) ; do
	IS_OPTION_ARG=0 # safest to avoid infinite loops
	if [ "$1" == "-v" ] ; then
		IS_OPTION_ARG=1
		VERBOSE=1
		VERBOSE_ARG="-v"
		shift # discard the verbose argument and continue
	fi
	if [ "$1" == "-g" ] ; then
		IS_OPTION_ARG=1
		REPLACE_ALL="g"
		shift # discard the verbose argument and continue
		if [ $VERBOSE ] ; then
			DebugMessage "ReplaceObjectTags: ReplaceAll is on!"
		fi
	fi
done		

if [ "$#" -ne "3" ] ; then
	DebugMessage "ERROR: ReplaceObjectTags requires 3 args, got $#.  args=\"$*\"!  Exiting abnormally."
	exit -200
else

	# todo: attempt to deal with the situation of comments: removing the close-comment tag from an insert block when we're already in a comment in the main block

	pObjectTag="`/projects/webpics/FormatTagRegexMatchString.pl "${2}"`"
	pReplaceString="`/projects/webpics/EscapeSpecialChars.pl "${3}"`"

	# note: this could be much more easily done by processing a slurp-mode stream in Perl and only changing the first instance
	line_num_first_instance=""
	if [ ! "${REPLACE_ALL}" = "g" ] ; then
		line_num_first_instance="`echo "${1}" | grep -n "${pObjectTag}" | head -n 1 2>/dev/null | sed "s/\:.*//g"`"
	fi

	# stip comments from the replacement
	pRegexReplaceTag="${line_num_first_instance}s/${pObjectTag}/${pReplaceString}/${REPLACE_ALL}"

	echo "${1}" | sed "${pRegexReplaceTag}" #working

	if [ $VERBOSE ] ; then
		if [ "${REPLACE_ALL}" = "g" ] ; then
			DebugMessage "replaced all instances of \"`echo -e ${pObjectTag}`\" with \"`echo -e ${pReplaceString}`\""
		else
			DebugMessage -e "replaced first instance of \"`echo -e ${pObjectTag}`\" with \"`echo -e ${pReplaceString}`\" on line ${line_num_first_instance}"
		fi
		DebugMessage "pRegexReplaceTag=${pRegexReplaceTag}"
#		WaitForKey
	fi

fi

