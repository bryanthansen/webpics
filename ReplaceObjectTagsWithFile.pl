#!/usr/bin/perl -w

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#         @(#) ReplaceObjectTagsWithFile.pl 	15.10.2006
#
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         Replaces object tags with a string.  Does necessary formatting and escaping of string
#
#<!-- copyright (c) 2006-2008 by Bryant Hansen -->
#<!-- all rights reserved -->


use strict;

$/ = undef;

my $USAGE="$0  [ -v ] [ -g ] OriginalFileString ObjectTag FileWithReplaceString";

my $REPLACE_ALL="";
my $IS_OPTION_ARG=1;
my $VERBOSE="";
my $VERBOSE_ARG="";

my $f = "";
while ($ARGV[0] =~ /^-[A-Za-z]$/) {
	$f = shift;
	if ($f eq "-v") {
		$VERBOSE=1;
		$VERBOSE_ARG="-v";
		print STDERR "ReplaceObjectTags: Verbose is on!";		
	}
	if ($f eq "-g") {
		$REPLACE_ALL="g";
	}
}

my  $inFile = shift;
if ($#ARGV != 1) {
	print "$inFile"; # we're probably expecting this for standard output, no need to test if it exists
	print STDERR "ERROR: ReplaceObjectTags requires 3 args, got $#ARGV.  args=\"@ARGV\"!  Exiting abnormally.";
	exit -200;
}
else {

	# todo: attempt to deal with the situation of comments: removing the close-comment tag from an insert block when we're already in a comment in the main block

	my  $TAG = shift;
	my  $OBJECT_TYPE_TAG="PhotoAlbumTag";
	my  $pObjectTag = "\Q<object\E[\t ]*\Qid=\"$TAG\"\E[\t ]*\Qtype=\"$OBJECT_TYPE_TAG\"\E[^>]*>";
	my  $pFileWithReplaceString = shift;
	
	# TODO: test if the file exists
	
	my $STRING1 = `cat "$inFile"`;
	my $pReplaceString = `cat "$pFileWithReplaceString"`;
	
	# do some sanity checking

	if ($pObjectTag ne "") {
		if ($REPLACE_ALL eq "g") {
			$STRING1 =~ s/$pObjectTag/$pReplaceString/g;
		}
		else {
			$STRING1 =~ s/$pObjectTag/$pReplaceString/;
		}
	}
	
	open (OUT, ">$inFile") || die "Couldn't open $inFile for writing: $!. pwd=$ENV{'PWD'}";	
	print OUT $STRING1;
	close (OUT);
	
	if ( $VERBOSE ) {
		if ( $REPLACE_ALL eq "g" ) {
			print STDERR "replaced all instances of \"$pObjectTag\" with \"$pReplaceString\"";
		}
		else
		{
			print STDERR "replaced first instance of \"$pObjectTag\" with \"$pReplaceString\"";
		}
		print STDERR "$STRING1";
#		WaitForKey
	}
}

