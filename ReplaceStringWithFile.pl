#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#         MakePicturePages.sh 	1.1.0  13 Aug. 2004 	
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#         Creates an HTML photo album which displays 1 picture at a time, 
#
#         copyright (c) 2006-2008 by Bryant Hansen

my $base_file;
my $replace_file;
my $out_file;
my $content;
my $verbose;
my $result = -1;

$base_file = shift;
if ($base_file == "-v")
{
	print "verbose is on\n";
	$verbose = 1;
#	$base_file = shift;
}
$replace_file = shift;
$tag = shift;

# use the program that we're running as a part of the temporary filename
$basename_basefile = `basename $0`;
chomp ($basename_basefile);
$out_file = './' . $base_file . '.' . $basename_basefile . '.tmp';

print "base_file=$base_file, replace_file=$replace_file, tag=$tag, out_file=$out_file\n";

$file1=`cat $replace_file`;

open (IN, $base_file) || die "Couldn't open $base_file: $!";
open (OUT, ">$out_file") || die "Couldn't open $out_file for writing: $!. pwd=$ENV{'PWD'}";

# $/ is record seperator, usually '\n'
$/ = undef; # slurp mode
$content = <IN>;

my $temp;
$temp = `cat $base_file`;
$temp =~ s/\\/\\\\/g;
$temp =~ s/\#/\\\#/g;
$temp =~ s/\./\\\./g;
$temp =~ s/\*/\\\*/g;
$temp =~ s/\?/\\\?/g;
$temp =~ s/\[/\\\[/g;
$temp =~ s/\]/\\\]/g;
$temp =~ s/\(/\\\(/g;
$temp =~ s/\)/\\\)/g;
$temp =~ s/\{/\\\{/g;
$temp =~ s/\}/\\\}/g;
$temp =~ s/\^/\\\^/g;
$temp =~ s/\$/\\\$/g;
$temp =~ s/\|/\\\|/g;

print ("base_file=$temp\n\n");
print ("replace_file = $file1\n\n");
print ("tag=$tag\n\n");


$content =~  s/$tag/$file1/g;
# $content =~  s/\Q$tag\E/$file1/g;
print OUT $content;
close(IN);
close(OUT);

system ("mv -f \"$out_file\" \"$base_file\"");

print "$0: done\n";

exit ($result);
