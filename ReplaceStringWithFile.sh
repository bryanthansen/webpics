# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) ReplaceStringWithFile.sh     1.0     16 Mar 2003
#
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         Searches a file for instances of a string and replaces all instances with the data 
#         in a second file
#

USAGE="$0 [ -v ] BASE_FILE INSERT_FILE FILE_INSERT_TAG"
VERBOSE=""
TAGFOUND=0

TEMP_FILE="$0.tmp"

echo "calling ReplaceStringWithFile.pl instead of normal routine with args: $*"
/projects/webpics/ReplaceStringWithFile.pl $*
RESULT="$?"
echo "ReplaceStringWithFile.pl done."

exit $RESULT

if [ "$1" = "-v" ] ; then
   VERBOSE=1
   shift # discard the verbose argument and continue
fi

if [ $# -eq 3 ] ; then
   BASE_FILE="$1"
   INSERT_FILE="$2"
   FILE_INSERT_TAG="$3"
else
   echo "$0 ERROR: invalid arguments."
   echo "USAGE: $USAGE"
   echo "Press any key to continue"
   read -n 1
   exit -1
fi
if [ $VERBOSE ] ; then
   echo "Entering $0 (BASE_FILE=$BASE_FILE, INSERT_FILE=$INSERT_FILE, FILE_INSERT_TAG=$FILE_INSERT_TAG)"
	less "$BASE_FILE"
	less "$INSERT_FILE"
fi

if [ ! -f "$BASE_FILE" ] ; then
	echo "$0 error: BASE_FILE \"$BASE_FILE\" not found!"
	echo "params: BASE_FILE=$BASE_FILE, INSERT_FILE=$INSERT_FILE, FILE_INSERT_TAG=$FILE_INSERT_TAG"
	exit -65
fi

if [ ! -f "$INSERT_FILE" ] ; then
	echo "$0 error: INSERT_FILE \"$INSERT_FILE\" not found!"
	echo "params: BASE_FILE=$BASE_FILE, INSERT_FILE=$INSERT_FILE, FILE_INSERT_TAG=$FILE_INSERT_TAG"
	exit -66
fi

if [ "$FILE_INSERT_TAG" == "" ] ; then
	echo "$0 error: FILE_INSERT_TAG is NULL!"
	echo "params: BASE_FILE=$BASE_FILE, INSERT_FILE=$INSERT_FILE, FILE_INSERT_TAG=$FILE_INSERT_TAG"
	exit -67
fi

if [ -f "$BASE_FILE" ] && [ -f "$INSERT_FILE" ] && [ "$FILE_INSERT_TAG" != "" ] ; then
	TEMP_FILE="${BASE_FILE}.$0.tmp"
   rm -f "${TEMP_FILE}"
   cat "${BASE_FILE}" | while read 'line' ; do
   {
      if [ ! "$line" = "${line#*$FILE_INSERT_TAG*}" ] ; then
         TAGFOUND=1
         echo            "${line%<!--*$FILE_INSERT_TAG*}" | sed -e s/"<!--*$"// >> "${TEMP_FILE}"
         # tag found
         while read 'insertLine' ; do
         {
            echo "$insertLine" >> "${TEMP_FILE}"
         }
         done <"$INSERT_FILE"
         echo "${line#*$FILE_INSERT_TAG}" | sed -e s/"-->*"// >> "${TEMP_FILE}"
      else
         echo $line >> "${TEMP_FILE}"
      fi
   }
   done
else
   echo "$0 error: bad initial conditions!"
   echo "USAGE: $USAGE"
   echo "params: BASE_FILE=$BASE_FILE, FILE_INSERT_TAG=$FILE_INSERT_TAG, INSERT_FILE=$INSERT_FILE"
   exit -68
fi

if [ $TAGFOUND -eq 0 ] ; then
   echo "$0 error: tag \"$FILE_INSERT_TAG\" not found!"
else
   mv -f "${TEMP_FILE}" "$BASE_FILE"
   exit 1
fi

