# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         Verifies that all of the template files necessary to produce pages for MakePicindex.sh and MakePicturePages.sh
#
# copyright (c) 2006-2014 by Bryant Hansen

USAGE="$0 [ -v ] TARGET_DIR [ SOURCE_DIR ]"
# USAGE="$0 [ -v ] TARGET_DIR [ SOURCE_DIR ]"

VERBOSE=""
VERBOSE_ARG=""
INTERERACTIVE=1

SOURCE_DIR=""
TARGET_DIR="."

TEMPLATE_DIR=templates
MANIFEST_FILE=manifest.txt

# TODO: comment what the point of this is
# answer: verbose could be a null argument
[[ ! "$1" ]] && shift 

if [[ "$1" = "-v" ]] ; then
    VERBOSE=1
    echo "$0 called, verbose arg is set." 1>&2
    shift # discard the verbose argument and continue
fi

WaitForKey() {
    local tmp
    echo "Press Any Key" 1>&2
    read -n 1 'tmp' <&1
#	read -n 1
    return 0
}

DebugMessage() {
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`: "

    while [ "$OPTION_FOUND" ] ; do
        OPTION_FOUND=""
        if [ "$1" = "-e" ] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [ "$1" = "-n" ] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done		

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}

relpath2() {
    # both $1 and $2 are absolute paths
    # returns $2 relative to $1

    source=$1
    target=$2

    common_part=$source
    back=""
    while [[ "${target#$common_part}" = "${target}" ]] ; do
        common_part=$(dirname $common_part)
        back="../${back}"
    done

    echo ${back}${target#$common_part/}
}

relpath() {
    #TARGDIR=/data/media/pictures/OccupyParadeplatz\ 20111015/templates ; SRCDIR=/data/media/pictures/OccupyParadeplatz\ 20111015/.thumbnails256/templates ; iter=0 ; targ="" ; rel="" ; com="" ; for (( iter=0 ; iter<10 ; iter++ )) ; do [[ ! "$targ" ]] && targ="${TARGDIR%/}" ; [[ "${SRCDIR#${targ}}" != "${SRCDIR}" ]] && echo "$targ is common; rel=$rel dir=${rel}${SRCDIR#${targ}/}" && break ; targ="$(dirname "$targ")" ; rel="../${rel}" ; iter=$(( iter++ )) ; echo "targ=$targ  rel=$rel iter=$iter" ; done
    SRCDIR="${1%/}"
    TARGDIR="${2%/}"

    targ="$TARGDIR"
    rel=""
    com=""
    MAXPATH=64
    for (( iter=0 ; iter<${MAXPATH} ; iter++ )) ; do
        [[ ! "$targ" ]] && targ="${TARGDIR%/}"
        if [[ "${SRCDIR#${targ}}" != "${SRCDIR}" ]] ; then
            #echo "$targ is common; rel=$rel dir=${rel}${SRCDIR#${targ}/}"
            echo "${rel}${SRCDIR#${targ}/}"
            break
        fi
        targ="$(dirname "$targ")"
        rel="../${rel}"
        #echo "targ=$targ  rel=$rel iter=$iter"
    done
}

test_source_dir_validity()
{
    local SRC="$1/$TEMPLATE_DIR"
    local TARG="$2/$TEMPLATE_DIR"

    (( $VERBOSE )) && DebugMessage "Enter test_source_dir_validity: source dir=($1), targ dir=($2)"

    # make absolute
    [[ "${SRC:0:1}" = "." ]] && SRC="$PWD/$SRC"
    [[ "${TARG:0:1}" = "." ]] && TARG="$PWD/$TARG"

    # get rid of /dirname1/./dirname2..  /./ becomes /
    SRC=${SRC//\/.\//\/}
    TARG=${TARG//\/.\//\/}

    # get rid of /dirname1/../dirname2/  /dirname1/../ becomes /
    SRC="`echo "$SRC"   | sed "s/\/[~/]*\/..\//\//g"`"
    TARG="`echo "$TARG" | sed "s/\/[~/]*\/..\//\//g"`"

    # replace // with /
    SRC=${SRC//\/\//\/}	# wow, that's cryptic //=replace all; \/\/=2 escaped slashes; /=delimiter between replace string and replace with string; \/=1 escaped slash
    TARG=${TARG//\/\//\/}

    if [ "$SRC" = "$TARG" ] ; then
        #(( $VERBOSE )) && \
            DebugMessage "test_source_dir_validity: source dir ($1) and targ dir ($2) are the same!"
        return 255
    fi
    if [ ! -d "$SRC" ] ; then
        #(( $VERBOSE )) && \
            DebugMessage "test_source_dir_validity: source dir ($SRC) does not exist!"
        return 255
    fi
    if [ ! -f "$SRC/${MANIFEST_FILE}" ] ; then
        #(( $VERBOSE )) && \
            DebugMessage "test_source_dir_validity: source dir ($SRC) does not contain the critical test template file, ${MANIFEST_FILE}!"
        return 255
    fi
    if [ "`grep "Thumbnail n" "$SRC/index.html" | grep -v "PhotoAlbumTag"`" ] ; then 
        DebugMessage "WARNING: \"$SRC/index.html\" appears to use old style tags: \"Thumbnail n\""
    fi
    echo "VALID_SOURCE_DIR"
}

################################################################
# main

if [[ $# -gt 2 ]] ; then
    DebugMessage "VerifyTemplateFiles ERROR: invalid number of arguments: $#!"
    DebugMessage "USAGE: $USAGE"
    exit -1
else
    TARGET_DIR="$1"
fi

if [[ ! -d "$1" ]] ; then
    if [[ "$VERBOSE" ]] ; then
        DebugMessage "ERROR: $1 does not exist and cannot be created.  Exiting abnormally."
    fi
    mkdir "$1"
fi
if [[ ! -d "$1" ]] ; then
    DebugMessage "ERROR: $1 does not exist and cannot be created.  Exiting abnormally."
    exit -5
fi
# cd "$1"

DEFAULT_SOURCE_DIR="."
if [[ "$#" -eq "2" ]] ; then
    if [[ -d "$2" ]] ; then
        DEFAULT_SOURCE_DIR="$2"
    else
        DebugMessage "ERROR: Default source dir: \"$2\" does not exist!  Using normal template paths."
    fi
fi

#check for an HTML template file in this directory
# TODO: verify that we don't use the same SOURCE_DIR and TARGET_DIR
# path search order: 

SOURCE_DIR="${DEFAULT_SOURCE_DIR}"
if [ -z "`test_source_dir_validity "$SOURCE_DIR" "$TARGET_DIR"`" ] ; then
    # DEFAULT_SOURCE_DIR is bad; test the current directory
    SOURCE_DIR="."
    if [ -z "`test_source_dir_validity "$SOURCE_DIR" "$TARGET_DIR"`" ] ; then
        # CURRENT is bad; test the parent directory
        SOURCE_DIR=".."
        if [ -z "`test_source_dir_validity "$SOURCE_DIR" "$TARGET_DIR"`" ] ; then
            # CURRENT is bad; test the home($HOME) directory
            SOURCE_DIR=$HOME
            if [ -z "`test_source_dir_validity "$SOURCE_DIR" "$TARGET_DIR"`" ] ; then
                # $HOME is bad; test the home(~) directory
                SOURCE_DIR=~
                if [ -z "`test_source_dir_validity "$SOURCE_DIR" "$TARGET_DIR"`" ] ; then
                    # home directory is bad; test the etc directory
                    SOURCE_DIR="/etc"
                    if [ -z "`test_source_dir_validity "$SOURCE_DIR" "$TARGET_DIR"`" ] ; then
                        # /etc is bad; give up
                        SOURCE_DIR=""
                        DebugMessage "$0 $* (in $PWD): Could not find a valid source dir; giving up."
                        exit 2
                    fi
                fi
            fi
        fi
    fi
fi
SOURCE_DIR="${SOURCE_DIR}/$TEMPLATE_DIR"

DebugMessage "$0 $* (in $PWD): SOURCE_DIR=\"${SOURCE_DIR}\", TARGET_DIR=\"${TARGET_DIR}/${TEMPLATE_DIR}\""

if [[ ! -d "$SOURCE_DIR" ]] ; then
    DebugMessage "$0 ERROR: Template Source Directory \"${SOURCE_DIR}\" does not exist."
    DebugMessage "USAGE=$USAGE"
    if [[ $INTERACTIVE ]] ; then
        DebugMessage "Press any key"
        read -n 1
    fi
    exit 3
fi

# ensure that we have all of the proper template files in the current directory
if [[ ! -d "${TARGET_DIR}/${TEMPLATE_DIR}" ]] ; then
    DebugMessage "Creating template directory in \"${TARGET_DIR}\"."
    mkdir -p "${TARGET_DIR}/${TEMPLATE_DIR}"
fi

FILES_VERIFIED=1

targdir="$PWD"
srcdir="$(relpath "${SOURCE_DIR}" "${TARGET_DIR}/${TEMPLATE_DIR}")"
DebugMessage "srcdir=$srcdir  PWD=$PWD"

pushd "${TARGET_DIR}/${TEMPLATE_DIR}" > /dev/null

if [[ ! -f "${srcdir}/${MANIFEST_FILE}" ]] ; then
    DebugMessage "ERROR: ${MANIFEST_FILE} not found in \"${SOURCE_DIR}\".  (PWD=$PWD)  Exiting Abnormally"
    exit 4
fi

DebugMessage "$0: reading needed files from the manifest (${srcdir}/${MANIFEST_FILE}).  PWD=$PWD"
# copy all template files from the base templete directory to the new template directory

cat "${srcdir}/${MANIFEST_FILE}" | while read f ; do
    if [[ -f "${srcdir}/${f}" ]] ; then
        if [[ ! -f "./${f}" ]] ; then
            ln -fs "${srcdir}/${f}" "./${f}"
        else
            [[ "$VERBOSE" ]] && (( "$VERBOSE" )) && \
                echo "${targdir}/${f} exists"
        fi
        if [[ ! -f "./${f}" ]] ; then
            DebugMessage "ERROR: failed to create link to template file $f in \"${targdir}\" return code $?  (PWD=$PWD)"
        fi
    else
        DebugMessage "WARNING: VerifyTemplatesExist: base template file \"${srcdir}/${f}\" does not exist.  Check ${MANIFEST_FILE}!  (PWD=$PWD)"
        FILES_VERFIED=0
    fi
done

popd > /dev/null
if (( ! "$FILES_VERIFIED" )) ; then
    exit 100
fi

# copy the text description is we have one
DESCRIPTION_FILE="$SOURCE_DIR/$CHILD_DIR.txt"
CHILD_DIR_NAME=""
if [[ -f "$DESCRIPTION_FILE" ]] ; then
    if [ cd "$CHILD_DIR" ] ; then
        # if the CHILD_DIR is a link, it may have a different name once we're inside
        # the current directory may have a different name from the one we cd'd to
        local CHILD_DIR_NAME=`basename "$PWD"`
        CopyNoOverwrite $VERBOSE_ARG "$DESCRIPTION_FILE" "$CHILD_DIR_NAME.txt"
        cd ..
    fi
fi

if [[ -f "$SOURCE_DIR/excludes.txt" ]] ; then
    if [[ ! -f "$TARGET_DIR/excludes.txt" ]] ; then
        # ln -s "$SOURCE_DIR/excludes.txt" "$TARGET_DIR/excludes.txt"
        cp -a "$SOURCE_DIR/excludes.txt" "$TARGET_DIR/excludes.txt"
    fi
fi


if [[ $VERBOSE ]] ; then
    DebugMessage "completed normally."
fi
exit 0
