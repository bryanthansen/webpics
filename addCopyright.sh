# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# addCopyright.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/bash

input_image="$1"
output_image="$2"

background_color="rgb(15,15,15)"
text_color="rgb(215,215,215)"
COPYRIGHT_FONT="helvetica"

COPYRIGHT_STRING="copyright (c)2011 B. Hansen"
COPYRIGHT_GRAVITY="SouthWest"

TEXT_COLOR="rgb(0,0,0)"
TEXT_HIGHLIGHT_COLOR="rgb(200,200,200)"

CONVERT=`which convert`
[[ "$CONVERT" ]] || CONVERT=/usr/bin/convert
######################################################



######################################################
# Functions
######################################################

SourceFiles()
{
    CONF_FILE="album.conf"
    [[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
    [[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
    [[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
    [[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}
    FILE_SPECIFIC_CONF="$(dirname "$output_image")/.$(basename "$output_image").conf"
    [[ -f "$FILE_SPECIFIC_CONF" ]]       && . "$FILE_SPECIFIC_CONF"
}

DebugMessage()
{
    local NO_CR_ARG=""
    local SPECIAL_CHAR_ARG=""
    local ARGS=""
    local OPTION_FOUND="0"
    local LOG_RECORD_HEADER="`date "+%Y%m%d%H%M%S"`:`basename "$0"`($THUMB_SIZE): "

    while [[ "$OPTION_FOUND" ]] ; do
        OPTION_FOUND=""
        if [[ "$1" = "-e" ]] ; then
            OPTION_FOUND="-e"
            ARGS="${ARGS} -e"
            shift
        fi
        if [[ "$1" = "-n" ]] ; then
            OPTION_FOUND="-n"
            ARGS="${ARGS} -n"
            LOG_RECORD_HEADER=""
            shift
        fi
    done

    echo ${ARGS} "${LOG_RECORD_HEADER}${*/${ARGS}/}" 1>&2

    return 0
}

function getCopyrightString() {
    local copyright_string="(c)2011 Bryant Hansen"
    local STRIP_HTML="s/<[^>]*>//g"

    # last one wins
    [[ "$COPYRIGHT_STRING" ]] && copyright_string="$COPYRIGHT_STRING"

    copyright_file="templates/copyright.html"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    copyright_file="copyright.txt"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    copyright_file="${input_image}.copyright"
    [[ -f "$copyright_file" ]] && \
        copyright_string="`cat "$copyright_file" | sed "$STRIP_HTML"`" && \
        DebugMessage "  copyright string read from ${copyright_file}: ${copyright_string}"

    DebugMessage "copyright=$copyright_string"

    echo "$copyright_string"
    return 0
}

######################################################
# Setup Environment
######################################################
ME="$(basename "$0")"
MYDIR="$(dirname "$0")"
USAGE="${ME} [ [ -v | --verbose ] | [ -s size | --size size | --size=size ] | [ -c copyright_string | --copyright-string copyright_string | --copyright-string=copyright_string ]  input_image  output_image  size  [ BACKGROUND_COLOR ] "

IMAGE_INFO_DIR=".image_info"
CONF_FILE="album.conf"
[[ -f "/etc/webpics/${CONF_FILE}" ]] && . /etc/webpics/${CONF_FILE}
[[ -f "~/templates/${CONF_FILE}" ]]  && . ~/templates/${CONF_FILE}
[[ -f "./templates/${CONF_FILE}" ]]  && . ./templates/${CONF_FILE}
[[ -f "./${CONF_FILE}" ]]            && . ./${CONF_FILE}
FILE_SPECIFIC_CONF="$(dirname "$output_image")/.$(basename "$output_image").conf"
[[ -f "$FILE_SPECIFIC_CONF" ]]       && . "$FILE_SPECIFIC_CONF"

DebugMessage -e "\nStarting ${ME}..."


w="`identify -format "%w" "$input_image"`"
h="`identify -format "%h" "$input_image"`"

h2="`expr $h + 20`"

size="$w"
(( "$h" > "$size" )) && size="$h"

[[ ! "$FONT_SIZE" ]] && FONT_SIZE="`expr $size \/ 70 + 5`"

FRAME_THICKNESS="20"

COPYRIGHT_X="`expr $FRAME_THICKNESS + 0`"
#COPYRIGHT_X="`expr $FRAME_THICKNESS \* 80 \/ 100 + 40`"

# Y position has caused lots of problems
MARGIN_SIZE="`expr $FRAME_THICKNESS - $FONT_SIZE`"
COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100 + \( $FRAME_THICKNESS - $FONT_SIZE \) \* 100 \/ 100 + 0`"
COPYRIGHT_Y="`expr $FONT_SIZE \* 120 \/ 100 + $MARGIN_SIZE`"
COPYRIGHT_Y="`expr $FONT_SIZE \* 20 \/ 100` - 10"
COPYRIGHT_Y="0"

SourceFiles

COPYRIGHT_STRING="$(getCopyrightString)"
echo "  COPYRIGHT_STRING=\"${COPYRIGHT_STRING}\""
echo "  COPYRIGHT_FONT=${COPYRIGHT_FONT}"
echo "  FONT_SIZE=${FONT_SIZE}"
echo "  TEXT_COLOR=${TEXT_COLOR}"
echo "  TEXT_HIGHLIGHT_COLOR=${TEXT_HIGHLIGHT_COLOR}"
echo "  COPYRIGHT_X,Y=(${COPYRIGHT_X},${COPYRIGHT_Y})" >&2

CONVERT_CMD=" \
    $CONVERT \
        "${input_image}" \
"

CONVERT_CMD="${CONVERT_CMD} \
    -gravity \"${COPYRIGHT_GRAVITY}\" \
    -font \"${COPYRIGHT_FONT}\" \
    -pointsize \"${FONT_SIZE}\" \
    -fill \"${TEXT_HIGHLIGHT_COLOR}\" \
    -draw 'text ${COPYRIGHT_X},${COPYRIGHT_Y} \"${COPYRIGHT_STRING}\"' \
    -fill \"${TEXT_COLOR}\" \
    -draw 'text $(expr ${COPYRIGHT_X} + 1),$(expr ${COPYRIGHT_Y} + 1) \"${COPYRIGHT_STRING}\"' \
"

#            -gravity \"${COPYRIGHT_GRAVITY}\" \
#            -font \"${COPYRIGHT_FONT}\" \
#            -pointsize \"${FONT_SIZE}\" "
#        if [[ "$DOUBLE_HIGHLIGHT" ]] && (( "$DOUBLE_HIGHLIGHT" )) ; then
#            CONVERT_DRAW_COPYRIGHT="$CONVERT_DRAW_COPYRIGHT \
#            -fill \"rgb(0,0,0)\" \
#            -draw 'text $(expr ${COPYRIGHT_X} + 2),$(expr ${COPYRIGHT_Y} + 2) \"${COPYRIGHT_STRING}\"' \
#            -blur 1,1 "
#        fi
#        CONVERT_DRAW_COPYRIGHT="$CONVERT_DRAW_COPYRIGHT \
#            -fill \"${TEXT_HIGHLIGHT_COLOR}\" \
#            -draw 'text ${COPYRIGHT_X},${COPYRIGHT_Y} \"${COPYRIGHT_STRING}\"' \
#            -blur 1,1    \
#            -fill \"${TEXT_COLOR}\" \
#            -draw 'text $(expr ${COPYRIGHT_X} + 1),$(expr ${COPYRIGHT_Y} + 1) \"${COPYRIGHT_STRING}\"' \



#  -gravity "SouthEast"  \
#  -font "/usr/share/fonts/gothic/minster6.ttf"  \
#  -pointsize "37"       \
#  -fill "rgb(255,255,255)"      \
#  -draw 'text 7,1 "     \
#  2013 B Hansen"'       \
#  -blur 1,1     \
#  -fill "rgb(0,0,0)"    \
#  -draw 'text 8,2 "     \
#  2013 B Hansen"'       \


CONVERT_CMD=" \
    ${CONVERT_CMD} \
        "${output_image}"
"


echo "$CONVERT_CMD"
echo "$CONVERT_CMD" | /bin/bash

exit 0

convert \
    -size ${w}x${h2} \
    xc:none \
    -fill "$background_color" \
    -draw "rectangle 0,0 ${w},${h2}" \
    -gravity "North" \
    -draw "image Over 0,0 0,0 $input_image" \
    -gravity "SouthEast" \
    -font \"${COPYRIGHT_FONT}\" \
    -pointsize 12 \
    -fill "$TEXT_COLOR" \
    -draw "text 7,2 \"${COPYRIGHT_STRING}\"" \
    "${image}.copyright.png"

