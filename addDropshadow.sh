# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# addDropshadow.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

#!/bin/bash

image="$1"
background_color="rgb(215,215,215)"

h="`identify -format "%h" "$image"`"
w="`identify -format "%w" "$image"`"

h2="`expr $h + 20`"
w2="`expr $w + 20`"

echo "$image: ${h}x${w} becomes ${h2}x${w2}"

# create the dropshadow image
#convert \
#	-size ${w2}x${h2} \
#	xc:none \
#	-fill "$background_color" \
#	-draw "rectangle 0,0 ${w2},${h2}" \
#	-fill "rgb(0,0,0)" \
#	-draw "rectangle 25,25 ${w},${h}" \
#	-blur "30,65" \
#	-draw "image Over 20,20 0,0 $image" \
#	"${image}.dropshadow.png"
convert \
	-size ${w2}x${h2} \
	xc:none \
	-fill "$background_color" \
	-draw "rectangle 0,0 ${w2},${h2}" \
	-fill "rgb(0,0,0)" \
	-draw "rectangle 40,40 ${w},${h}" \
	-blur "30,15" \
	-draw "image Over 0,0 0,0 $image" \
	"${image}.dropshadow.png"
