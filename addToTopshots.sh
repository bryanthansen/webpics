#!/bin/bash
# Bryant Hansen
# 20110423

# from the gqview script:
# [[ ! -d ./.top-shots ]] && mkdir ./.top-shots ; cp -al %f ./.top-shots/

file="$1"
dir="$(dirname "$file")"
[[ ! -d "$dir"/.top-shots ]] && mkdir -p "$dir"/.top-shots
cp -al "$file" "$dir"/.top-shots/
