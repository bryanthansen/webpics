#!/bin/bash

# from http://studio.imagemagick.org/pipermail/magick-users/2004-January/011814.html

USAGE="$0 imageFilename"

set -o verbose

if ($# != 1) ; then
   echo "$0 requires 1 argument"
   echo "usage=$USAGE"
   exit -2
fi

# Extract mask of original image  (assuming IM 5.5.7 beta)
#      convert -fx 'u.opacity[0]' null "$1" -negate mask.jpg
      convert -fx u.opacity[0] null "$1" -negate mask.png

# apply guassian transform on mask to generate fade out
#   (border will become 50% transparent)
      convert mask.png -gaussian 0x6 +matte mask_fade.png

# overlay the original image onto a canvas of the same size to
#   set the color of fade out, outside the image (say black)
      convert -threshold 100%  "$1"  miff:- | composite $1 - orig_fade.png

# now re-add the mask..
     composite -compose CopyOpacity mask_fade.png orig_fade.png fadeout.jpg


exit 0
