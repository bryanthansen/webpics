#!/bin/bash
# Bryant Hansen

# read input from a makefile
# in makefile format
# ideally using makefile keyword expansion tools
#   - with whitespace-in-filename improvements

echo "PWD=$PWD" >&2
echo "0=$0" >&2

if [[ ! -f Makefile ]] ; then
	echo "ERROR: Makefile not found" >&2
	echo "exiting abnormally" >&2
	exit 2
fi

DELETE_COMMENTS="s/\#.*//g"
DELETE_AFTER_COLON="s/:.*$//g"
DELETE_BLANKS="/^[ \t]*$/d"
TRIM=""
cat Makefile | \
    grep ":" | \
#    sed "/^.*\:.*$/d;s/:.*$//g;/^[ \t]*$/d" | \
#    sed "s/\#.*//g;s/:.*$/:/g;/^[ \t]*$/d" | \
    sed "${DELETE_COMMENTS};${DELETE_AFTER_COLON};${DELETE_BLANKS}" | \
    sort | \
    uniq | \
    while read l ; do
        echo "process line: \"${l}\"" >&2
        echo "$l"
    done | \
    while read l ; do
        grep "^${l}.*" Makefile
    done | \
    while read l2 ; do 
        target="${l2//:*/}"
        [[ "$target" == ".PHONY" ]] && continue
        dependency="${l2//*:/}"
        [[ ! "$dependency" ]] && echo -e "target \"${target}\" has no dependencies" && continue
        echo ""
        echo "$l2"
        echo -e "target=${target} \tdependency=${dependency}"
        [[ ! "${target//*%*/}" ]] && echo "check $target"
        match="${target//%/*}"
        echo "match=$match"
        #varlines="$(grep -E "^${targets}[ \t]*:*=.*$" Makefile)"
        varlines="$(grep -E ".*${target}.*=.*$" Makefile)"
        echo "varlines=$varlines"
        list="$(ls -1 $match 2>/dev/null | tr "\n" " ")"
        [[ "$list" ]] && echo -e "target list: ${list}"
        [[ ! "$list" ]] && echo -e "targets \"${target}\" not found"
    done \
    | less

# ./checkDependencies.sh Makefile 2>/dev/null | while read l ; do grep "^${l}.*" Makefile ; done | while read l2 ; do echo "$l2" ; target="${l2//:*/}" ; dependency="${l2//*:/}" ; echo -e "target=${target} \tdependency=${dependency}" ; [[ ! "${target//*%*/}" ]] && echo "check $target" ; match="${target//%/*}" ; echo "match=$match" ; list="$(ls -1 $match 2>/dev/null)" ; [[ "$list" ]] && echo -e "-----\nlist\n-----\n${list}\n-----" ; echo "" ; done | less