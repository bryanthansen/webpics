#!/bin/bash
# Bryant Hansen

# convert exif to xml for http publishing

# initial 1-liner:
#    cat .image_info/20120915_122753_IMG_0422.JPG.txt | head -n 20 | sed "s/^/\|/" | while read l ; do l2="${l#|}" ; l3="${l2%%[! ]*}" ; spaces="${l3}" ; echo -n "indent_level=${#spaces} " ; echo "$l" ; done


if [[ "$1" ]] ; then
    echo "dumping XML from $1" >&2
else
    echo "reading from stdin and dumping to XML" >&2
fi

# if $1 does not exist, read from stdin
# add a start-of-line character so the leading spaces are not thrown away in the while read loop
# add 2 spaces to the end to process last_lines in next stages
(
    if [[ "$1" ]] ; then
        cat "$1" | sed "s/^/\|/"
    else
        sed "s/^/\|/" | \
        while read l ; do
            echo "$l"
        done
    fi
    echo ""
    echo ""
) | \
    while read l ; do
        # This stage processes the standard var: val lines, using greater indent levels
        # as an indication of a directory
        # TODO: consider using last of val as a directory indicator
        #   it seems that the indents are the only good way (I can think of) to traverse up
        #   multiple directories
        l2="${l#|}"
        l3="${l2%%[! ]*}"
        spaces="${l3}"
        indent_level="${#spaces}"
        d1="${last_line#|}"
        d2="${d1##*  }"
        var="${d2%%:*}"
        var="${var// /_}"
        val="${d2#*:}"
        val="${val# }"
        #echo "$l  #space=${#spaces}  #last_spaces=${#last_spaces}"
        if [[ "$last_indent_level" ]] ; then
            if [[ "$indent_level" == "$last_indent_level" ]] ; then
                # this is an attribute
                echo "${spaces}<${var}>${val}</${var}>"
                last_var="$var"
            elif [[ "$indent_level" -gt "$last_indent_level" ]] ; then
                # this is a directory
                #echo "# dir[${last_indent_level}]=$last_line <${d3}>" >&2
                #echo "# val=$val"
                echo "${last_spaces}<${var}>"
                dir[${last_indent_level}]="${var}"
            elif [[ "$indent_level" -lt "$last_indent_level" ]] ; then
                # this is a directory end
                #echo "# val=$val"
                echo "${last_spaces}<${var}>${val}</${var}>"
                i="$last_indent_level"
                while [[ "$i" -ge "$indent_level" ]] ; do
                    if [[ "$i" != "$last_indent_level" ]] ; then
                        echo "${last_spaces#  }</${dir[$i]}>"
                        last_spaces="${last_spaces#  }"
                    fi
                    i=$(expr $i - 2)
                done
            fi
        fi
        last_indent_level="$indent_level"
        last_line="$l2"
        last_spaces="$spaces"
    done |
    sed "s/^/\|/" | \
    while read l ; do
        [[ ! "$level" ]] && level=0
        l2="${l#|}"
        l3="${l2%%[! ]*}"
        spaces="${l3}"
        var="${l#|}"
        val="${var#*>}"
        var="${var%%>*}>"
        [[ ! "${var/* <\/*/}" ]] && var=""
        read  -rd '' var <<< "$var"
        var="${var// /_}"
        if [[ "$var" ]] && [[ "$last_var" == "$var" ]] ; then
            if [[ "$last_duplicate" != "$var" ]] ; then
                echo "${last_spaces}${last_var}"
                dir[$level]="$var"
                level=$(expr $level + 1)
                v="$last_line"
                v2="${v#*${var}}"
                v3="${v2%${var/</</}*}"
                subvar="${v3%%:*}"
                subval="${v3#*:}"
                subval="${subval# }"
                subval="${subval// /_}"
                echo "  ${last_spaces}<${subvar}>${subval}</${subvar}>"
            fi
            v="$l"
            v2="${v#*${var}}"
            v3="${v2%${var/</</}*}"
            subvar="${v3%%:*}"
            subval="${v3#*:}"
            subval="${subval# }"
            subval="${subval// /_}"
            echo "  ${last_spaces}<${subvar}>${subval}</${subvar}>"
            last_duplicate="$var"
        else
            if [[ ! "$subval" ]] ; then
                echo "${last_line#|}"
            fi
            unset subval
        fi
        if [[ "$var" ]] && [[ "$last_var" != "$var" ]] ; then
            if [[ "$last_var" ]] && [[ "$level" -gt 0 ]] ; then
                echo "${last_spaces}${last_var/</</}"
                level=$(expr $level - 1)
            fi
        fi
        last_var="$var"
        last_line="$l"
        last_spaces="$spaces"
    done

exit 0

#indent_level=3 |  Depth: 8-bit
#indent_level=3 |  Channel depth:
## dir[3]=|  Channel depth:

# INPUT:
# Image: 20120915_122753_IMG_0422.JPG
#   Format: JPEG (Joint Photographic Experts Group JFIF format)
#   Class: DirectClass
#   Geometry: 4000x3000+0+0
#   Resolution: 180x180
#   Print size: 22.2222x16.6667
#   Units: PixelsPerInch
#   Type: TrueColor
#   Endianess: Undefined
#   Colorspace: sRGB
#   Depth: 8-bit
#   Channel depth:
#     red: 8-bit
#     green: 8-bit
#     blue: 8-bit
#   Channel statistics:
#     Red:
#       min: 0 (0)
#       max: 255 (1)
#       mean: 83.8543 (0.32884)

# OUTPUT (stage1):
# indent_level=0 |Image: 20120915_122753_IMG_0422.JPG
# indent_level=2 |  Format: JPEG (Joint Photographic Experts Group JFIF format)
# indent_level=2 |  Class: DirectClass
# indent_level=2 |  Geometry: 4000x3000+0+0
# indent_level=2 |  Resolution: 180x180
# indent_level=2 |  Print size: 22.2222x16.6667
# indent_level=2 |  Units: PixelsPerInch
# indent_level=2 |  Type: TrueColor
# indent_level=2 |  Endianess: Undefined
# indent_level=2 |  Colorspace: sRGB
# indent_level=2 |  Depth: 8-bit
# indent_level=2 |  Channel depth:
# indent_level=4 |    red: 8-bit
# indent_level=4 |    green: 8-bit
# indent_level=4 |    blue: 8-bit
# indent_level=2 |  Channel statistics:
# indent_level=4 |    Red:
# indent_level=6 |      min: 0 (0)
# indent_level=6 |      max: 255 (1)
# indent_level=6 |      mean: 83.8543 (0.32884)

# OUTPUT (stage2):
#<Image>
#  <Format>JPEG (Joint Photographic Experts Group JFIF format)</Format>
#  <Class>DirectClass</Class>
#  <Geometry>4000x3000+0+0</Geometry>
#  <Resolution>180x180</Resolution>
#  <Print size>22.2222x16.6667</Print size>
#  <Units>PixelsPerInch</Units>
#  <Type>TrueColor</Type>
#  <Endianess>Undefined</Endianess>
#  <Colorspace>sRGB</Colorspace>
#  <Depth>8-bit</Depth>
#  <Channel depth>
#    <red>8-bit</red>
#    <green>8-bit</green>
#  </Channel depth>
#  <Channel statistics>
#    <Red>
#      <min>0 (0)</min>
#      <max>224 (0.878431)</max>
#      <mean>127.709 (0.50082)</mean>
#      <standard deviation>51.927 (0.203635)</standard deviation>
#      <kurtosis>-0.264813</kurtosis>
#    </Red>
#    <Green>
#      <min>2 (0.00784314)</min>
#      <max>224 (0.878431)</max>
#      <mean>133.046 (0.521749)</mean>
#      <standard deviation>54.1682 (0.212424)</standard deviation>
#      <kurtosis>-0.28781</kurtosis>
#    </Green>
#    <Blue>
#      <min>2 (0.00784314)</min>
#      <max>232 (0.909804)</max>
#      <mean>140.38 (0.55051)</mean>
#      <standard deviation>56.2725 (0.220676)</standard deviation>
#      <kurtosis>-0.228778</kurtosis>
#    </Blue>
#  </Channel statistics>
#  <Image statistics>

