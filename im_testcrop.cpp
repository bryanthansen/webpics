#include <Magick++.h>
#include <iostream>
#include <string.h>

using namespace std;
using namespace Magick;
int main(int argc,char **argv)
{
	// Construct the image object. Seperating image construction from the
	// the read operation ensures that a failure to read the image file
	// doesn't render the image object useless.
	Image image;

	if (argc < 2) {
		cout << "ERROR: this program requires at least 1 argument!\n";
		return -2;
	}

	try {
		cout << "reading image...\n";

		// Read a file into image object
		image.read( argv[1] );

		// Crop the image to specified size (width, height, xOffset, yOffset)
		image.crop( Geometry(100,100, 100, 100) );

		// Write the image to a file
		char newfilename[256];

		strncpy(newfilename, argv[1], 256);
		strcpy(newfilename + strlen(argv[1]), ".2.jpg");
		cout << "writing " << newfilename << "...\n";

//		cout << "writing " << argv[1] << "...\n";


		image.write( newfilename );
	}
	catch( Exception &error_ )
		{
			cout << "Caught exception: " << error_.what() << endl;
			return 1;
		}
	return 0;
}

