# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# link_to_random_pic.sh
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# last updated: 20080708
#
# copyright (c) 2008 by Bryant Hansen
##################################################################
#!/bin/bash

# set defaults
random_link="random_pic.jpg"
LIST_FILE=.webpics_randomlist.txt

if [[ "$1" ]] && [[ -f "$1" ]] ; then
	LIST_FILE="$1"
	if [[ "$2" ]] ; then
		random_link="$2"
	fi
fi

num_lines=$(wc -l "$LIST_FILE" | cut -f 1 -d " ")
line_num=$(expr $RANDOM % $num_lines)
random_pic=$(sed -n "${line_num}p" "$LIST_FILE")

if [[ "$random_pic" ]] && [[ -f "$random_pic" ]] ; then
	ln -fs "$random_pic" "$random_link"
else
	echo "ERROR: picture \"$random_pic\" not found!"
fi

