# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) lrot.sh
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         rotate an image rot the left 90 degrees, using the best method availablie
#         currently that's jpegtran, but the ImageMagic convert utility can be used as well
#         the advantage of jgegtran is that it's lossless
#
#         copyright (c) 2002-2008 by Bryant Hansen

# NOTE: a side-effect of this script is that the file permissions will be changed
#       to the default creation permissions of the user that runs the script
#       Ownership is changed as well.

# TODO: solve the above issue by cat $1.tmp >> $1, instead of a mv operation
#       this should preserve the properties of the original, except the last
#       modified time, which should be updated anyway

USAGE="$0 <filename>"
if [ $# -lt 1 ] ; then
  echo "Wrong number of arguments: $#!  Expected 1 or more."
  echo "USAGE=$USAGE"
  exit 65
fi

if [ ! -f "$1" ] ; then
  echo "ERROR: $1 not found"
  shift
fi

while [ -f "$1" ]
do
  if [ ! -f "$1" ] ; then
    echo "ERROR: $1 not found"
  else
    echo "Rotating $1 90 degrees to the left..."
  #  convert -rotate "-90" "$1" "$1"
    if [ -f "$1.tmp" ] ; then
      rm -f "$1.tmp"
    fi
    jpegtran -copy all -rotate 270 "$1" > "$1.tmp"
    if [ -f "$1.tmp" ] ; then
      mv -f "$1.tmp" "$1"
      # update exif info to assume that the orientation is now upright
      exiv2 -M"set Exif.Image.Orientation 1" "$1"
    fi
  fi
  shift
done
