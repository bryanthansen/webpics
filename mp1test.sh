# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
#         @(#) MakePicindex.sh 	1.0 	14 Dec, 2002
#                         latest update: 4 June 2004
#
#         by Bryant Hansen <sourcecode@bryanthansen.net>
#
#         Creates an HTML photo album which will layout pictures and/or image sets on a web page in a tabular form.
#
#<!-- copyright (c) 2006 by Bryant Hansen -->
#<!-- all rights reserved -->

USAGE="$0 [ -r ] [ -v ] [ -ns ] [ --remove-new-templates ] [ --template-name=html_filename ] \
[ -o | --use-object-tags ] [ DIRECTORY_NAME ]"

# VERBOSE=""
VERBOSE_ARG=""
INTERERACTIVE=1

CONVERT=convert
BASE_DIR=""
THUMB_DIR_BASENAME=".thumbnails"
DEFAULT_THUMB_SIZE=256
THUMB_SIZE=256
THUMB_DIR="${THUMB_DIR_BASENAME}${DEFAULT_THUMB_SIZE}"
REMOVE_NEW_TEMPLATES=0
DEFAULT_IMAGE_SIZE=600
IMAGE_SIZE=0
SLIDESHOW=0  # variable to set whether the slide show is generates and a link to it below every picture
SHOWHEADING=1
USE_OBJECT_TAGS=0
BACKUP_ORIGINAL=0

PICTURE_FILENAME="index.html"
PICINDEX_TEMPLATE="index.html"
PICTURE_TEMPLATE="picture.html"

IMAGE_SET_TABLENAME="Table:ImageSubsets"
CURRENT_SET_TABLENAME="Table:CurrentSet"
IMAGE_TABLENAME="Table:Images"
MOVIE_TABLENAME="Table:Movies"

IMAGE_SET_FILENAME="ImageSetTable.html"
CURRENT_SET_FILENAME="CurrentSetTable.html"
IMAGE_SUBSET_FILENAME="ImageSubsetTable.html"
MOVIETABLE_FILENAME="MovieTable.html"

OBJECT_TYPE_TAG="PhotoAlbumTag"

IMAGE_TAG="ThumbnailN"
DESCRIPTION_TAG="DescriptionN"
TEXT_COMMENT_TAG="$TAG_SPEC text_comment"

CURRENT_SET_TAG="CurrentSet"
CURRENT_SET_DESCRIPTION="CurrentSetDescription"

IMAGE_SUBSET_TAG="ImageSubsets"
IMAGE_SUBSET_DESCRIPTION="ImageSubsetDescription"

FILENAME_TAG="$TAG_SPEC Filename"

CURRENT_DIRECTORY_TAG="CurrentDirectory"

LINK_PARENT_TAG="LinkParent"

RESERVED_IMAGE=0
RESERVED_IMAGES="forward.gif backward.gif heading.jpg heading.gif"

TEMPLATE_DIR="templates"
HTML_DIR="html"

WaitForKey()
{
	echo "Press Any Key"
	read -n 1
}

DebugMessage()
{
	echo "$0: $*"
}

FixThumbDirs()
{
	# FixThumbDirs: if there is already a directory that's not hidden (old style)
	# 
	
	# if we're using hidden directories now
	if [ "${THUMB_DIR_BASENAME:0:1}" = "." ] ; then
		# but the non-hidden directory exists and the hidden one does not
		if [ ! -f "$1" ] && [ -f "{$1:1}" ] ; then
			mv "{$1:1}" "$1"
		fi
	fi
	DebugMessage "Exit FixThumbDirs()" ; WaitForKey
}

ReadTableTemplates()
{
	local USAGE="ReadTableTemplates"
	
	if [ $# -ne 0 ] ; then
		DebugMessage "ReadTableTemplates ERROR: function called with $# parameters.  Expected 0.  Exiting abnormally..."
		DebugMessage "USAGE=$USAGE"
		WaitForKey
		exit 65
	fi

	if [ -f ${IMAGE_SET_FILENAME} ] ; then
		ImageData="`cat "${IMAGE_SET_FILENAME}"`"
	else
		DebugMessage "ERROR: ImageTableTemplate file: \"${IMAGE_SET_FILENAME}\" not found"
	fi
	if [ -f ${CURRENT_SET_FILENAME} ] ; then
		CurrentSetData="`cat "${CURRENT_SET_FILENAME}"`"
	else
		DebugMessage "ERROR: CurrentSetTableTemplate file: \"${CURRENT_SET_FILENAME}\" not found"
	fi
	if [ -f ${IMAGE_SUBSET_FILENAME} ] ; then
		ImageSubsetData="`cat "${IMAGE_SUBSET_FILENAME}"`"
	else
		DebugMessage "ERROR: ImageSubsetTableTemplate file: \"${IMAGE_SUBSET_FILENAME}\" not found"
	fi
	if [ -f ${MOVIETABLE_FILENAME} ] ; then
		MovieSetData="`cat "${MOVIETABLE_FILENAME}"`"
	else
		DebugMessage "ERROR: MovieTableTemplate file: \"${MOVIETABLE_FILENAME}\" not found"
	fi
}

CreateBackupOfFile()
{
	local USAGE="CreateBackupOfFile [ -v ] FILENAME"
	
	local FILENAME="$1"

	DebugMessage -n "backing up `pwd`/$FILENAME to $FILENAME.`date +%Y%m%d`..."

	if [ -f "$FILENAME" ] ; then
		if [ $VERBOSE ] ; then
			DebugMessage "backing up `pwd`/$FILENAME to $FILENAME.`date +%Y%m%d`"
		fi
		cp "`pwd`/$FILENAME" "$FILENAME.`date +%Y%m%d`"
		DebugMessage "backup done."
		return 1
	fi
	return 0
}

AddCurrentSetInfoToIndex()
{
	CURRENT_DIRECTORY_NAME=`basename "$PWD"`
	
	DIRECTORY_DESCRIPTION=""
	if [ -f "$CURRENT_DIRECTORY_NAME.txt" ] ; then
		DIRECTORY_DESCRIPTION="$CURRENT_DIRECTORY_NAME.txt"
		DebugMessage "   found description for current image set (\"$CURRENT_DIRECTORY_NAME\") at: $DIRECTORY_DESCRIPTION..."
	elif [ -f "$CURRENT_DIRECTORY_NAME"/"$CURRENT_DIRECTORY_NAME.txt" ] ; then
			DIRECTORY_DESCRIPTION="$CURRENT_DIRECTORY_NAME"/"$CURRENT_DIRECTORY_NAME.txt"
			DebugMessage "   found description for current image set (\"$CURRENT_DIRECTORY_NAME\") at: $DIRECTORY_DESCRIPTION..."
	else
		CurrentSetData=${CurrentSetData/"$CURRENT_SET_DESCRIPTION"/" - no comment (yet): the file \"$CURRENT_DIRECTORY_NAME.txt\" does not exist - "}
		DebugMessage "   no description found for \"$CURRENT_DIRECTORY_NAME\"."
	fi
	
	DebugMessage "CurrentSetData=$CurrentSetData"
	
	if [ -f "$DIRECTORY_DESCRIPTION" ] ; then
		while read 'DescriptionLine' ; do
			CurrentSetData=${CurrentSetData/"$CURRENT_SET_DESCRIPTION"/"$DescriptionLine $CURRENT_SET_DESCRIPTION"}
		done <"$DIRECTORY_DESCRIPTION"
	fi	
	CurrentSetData=${CurrentSetData/"$CURRENT_SET_DESCRIPTION"/""}
	DebugMessage "Exit AddCurrentSetInfoToIndex()"
}

AppendImageSetInformation()
{
	local dir="$1"
	# if this is the first image set, place the header at the top of the set
	if [ $IMAGE_SET_FOUND -eq 0 ] ; then
		IMAGE_SET_FOUND=1
	fi
	# CURRENT_SUBSET_TABLE=${CURRENT_SUBSET_TABLE/"$IMAGE_SUBSET_TAG"/"<A HREF=\"./${dir}.html\">$dir</A>"}
	ImageSetData=${ImageSetData/"$IMAGE_SUBSET_TAG"/"<A HREF=\"./${dir}/\">$dir</A>"}
	
	DIRECTORY_DESCRIPTION=""
	if [ -f "$dir.txt" ] ; then
			DIRECTORY_DESCRIPTION="$dir.txt"
	else
		if [ -f "$dir"/"$dir.txt" ] ; then
			DIRECTORY_DESCRIPTION="$dir"/"$dir.txt"
		fi
	fi
	if [ -f "$DIRECTORY_DESCRIPTION" ] ; then
		DebugMessage "   found text comment for directory \"$dir\".  Adding to output..."
		while read 'DescriptionLine' ; do
			ImageSetData=${ImageSetData/"$IMAGE_SUBSET_DESCRIPTION"/"$DescriptionLine $IMAGE_SUBSET_DESCRIPTION"}
		done <"$DIRECTORY_DESCRIPTION"
		ImageSetData=${ImageSetData/"$IMAGE_SUBSET_DESCRIPTION"/""}
	else
		ImageSetData=${ImageSetData/"$IMAGE_SUBSET_DESCRIPTION"/" - no comment (yet): the file \"$dir.txt\" does not exist - "}
	fi
	DebugMessage "Exit AppendImageSetInformation"
}

AddImageSetsToIndex()
{
	IMAGE_SET_FOUND=0
	
	# add all of the image sets from sortorder.txt
	if [ -f "sortorder.txt" ] ; then
		while read "line" ; do
			if [ -d "$line" ] ; then
				dir="$line"
				if [[ -L "$dir" && ! -z $VERBOSE ]] ; then   # ==> If directory is a symbolic link...
					echo "symlink: $dir" `ls -l "$dir" | sed 's/^.*'"$dir"' //'`
				fi
	
				# check if this directory is in our excludes list
				FileContainsLine $VERBOSE_ARG "excludes.txt" "$dir"
				DoesExcludeFileContainLine="$?"
	
				# use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
				grep -e "^[ ^t]*$dir[ ^t]*$" "excludes.txt"
				DoesExcludeFileContainLineGrep="$?"
	
				if (((($DoesExcludeFileContainLine == 1) && ($DoesExcludeFileContainLineGrep != 0)) \
				|| (($DoesExcludeFileContainLine != 1) && ($DoesExcludeFileContainLineGrep == 0)))) ; then
					DebugMessage "WARNING: AddImageSetsToIndex: file search algorithms don't match! (grep & non-grep)!"
				fi
	
				if [[ $DoesExcludeFileContainLine -eq 1 ]] ; then
					DebugMessage "The directory \"$dir\" is in both the  excludes list and the sortorder list.  Excluding file."
				else
					AppendImageSetInformation "$dir"
				fi
				if [ $VERBOSE ] ; then
					DebugMessage "processed directory \"$dir\".  "
				fi
			fi
		done <"sortorder.txt"
	fi
	
	# add all of the rest of the images (those that were not in the sortorder.txt file)
	DebugMessage "searching for all subdirectories in \"$PWD\"..."
	
	# first go through all of the subdirectories
	# we will create a new IMAGE SUBSET for each subdirectory
	for dir in * .* ; do
		if [ -d "$dir" ] && [ "$dir" != "." ] && [ "$dir" != ".." ]  ; then
	
			if [[ -L "$dir" && ! -z $VERBOSE ]] ; then   # ==> If directory is a symbolic link...
				echo "symlink: $dir" `ls -l "$dir" | sed 's/^.*'"$dir"' //'`
			fi
	
			# check if this directory is in our sortorder list
			FileContainsLine $VERBOSE_ARG "sortorder.txt" "$dir"
			DoesSortOrderContainLine="$?"
	
			# use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
			grep -e "^[ ^t]*$dir[ ^t]*$" "sortorder.txt"
			DoesSortOrderContainLineGrep="$?"
	
			if (((($DoesSortOrderContainLine == 1) && ($DoesSortOrderContainLineGrep != 0)) \
				|| (($DoesSortOrderContainLine != 1) && ($DoesSortOrderContainLineGrep == 0)))) ; then
				DebugMessage "WARNING: AddImageSetsToIndex: file search algorithms don't match! (grep & non-grep)!"
			fi
	
			# check if this directory is in our excludes list
			FileContainsLine $VERBOSE_ARG "excludes.txt" "$dir"
			DoesExcludeFileContainLine="$?"
	
			# use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
			grep -e "^[ ^t]*$dir[ ^t]*$" "excludes.txt"
			DoesExcludeFileContainLineGrep="$?"
	
			if (((($DoesExcludeFileContainLine == 1) && ($DoesExcludeFileContainLineGrep != 0)) \
				|| (($DoesExcludeFileContainLine != 1) && ($DoesExcludeFileContainLineGrep == 0)))) ; then
				DebugMessage "WARNING: AddImageSetsToIndex: file search algorithms don't match! (grep & non-grep)!"
			fi
	
	
			if [[ $DoesExcludeFileContainLine -eq 1 || $DoesSortOrderContainLine -eq 1 ]] ; then
				if [ $DoesExcludeFileContainLine -eq 1 ] ; then
					if [ $VERBOSE ] ; then
						DebugMessage "The directory \"$dir\" is in the excludes list."
					fi
				fi
			else
				# reorganize this
	
				if	[ ! "`basename "$dir"`" = "$TEMPLATE_DIR" ] && \
					[ ! "`basename "$dir"`" = "$HTML_DIR" ] ; then
						DebugMessage "calling MakePicturePages on $dir"	
						MakePicturePages $VERBOSE_ARG "$dir"
						if (( $SLIDESHOW )) ; then
							MakePicturePages $VERBOSE_ARG "--template=slide.html" "$dir"
						fi
				fi

				# we don't want to descend into thumbnail directories
				thumbextract=`expr "$dir" : "\($THUMB_DIR_BASENAME\)"`
				if \
				[ -z $thumbextract ] && \
				[ ! "`basename "$dir"`" = "$TEMPLATE_DIR" ] && \
				[ ! "`basename "$dir"`" = "$HTML_DIR" ]
				then
					AppendImageSetInformation "$dir"
				else
					if [ ! -z $thumbextract ] ; then
						if [ $VERBOSE ] ; then
							#display thumb dir warning only if we are verbose
							DebugMessage "Thumb dir found: \"$dir\"."
						fi
						if [ -f "./sortorder.txt" ] ; then
							cp -f "./sortorder.txt" "./$dir/sortorder.txt"
						fi

						DebugMessage "calling VerifyTemplateFiles on $dir"	
						VerifyTemplateFiles $VERBOSE_ARG "$dir"
		
						# verify that all of our thumbnails are properly refreshed 
						DebugMessage "calling MakeThumbnails on $dir"	
						MakeThumbnails $VERBOSE_ARG "--size=${dir#$THUMB_DIR_BASENAME}"
		
						THUMBDIRS="$THUMBDIRS $dir"
					fi
				fi
			fi
		fi
	done
	DebugMessage "THUMBDIRS=$THUMBDIRS"
	DebugMessage "Exit AddImageSetsToIndex()"
}

AppendImageInformation()
{
	local ImageFile="$1"
	DebugMessage "Enter AppendImageInformation.  ImageFile=\"$ImageFile\", ImageData=\"$ImageData\" CURRENT_TABLE=${CURRENT_TABLE}"
	if [ -f "$ImageFile" ] ; then
		Ending="`expr ${ImageFile/*./.}`"
		if \
			[ $Ending = ".jpg" ] || \
			[ $Ending = ".GIF" ] || \
			[ $Ending = ".gif" ] || \
			[ $Ending = ".jpeg" ] || \
			[ $Ending = ".png" ] || \
			[ $Ending = ".PNG" ] || \
			[ $Ending = ".JPG" ] 
		then
			RESERVED_IMAGE=0
			for reserved_image in $RESERVED_IMAGES ;  do
				if [ "$reserved_image" = "$ImageFile" ] ; then
				RESERVED_IMAGE=1
				fi
	
				# check if this file is in our excludes list
				FileContainsLine $VERBOSE_ARG "excludes.txt" "$ImageFile"
	
				# get the return value of FileContainsLine
				DoesExcludeFileContainLine="$?"
	
				# use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
				grep -e "^[ ^t]*$ImageFile[ ^t]*$" "excludes.txt"
				DoesExcludeFileContainLineGrep="$?"
	
				if (((($DoesExcludeFileContainLine == 1) && ($DoesExcludeFileContainLineGrep != 0)) \
				  || (($DoesExcludeFileContainLine != 1) && ($DoesExcludeFileContainLineGrep == 0)))) ; then
					DebugMessage "WARNING: AppendImageSetInformation file search algorithms don't match! (grep & non-grep)!"
				fi
	
				if [ $DoesExcludeFileContainLine -eq 1 ] ; then
					RESERVED_IMAGE=1
				fi
			done
			if [ $RESERVED_IMAGE -eq 0 ] ; then
					IMAGE_FOUND=`expr $IMAGE_FOUND + 1`  # ==> Increment image count.
					#insure that we have an appropriate thumbnail subdirectory first
				if [ ! -d "./$THUMB_DIR" ] ; then
					DebugMessage "ERROR: thumbnail directory: \"./$THUMB_DIR\" does not exist!  pwd=\"`pwd`\".  Exiting..."
					ERROR_NO_THUMB_DIR=102
					exit $ERROR_NO_THUMB_DIR
				fi
				if [ ! -d "./$IMAGE_DIR" ] ; then
					DebugMessage "ERROR: default image directory: \"./$IMAGE_DIR\" does not exist!  pwd=\"`pwd`\".  Exiting..."
					ERROR_NO_IMAGE_DIR=103
					exit $ERROR_NO_IMAGE_DIR
				fi
				if [ "$CURRENT_TABLE" = "${CURRENT_TABLE#*$IMAGE_TAG*}" ] ; then
					if [ $VERBOSE ] ; then
						DebugMessage "   adding new row / rowset to image table..."
					fi
					CURRENT_TABLE="${CURRENT_TABLE}\n $ImageData"
				fi
	
				# PICTURE_LINK="$ImageFile"
				PICTURE_LINK="${PICTURE_TEMPLATE/%.html/}_$ImageFile"
				DebugMessage "looking for ./$IMAGE_DIR/$HTML_DIR/$PICTURE_LINK.html"
				if [ -f "./$IMAGE_DIR/$HTML_DIR/$PICTURE_LINK.html" ] ; then
					DebugMessage "found ./$IMAGE_DIR/$HTML_DIR/$PICTURE_LINK.html"
					PICTURE_LINK="./$IMAGE_DIR/$HTML_DIR/$PICTURE_LINK.html"
				else
					if [ -f "./$HTML_DIR/$ImageFile.html" ] ; then
						DebugMessage "found ./$HTML_DIR/$ImageFile.html"
						PICTURE_LINK="./$HTML_DIR/$ImageFile.html"
					fi
				fi
	
				# replace the image tag with an html image link
				CURRENT_TABLE=${CURRENT_TABLE/"$IMAGE_TAG"/"<A HREF=\"./$PICTURE_LINK\"><img border=0 src=\"./$THUMB_DIR/$ImageFile\" alt=\"./$THUMB_DIR/$ImageFile\"></A>"}
		
				# replace the description tag with a description
				CURRENT_TABLE=${CURRENT_TABLE/"$DESCRIPTION_TAG"/"<A HREF=\"./$PICTURE_LINK\">$ImageFile</A> $DESCRIPTION_TAG "}
		
				# replace the filename tag with a filename
				CURRENT_TABLE=${CURRENT_TABLE/"$FILENAME_TAG"/"$ImageFile"}
	
				# TODO: do a proper ReplaceObjectTags here
				if [ -f "$ImageFile.txt" ] ; then
					DebugMessage "   found text comment for picture \"$ImageFile\".  Adding to output..."
					while read 'DescriptionLine' ; do
						CURRENT_TABLE=${CURRENT_TABLE/"$DESCRIPTION_TAG"/"<BR><BR>$DescriptionLine $DESCRIPTION_TAG"}
					done <"$ImageFile.txt"
				fi
				CURRENT_TABLE=${CURRENT_TABLE/"$DESCRIPTION_TAG"/""}
			fi
		fi
	fi
#	DebugMessage "CURRENT_TABLE=\"$CURRENT_TABLE\""
	DebugMessage "Exit AppendImageInformation()"
}

AddImageFilesToIndex()
{
	DebugMessage "searching for all image files in \"$PWD\""
	IMAGE_FOUND=0
	CURRENT_TABLE=""
	
	# add all of the image sets from sortorder.txt
	# however, exludes.txt will override what is in sortorder.txt
	if [ -f "sortorder.txt" ] ; then
		while read "line" ; do
			if [ -f "$line" ] ; then
				f1="$line"
				if [[ -L "$f1" && ! -z $VERBOSE ]] ; then
					echo "symlink: $f1" `ls -l "$f1" | sed 's/^.*'"$f1"' //'`
				fi
		
				# check if this file is in our excludes list
				FileContainsLine $VERBOSE_ARG "excludes.txt" "$f1"
				DoesExcludeFileContainLine="$?"
		
				# use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
				grep -e "^[ ^t]*$f1[ ^t]*$" "excludes.txt"
				DoesExcludeFileContainLineGrep="$?"
		
				if (((($DoesExcludeFileContainLine == 1) && ($DoesExcludeFileContainLineGrep != 0)) \
				|| (($DoesExcludeFileContainLine != 1) && ($DoesExcludeFileContainLineGrep == 0)))) ; then
					DebugMessage "WARNING: AddImageFilesToIndex file search algorithms don't match! (grep & non-grep)!"
				fi
		
				if [[ $DoesExcludeFileContainLine -eq 1 ]] ; then
					DebugMessage "The file \"$f1\" is in both the  excludes list and the sortorder list.  Excluding file."
				else
					AppendImageInformation "$f1"
				fi
			fi
		done <"sortorder.txt"
	fi
	
	# then go through all of the files
	for f1 in * ; do
		if [ -f "$f1" ] ; then
	
			if [[ -L "$f1" && $VERBOSE ]] ; then
				echo "symlink: $f1" `ls -l "$f1" | sed 's/^.*'"$f1"' //'`
			fi

			# check if this file is in our sortorder list
			FileContainsLine $VERBOSE_ARG "sortorder.txt" "$f1"
			DoesSortOrderContainLine="$?"
	
			# use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
			grep -e "^[ ^t]*$f1[ ^t]*$" "sortorder.txt"
			DoesSortOrderContainLineGrep="$?"
	
			if (((($DoesSortOrderContainLine == 1) && ($DoesSortOrderContainLineGrep != 0)) \
				|| (($DoesSortOrderContainLine != 1) && ($DoesSortOrderContainLineGrep == 0)))) ; then
				DebugMessage "WARNING: AddImageFilesToIndex: file search algorithms don't match! (grep & non-grep)!"
			fi


			# check if this file is in our excludes list
			FileContainsLine $VERBOSE_ARG "excludes.txt" "$f1"
			DoesExcludeFileContainLine="$?"
	
			# use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
			grep -e "^[ ^t]*$f1[ ^t]*$" "excludes.txt"
			DoesExcludeFileContainLineGrep="$?"
	
			if (((($DoesExcludeFileContainLine == 1) && ($DoesExcludeFileContainLineGrep != 0)) \
			  || (($DoesExcludeFileContainLine != 1) && ($DoesExcludeFileContainLineGrep == 0)))) ; then
				DebugMessage "WARNING: AddImageSetsToIndex: file search algorithms don't match! (grep & non-grep)!"
			fi

			if [[ $DoesExcludeFileContainLine -eq 1 || $DoesSortOrderContainLine -eq 1 ]] ; then
				if [ $DoesExcludeFileContainLine -eq 1 ] ; then
					if [ $VERBOSE ] ; then
						DebugMessage "The file \"$f1\" is in the excludes list."
					fi
				fi
			else
				AppendImageInformation "$f1"
			fi
		fi
	done

	ImageData="${CURRENT_TABLE}"

}

BuildImageTable()
{
}

BuildImageSubsetTable()
{
}

BuildCurrentSetTable()
{
}

ReplaceTags()
{

	if [ "$?" -ne 2 ] ; then
		exit -200
	else	
		local replace=$1
		local with=$2
		local TEMP_FILENAME="$PICTURE_FILENAME.tmp"

		# perform search & replace of tag params
		#   cat "$PICTURE_FILENAME" | sed 's/'"$PHOTO_ALBUM_TAG*$DESCRIPTION_TAG"'//g' >> "$PICTURE_FILENAME.tmp"
		cat "$PICTURE_FILENAME" | sed 's/'"$replace"'/'"$with"'/g' >> "${TEMP_FILENAME}"
		mv -f "${TEMP_FILENAME}" "$PICTURE_FILENAME"
	fi
	DebugMessage "Exit ReplaceTags()" ; WaitForKey
}

ReplaceObjectTags()
{
	USAGE="ReplaceObjectTags [ -v ] Filename ObjectTag ReplaceString"

	if [ "$1" == "-v" ] ; then
		VERBOSE=1
		VERBOSE_ARG="-v"
		shift # discard the verbose argument and continue
	fi

	# consider whether I need to pass this as a file

	if [ "$?" -ne 3 ] ; then
		exit -200
	else
		local FILENAME="$1"
		local TAG="$2"
		local WITH="$3"
		local TEMP_FILENAME=".${FILENAME}.`basename "$0"`.tmp"
#		local REPLACE="<object[ \t].*id=\"$TAG\"[ \t].*type="$PHOTO_ALBUM_TAG".*>"
		local REPLACE="<object id=\"$TAG\" type=\"$PHOTO_ALBUM_TAG\">"
	# perform search & replace of tag params
	#   cat "$PICTURE_FILENAME" | sed 's/'"$PHOTO_ALBUM_TAG*$DESCRIPTION_TAG"'//g' >> "$PICTURE_FILENAME.tmp"
		cat "$FILENAME" | sed 's/'"$REPLACE"'/'"$WITH"'/g' >> "${TEMP_FILENAME}"
		DebugMessage "Replaceing all instances of \"$REPLACE\" with \"$WITH\" in \"$FILENAME\".  Press any key."
		WaitForKey
		mv -f "${TEMP_FILENAME}" "$FILENAME"
	fi
	DebugMessage "Exit ReplaceTags()" ; WaitForKey
}

ProcessTemplateFile()
{
	USAGE="ProcessTemplateFile [ -v ] TemplateFilename NewFilename"

	local TEMP_FILENAME=".${NEW_FILENAME}.`basename "$0"`.tmp"

	if [ "$1" == "-v" ] ; then
		VERBOSE=1
		VERBOSE_ARG="-v"
		shift # discard the verbose argument and continue
	fi

	# TODO: validate arguments and files

	TEMPLATE_FILENAME="$1"
	NEW_FILENAME="$2"

	if [ -f "${TEMPLATE_FILENAME}" ] ; then
		cp -f "${TEMPLATE_FILENAME}" "${NEW_FILENAME}"
	fi

	CURRENT_DIRECTORY="$PWD"
	CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*pictures}"
	CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*private_pics}"
	CURRENT_DIRECTORY="${CURRENT_DIRECTORY##*public_html}"
	CURRENT_DIRECTORY="${CURRENT_DIRECTORY/\//}"

	# format the display name of the directory that contains all of the images
	TOPIC_DIR="${TOPIC_DIR//\// - }"
	TOPIC_DIR="${TOPIC_DIR/%$THUMB_DIR_BASENAME*/}"
	TOPIC_DIR="${TOPIC_DIR% - }"
	TOPIC_DIR="${TOPIC_DIR# - }"

	ReplaceObjectTags "${CURRENT_DIRECTORY_TAG}" "${CURRENT_DIRECTORY}"

	# TODO: determine of "." is always the right path for the LINK_PARENT
	ReplaceObjectTags  $VERBOSE_ARG "${LINK_PARENT_TAG}" "."

#      DebugMessage "BASE_DIR=$BASE_DIR TOPIC_DIR=$TOPIC_DIR"
	ReplaceObjectTags $VERBOSE_ARG "${TEMP_FILENAME}" "$SERIES_TAG" "$TOPIC_DIR"
	
	GeneratedOn="<br><br>HTML File generated on `date` by \"$0\""
	ReplaceObjectTags $VERBOSE_ARG "${TEMP_FILENAME}" "${GENERATION_TAG}" "${GeneratedOn}"
	
}

ProcessTemplateFiles()
{
	ProcessTemplateFile "${TEMPLATE_DIR}/index.html" "./index.html"
}

ProcessDirectory() 
{
	DebugMessage "ProcessDirectory: IMAGE_TABLENAME=\"${IMAGE_TABLENAME}\", PICTURE_FILENAME=\"$PICTURE_FILENAME\""

	if [ $VERBOSE ] ; then
		DebugMessage "ProcessDirectory: dirname=\"`dirname "$0"`\""
	fi
	
	# Perform validations and announce errors in initial conditions
	VerifyTemplateFiles $VERBOSE_ARG "."

	# TODO: if I'm going to backup this file, I should be backing up all of them!
	if [ -f "$PICTURE_FILENAME" ] ; then
		CreateBackupOfFile "$PICTURE_FILENAME"
		if [ "$?" = "1" ] ; then
			rm "$PICTURE_FILENAME"
		else
			DebugMessage "Error: failed to create backup of file \"$PICTURE_FILENAME\".  Exiting abnormally." 1>&2
			return -6
		fi
	fi

	if [ ! -d "./$HTML_DIR" ] ; then
		mkdir "./$HTML_DIR"
	fi
	cp -af ./"${TEMPLATE_DIR}"/* ./"${HTML_DIR}"/
	if [ "$?" -ne 0 ] ; then
		DebugMessage "ERROR: ProcessDirectory: the following command failed \'cp -af ./\"${TEMPLATE_DIR}\"/* ./\"${TEMPLATE_DIR}\"/.* ./\"${HTML_DIR}\"/.  pwd=`pwd`"
	fi
#	mv "./$HTML_DIR/$PICTURE_FILENAME" ./

	# start by insuring that we have all of the necessary thumbnail files in the current directory
	# replaces this call in the file loop: $CONVERT -geometry ${THUMB_SIZE}x${THUMB_SIZE}+0+0 "$dir" "./$THUMB_DIR/$dir"
	if [ "$THUMB_SIZE" -gt 0 ] ; then
		COMMAND_OPTIONS="--size=$THUMB_SIZE"
		if [ $VERBOSE ] ; then
			DebugMessage "Calling MakeThumbnails with the following command options: \"$VERBOSE_ARG $COMMAND_OPTIONS\""
		fi
		MakeThumbnails $VERBOSE_ARG $COMMAND_OPTIONS
	fi

	# now verify that we have all the images of default size that we want to view
	if [ "$IMAGE_SIZE" -gt 0 ] ; then
		COMMAND_OPTIONS="--size=$IMAGE_SIZE"
		if [ $VERBOSE ] ; then
			DebugMessage "Calling MakeThumbnails with the following command options: \"$VERBOSE_ARG $COMMAND_OPTIONS\""
		fi
		MakeThumbnails $VERBOSE_ARG $COMMAND_OPTIONS
	fi

	AddCurrentSetInfoToIndex
	AddImageSetsToIndex
	AddImageFilesToIndex
	AddMovieFilesToIndex
	
	ProcessTemplateFiles

	CHANGE_MOD=0
	if [ $CHANGE_MOD -ne 0 ] ; then
		chmod 644 "$PICTURE_FILENAME"
	fi

	if [ $RECURSE ] ; then
		local subdir=""
		for subdir in * .* ; do
			if [ -d "$subdir" ] && [ "$subdir" != "." ] && [ "$subdir" != ".." ] ; then
	
				if [[ -L "$subdir" && ! -z $VERBOSE ]] ; then
				echo "symbolic link: $subdir" `ls -l "$subdir" | sed 's/^.*'"$subdir"' //'`
				fi
	
				# check if this directory is in our excludes list
				FileContainsLine $VERBOSE_ARG "excludes.txt" "$subdir"
				DoesExcludeFileContainLine="$?"

				# use a grep with a regular expression to detect our line in the file, allowing leading & trailing whitespace
				grep -e "^[ ^t]*$subdir[ ^t]*$" "excludes.txt"
				DoesExcludeFileContainLineGrep="$?"
	
				if (((($DoesExcludeFileContainLine == 1) && ($DoesExcludeFileContainLineGrep != 0)) \
				|| (($DoesExcludeFileContainLine != 1) && ($DoesExcludeFileContainLineGrep == 0)))) ; then
					DebugMessage "WARNING: ProcessDirectory file search algorithms don't match! (grep & non-grep)!"
				fi

				if [ $DoesExcludeFileContainLine -eq 1 ] ; then
					if [ $VERBOSE ] ; then
						DebugMessage "The directory \"$subdir\" is in the excludes list."
					fi
				else
					# we don't want to descend into thumbnail directories
					thumbextract=`expr "$subdir" : "\($THUMB_DIR_BASENAME\)"`
					if \
						[ -z $thumbextract ] && \
						[ ! "`basename "$subdir"`" = "$TEMPLATE_DIR" ] && \
						[ ! "`basename "$subdir"`" = "$HTML_DIR" ]
					then
		#                  VerifyTemplateFiles -v "." "./$subdir"
						DebugMessage "calling VerifyTemplateFiles on directory ./$subdir..."
						VerifyTemplateFiles $VERBOSE_ARG "./$subdir"
						if cd "$subdir" ; then              # ==> If can move to subdirectory...
							ProcessDirectory "$subdir"
							cd ..
						fi
					fi
				fi
			fi
		done
	fi
	DebugMessage "Exit ProcessDirectory()" ; WaitForKey
}

######################################################
# - Main -
######################################################

# Set defaults
THUMB_SIZE="$DEFAULT_THUMB_SIZE"
THUMB_DIR="$THUMB_DIR_BASENAME$THUMB_SIZE"
IMAGE_SIZE="$DEFAULT_IMAGE_SIZE"
IMAGE_DIR="$THUMB_DIR_BASENAME$IMAGE_SIZE"

DebugMessage "`basename "$0"` called"
until [ -z "$1" ] ; do
   if [ ! -z `expr "$1" : "\(-\)"` ] ; then
      if [ ! -z `expr "$1" : "\(--size=\)"` ] ; then
         THUMB_SIZE_ARG=${1#--size=}
         if [ "$THUMB_SIZE_ARG" -gt "0" ] ; then
            THUMB_SIZE=$THUMB_SIZE_ARG
         else
            DebugMessage "INVALID Thumbnail size arg: $1"
         fi
      fi   
      if [ ! -z `expr "$1" : "\(-h\)"` ] ; then
         DebugMessage "TODO: must create help"
         DebugMessage "USAGE=$USAGE"
         exit 0
      fi
      if [ ! -z `expr "$1" : "\(-b\)"` ] \
      || [ ! -z `expr "$1" : "\(--backup-original\)"` ] ; then
         BACKUP_ORIGINAL=1
      fi
      if [ ! -z `expr "$1" : "\(-o\)"` ] \
      || [ ! -z `expr "$1" : "\(--use-object-tags\)"` ] ; then
         USE_OBJECT_TAGS=1
      fi
      if [ ! -z `expr "$1" : "\(-r\)"` ] ; then
         RECURSE=1
      fi
      if [ ! -z `expr "$1" : "\(-ns\)"` ] ; then
         SLIDESHOW=0
      fi
      if [ ! -z `expr "$1" : "\(-nh\)"` ] ; then
         SHOWHEADING=0
      fi
      if [ ! -z `expr "$1" : "\(-v\)"` ] ; then
         VERBOSE=1
         VERBOSE_ARG="-v"
      fi
      if [ ! -z `expr "$1" : "\(--rn\)"` ] ; then
         REMOVE_NEW_TEMPLATES=1
      fi
      if [ ! -z `expr "$1" : "\(--remove-new-templates\)"` ] ; then
         REMOVE_NEW_TEMPLATES=1
      fi
   fi
   LAST_PARAM=$1
   shift
done
if [ -d "$LAST_PARAM" ] && [ ! -z "$LAST_PARAM" ]; then
#   BASE_DIR="${LAST_PARAM%\/}"       # ==> Otherwise, move to indicated directory.
   BASE_DIR="$LAST_PARAM"       # ==> Otherwise, move to indicated directory.
else
   BASE_DIR="`pwd`"    # ==> No args to script, then use current working directory.
fi

if [ $VERBOSE ] ; then
   DebugMessage "Script Options:"
   DebugMessage "   VERBOSE is on.  VERBOSE=$VERBOSE"
   DebugMessage "   RECURSE=$RECURSE"
   DebugMessage "   THUMB_SIZE=$THUMB_SIZE"
   DebugMessage "   THUMB_DIR=$THUMB_DIR"
   DebugMessage "   IMAGE_SIZE=$IMAGE_SIZE"
   DebugMessage "   IMAGE_DIR=$IMAGE_DIR"
   DebugMessage "   TEMPLATE_DIR=$TEMPLATE_DIR"
   DebugMessage "   PICINDEX_TEMPLATE=$PICINDEX_TEMPLATE"
   DebugMessage "   BASE_DIR=$BASE_DIR"
   DebugMessage "   SLIDESHOW=$SLIDESHOW"
   DebugMessage "   SHOWHEADING=$SHOWHEADING"
fi

DebugMessage "main: IMAGE_TABLENAME=${IMAGE_TABLENAME}"

cd "$BASE_DIR"
BASE_DIR=`pwd`
DebugMessage "   Initial directory=\"`pwd`\""

ProcessDirectory

DebugMessage "$0 complete, exiting normally..."
exit 0


