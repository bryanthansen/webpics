#!/bin/bash
# Bryant Hansen
# 20100620

# example call:
# /projects/webpics/offsetMtime.sh IMG_4670.JPG "- 5 hours"
# for f in *.JPG ; do /projects/webpics/offsetMtime.sh "$f" "- 5 hours" ; done

# initial 1-liner
# f=IMG_4670.JPG ; offset="- 5 hours" ; date -d "$(stat --format="%y" IMG_4670.JPG) $offset "

#f=IMG_4670.JPG
#offset="- 5 hours"
f="$1"
offset="$2"
oldDt="$(stat --format="%y" "$f")"
newDt="$(date -d "$oldDt $offset")"

echo "# $f"
echo "# old time: $oldDt"
echo "# new time: $newDt"
echo "touch --date \"${newDt}\" \"$f\""

