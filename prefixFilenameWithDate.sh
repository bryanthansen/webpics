#!/bin/bash
# Bryant Hansen
# 20110925

# 1-liner:
# ls -1 . | while read f ; do f2="$(stat --format="%y" "$f")" ; f3="${f2%.*}" ; f4="${f3//[:-]/}" ; f5="${f4/ /_}" ; echo "mv $f ${f5}_$f" ; done

# TODO: consider this from the man exiv2:
#       exiv2 -r':basename:_%Y%m' rename img_1234.jpg
#              Renames img_1234.jpg to img_1234_200511.jpg
# MOD:
#       exiv2 -r'%Y%m%d_%H%M%S_:basename:' rename img_1234.jpg
#              Renames img_1234.jpg to 20140511_144927_img_1234.jpg (hopefully)

TAG="Exif.Image.DateTime"
TAG="Exif.Photo.DateTimeOriginal"

get_timestamp() {
    exiv2 -pa "$1" | grep "^${TAG}[ \t]"
}

read_exif_timestamp_tag() {
    exiv2 -pa "$1" \
    | grep "^${TAG}[ \t]" \
    | while read name type size timestamp ; do
        echo "$timestamp"
    done
}

get_timestamp_field() {
    e1="$(read_exif_timestamp_tag "$1")"
    if [[ "$e1" ]] ; then
        # remove all colons (:) and dashes (-)
        e2="${e1//[:-]/}"
        # replace the space with an underscore
        timestamp="${e2/ /_}"
        #echo "# ${f}: e1=$e1, e2=$e2, timestamp=$timestamp" >&2
    else
        echo "# Failed to extract exif timestamp from $1" >&2
        # get the file date, example:  2011-09-23 18:51:36.000000000 +0200
        t1="$(stat --format="%y" "$1")"
        # strip off everything after the last period
        t2="${t1%.*}"
        # remove all colons (:) and dashes (-)
        t3="${t2//[:-]/}"
        # replace the space with an underscore
        timestamp="${t3/ /_}"
        #echo "# ${f}: t1=$t1, t2=$t2, t3=$t3, timestamp=$timestamp" >&2
    fi
    echo "$timestamp"
}

# sample values:
#f=IMG_4670.JPG
#offset="- 5 hours"

f="$1"

if [[ "$1" ]] ; then
    while [[ "$1" ]] ; do
        echo "# input: $1" >&2
        echo "$1"
        shift
    done
else
    echo "# no input.  using all files in current directory" >&2
    ls -1 .
fi | \
while read f ; do
    # first attempt to get the timestamp from the exif info
    # if the exif info cannot be read, then use the file last_modified time
    timestamp="$(get_timestamp_field "$f")"
    echo "mv '$f' '${timestamp}_$f'"
done

echo "# To execute, pipe the output to /bin/bash:   $0 | /bin/bash" >&2

# File Timestamp:
# gink@silvia -> stat DSCF5340.JPG
#   File: „DSCF5340.JPG“
#   Size: 1308228         Blocks: 2568       IO Block: 4096   reguläre Datei
# Device: 803h/2051d      Inode: 2566891     Links: 1
# Access: (0644/-rw-r--r--)  Uid: ( 1001/    gink)   Gid: ( 1001/    gink)
# Access: 2011-09-25 12:14:31.000000000 +0200
# Modify: 2011-09-25 12:14:31.000000000 +0200
# Change: 2011-09-25 12:14:31.000000000 +0200
#  Birth: -

