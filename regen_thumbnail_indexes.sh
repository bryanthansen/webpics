#!/bin/bash

# for d in .thumbnails* ; do pushd "$d" ;  ls -1 *.jpg *.png | while read f ; do echo "<img src=\"${f}\" />" ; done > index.html ; popd ; done

# ...and with link to the original:
# ls -1 *.JPG | while read f ; do echo "<a href=\"../${f}\"><img src=\"${f}\" /></a>" ; done > index.html

for d2 in 256 600 1024 ; do
	d=".thumbnails${d2}"
	(
		MakeThumbnails --size=${d2} >> mth${d2}.log 2>&1
		pushd "$d" > /dev/null
		ls -1 *.jpg *.png *.JPG 2>/dev/null | sort | \
		while read f ; do
			echo "<a href=\"../.thumbnails1024/html/picture_${f}.html\"><img src=\"${f}\" /></a><br>"
		done > index.html
                cat ../templates/index-header2.html index.html ../templates/index-footer2.html > index3.html
		popd > /dev/null
	) &
done

