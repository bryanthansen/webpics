#!/bin/bash
# Bryant Hansen
# 20100620

# original 1-liner:
# f="IMG_4559.JPG" ; identify -verbose "$f" | grep "exif:DateTimeDigitized" | while read d ; do d1="${d/*exif:DateTimeDigitized: /}" ; d2="${d1//[: ]/_}" ; d3="${d1/:/-}" ; d4="${d3/:/-}" ; echo "$d1" ; echo "$d2" ; echo "$d3" ; echo "$d4" ; touch --date "$d4" "$f" ; done

f="$1"

identify -verbose "$f" | grep "exif:DateTimeDigitized" | while read d ; do 
	d1="${d/*exif:DateTimeDigitized: /}"
	d2="${d1//[: ]/_}"
	d3="${d1/:/-}"
	d4="${d3/:/-}"
	echo "# $f"
	echo "# $d"
	echo "# $d4"
	echo "touch --date \"$d4\" \"$f\""
done
