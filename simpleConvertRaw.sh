#!/bin/bash
# Bryant Hansen

# 1-liner:
# ls -1 *.CR2 | while read f ; do [[ ! -f "$f".png ]] && [[ ! -f "$f".jpg ]] && [[ ! -f .tmp2/"$f".jpg ]] && echo "ufraw-batch --clip=film --saturation=1.70 --contrast=1.20 --exposure=0.50 --rotate=camera --create-id=also --out-type=jpg --output .tmp2/${f}.jpg $f" &&  [[ ! -f ".tmp2/$f" ]] && echo "cp -al $f .tmp2/$f" ; done

ls -1 *.CR2 | \
while read f ; do 
	[[ ! -f "$f".png ]] && \
	[[ ! -f "$f".jpg ]] && \
	[[ ! -f .tmp2/"$f".jpg ]] && \
		echo "ufraw-batch --clip=film --saturation=1.70 --contrast=1.20 --exposure=0.50 --rotate=camera --create-id=also --out-type=jpg --output .tmp2/${f}.jpg $f" && \
		[[ ! -f ".tmp2/$f" ]] && \
			echo "cp -al $f .tmp2/$f"
done
