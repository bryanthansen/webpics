/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Copyright (c) 2007-2008 by Bryant Hansen and various others
	all rights reserved
*/


var nextImageInst = new Image();
var nextImageUrl = "";
var nextImageObj;
var urlQueued = false;
var image;
var SlideshowMode = 1;
var ShowDebugMessages = 0;

var NextImageLoaded = 0;

var mySlideshowCheckbox;
var myDebugMessagesCheckbox;

var imageX = 0;
var imageY = 0;

var motionX = 0;
var motionY = 0;

var ImageObject;
var imageObj;

var StartTime;

var gImageId;

var args;

var is;

function setOpacity(obj, opacity) {

    opacity = (opacity == 100)?99.999:opacity;

    // IE/Win
    obj.style.filter = "alpha(opacity:"+opacity+")";

    // Safari<1.2, Konqueror
    obj.style.KHTMLOpacity = opacity/100;

    // Older Mozilla and Firefox
    obj.style.MozOpacity = opacity/100;

    // Safari 1.2, newer Firefox and Mozilla, CSS3
    obj.style.opacity = opacity/100;

    //    DebugMessage ("  setting opacity to " + opacity);
}

function fadeOut(obj,opacity) {
    if (!SlideshowMode) return;
      if (opacity > 0) {
        if (opacity >= 6) opacity -= 6;
	else opacity = 0;
        setOpacity(obj,opacity);
        window.setTimeout("fadeOut('"+obj+"',"+opacity+")", 50);
      }
      else {
	  //        queueNextUrl(nextImageUrl, 10000);
	  //	  DebugMessage ("forwarding to " + nextImageUrl + " in 15 seconds");
      }
}

function fadeOut2(objId,opacity) {
	if (!SlideshowMode) return;
	if (document.getElementById) {
		obj = document.getElementById(objId);
		if (opacity > 0) {
			if (opacity >= 6) opacity -= 6;
			else opacity = 0;
			setOpacity(obj,opacity);
			window.setTimeout("fadeOut2('"+objId+"',"+opacity+")", 50);
		}
		else {
			queueNextUrl(nextImageUrl, 100);
			DebugMessage ("forwarding to " + nextImageUrl + " in 100 milliseconds");
		}
	}
}

function fadeIn(obj,opacity) {
    //    if (!SlideshowMode) return;
    // DebugMessage ("Enter fadeIn: opacity = " + opacity);
    if (opacity <= 100) {
        opacity += 6;
	if (opacity > 100) opacity = 100;
        setOpacity(obj,opacity);
        window.setTimeout("fadeIn('"+obj+"',"+opacity+")", 50);
    }
    else {
	if (SlideShowMode) {
	    window.setTimeout("fadeOut('"+obj+"',"+opacity+")", 6000);
	}
    }
}

function fadeIn2(objId,opacity) {
    // DebugMessage ("Enter fadeIn2: opacity = " + opacity);
    if ( mySlideshowCheckbox.checked == true ) SlideShowmode = 1;
    else {
		SlideShowmode = 0;
    }

    if (document.getElementById) {
		obj = document.getElementById(objId);
		if (opacity <= 100) {
			setOpacity(obj,opacity);
			opacity += 6;
			window.setTimeout("fadeIn2('"+objId+"',"+opacity+")", 50);
		}
		else {
			if (SlideshowMode) {
			window.setTimeout("fadeOut2('"+objId+"',"+opacity+")", 2000);
			}
		}
    }
}

function updateImagePosition() {

    myImage = document.getElementById("imageDiv");
    // DebugMessage ("moveImage myImage=" + myImage.toString() + ", left=" + myImage.style.left);

    if (myImage) {
	// TODO: make time-dependent motion
	// document.getElementById("detailpopup").style.left = e.clientX + 10 + "px";
	if ((imageX < 5000) && (imageY < 5000)) {
	    imageX += motionX;
	    imageY += motionY;
	    myImage.style.top = imageY + "px";
	    myImage.style.left = imageX + "px";
	    window.setTimeout("updateImagePosition('"+myImage+"')", 50);
	    //	    DebugMessage ("  ImagePos=" + imageX);
	}
	else {
	    //	    DebugMessage ("done moving image.");
	}
    }
}

function moveImage() {

    motionRangeX = 6;
    motionRangeY = 6;

    positionRangeX = Math.round((window.innerWidth - imageObj.width) / 2) - 200;
    positionRangeY = Math.round((window.innerHeight - imageObj.height) / 2) - 200;

    motionX = Math.round( Math.random() * motionRangeX ) - (motionRangeX / 2);
    // motionX = Math.round( Math.random() * motionRangeX );
    DebugMessage ("  motionX=" + motionX + "\n");

    motionY = Math.round( Math.random() * motionRangeY ) - (motionRangeY / 2);
    // motionY = Math.round( Math.random() * motionRangeY );
    DebugMessage ("  motionY=" + motionY + "\n");

    initialX = Math.round( Math.random() * positionRangeX );
    DebugMessage ("  initialX=" + initialX + "\n");

    initialY = Math.round( Math.random() * positionRangeY );
    DebugMessage ("  initialY=" + initialY + "\n");

    myImageDiv.style.top = initialY + "px";
    myImageDiv.style.left = initialX + "px";
 
}

function StartSlideShow() {
    DebugMessage ("StartSlideShow nextImageObj.toString()=" + nextImageObj.toString());
    SlideshowMode = 1;
    //    window.setTimeout("fadeOut2('"+nextImageObj+"',"+100+")", 2000);
    window.setTimeout("fadeOut2('"+gImageId+"',"+100+")", 200);
    //    queueNextUrl(nextImageUrl, 15000);
}

function StopSlideShow() {
    DebugMessage ("StopSlideShow");
    SlideshowMode = 0;
    setOpacity(imageObj, 100);
    imageObj.style.visibility = 'visible';
}

function ToggleSlideShow() {
    DebugMessage ("ToggleSlideShow");
    //    if (SlideshowMode) StopSlideShow();
    if ( mySlideshowCheckbox.checked == true ) {
	DebugMessage ("  ToggleSlideShow: slideshow is checked!");
	StartSlideShow();
    }
    else {
	DebugMessage ("  ToggleSlideShow: slideshow is not checked!");
	StopSlideShow();
    }
}

function ToggleDebugMessages() {
    DebugMessage ("ToggleDebugMessages");
    if ( myDebugMessagesCheckbox.checked == true ) {
	DebugMessage ("  Debug Messages are enabled");
	if (document.myform) {
	    document.myform.thetext1.style.visibility = 'visible';
	}
	ShowDebugMessages = 1;
    }
    else {
	DebugMessage ("  Debug Message are disabled");
	if (document.myform) {
	    document.myform.thetext1.style.visibility = 'hidden';
	}
	ShowDebugMessages = 0;
    }
}

function ImageLoadedEvent(myImage) {
    DebugMessage("PRELOADED!  Queued Image Loaded!");
}

function preloadImage(url) {
    if (document.images) {
	DebugMessage("preloading next image: " + url + ".  Setting onload event handler...");
	preload_image_object = new Image();
	preload_image_object.onload = ImageLoadedEvent;
	preload_image_object.src = url;
    }     
}

function forward (url) {
    var full_url = url;
    var first = 1;
    if (SlideshowMode) {
	if (first) full_url = full_url + "?";
	else full_url = full_url + "&";
	full_url  = full_url + "SlideshowMode=On";
	first = 0;
    }
    if (ShowDebugMessages) {
	if (first) full_url = full_url + "?";
	else full_url = full_url + "&";
	full_url = full_url + "DebugMessages=On";
	first = 0;
    }
    location.href = full_url;
}

function queueNextUrl(url, timeout) {
    DebugMessage ("  queueNextUrl: jumping to " + url + " in " + timeout + " seconds...");
    setTimeout ("forward('"+url+"',"+timeout+")");
}

function DebugMessage(newText) {
	// var newtext = document.myform.inputtext.value;
	d = new Date();
	t = d.getTime();
	var elapsed = t - StartTime;
	if (document.myform) {
		document.myform.thetext1.value += elapsed + ": " + newText + "\n";
	}
}

function getScreenWidth() {

    var default_width = 768;
    var image1_width;

    if (document.body.clientWidth) {
		available_width=document.body.clientWidth;
		DebugMessage("  browser supports document.body.clientWidth = " + available_width);
    }
    else if (window.innerWidth) {
		available_width=window.innerWidth;
		DebugMessage("  browser supports window.innerWidth = " + available_width);
    }
    else {
		DebugMessage ("  browser does not support known methods to get the screen width.  using default (" + default_height + ")...");
		available_height = default_height;
    }
    return available_height;

    /*
    var default_width = 600;
    var image1_width;

    available_width = default_width;
    if(is.ns4 || is.ns6) {
	available_width = innerWidth;
    }
    else if(is.ie4 || is.ie5 || is.ie55 || is.ie6) {
	available_width=document.body.clientWidth;
    }

    if(is.ie4 || is.ie5 || is.ie55 || is.ie6 || is.ns6 || is.ns4) {
	// use 70% of the screen width
	image1_width = (available_width * 1.0);
    }
    else {
	DebugMessage ("  failed to retrieve image width; no browser match.  trying check of document.body.clientWidth...");
	if (document.body.clientWidth) {
	    available_height=document.body.clientWidth;
	    DebugMessage ("    got width on second try, document.body.clientWidth = " + document.body.clientWidth);
	}
	else {
	    DebugMessage ("  failed to retrieve image width; no browser match.  setting to default (" + default_width + ")...");
	}
    }
    DebugMessage ("getScreenWidth: screen width = " + image1_width);
    return image1_width;
    */
}

function getAvailableImageWidth() {
    imageCellObj = document.getElementById("ImageCell");
    DebugMessage ("getAvailableImageWidth: cell width = " + imageCellObj.offsetWidth);
    var image1_width = imageCellObj.offsetWidth * 0.9;
    return image1_width;
}

function getScreenHeight() {
    var default_height = 768;
    var image1_height;

    if (document.body.clientHeight) {
	available_height=document.body.clientHeight;
	DebugMessage("  browser supports document.body.clientHeight = " + available_height);
    }
    else if (window.innerHeight) {
	available_height=window.innerHeight;
	DebugMessage("  browser supports window.innerHeight = " + available_height);
    }
    else {
	DebugMessage ("  browser does not support document.body.clientHeight.  using default (" + default_height + ")...");
	available_height = default_height;
    }
    return available_hegiht;

    /*
    available_height = default_height;
    if(is.ns4 || is.ns6) {
	available_height = innerHeight;
    }
    else if(is.ie4 || is.ie5 || is.ie55 || is.ie6) {
	available_height=document.body.clientHeight;
    }
    else {
	DebugMessage ("  failed to retrieve image height; no browser match.  trying check of document.body.clientHeight...");
	if (document.body.clientHeight) {
	    available_height=document.body.clientHeight;
	}
	else {
	    DebugMessage ("  failed to retrieve image height; no browser match.  trying check of document.body.clientHeight...");
	}
    }
    if(is.ie4 || is.ie5 || is.ie55 || is.ie6 || is.ns6 || is.ns4) {
	image1_height = (available_height * 1.0);
    }
    DebugMessage ("getScreenHeight: screen height = " + image1_height);
    return image1_height;
    */
}

function getAvailableImageHeight () {
    imageCellObj = document.getElementById("ImageCell");
    DebugMessage ("getAvailableImageHeight: cell height = " + imageCellObj.offsetHeight);
    // use 80% of the screen height
    var image1_height = imageCellObj.offsetHeight * 0.8;
    return image1_height;

}

function updatePageLayout () {
    /*
    if (imageObj.width > getAvailableImageWidth()) {
	DebugMessage ("  shrinking image to available width - " + imageObj.width + "px wide to " + getAvailableImageWidth() + " px wide...");
	imageObj.width = getAvailableImageWidth();
    }
    if (imageObj.height > getAvailableImageHeight()) {
	DebugMessage ("  shrinking image to available height - " + imageObj.height + "px high to " + getAvailableImageHeight() + " px high...");
	imageObj.height = getAvailableImageHeight();
    }
    */

    var a_width = getAvailableImageWidth();
    var a_height = getAvailableImageHeight();
    if ((imageObj.width > a_width) || (imageObj.height > a_height)) {
	if ((imageObj.width * 100 / a_width) > (imageObj.height * 100 / a_height)) {
	    // width is the limiting factor - with proportional scaling - we have extra space for the height
	    DebugMessage ("  shrinking image to available width - " + imageObj.width + "px wide to " + a_width + " px wide...");
	    imageObj.width = a_width;
	}
	else {
	    // height is the limiting factor
	    DebugMessage ("  shrinking image to available height - " + imageObj.height + "px high to " + a_height + " px high...");
	    imageObj.height = a_height;
	}
    }
}

// from http://www.webreference.com/programming/javascript/images/
function Is() {
    agent = navigator.userAgent.toLowerCase();
    this.major = parseInt(navigator.appVersion);
    this.minor = parseFloat(navigator.appVersion);
    this.ns = ((agent.indexOf('mozilla') != -1) && ((agent.indexOf('spoofer')== -1) && (agent.indexOf('compatible') ==  -1)));
    this.ns2 = (this.ns && (this.major == 3));
    this.ns3 = (this.ns && (this.major == 3));
    this.ns4b = (this.ns && (this.major == 4) && (this.minor <= 4.03));
    this.ns4 = (this.ns && (this.major == 4));
    this.ns6 = (this.ns && (this.major >= 5));
    this.ie = (agent.indexOf("msie") != -1);
    this.ie3 = (this.ie && (this.major < 4));
    this.ie4 = (this.ie && (this.major == 4) && (agent.indexOf("msie 5.0") == -1));
    this.ie5 = (this.ie && (this.major == 4) && (agent.indexOf("msie 5.0") != -1));
    this.ie55 = (this.ie && (this.major == 4) && (agent.indexOf("msie 5.5") != -1));
    this.ie6 = (this.ie && (agent.indexOf("msie 6.0")!=-1) );
    this.aol = (agent.indexOf("aol") != -1);
    this.aol3 = (this.aol && this.ie3);
    this.aol4 = (this.aol && this.ie4);
    this.aol5 = (this.aol && this.ie5);
}

function winResize() {
    if(is.ns4 ||is.ns6||is.ie4||is.ie5||is.ie55||is.ie6) {
	history.go(0);
    }
}

/*******************************************************************************

'args.js', by Charlton Rose

Permission is granted to use and modify this script for any purpose,
provided that this credit header is retained, unmodified, in the script.

*******************************************************************************/


// This function is included to overcome a bug in Netscape's implementation
// of the escape () function:

function myunescape (str)
{
    str = '' + str;
    while (true)
	{
	    var i = str . indexOf ('+');
	    if (i < 0)
		break;
	    str = str . substring (0, i) + ' ' + str . substring (i + 1, str . length);
	}
    return unescape (str);
}



// This function creates the args [] array and populates it with data
// found in the URL's search string:
function args_init ()
{
	args = new Array ();
	var argstring = window . location . search;
	if (argstring . charAt (0) != '?') return;

	argstring = argstring . substring (1, argstring . length);
	var argarray = argstring . split ('&');
	var i;
	var singlearg;
	for (i = 0; i < argarray . length; ++ i)
	{
	    singlearg = argarray [i] . split ('=');
	    if (singlearg . length != 2)
		continue;
	    var key = myunescape (singlearg [0]);
	    var value = myunescape (singlearg [1]);
	    args [key] = value;
	}
}


function loadPage(imageId, nextImageId, previousImageId) {

    // loadObjects
    // readPreferences
    // updatePageLayout

	gImageId = imageId;

    d = new Date();
    StartTime = d.getTime();

    is = new Is();

    if (document.myform) {
		document.myform.thetext1.value = "";
    }
    DebugMessage ("loadPage imageId=" + imageId + ", nextImageId=" + nextImageId + ", previousImageId=" + previousImageId);

	if (document.getElementById) {
		imageObj = document.getElementById(imageId);
    }
    if (!imageObj) {
		document.myform.thetext1.style.visibility = 'visible';
		DebugMessage ("loadPage: ERROR: can't load an image object for the page!  Exiting abnormally!");
		return;
    }

    // Call the args_init () function to set up the args [] array:
    args_init ();

    // DebugMessage ("moveImage myImage=" + myImage.toString() + ", left=" + myImage.style.left + "\n");

    setOpacity(imageObj, 0);
    //    document.image1.style.visibility = 'visible';
    updatePageLayout();
    imageObj.style.visibility = 'visible';


    if (args ['DebugMessages'] == "On") {
		// DebugMessage ("SlideshowMode is on; activate the checkbox");
		myDebugMessagesCheckbox.checked = true;
    }
    if ( myDebugMessagesCheckbox.checked == true ) {
		// DebugMessage ("  loadPage: slideshow is checked!");
		ShowDebugMessages = 1;
		if (document.myform) {
			document.myform.thetext1.style.visibility = 'visible';
		}
    }
    else {
		// DebugMessage ("  loadPage: slideshow is not checked!");
		ShowDebugMessages = 0;
		if (document.myform) {
			document.myform.thetext1.style.visibility = 'hidden';
		}
    }

    nextImageUrl = nextImageObj.href;
    preloadImage (nextImageUrl);
    previousImageUrl = previousImageObj.href;

    DebugMessage ("  nextImageUrl=" + nextImageUrl + "\n  previousImageUrl=" + previousImageUrl);
    DebugMessage ("  window.clientX=" + window.clientX + ", window.clientY=" + window.clientY);
    DebugMessage ("  window.innerWidth=" + window.innerWidth + ", window.innerHeight=" + window.innerHeight);
    DebugMessage ("  document.body.offsetWidth=" + document.body.offsetWidth + ", document.body.offsetHeight=" + document.body.offsetHeight);
    DebugMessage ("  screen.width=" + screen.width + ", screen.height=" + screen.height);
    DebugMessage ("  imageObj.width=" + imageObj.width + ", imageObj.height=" + imageObj.height);

    DebugMessage ("SlideshowMode = " + args ['SlideshowMode']);
    if (args ['SlideshowMode'] == "On") {
		// DebugMessage ("SlideshowMode is on; activate the checkbox");
		mySlideshowCheckbox.checked = true;
    }
    if ( mySlideshowCheckbox.checked == true ) {
		// DebugMessage ("  loadPage: slideshow is checked!");
		SlideshowMode = 1;
    }
    else {
		// DebugMessage ("  loadPage: slideshow is not checked!");
		SlideshowMode = 0;
    }

    fadeIn2(imageId,0);
}
