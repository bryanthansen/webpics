/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Copyright (c) 2007-2008 by Bryant Hansen <sourcecode@bryanthansen.net> and various others
	all rights reserved
*/

/* CONSTANTS */

// object IDs
var imageId = "image1";
var nextImageId = "";
var previousImageId = "";
var imageDivId = "ImageDiv";
var imageTableId = "imageTable";
var slideshowCheckboxId = "";
var debugMessagesCheckboxId = "";


// cached objects
var imageObj;
var nextImageObj;
var previousImageObj;
var debugTextboxObj;
var mySlideshowCheckbox;
var myDebugMessagesCheckbox;
var myScaleImageCheckbox;
var myClipToFillCheckbox;
var myFadeEffectCheckbox;
var myZoomEffectRadioButton;
var myPanEffectRadioButton;
var myRandomEffectRadioButton;
var myNoEffectRadioButton;

// all timing in milliseconds except FPS
var slideViewDuration = 7000;

var slidePreloadStartTime = 0;
var slidePreloadTimeout = 10000;

// Fade Parameters
var fadeDuration = 1500;
var fadeFPS = 20;
var fadeNumFrames = (fadeDuration / 1000) * fadeFPS;
var fadeOpacityStep = 100 / fadeNumFrames;  // (percent per step)
var fadeInInterval = fadeDuration / fadeFPS;
var fadeOutInterval = fadeDuration / fadeFPS;
var g_imageOpacity;

// General Effect parameters
var effectFPS = 20;
var effectDuration = slideViewDuration + fadeDuration + fadeDuration;

/* GLOBALS */
var args;
var is;
var StartTime;
var ImagePreloadTasks = 0;

// Global settings
var SlideshowMode = 1;
var ShowDebugMessages = 1;
var ClipToFill = 1;
var ScaleImage = 1;
var ZoomEffect = 0;
var PanEffect = 0;
var RandomEffect = 1;
var FadeEffect = 1;

// misc global parameters
var nextImageUrl;
var previousImageUrl;

var imageX;
var imageY;
var initialX;
var initialY;
var movementX;
var movementY;
var motionInterval = 1000 / effectFPS;
var originalWidth;
var originalHeight;

/* Philosophy question:
	How should I pass information about document elements to this script?
		* direct references to element IDs and/or names of document elements?
			A: use IDs - see WC3 page
		* element IDs or names passed by function call?
		* should Image objects be treated differently that textboxes or links?
	One answer: always use object ID's instead of names, since it will be consistent with how style sheets deal with images
	  current exception to this rule is the checkboxes
	Should objects be cached?  Should 
*/


/* FUNCTIONS */

function DebugMessage(newText) {
	d = new Date();
	t = d.getTime();
	var elapsed = t - StartTime;
	if (debugTextboxObj) {
		debugTextboxObj.value += elapsed + ": " + newText + "\n";
//		if ((debugTextboxObj.scrollTop) && (debugTextboxObj.scrollHeight)) {
			debugTextboxObj.scrollTop =  debugTextboxObj.scrollHeight;
//		}
	}
}

function ForwardToUrl (url) {
	
	var argstring = url;
    var full_url = url; // TODO: throw away the "?" and everything after it!
	if (url.indexOf("?")) {
		full_url = url.substring (0, url.indexOf("?"));
		if (full_url.length < 1) full_url = url;
	}
	else {
		full_url = url;
	}	
	
	DebugMessage ("ForwardToUrl: raw url = " + url + ", base url = " + full_url + "");

    var first = 1;
    if (SlideshowMode) {
		if (first) full_url = full_url + "?";
		else full_url = full_url + "&";
		full_url  = full_url + "SlideshowMode=On";
		first = 0;
    }
    if (ShowDebugMessages) {
		if (first) full_url = full_url + "?";
		else full_url = full_url + "&";
		full_url = full_url + "DebugMessages=On";
		first = 0;
    }
    if (FadeEffect) {
		if (first) full_url = full_url + "?";
		else full_url = full_url + "&";
		full_url = full_url + "FadeEffect=On";
		first = 0;
    }
    if (RandomEffect) {
		if (first) full_url = full_url + "?";
		else full_url = full_url + "&";
		full_url = full_url + "RandomEffect=On";
		first = 0;
    }
    if (ZoomEffect) {
		if (first) full_url = full_url + "?";
		else full_url = full_url + "&";
		full_url = full_url + "ZoomEffect=On";
		first = 0;
    }
    if (PanEffect) {
		if (first) full_url = full_url + "?";
		else full_url = full_url + "&";
		full_url = full_url + "PanEffect=On";
		first = 0;
    }
    if (ScaleImage) {
		if (first) full_url = full_url + "?";
		else full_url = full_url + "&";
		full_url = full_url + "ScaleImage=On";
		first = 0;
    }
    if (ClipToFill) {
		if (first) full_url = full_url + "?";
		else full_url = full_url + "&";
		full_url = full_url + "ClipToFill=On";
		first = 0;
    }
// comment out to debug slideshow / forwarding problems
//    DebugMessage ("  ForwardToUrl: temporarily disabled.");
//    return;
    location.href = full_url;
}

function QueueNextUrl(url, timeout) {
    DebugMessage ("  QueueNextUrl: jumping to " + url + " in " + timeout + " ms...");
	window.setTimeout("ForwardToUrl('"+url+"')", timeout);
}

function SetOpacity(obj, opacity) {

    opacity = (opacity == 100)?99.999:opacity;

    // IE/Win
    obj.style.filter = "alpha(opacity:"+opacity+")";

    // Safari<1.2, Konqueror
    obj.style.KHTMLOpacity = opacity/100;

    // Older Mozilla and Firefox
    obj.style.MozOpacity = opacity/100;

    // Safari 1.2, newer Firefox and Mozilla, CSS3
    obj.style.opacity = opacity/100;

    //    DebugMessage ("  setting opacity to " + opacity);
}

function GetScreenWidth() {

    var defaultWidth = 768;
    var image1_width;
	var availableWidth = defaultWidth;
	
    if (window.innerWidth) {
		availableWidth=window.innerWidth;
		// DebugMessage("  browser supports window.innerWidth = " + availableWidth);
    }
    else if (document.body.clientWidth) {
		availableWidth=document.body.clientWidth;
		// DebugMessage("  browser supports document.body.clientWidth = " + availableWidth);
    }
    else {
		DebugMessage ("  browser does not support known methods to get the screen width.  using default (" + defaultWidth + ")...");
    }
    return availableWidth;
}

function GetClientWidth() {
    var defaultWidth = 768;
    var image1Width;

    if (window.innerWidth) {
		availableWidth=window.innerWidth;
		DebugMessage("  browser supports window.innerHeight = " + availableWidth);
    }
    else if (document.body.clientWidth) {
		availableWidth=document.body.clientWidth;
		DebugMessage("  browser supports document.body.clientWidth = " + availableWidth);
    }
    else {
		DebugMessage ("  browser does not support document.body.clientWidth.  using default (" + defaultWidth + ")...");
		availableWidth = defaultWidth;
    }
    return availableWidth;
}

function GetAvailableImageWidth() {
    imageCellObj = document.getElementById("ImageCell");
//    DebugMessage ("GetAvailableImageWidth: cell width = " + imageCellObj.offsetWidth);
    return imageCellObj.offsetWidth;
}

function GetScreenHeight() {
    var default_height = 768;
    var image1_height;

    if (document.body.clientHeight) {
		available_height=document.body.clientHeight;
		DebugMessage("  browser supports document.body.clientHeight = " + available_height);
    }
    else if (window.innerHeight) {
		available_height=window.innerHeight;
		DebugMessage("  browser supports window.innerHeight = " + available_height);
    }
    else {
		DebugMessage ("  browser does not support document.body.clientHeight.  using default (" + default_height + ")...");
		available_height = default_height;
    }
    return available_height;

}

function GetClientHeight() {
    var default_height = 768;
    var image1_height;

    if (window.innerHeight) {
		available_height=window.innerHeight;
		DebugMessage("  browser supports window.innerHeight = " + available_height);
    }
    else if (document.body.clientHeight) {
		available_height=document.body.clientHeight;
		DebugMessage("  browser supports document.body.clientHeight = " + available_height);
    }
    else {
		DebugMessage ("  browser does not support document.body.clientHeight.  using default (" + default_height + ")...");
		available_height = default_height;
    }
    return available_height;
}

function GetAvailableImageHeight () {
    imageCellObj = document.getElementById("ImageCell");
//    DebugMessage ("GetAvailableImageHeight: cell height = " + imageCellObj.offsetHeight);
    return imageCellObj.offsetHeight;

}

var fadeInStartTime;
var fadeInUpdateTime;
var numFadeInUpdateFrames = 0;
var maxFadeInFPS = 0;
var minFadeInFPS = 0;
var averageFadeInFPS = 0;

function FadeIn(objId,opacity) {
    // DebugMessage ("Enter FadeIn: opacity = " + opacity);
	var d = new Date();
	var t = d.getTime();
	if (!fadeInStartTime) {
	    DebugMessage ("Starting fadeIn effect: opacity = " + opacity);
		fadeInStartTime = t;
		fadeInUpdateTime = t;
	}
    if ( mySlideshowCheckbox.checked == true ) SlideShowmode = 1;
    else {
		SlideShowmode = 0;
    }
    var obj;
    if (document.getElementById) {
		obj = document.getElementById(objId);
	}
	if (obj) {
		if (FadeEffect) {
			var deltaFromPrevious = t - fadeInUpdateTime;
			var elapsed = fadeInUpdateTime - fadeInStartTime;
			var currentFPS = 0;
			if (imagePositionUpdateTime) {
				var deltaFromPrevious = t - imagePositionUpdateTime;
				if (deltaFromPrevious <= 0) currentFPS = 0;
				else                        currentFPS = (1 / deltaFromPrevious) * 1000; 
				if (!minFadeInFPS) minFadeInFPS = currentFPS;
				if (minFadeInFPS > currentFPS) minFadeInFPS = currentFPS; 
				if (!maxFadeInFPS) maxFadeInFPS = currentFPS;
				if ((maxFadeInFPS < currentFPS) && (currentFPS < 500)) maxFadeInFPS = currentFPS; 
			}
			numFadeInUpdateFrames++;
			if (elapsed <= fadeDuration) {
				g_imageOpacity = opacity;
				opacity = Math.round(100 * (elapsed / (fadeDuration * 0.95)));
				if (opacity > 100) opacity = 100;
				// TODO: move this to a general GUI update function - don't want to rely on the effect functions to do this
				if (!ZoomEffect && !PanEffect) SetOpacity(obj, opacity);
				window.setTimeout("FadeIn('"+objId+"',"+opacity+")", fadeInInterval);
			}
			else {
				var averageFPS = (numFadeInUpdateFrames / elapsed) * 1000;
				DebugMessage ("FadeIn: INFO: time expired (fadeDuration = " + fadeDuration + ").  Previous Opacity = " + opacity + ", Average FPS = " + averageFPS.toFixed(2) + "fps");
				DebugMessage ("FadeIn: VERBOSE: Average FPS = " + averageFPS.toFixed(2) + "fps, min=" + minFadeInFPS.toFixed(2) + "fps, max=" + maxFadeInFPS.toFixed(2) + "fps");
				// finished
				opacity = 100;
				g_imageOpacity = opacity;
				if (!ZoomEffect && !PanEffect) SetOpacity(obj, opacity);
				numFadeInUpdateFrames++;
				if (SlideshowMode) {
					window.setTimeout("FadeOut('"+objId+"',"+opacity+")", (slideViewDuration - fadeDuration));
				}
			}
			fadeInUpdateTime = t;
		}
		else {
			DebugMessage ("Fade Effect is not enabled!  Stopping...");
			opacity = 100;
			g_imageOpacity = opacity;
			SetOpacity(obj, opacity);
			return;
		}
    }
}

var fadeOutStartTime;
var fadeOutUpdateTime;
var numFadeOutUpdateFrames = 0;
var maxFadeOutFPS = 0;
var minFadeOutFPS = 0;
var averageFadeOutFPS = 0;

function FadeOut(objId,opacity) {
    // DebugMessage ("Enter FadeOut: opacity = " + opacity);
	var d = new Date();
	var t = d.getTime();
	if (!fadeOutStartTime) {
	    DebugMessage ("Enter fadeOut: opacity = " + g_imageOpacity);
		fadeOutStartTime = t;
		fadeOutUpdateTime = t;
	}
    if ( mySlideshowCheckbox.checked == true ) SlideShowmode = 1;
    else {
		SlideShowmode = 0;
    }
    var obj;
    if (document.getElementById) {
		obj = document.getElementById(objId);
	}
	if (obj) {
		var deltaFromPrevious = t - fadeOutUpdateTime;
		var elapsed = fadeOutUpdateTime - fadeOutStartTime;
		var currentFPS = 0;
		if (imagePositionUpdateTime) {
			var deltaFromPrevious = t - imagePositionUpdateTime;
			if (deltaFromPrevious <= 0) currentFPS = 0;
			else                        currentFPS = (1 / deltaFromPrevious) * 1000; 
			if (!maxFadeOutFPS) maxFadeOutFPS = currentFPS;
			if (maxFadeOutFPS > currentFPS) maxFadeOutFPS = currentFPS; 
			if (!maxFadeOutFPS) maxFadeOutFPS = currentFPS;
			if ((maxFadeOutFPS < currentFPS) && (currentFPS < 500)) maxFadeOutFPS = currentFPS; 
		}
		numFadeOutUpdateFrames++;
		if (elapsed <= fadeDuration) {
			opacity = Math.round(100 - (100 * (elapsed / fadeDuration)));
			g_imageOpacity = opacity;
			window.setTimeout("FadeOut('"+objId+"',"+opacity+")", fadeOutInterval);
		}
		else {
			var averageFPS = (numFadeOutUpdateFrames / elapsed) * 1000;
			if ((ImagePreloadTasks > 0) && (slidePreloadStartTime)) {
				elapsed = fadeOutUpdateTime - slidePreloadStartTime;
				DebugMessage ("fadeOut: WARNING: can't load next url because preload hasn't completed.  ImagePreloadTasks = " + ImagePreloadTasks + ", elapsed = " + elapsed + ", timeout = " + slidePreloadTimeout + "");
				// we're still preloading; can't forward yet.  try again later
				if (elapsed < slidePreloadTimeout) {
				    window.setTimeout("fadeOut('"+objId+"',"+opacity+")", fadeOutInterval);
				}
				else {
				    DebugMessage ("fadeOut: WARNING: slidePreload has not completed, but the timeout (" + slidePreloadTimeout + ") has expired!");
				}
			}
			else {
				DebugMessage ("FadeOut: INFO: time expired.  Average FPS = " + averageFPS.toFixed(2) + "fps");
				DebugMessage ("FadeOut: VERBOSE: Average FPS = " + averageFPS.toFixed(2) + "fps, min=" + minFadeOutFPS.toFixed(2) + "fps, max=" + maxFadeOutFPS.toFixed(2) + "fps");
				// finished
				numFadeOutUpdateFrames++;
				if (SlideshowMode) {
					opacity = 0;
					g_imageOpacity = opacity;
					// finished
					// QueueNextUrl(nextImageUrl, 30);
					DebugMessage ("forwarding to " + nextImageUrl + "");
					ForwardToUrl(nextImageUrl);
				}
				else {
					opacity = 100;
				}
				g_imageOpacity = opacity;
				DebugMessage ("Exit FadeOut: opacity = " + g_imageOpacity);
			}
		}
		fadeOutUpdateTime = t;
    }
}

// TODO: make time-based fade
function FadeOut2(objId,opacity) {
	if (!SlideshowMode) return;
	if (document.getElementById) {
		obj = document.getElementById(objId);
		if (opacity > 0) {
			if (opacity >= fadeOpacityStep) opacity -= fadeOpacityStep;
			else opacity = 0;
			SetOpacity(obj,opacity);
			window.setTimeout("FadeOut('"+objId+"',"+opacity+")", fadeOutInterval);
		}
		else {
			// finished
			QueueNextUrl(nextImageUrl, 100);
			DebugMessage ("forwarding to " + nextImageUrl + " in 100 milliseconds");
		}
	}
}

function SetImageSizeForPan (a_width, a_height, zoomPercent) {
	// the minimum dimension should be scaled to the zoom - we don't want to allow the background to show.
	if ((imageObj.width * 100 / a_width) > (imageObj.height * 100 / a_height)) {
		// height is the limiting factor - with proportional scaling - we have extra space for the height
		// TODO: take into account available space
		var newHeight = Math.round(a_height * zoomPercent / 100);
		DebugMessage ("SetImageSizeForPan: setting the image size to " + zoomPercent + "% of the available height: " + imageObj.height + "px high to " + newHeight + " px high...");
		imageObj.height = newHeight;
	}
	else {
		// width is the limiting factor
		var newWidth = Math.round(a_width * zoomPercent / 100);
		DebugMessage ("SetImageSizeForPan: setting the image size to " + zoomPercent + "% of the available width: " + imageObj.width + "px wide to " + newWidth + " px wide...");
		imageObj.width = newWidth;
	}
}

// TODO: make time-based, instead of increment-based
var imagePanStartTime = 0;
var imagePositionUpdateTime = 0;
var numImagePanUpdateFrames = 0;
var maxPanFPS = 0;
var minPanFPS = 0;
var averagePanFPS = 0;
var minPosX;
var maxPosX;
var minPosY;
var maxPosY;
function UpdateImagePosition(objId) {
	var obj;
	if (document.getElementById) {
		obj = document.getElementById(objId);
	}	
	if (!obj) {
		DebugMessage ("UpdateImagePosition: cannot retrieve object for object ID " + objId + "!  Exiting abnormally!");
		return;
	}
    if (obj) {
		// TODO: make time-dependent motion
		// document.getElementById("detailpopup").style.left = e.clientX + 10 + "px";
		var w = 0;
		if (obj.style.width) w = obj.style.width;
		else if (obj.width) w = obj.width;
		var h = 0;
		if (obj.style.height) h = obj.style.height;
		else if (obj.height) h = obj.height;
		
		var d = new Date();
		var t = d.getTime();
		var elapsed = imagePositionUpdateTime - imagePanStartTime;
		var averageFPS = (numImagePanUpdateFrames / elapsed) * 1000;
		
		// DebugMessage ("enter UpdateImagePosition imagePanStartTime = " + imagePanStartTime + ", imagePositionUpdateTime = " + imagePositionUpdateTime);
		if (imagePanStartTime && imagePositionUpdateTime) {
			imageX = initialX + (movementX * elapsed / effectDuration);
			imageY = initialY + (movementY * elapsed / effectDuration);
//			DebugMessage ("UpdateImagePosition: INFO: imageX=" + imageX + ", imageY=" + imageY + ", initialX=" + initialX + ", initialY=" + initialY + ", movementX=" + movementX + ", movementY=" + movementY, ", elapsed=" + elapsed + ", effectDuration=" + effectDuration);
		}
		
		if ((imageX <= maxPosX) && (imageX >= minPosX) && (imageY <= maxPosY) && (imageY >= minPosY)) {
			// do all operations in a block; hopefully it doesn't trigger refresh operations after each one
			// TODO: update the update() routine - move this into a general update function
			obj.style.top = imageY + "px";
			obj.style.left = imageX + "px";
			SetOpacity(obj,g_imageOpacity);
			numImagePanUpdateFrames++;
			
			var deltaFromPrevious = t - imagePositionUpdateTime;
			var currentFPS = 0;
			if (imagePositionUpdateTime) {
				var deltaFromPrevious = t - imagePositionUpdateTime;
				if (deltaFromPrevious <= 0) currentFPS = 0;
				else                        currentFPS = (1 / deltaFromPrevious) * 1000; 
				if (!minPanFPS) minPanFPS = currentFPS;
				if (minPanFPS > currentFPS) minPanFPS = currentFPS; 
				if (!maxPanFPS) maxPanFPS = currentFPS;
				if ((maxPanFPS < currentFPS) && (currentFPS < 500)) maxPanFPS = currentFPS; 
			}
			imagePositionUpdateTime = t;
//			DebugMessage ("UpdateImagePosition imagePositionUpdateTime = " + imagePositionUpdateTime);
			elapsed = imagePositionUpdateTime - imagePanStartTime;
			if (elapsed > effectDuration) {
				var averageFPS = (numImagePanUpdateFrames / elapsed) * 1000;
				DebugMessage ("UpdateImagePosition: INFO: time expired.  Average FPS = " + averageFPS.toFixed(2) + "fps");
				DebugMessage ("UpdateImagePosition: VERBOSE: Average FPS = " + averageFPS.toFixed(2) + "fps, min=" + minPanFPS.toFixed(2) + "fps, max=" + maxPanFPS.toFixed(2) + "fps");
			}
			else {
				window.setTimeout("UpdateImagePosition('"+objId+"')", 1000 / effectFPS);
				// DebugMessage ("  ImagePos=(" + imageX + ", " + imageY + ")");
			}
		}
		else {
			DebugMessage ("UpdateImagePosition: INFO: done moving image - beyond boundaries.  imageX=" + imageX + ", obj.style.width=" + obj.style.width + ", obj.width=" + obj.width + ", imageY=" + imageY + ", obj.style.height=" + obj.style.height + ", obj.height=" + obj.height + ".");
			DebugMessage ("UpdateImagePosition: VERBOSE: Average FPS = " + averageFPS.toFixed(2) + "fps, min=" + minPanFPS.toFixed(2) + "fps, max=" + maxPanFPS.toFixed(2) + "fps");
			if (imageX > maxPosX) imageX = maxPosX;
			if (imageX < minPosX) imageX = minPosX;
			if (imageY > maxPosY) imageY = maxPosY;
			if (imageY < minPosY) imageY = minPosY;
			obj.style.top = imageY + "px";
			obj.style.left = imageX + "px";
			SetOpacity(obj,g_imageOpacity);
			numImagePanUpdateFrames++;
		}
    }
}

function PanObject(objId) {
	
	var obj;	
	if (document.getElementById) {
		obj = document.getElementById(objId);
	}	
	if (!obj) {
		DebugMessage ("PanObject: ERROR: cannot retrieve object for object ID " + objId + "!  Exiting abnormally!");
		return;
	}
	if (ScaleImage) {
		// vary the zoom from 120 to 150 percent of the image
		var ZoomPercent = 120 + Math.round(30 * Math.random());
		SetImageSizeForPan(GetAvailableImageWidth(), GetAvailableImageHeight(), ZoomPercent); 
	}
	
	d = new Date();
	t = d.getTime();
	imagePanStartTime = t;
	
	motionRangeX = 6;
	motionRangeY = 6;
	
	DebugMessage ("PanObject: INFO: obj size = (" + obj.width + ", " + obj.height + ")");
	
	// TODO: scale the image to something reasonable
	var newWidth = GetAvailableImageWidth() * (1.2 + (Math.random() * 0.5));
	
	DebugMessage ("PanObject: INFO: view window size = (" + GetAvailableImageWidth() + ", " + GetAvailableImageHeight() + ")");
	
	var positionRangeX = Math.round(obj.width - GetAvailableImageWidth());
	var positionRangeY = Math.round(obj.height - GetAvailableImageHeight());
	DebugMessage ("PanObject: INFO: position range = (" + positionRangeX + ", " + positionRangeY + ")");
	
	// this was position ranges when the image was inside a table cell; it doesn't work when it's inside the DIV
	// initialX = Math.round((Math.random() * positionRangeX) - (positionRangeX / 2));
	// initialY = Math.round((Math.random() * positionRangeY) - (positionRangeY / 2));
	
	// the coordinates of the image (top-left position) when the image is centered
	var centerX = 0 - (positionRangeX / 2);
	var centerY = 0 - (positionRangeY / 2);
	
	// TODO: movement should be able to be positive or negative
	movementX = Math.round((Math.random() * positionRangeX * 2) - (positionRangeX / 2));
	movementY = Math.round((Math.random() * positionRangeY * 2) - (positionRangeY / 2));
	
	initialX = centerX - (movementX / 2);
	initialY = centerY - (movementY / 2);
	DebugMessage ("PanObject: INFO: initial position = (" + initialX + ", " + initialY + ")");
	
	var finalX = initialX + movementX;
	var finalY = initialY + movementY;
	
	if (finalX > initialX) {
		maxPosX = finalX;
		minPosX = initialX;
	}
	else {
		maxPosX = initialX;
		minPosX = finalX;
	}
	if (finalY > initialY) {
		maxPosY = finalY;
		minPosY = initialY;
	}
	else {
		maxPosY = initialY;
		minPosY = finalY;
	}
	
//	var numSteps = 100;
	var numSteps = effectFPS * effectDuration;
	// motion used to be pixel movements per iteration
	// now this is done time-based, since iteration time can't be clearly determined
	var motionX = Math.round(movementX / effectDuration * 1000);
	var motionY = Math.round(movementY / effectDuration * 1000);
	DebugMessage ("PanObject: INFO: motion vector (pixels per second) = (" + motionX + ", " + motionY + ")");
	
	DebugMessage ("PanObject: INFO: moving image center from  = (" + (initialX - centerX) + ", " + (initialY - centerY) + ") to (" + (finalX - centerX) + ", " + (finalY - centerY) + ")");
	
	obj.style.position = "relative";
//	obj.style.position = "absolute";
	obj.style.left = initialX + "px";
	obj.style.top = initialY + "px";
	
	imageX = initialX;
	imageY = initialY;
	UpdateImagePosition(objId);
	
}

var imageZoomStartTime;
var imageZoomUpdateTime;
var numImageZoomUpdateFrames = 0;
var maxZoomFPS = 0;
var minZoomFPS = 0;
// TODO: make time-based, instead of increment-based
function UpdateImageZoom(objId, zoomPercent, zoomPercentStart, zoomPercentEnd, percentChangePerFrame, baseTop, baseLeft, baseWidth, baseHeight) {
	var obj;
	if (document.getElementById) {
		obj = document.getElementById(objId);
	}	
	if (!obj) {
		DebugMessage ("UpdateImageZoom: cannot retrieve object for object ID \"" + objId + "\"!  Exiting abnormally!");
		return;
	}
    if (obj) {
		var effectDuration = slideViewDuration + fadeDuration + fadeDuration + fadeDuration + fadeDuration;
		var d = new Date();
		var t = d.getTime();
		var elapsed = t - imageZoomStartTime;
		
		zoomPercent = Math.round(zoomPercentStart + ((zoomPercentEnd - zoomPercentStart) * elapsed / effectDuration) + 0.5);
		
		var h = baseHeight * (zoomPercent / 100);
		
		// center the image -- not working - moves from left to center or from center to left - larger = more left!
		// should zoom out proportionally from the center of the image - do we need to record the starting point?
		// new top is the base plus half the height increase from the base.  makes sense, right?  why doesn't it work?!?
		var newTop = Math.round((GetAvailableImageHeight() - h) / 2);
		var w = baseWidth * (zoomPercent / 100);
		var newLeft = Math.round((GetAvailableImageWidth() - w) / 2);
		if (w > GetAvailableImageWidth()) {
			obj.style.left = newLeft;  // doesn't work with center alignment
		}
		obj.style.height = h + "px";
		obj.style.top = newTop + "px";
		SetOpacity(obj,g_imageOpacity);
		// DebugMessage ("UpdateImageZoom: zoom=" + zoomPercent + "%; top=" + newTop + ", left=" + newLeft + ", width=" + w.toFixed(0) + ", height=" + h.toFixed(0) + "");
		numImageZoomUpdateFrames++;
			
		var currentFPS = 0;
		if (imageZoomUpdateTime) {
			var deltaFromPrevious = t - imageZoomUpdateTime;
			if (deltaFromPrevious <= 0) currentFPS = 0;
			else                        currentFPS = (1 / deltaFromPrevious) * 1000; 
			if (!minZoomFPS) minZoomFPS = currentFPS;
			if (minZoomFPS > currentFPS) minZoomFPS = currentFPS; 
			if (!maxZoomFPS) maxZoomFPS = currentFPS;
			if ((maxZoomFPS < currentFPS) && (currentFPS < 500)) maxZoomFPS = currentFPS; 
		}
		imageZoomUpdateTime = t;
		var elapsed = imageZoomUpdateTime - imageZoomStartTime;
		// DebugMessage ("UpdateImageZoom: VERBOSE: currentFPS=" + currentFPS.toFixed(2) + "fps, minZoomFPS=" + minZoomFPS.toFixed(2) + "fps, maxZoomFPS=" + maxZoomFPS.toFixed(2) + "fps");
		if (elapsed > effectDuration) {
			var averageZoomFPS = (numImageZoomUpdateFrames / elapsed) * 1000;
			DebugMessage ("UpdateImageZoom: time expired.  Average FPS = " + averageZoomFPS.toFixed(2) + "fps, min=" + minZoomFPS.toFixed(2) + ", max=" + maxZoomFPS.toFixed(2) + "fps");
		}
		else {
			// DebugMessage ("UpdateImageZoom:  " + objId + ", " + percentChangePerFrame + ", " + obj.style.height);
			window.setTimeout("UpdateImageZoom('"+objId+"', "+zoomPercent+", "+zoomPercentStart+", "+zoomPercentEnd+", "+percentChangePerFrame+", "+baseTop+", "+baseLeft+", "+baseWidth+", "+baseHeight+")", 1000 / effectFPS);
		}
	}
}

function ZoomObject(objId, baseTop, baseLeft, baseWidth, baseHeight) {
	
	var obj;	
	if (document.getElementById) {
		obj = document.getElementById(objId);
	}	
	if (!obj) {
		DebugMessage ("ZoomObject: cannot retrieve object for object ID " + objId + "!  Exiting abnormally!");
		return;
	}
	
	d = new Date();
	t = d.getTime();
	imageZoomStartTime = t;
	// start by increasing height & width percentage-wise
	// image will zoom between 100 & 135%, in or out
	var zoomRange = Math.round(Math.random() * 20) + 15;
	var zoomMin;
	if (ClipToFill) {
		// if we ClipToFill, our minimum zoom is set to make the width of the smallest dimension fit the screen
		if ((obj.width / obj.height) > (GetAvailableImageWidth() / GetAvailableImageHeight())) {
			// the object width is proportionally wider than the viewing area
			// we are clipping to fill, so scale to height; min zoom is set to make the image height fit the area
			zoomMin = 100 * (GetAvailableImageHeight() / obj.height);
		}
		else {
			zoomMin = 100 * (GetAvailableImageWidth() / obj.width);
		}
	}
	else {
		if ((obj.width / obj.height) > (GetAvailableImageWidth() / GetAvailableImageHeight())) {
			// the object width is proportionally wider than the viewing area
			// we are clipping to fill, so scale to height; min zoom is set to make the image height fit the area
			zoomMin = 100 * (GetAvailableImageWidth() / obj.width);
		}
		else {
			zoomMin = 100 * (GetAvailableImageHeight() / obj.height);
		}
	}
	// random whether we zoom in or out
	if (Math.random() > 0.5) {
		var zoomPercentStart = zoomMin;
		var zoomPercentEnd = zoomMin + zoomRange;
	}
	else {
		var zoomPercentStart = zoomMin + zoomRange;
		var zoomPercentEnd = zoomMin;
	}
	var numSteps = 100;
	var percentChangePerFrame = (zoomPercentEnd - zoomPercentStart) / numSteps;
	
	DebugMessage ("ZoomObject: obj size = (" + obj.width + ", " + obj.height + "), zoom range = " + zoomRange);
	
	if (percentChangePerFrame > 0) {
		DebugMessage ("ZoomObject: zooming in from " + zoomPercentStart + "% to " + zoomPercentEnd + "% at " + percentChangePerFrame + "% per frame.");
	}
	else {
		DebugMessage ("ZoomObject: zooming out from " + zoomPercentStart + "% to " + zoomPercentEnd + "% at " + percentChangePerFrame + "% per frame.");
	}	
	
	obj.style.position = "relative";
	UpdateImageZoom(objId, zoomPercentStart, zoomPercentStart, zoomPercentEnd, percentChangePerFrame, baseTop, baseLeft, baseWidth, baseHeight);
	
}

function StartSlideShow() {
	DebugMessage ("StartSlideShow nextImageObj.toString()=" + nextImageObj.toString());
	SlideshowMode = 1;
	//    window.setTimeout("FadeOut2('"+nextImageObj+"',"+100+")", 2000);
	if (FadeEffect) {
		window.setTimeout("FadeOut('"+imageId+"',"+100+")", 200);
	}
	else {
		QueueNextUrl(nextImageUrl, 8000);
	}
}

function StopSlideShow() {
	DebugMessage ("StopSlideShow");
	SlideshowMode = 0;
	SetOpacity(imageObj, 100);
	g_imageOpacity = 100;
	imageObj.style.visibility = 'visible';
}

function CenterObject(obj, availableWidth, availableHeight) {
//	if (isNaN(availableWidth)) availableWidth = availableWidth.replace(/px/,"");
//	if (isNaN(availableHeight)) availableHeight = availableHeight.replace(/px/,"");
	// availableWidth = rm_px(availableWidth);
	// availableHeight = rm_px(availableHeight);
	
	var w = obj.width;
	if (obj.style.width) w = obj.style.width.replace('px',"");
	var h = obj.height;
	if (obj.style.height) h = obj.style.height.replace('px',"");
	
	var debugInfo;
	var centerX = 0;
	var centerY = 0;
	
	// we're already centered
	// however, if the image is larger than the area, browsers seem to fix the top and the left to 0
	// the top and the left need to be adjusted if the image is wider and/or higher than the available area
	if (w > availableWidth) {
		centerX = (availableWidth - w) / 2;
	}
	if (h > availableHeight) {
		centerY = (availableHeight - h) / 2;
	}
	
	if (obj.style.align) debugInfo = debugInfo + "obj.style.align = " + obj.style.align + ", ";
	if (obj.style.valign) debugInfo = debugInfo + "obj.style.valign = " + obj.style.valign + ", ";
	DebugMessage ("CenterObject: INFO: w = " + w + ", h = " + h + ", availableWidth = " + availableWidth + ", availableHeight = " + availableHeight + ", Centering image (" + centerX + ", " + centerY + ")...");
	DebugMessage ("CenterObject: INFO: " + debugInfo);
	obj.style.position = "relative";
	obj.style.left = Math.round(centerX) + "px";
	obj.style.top = Math.round(centerY) + "px";
}

function ScaleImageToFit (a_width, a_height) {
	DebugMessage ("ScaleImageToFit: imageObj.width = " + originalWidth + ", imageObj.height = " + originalHeight + ", availableWidth = " + a_width + ", availableHeight = " + a_height);
	imageObj.style.position = "relative";
    if ((originalWidth > a_width) || (originalHeight > a_height)) {
		if ((originalWidth * 100 / a_width) > (originalHeight * 100 / a_height)) {
			// width is the limiting factor - with proportional scaling - we have extra space for the height
			if (ClipToFill) {
				// scale to height, and clipping the extra width out of the image
				DebugMessage ("  shrinking image to available height - " + originalHeight + "px high to " + a_height + " px high...");
				imageObj.style.width = null;
				imageObj.style.height = a_height;
			}
			else {
				// scale to width and allow blank space for height on the top & bottom
				DebugMessage ("  shrinking image to available width - " + originalWidth + "px wide to " + a_width + " px wide...");
				imageObj.style.height = null;
				imageObj.style.width = a_width;
			}
		}
		else {
			// height is the limiting factor
			if (ClipToFill) {
				// height is the limiting factor; scale to height and allow blank background for the left & right
				DebugMessage ("  shrinking image to available width - from " + originalWidth + "px wide to " + a_width + " px wide...");
				imageObj.style.height = null;
				imageObj.style.width = a_width;
			}
			else {
				// height is the limiting factor; scale to height and allow blank background for the left & right
				DebugMessage ("  shrinking image to available height - from " + originalHeight + "px high to " + a_height + " px high...");
				imageObj.style.width = null;
				imageObj.style.height = a_height;
			}
		}
    }
    else {
		imageObj.style.width = originalWidth + "px";
		imageObj.style.height = originalHeight + "px";
		DebugMessage ("ScaleImageToFit: nothing to scale.  Image is smaller than display area.  Centering instead....");
    }
	CenterObject(imageObj, a_width, a_height);
}

function ToggleSetting(sName) {
	DebugMessage ("ToggleSetting: INFO: sName = " + sName);
	if (sName == "SlideShow") ToggleSlideShow();
	else if (sName == "DebugMessages") ToggleDebugMessages();
	else if (sName == "ClipToFill") ToggleClipToFill();
	else DebugMessage ("ToggleSetting: ERROR: Unknown Setting: " + sName + "!");
}

function ToggleClipToFill() {
	if (ClipToFill) {
		ClipToFill = 0;
		DebugMessage ("ToggleClipToFill: INFO: Disabled ClipToFill.");
	}
	else {
		ClipToFill = 1;
		DebugMessage ("ToggleClipToFill: INFO: Enabled ClipToFill.");
	}
}

function ToggleScaleImage() {
	if (ScaleImage) {
		ScaleImage = 0;
		imageObj.style.height = originalHeight + "px";
		imageObj.style.width = originalWidth + "px";
		DebugMessage ("ToggleScaleImage: INFO: Disabled ScaleImage.");
	}
	else {
		ScaleImage = 1;
		ScaleImageToFit(GetAvailableImageWidth(), GetAvailableImageHeight());
		DebugMessage ("ToggleScaleImage: INFO: Enabled ScaleImage.");
	}
	DebugMessage ("ToggleScaleImage: INFO: Disabled ScaleImage.  imageObj.style.height set to " + imageObj.style.height + ", imageObj.style.width set to " + imageObj.style.width);
	DebugMessage ("ToggleScaleImage: INFO: AvailableImageWidth() = " + GetAvailableImageWidth() + ", GetAvailableImageHeight() = " + GetAvailableImageHeight());
	var w = GetAvailableImageWidth();
	var h = GetAvailableImageHeight();
	CenterObject(imageObj, w, h);
}

function ToggleSlideShow() {
	DebugMessage ("ToggleSlideShow");
	//    if (SlideshowMode) StopSlideShow();
	if ( mySlideshowCheckbox.checked == true ) {
		DebugMessage ("ToggleSlideShow: slideshow is checked!");
		StartSlideShow();
	}
	else {
//		DebugMessage ("ToggleSlideShow: slideshow is not checked!");
		StopSlideShow();
	}
}

function ToggleDebugMessages() {
	DebugMessage ("ToggleDebugMessages");
	var obj;
	var tableCellObj;
	if (document.getElementById) {
		obj = document.getElementById("debugTextbox");
		tableCellObj = document.getElementById("DebugMessageCell");
	}
	if (!obj) {
		DebugMessage ("ToggleDebugMessages: failed to get debug messagebox ID: debugTextbox.  Cannot toggle debug message visibility!");
		return
	}
	if ( myDebugMessagesCheckbox.checked == true ) {
		DebugMessage ("  Debug Messages are enabled");
		if (obj.style) {
			obj.style.visibility = 'visible';
		}
		else {
			DebugMessage ("ToggleDebugMessages: ERROR - debugTextbox does not support the .style property");
		}
		if (tableCellObj) {
			tableCellObj.width = "33%";
		}
		ShowDebugMessages = 1;
	}
	else {
		DebugMessage ("  Debug Message are disabled");
		if (obj.style) {
			obj.style.visibility = 'hidden';
		}
		else {
			DebugMessage ("ToggleDebugMessages: ERROR - debugTextbox does not support the .style property");
		}
		
		// this doesn't work! :(
		if (tableCellObj) {
			if (tableCellObj.width) {
				tableCellObj.width = "0px";
			}
		}
		ShowDebugMessages = 0;
	}
}

// TODO: finish implementation
function ToggleCheckbox(objId) {
	DebugMessage ("ToggleDebugMessages");
	var obj;
	var tableCellObj;
	if (document.getElementById) {
		obj = document.getElementById("debugTextbox");
		tableCellObj = document.getElementById("DebugMessageCell");
	}
	if (!obj) {
		DebugMessage ("ToggleDebugMessages: failed to get debug messagebox ID: debugTextbox.  Cannot toggle debug message visibility!");
		return
	}
	if ( myDebugMessagesCheckbox.checked == true ) {
		DebugMessage ("  Debug Messages are enabled");
		if (obj.style) {
			obj.style.visibility = 'visible';
		}
		else {
			DebugMessage ("ToggleDebugMessages: ERROR - debugTextbox does not support the .style property");
		}
		if (tableCellObj) {
			tableCellObj.width = "33%";
		}
		ShowDebugMessages = 1;
	}
	else {
		DebugMessage ("  Debug Message are disabled");
		if (obj.style) {
			obj.style.visibility = 'hidden';
		}
		else {
			DebugMessage ("ToggleDebugMessages: ERROR - debugTextbox does not support the .style property");
		}
		
		// this doesn't work! :(
		if (tableCellObj) {
			tableCellObj.width = "0px";
		}
		ShowDebugMessages = 0;
	}
}

function UpdateEffects() {
	DebugMessage ("UpdateEffects called.");
	// DebugMessage ("UpdateEffects: INFO: effect=" + document.effect.value);
	var randomEffectObj;
	var zoomEffectObj;
	var panEffectObj;
	var noEffectObj;
	if (document.getElementById) {
		randomEffectObj = document.getElementById("randomEffectId");
		zoomEffectObj = document.getElementById("zoomEffectId");
		panEffectObj = document.getElementById("panEffectId");
		noEffectObj = document.getElementById("noEffectId");
		fadeEffectObj = document.getElementById("fadeEffectId");
		scaleImageObj = document.getElementById("scaleImageId");
		clipToFillObj = document.getElementById("clipToFillId");
	}
	if (randomEffectObj) {
		if (randomEffectObj.checked) {
			RandomEffect = 1;
			DebugMessage("UpdateEffects: INFO: Random effect is set");
		}
		else RandomEffect = 0;
	}
	else {
		DebugMessage ("UpdateEffects: ERROR: Random Effect ID element not found!");
	}
	if (zoomEffectObj) {
		if (zoomEffectObj.checked) {
			 ZoomEffect = 1;
			DebugMessage("UpdateEffects: INFO: Zoom effect is set");
		}
		else ZoomEffect = 0;
	}
	else {
		DebugMessage ("UpdateEffects: ERROR: Zoom Effect element ID not found!");
	}
	if (panEffectObj) {
		if (panEffectObj.checked) {
			PanEffect = 1;
			DebugMessage("UpdateEffects: INFO: Pan effect is set");
		}
		else PanEffect = 0;
	}
	else {
		DebugMessage ("UpdateEffects: ERROR: Pan Effect element ID not found!");
	}
	if (noEffectObj) {
		if (noEffectObj.checked) {
			DebugMessage("UpdateEffects: INFO: No transitional effect set.");
		}
	}
	else {
		DebugMessage ("UpdateEffects: ERROR: No Effect element ID not found!");
	}
	if (fadeEffectObj) {
		if (fadeEffectObj.checked) {
			FadeEffect = 1;
			DebugMessage("UpdateEffects: INFO: Fade effect is set");
		}
		else FadeEffect = 0;
	}
	else {
		DebugMessage ("UpdateEffects: ERROR: Fade Effect element ID not found!");
	}
	if (scaleImageObj) {
		if (scaleImageObj.checked) {
			ScaleImage = 1;
			DebugMessage("UpdateEffects: INFO: Scale Image is set");
		}
		else ScaleImage = 0;
	}
	else {
		DebugMessage ("UpdateEffects: ERROR: Scale Image element ID not found!");
	}
	if (clipToFillObj) {
		if (clipToFillObj.checked) {
			ClipToFill = 1;
			DebugMessage("UpdateEffects: INFO: Clip To Fill is set");
		}
		else ClipToFill = 0;
	}
	else {
		DebugMessage ("UpdateEffects: ERROR: ClipToFill element ID not found!");
	}
	// refresh page with current setting
	ForwardToUrl(window.location.href);
}

function ImageLoadedEvent(myImage) {
    ImagePreloadTasks -= 1;
    DebugMessage("PRELOADED!  Queued Image Loaded!  Remaining ImagePreloadTasks = " + ImagePreloadTasks + "");
}

// TODO: we need to be calling this again!
function PreloadImage(url) {
    if (document.images) {
		DebugMessage("preloading next image: " + url + ".  Setting onload event handler...");
		preload_image_object = new Image();
		preload_image_object.onload = ImageLoadedEvent;
	    ImagePreloadTasks += 1;
		preload_image_object.src = url;
    }
}

// from http://www.webreference.com/programming/javascript/images/
function Is() {
    agent = navigator.userAgent.toLowerCase();
    this.major = parseInt(navigator.appVersion);
    this.minor = parseFloat(navigator.appVersion);
    this.ns = ((agent.indexOf('mozilla') != -1) && ((agent.indexOf('spoofer')== -1) && (agent.indexOf('compatible') ==  -1)));
    this.ns2 = (this.ns && (this.major == 3));
    this.ns3 = (this.ns && (this.major == 3));
    this.ns4b = (this.ns && (this.major == 4) && (this.minor <= 4.03));
    this.ns4 = (this.ns && (this.major == 4));
    this.ns6 = (this.ns && (this.major >= 5));
    this.ie = (agent.indexOf("msie") != -1);
    this.ie3 = (this.ie && (this.major < 4));
    this.ie4 = (this.ie && (this.major == 4) && (agent.indexOf("msie 5.0") == -1));
    this.ie5 = (this.ie && (this.major == 4) && (agent.indexOf("msie 5.0") != -1));
    this.ie55 = (this.ie && (this.major == 4) && (agent.indexOf("msie 5.5") != -1));
    this.ie6 = (this.ie && (agent.indexOf("msie 6.0")!=-1) );
    this.aol = (agent.indexOf("aol") != -1);
    this.aol3 = (this.aol && this.ie3);
    this.aol4 = (this.aol && this.ie4);
    this.aol5 = (this.aol && this.ie5);
}

/*******************************************************************************

'args.js', by Charlton Rose

Permission is granted to use and modify this script for any purpose,
provided that this credit header is retained, unmodified, in the script.

*******************************************************************************/


// This function is included to overcome a bug in Netscape's implementation
// of the escape () function:

function myunescape (str)
{
    str = '' + str;
    while (true)
	{
	    var i = str . indexOf ('+');
	    if (i < 0)
		break;
	    str = str . substring (0, i) + ' ' + str . substring (i + 1, str . length);
	}
    return unescape (str);
}

// This function creates the args [] array and populates it with data
// found in the URL's search string:
function args_init ()
{
	args = new Array ();
	var argstring = window . location . search;
	if (argstring . charAt (0) != '?') return;
	argstring = argstring . substring (1, argstring . length);
	var argarray = argstring . split ('&');
	var i;
	var singlearg;
    for (i = 0; i < argarray . length; ++ i)
	{
		singlearg = argarray [i] . split ('=');
		if (singlearg . length != 2) continue;
		var key = myunescape (singlearg [0]);
		var value = myunescape (singlearg [1]);
		args [key] = value;
	}
}

function updatePageLayout () {
    /*
    if (imageObj.width > GetAvailableImageWidth()) {
	DebugMessage ("  shrinking image to available width - " + imageObj.width + "px wide to " + GetAvailableImageWidth() + " px wide...");
	imageObj.width = GetAvailableImageWidth();
    }
    if (imageObj.height > GetAvailableImageHeight()) {
	DebugMessage ("  shrinking image to available height - " + imageObj.height + "px high to " + GetAvailableImageHeight() + " px high...");
	imageObj.height = GetAvailableImageHeight();
    }
    */

    var pImageObj; 
	var heightBuffer = 40;

	var s_height = GetClientHeight();
    var imageTableObj;

	if (document.getElementById) {
		imageTableObj = document.getElementById(imageTableId);
		pImageObj = document.getElementById(imageId);
	}	
	if (!imageTableObj) {
		DebugMessage ("updatePageLayout: cannot retrieve object for object ID " + imageTableId + "!  Exiting abnormally!");
		return;
	}
	if (pImageObj) {
		originalWidth = pImageObj.width;
		originalHeight = pImageObj.height;
	}
	DebugMessage("updatePageLayout: imageTableObj.style.height=" + imageTableObj.style.height + "px, window.innerHeight=" + s_height + "px.");
	if (imageTableObj.style.height) {
		DebugMessage("updatePageLayout: shrinking imageTable from " + imageTableObj.style.height + "px to " + s_height + "px.");
		imageTableObj.style.height = s_height - heightBuffer + "px";
	}
	/*
	if (imageObj.style.height) {
		DebugMessage("updatePageLayout: shrinking imageTable from " + imageObj.style.height + "px to " + s_height - heightBuffer + "px.");
		imageObj.style.height = s_height - heightBuffer + "px";
	}
	*/
	
	// TODO: calculate this buffer more precisely
	document.getElementById("InfoCell").style.height = 100 + "px";
	document.getElementById("ImageCell").style.height = s_height - 100 - heightBuffer + "px";

	if (RandomEffect) {
	}
	else if (ZoomEffect) {
	}
	else if (PanEffect) {
	}
	else {
		imageObj.style.position = "relative";
		if (ScaleImage) {
			ScaleImageToFit(GetAvailableImageWidth(), GetAvailableImageHeight())
		}
		else {
			
		}
		var centerX = (GetAvailableImageWidth() - imageObj.style.width) / 2;
		var centerY = (GetAvailableImageHeight() - imageObj.style.height) / 2;
		DebugMessage ("loadPage: INFO: no effects have been set in the url arguments or from existing settings in the HTML GUI.  Centering image (" + centerX + ", " + centerY + ")...");
		imageObj.style.left = Math.round(centerX) + "px";
		imageObj.style.top = Math.round(centerY) + "px";
	}
	

}

// setting priorities
//   URL args
//   settings from HTML page
//   script defaults
function LoadSettings() {
}

function getFlag(guiObject, flag) {

	if (!guiObject) return 0;

//    DebugMessage (guiObject.id + " URL arg [" + flag + "]= " + args [flag]);
    if (args [flag] == "On") {
//		DebugMessage (guiObject.id + " is on; activate the checkbox");
		guiObject.checked = true;
    }
    if ( guiObject.checked == true ) {
//		DebugMessage ("setFlag: " + guiObject.id + " is checked!");
		return 1;
    }
    else {
//		DebugMessage ("setFlag: " + guiObject.id + " is not checked!");
		return 0;
    }
}

function loadPage(currentImageArg, nextImageArg, previousImageArg) {

	// loadObjects
	// readPreferences
	// updatePageLayout
	
	nextImageId = "nextImage";
	previousImageId = "previousImage";
	imageId = "image1";
	
	d = new Date();
	StartTime = d.getTime();
	
	is = new Is();
	
	// Call the args_init () function to set up the args [] array:
	args_init ();
	
    if (document.getElementById) {
		debugTextboxObj = document.getElementById("debugTextbox");
		imageObj = document.getElementById("image1");
		nextImageObj = document.getElementById("nextImage");
		previousImageObj = document.getElementById("previousImage");
		myImageDiv = document.getElementById("imageDiv");
		mySlideshowCheckbox = document.getElementById("slideshowId2");
		myDebugMessagesCheckbox = document.getElementById("debugMessagesId");
		myScaleImageCheckbox = document.getElementById("scaleImageId");
		myClipToFillCheckbox = document.getElementById("clipToFillId");
		myFadeEffectCheckbox = document.getElementById("fadeEffectId");
		myZoomEffectRadioButton = document.getElementById("zoomEffectId");
		myPanEffectRadioButton = document.getElementById("panEffectId");
		myRandomEffectRadioButton = document.getElementById("randomEffectId");
		myNoEffectRadioButton = document.getElementById("noEffectId");
    }
	if (debugTextboxObj) debugTextboxObj.value = "";
    DebugMessage ("loadPage imageId=" + imageId + ", nextImageId=" + nextImageId + ", previousImageId=" + previousImageId);

    SetOpacity(imageObj, 0);
    imageObj.style.visibility = 'visible';
    updatePageLayout();

    if (nextImageObj) {
    	nextImageUrl = nextImageObj.href;
	    DebugMessage ("  nextImageUrl = " + nextImageUrl);
    }
    else {
	    DebugMessage ("  WARNING: nextImageObj is not set");
   	}
    if (nextImageArg) {
    	PreloadImage (nextImageArg);
    }
    else {
	    DebugMessage ("  WARNING: nextImageArg is not set");
   	}
    if (previousImageObj) {
    	previousImageUrl = previousImageObj.href;
    }
    else {
	    DebugMessage ("  WARNING: previousImageObj is not set");
   	}
    if (previousImageArg) {
    	PreloadImage (previousImageArg);
    }
    else {
	    DebugMessage ("  WARNING: previousImageArg is not set");
   	}

    DebugMessage ("  nextImageUrl=" + nextImageUrl + "\n  previousImageUrl=" + previousImageUrl);
    DebugMessage ("  nextPicture=" + nextImageArg + "\n  previousPicture=" + previousImageArg);
    DebugMessage ("  window.clientX=" + window.clientX + ", window.clientY=" + window.clientY);
    DebugMessage ("  window.innerWidth=" + window.innerWidth + ", window.innerHeight=" + window.innerHeight);
    DebugMessage ("  document.body.offsetWidth=" + document.body.offsetWidth + ", document.body.offsetHeight=" + document.body.offsetHeight);
    DebugMessage ("  screen.width=" + screen.width + ", screen.height=" + screen.height);
    DebugMessage ("  imageObj.style.top=" + imageObj.style.top + ", imageObj.style.left=" + imageObj.style.left);
    DebugMessage ("  imageObj.width=" + imageObj.width + ", imageObj.height=" + imageObj.height);

	// such as this abstracted function
	SlideshowMode = getFlag(mySlideshowCheckbox, "SlideshowMode", "On");
	ClipToFill = getFlag(myScaleImageCheckbox, "ClipToFill", "On");
	RandomEffect = getFlag(myRandomEffectRadioButton, "RandomEffect", "On");
	ZoomEffect = getFlag(myZoomEffectRadioButton, "ZoomEffect", "On");
	PanEffect = getFlag(myPanEffectRadioButton, "PanEffect", "On");
	ScaleImage = getFlag(myScaleImageCheckbox, "ScaleImage", "On");
	FadeEffect = getFlag(myFadeEffectCheckbox, "FadeEffect", "On");
	DebugMessages = getFlag(myDebugMessagesCheckbox, "DebugMessages", "On");

//	ReadControlGui(); 	// set default control commands from the current GUI values
//	SetParametersFromCommandLine();
//	UpdateControlGui();

	if (ScaleImage) {
		DebugMessage ("loadPage: INFO: Image Scaling is enabled");
		if (ClipToFill) {
			DebugMessage ("loadPage: INFO: Image will be clipped to fill frame.");
		}
	}
	if (RandomEffect) {
		var m = Math.random();
		if (m < 0.5) {
			DebugMessage ("loadPage: INFO: RandomEffect - pan effect selected.  m = " + m);
			PanObject("image1");
			PanEffect = 1;
		}
		else {
			var baseTop = (GetAvailableImageHeight() - imageObj.height) / 2;
			DebugMessage ("loadPage: INFO: RandomEffect - zoom effect selected.  m = " + m + ", baseTop = " + baseTop);
			ZoomObject("image1", baseTop, 0, imageObj.width, imageObj.height);
			ZoomEffect = 1;
		}
	}
	else if (ZoomEffect) {
		var baseTop = (GetAvailableImageHeight() - imageObj.height) / 2;
		DebugMessage ("loadPage: INFO: RandomEffect - zoom effect selected.  baseTop = " + baseTop);
		ZoomObject("image1", baseTop, 0, imageObj.width, imageObj.height);
		ZoomEffect = 1;
	}
	else if (PanEffect) {
		DebugMessage ("loadPage: INFO: RandomEffect - pan effect selected.");
		PanObject("image1");
		PanEffect = 1;
	}
	else {
		if (ScaleImage) {
			ScaleImageToFit(GetAvailableImageWidth(), GetAvailableImageHeight());
		}
		else {
			imageObj.style.width = imageObj.width;
			imageObj.style.height = imageObj.height;
			CenterObject (imageObj, GetAvailableImageWidth(), GetAvailableImageHeight());
		}
	}
	
	ToggleDebugMessages();
	ToggleSlideShow();

	if (FadeEffect) {
		DebugMessage ("loadPage: INFO: FadeEffect is enabled.");
	    FadeIn(imageId,0);
	}
	else {
		DebugMessage ("loadPage: INFO: FadeEffect is disabled.");
		SetOpacity(imageObj,100);
	}
}

