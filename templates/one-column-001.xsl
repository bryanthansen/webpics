<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/album">
    <html>
  
      <head>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html" charset="ISO-8859-1" />
        <title>Online Photo Album at bryanthansen.net</title>
        <link rel="stylesheet" type="text/css" href="html/style.css"  MEDIA="screen" />
        <!-- META name="description" content="Bryant Hansen's online photo album: <object id="Series" type="PhotoAlbumTag"/> " -->
      </head>

      <body>
        <div>
          <h2><xsl:value-of select="title"/></h2>
        </div>
        <div>
          <!-- img src="{/pictures/picture/thumbnails/thumbnail/filename/node()}" class="picture"/ -->
          <xsl:for-each select="pictures/picture">
          <!-- xsl:value-of select="filename" /><br / -->
          <!-- xsl:value-of select="filedate" /><br / -->
          <div>
            <a href=".thumbnails1024/html/picture_{filename/node()}.html">
              <img src="{thumbnails/thumbnail/filename/node()}" class="picture"/>
            </a>
          </div>
          <br />
          </xsl:for-each>
        </div>
        <div>
        
        </div>
      </body>

    </html>
  </xsl:template>
</xsl:stylesheet>
