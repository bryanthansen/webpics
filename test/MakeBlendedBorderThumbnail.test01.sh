#!/bin/bash
# Bryant Hansen

TEST_IMAGE="IMG_5269h.png"
OUTDIR=.blendedBorderTest5

[[ ! -d "$OUTDIR" ]] && mkdir -p "$OUTDIR"

set -o verbose

n=0

#for size in 256 600 0 "" ; do
for size in 256 ; do
#    for bgcolor in black white "#f00516" "" random random random random random ; do
#    for bgcolor in black white "#f00516" "" random random ; do
    for bgcolor in white random random ; do
        echo "bgcolor is random \"${bgcolor})\""
        if [[ "$bgcolor" == "random" ]] ; then
            bgcolor = "rgb($(expr $RANDOM % 256),$(expr $RANDOM % 256),$(expr $RANDOM % 256))"
            echo "bgcolor is random (${bgcolor})" >&2
        fi
#        for options in \
#            " --no-overlay --no-blended-border --no-dropshadow" \
#            " --no-blended-border --no-dropshadow" \
#            " --no-blended-border" \
#            " --no-dropshadow" \
#            ""
#        do
        for options in \
            " --no-blended-border --no-dropshadow" \
            " --no-blended-border" \
            " --no-dropshadow"
        do
                n=`expr $n + 1`
                /projects/webpics/MakeBlendedBorderThumbnail.sh \
                    -v \
                    $options \
                    "$TEST_IMAGE" \
                    "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" \
                    $size \
                    $bgcolor | tee "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE".txt 2>&1
        done
    done
done

gqview "$OUTDIR" &

exit 0

/projects/webpics/MakeBlendedBorderThumbnail.sh "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" "#aaaa22"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600 "#aaaa22"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600 "#bbaa22"
n=`expr $n + 1`
/projects/webpics/MakeDropshadowThumbnails.sh "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600

n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600 "#bbaa22"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600 "#bbccaa"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600 "rgb($(expr $RANDOM % 256),$(expr $RANDOM % 256),$(expr $RANDOM % 256))"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "rgb($(expr $RANDOM % 256),$(expr $RANDOM % 256),$(expr $RANDOM % 256))"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 black
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 1024
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 1024 black
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 1024 white
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 256
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 600 "rgb($(expr $RANDOM % 256),$(expr $RANDOM % 256),$(expr $RANDOM % 256))"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "#f00516"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "#f00516"

n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v --no-blended-border --no-dropshadow "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "#f00516"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v --no-dropshadow "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "#f00516"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v --no-overlay --no-blended-border --no-dropshadow "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "#f00516"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v --no-overlay --no-blended-border "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "#f00516"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v --no-overlay --no-dropshadow "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "#f00516"
n=`expr $n + 1`
/projects/webpics/MakeBlendedBorderThumbnail.sh -v --no-overlay "$TEST_IMAGE" "$OUTDIR"/`printf "%03d" $n`_"$TEST_IMAGE" 0 "#f00516"
