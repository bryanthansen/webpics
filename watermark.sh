# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
################################################################################
# Header Info
################################################################################
# by Bryant Hansen <sourcecode@bryanthansen.net>
# copyright (c) 2003
# all rights reserved
# contact watermark-script@bryanthansen.net for licensing information
################################################################################
#        1         2         3         4         5         6         7         8
# 345678901234567890123456789012345678901234567890123456789012345678901234567890
################################################################################

USAGE="`basename $0` [ -v ] [ -h ] [ -i intensity ] [ -w WATERMARK_IMAGE ] ORIGINAL_IMAGE OUTPUT_IMAGE"


################################################################################
# set defaults
################################################################################
ECHO="echo -e"
WATERMARK_PROGRAM="composite -dissolve "
VERBOSE=0
WATERMARK_IMAGE="/projects/webpics/watermark-bryant.gif"
DEFAULT_WATERMARK=1
DEFAULT_INTENSITY=5
INTENSITY=$DEFAULT_INTENSITY
################################################################################


################################################################################
# read optional parameters
################################################################################
OPTIONAL_PARAM=1
while (( $OPTIONAL_PARAM == 1 ))
do
   OPTIONAL_PARAM=0
   if [ ${1:0:2} = "-v" ] ; then
      OPTIONAL_PARAM=1
      $ECHO "verbose mode is on"
      VERBOSE=1
      shift
   fi
   if [ ${1:0:2} = "-w" ] ; then
      OPTIONAL_PARAM=1
      shift
      DEFAULT_WATERMARK=0
      WATERMARK_IMAGE="$1"
      shift
   fi
   if [ ${1:0:2} = "-i" ] ; then
      OPTIONAL_PARAM=1
      shift
      INTENSITY=$1
      shift
   fi
   if [ ${1:0:2} = "-h" ] ; then
      $ECHO
      $ECHO "`basename $0` Help Information"
      $ECHO
      $ECHO "NAME"
      $ECHO "\t`basename $0` - watermark image with another image"
      $ECHO
      $ECHO "SYNOPSIS"
      $ECHO "\t$USAGE"
      $ECHO
      $ECHO "DESCRIPTION"
      $ECHO "\twatermark an image with another image"
      $ECHO
      $ECHO "\t-v"
      $ECHO "\t\tbe verbose"
      $ECHO
      $ECHO "\t-h"
      $ECHO "\t\tshow help"
      $ECHO
      $ECHO "\t-w WATERMARK_IMAGE"
      $ECHO -n "\t\tuse WATERMARK_IMAGE for the watermark, rather than the "
      $ECHO "watermark image"
      $ECHO
      $ECHO "\t-i INTENSITY"
      $ECHO -n "\t\tvary the intensity of the watermark on the base image. "
      $ECHO -n "valid settings are between 1 and 100 (inclusive).  The default "
      $ECHO "intensity setting is $DEFAULT_INTENSITY."
      $ECHO
      $ECHO "AUTHOR"
      $ECHO "\twritten by Bryant Hansen"
      $ECHO
      $ECHO "REPORTING BUGS"
      $ECHO "\tReport bugs to watermark-script@bryanthansen.net"
      $ECHO
      $ECHO "COPYRIGHT"
      $ECHO "\tcopyright (c) 2003 by Bryant Hansen"
      $ECHO "\tall rights reserved"
      $ECHO
      $ECHO "LICENSE"
      $ECHO "\tNO WARRANTY - NO GUARANTEE - USE AT YOUR OWN RISK"
      $ECHO "\tFree for personal use"
      $ECHO -n "\tcontact watermark-script@bryanthansen.net to obtain any special "
      $ECHO "permissions for professional use, source code use, or other"
      $ECHO
      $ECHO
      $ECHO
      $ECHO
      # we don't continue if we're displaying help informtion
      exit 0
   fi
done
################################################################################


################################################################################
# print optional settings
################################################################################
if (($VERBOSE)) ; then
   $ECHO "WATERMARK_IMAGE=\"$WATERMARK_IMAGE\""
   $ECHO "INTENSITY=$INTENSITY"
fi
################################################################################


################################################################################
# validate parameters
################################################################################
if [[ $INTENSITY -lt 1 ]] || [[ $INTENSITY -gt 100 ]] ; then
   $ECHO "ERROR #-101: Invalid Intensity Value: $INTENSITY!"
   $ECHO "USAGE=$USAGE"
   $ECHO "try \"$0 -h \" for a more detailed description of usage."
   exit -101
fi
if [ $# -ne 2 ] ; then
   $ECHO "ERROR #-65: wrong number of arguments!"
   $ECHO "USAGE=$USAGE"
   $ECHO "try \"$0 -h \" for a more detailed description of usage."
   exit -65
fi
if [ ! -f "$WATERMARK_IMAGE" ] ; then
   $ECHO "ERROR #-102: watermark image not found: \"$WATERMARK_IMAGE\"!"
   exit -102
fi
if [ ! -f "$1" ] ; then
   $ECHO "ERROR #-103: base image not found: \"$1\"!"
   exit -103
fi
################################################################################


################################################################################
# the program itself
################################################################################
if (($VERBOSE)) ; then
   echo "$WATERMARK_PROGRAM \"$INTENSITY\"\"x100\" -tile \"$WATERMARK_IMAGE\" \"$1\" \"$2\""
fi
$WATERMARK_PROGRAM ${INTENSITY}x100 -tile "$WATERMARK_IMAGE" "$1" "$2"
################################################################################

