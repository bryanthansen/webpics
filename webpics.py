#!/usr/bin/python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################################
# webpics.py
# written by: Bryant Hansen <sourcecode@bryanthansen.net>
# created: 20080904
# last updated: 20080907
#
# copyright (c) 2008 by Bryant Hansen
##################################################################

""" Program WebpicsHelperFunctions.py

This program is a set of utility functions that are intended to be used in many of the webpics scripts.

TODO: this is total crap.  My old learning-python project.  Rewrite me!
"""

import sys
import os
import os.path
import subprocess
import binascii
import hashlib
import time
import tempfile

IMAGE_INFO_DIR=".image_info"

verbose = 0
bash_script_output = 0

import sys

def TRACE (message):
    print (time.strftime("%Y-%m-%d %H:%M:%S") + " " + message)

def ERROR (message):
    sys.stderr.write(time.strftime("%Y-%m-%d %H:%M:%S") + " " + message)


def read_config(varname):
    # TODO: finish and test
    # should accept a sequential list of config files
    conf_files = ["/projects/webpics/album.conf", "/etc/webpics/album.conf", os.environ['HOME'] + "/.webpics/album.conf", "album.conf"]
    ret_val = ""
    ret_from_file = ""
    # read all conf files and all instances of the variable in a last-instance-wins fashion
    for file in conf_files:
        if os.path.exists(file):
            # TRACE(file + " exists")
            # TODO: there's probably a python way of doing this, rather than this ugly cat/egrep/sed way
            command = 'cat ' + file + ' | egrep "^[ \t]*' + varname + '=.*" | sed "s/^[ \t]*' + varname + '=//"'
            ret = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
            val = ret.stdout.readline()
            while val != "":
                ret_val = val.strip()
                val = ret.stdout.readline()
                ret_from_file = file
    if ret_val == "":
        TRACE("read_config(" + varname + "): no setting in any config file")
    else:
        TRACE("read_config: " + ret_from_file + ": " + varname + " = " + ret_val)
    return ret_val


def do_safe_command(ccmd, outfile):
    """
    This will make a temporary backup of the original file.
    It will then check if the new file is created properly.
    If the new file is not properly-created; the original is restored
    @param ccmd: a shell command to execute
    @param outfile: the outfile that we expect to generate from the command
    """
    fn = outfile
    if os.path.exists(outfile):
        t = tempfile.mkstemp()
        fn = t[1]
        TRACE("tempfile = %s  fn = %s" % (str(t), fn))
    p = subprocess.Popen(ccmd, shell=True)
    sts = os.waitpid(p.pid, 0)
    # validate
    if os.path.exists(fn):
        if not os.path.exists(outfile):
            try:
                os.rename(fn, outfile)
            except:
                ERROR(
                    "do_safe_command: failed to create %s and failed to "
                    "restore it from the backup!"
                    % outfile
                )
                pass


def is_fresh (file, dependencies):
    if not os.path.exists(file):
        return False

    for dep in dependencies:
        if os.path.getmtime(dep) > os.path.getmtime(file):
            return False
        return True


def getPicWidth(pic):
	width = 0
	ExifFile=os.path.join(os.path.dirname(pic), IMAGE_INFO_DIR, os.path.basename(pic)) + ".txt"
	if os.path.exists(ExifFile):
		# this command means to extract a line from the EXIF file that starts with the "Geometry=" tag, but removes it for the answer
		# TODO: translate this to python!
		# what this command does:
		# 	1) find and isolate the first line that starts with "Geometry:", with only optional whitespace before
		# 	2) strip out all data except for the numerical width value.  This includes:
		# 		- any leading whitewhitespace
		# 		- the Geometry tag with the trailing colon and any whitespace that follows
		# 		- everything after the width dimension, which starts with "x" through the end of the line
		CCMD="cat " + ExifFile + " | sed -n \"/^[ \t]*Geometry:/p\" | sed \"s/[ \t]*Geometry:[ \t]*//;s/x.*//g\""
#		DebugMessage ("CCMD = " + CCMD)
		proc1=subprocess.Popen(CCMD, shell=True, stdout=subprocess.PIPE)
		width=proc1.communicate()[0].strip()
#		width=Popen(CCMD, stdout=PIPE).communicate()[0]
		if verbose: DebugMessage ("reading width from info file: \"" + ExifFile + "\".  w=" + width + "")
	if width <= 0 or not width:
		# if VERBOSE:
		DebugMessage ("Exif data not properly read from " + ExifFile + ".  Reading manually....")
		width=subprocess.Popen("identify -format \"%w\" \"" + pic + "\"", shell=True, stdout=subprocess.PIPE).communicate()[0]
	return int(width)


def getPicHeight(pic):
	height = 0
	ExifFile=os.path.join(os.path.dirname(pic), IMAGE_INFO_DIR, os.path.basename(pic)) + ".txt"
	if os.path.exists(ExifFile):
		# this command means to extract a line from the EXIF file that starts with the "Geometry=" tag, but removes it for the answer
		# TODO: translate this to python!
		# what this command does:
		# 	1) find and isolate the first line that starts with "Geometry:", with only optional whitespace before
		# 	2) strip out all data except for the numerical height value.  This includes:
		# 		- any leading whitewhitespace
		# 		- the Geometry tag with the trailing colon
		# 		- the width dimension plus the following "x"
		# 		- everything after the height dimension, which starts with "+" through the end of the line
		CCMD="cat " + ExifFile + " | sed -n \"/^[ \t]*Geometry:/p\" | sed \"s/[ \t]*Geometry:[ \t].*x//;s/+.*//g\""
		height=subprocess.Popen(CCMD, shell=True, stdout=subprocess.PIPE).communicate()[0].strip()
		if verbose: DebugMessage ("reading height from info file: \"" + ExifFile + "\".  h=" + height + "")
	if height <= 0 or not height:
		# if VERBOSE:
		DebugMessage ("Exif data not properly read from " + ExifFile + ".  Reading manually....")
		height=subprocess.Popen("identify -format \"%h\" \"" + pic + "\"", shell=True, stdout=subprocess.PIPE).communicate()[0]
	return int(height)


# return a path that is a relative path to dir1 from dir2
def make_rel_path(todir, fromdir):
	# make sure our paths are absolute, before we make one a relative link to the other
	abs1 = os.path.abspath(todir)
	abs2 = os.path.abspath(fromdir)
	print ("make_rel_path: calculating relative path to \"" + abs1 + "\" from \"" + abs2 + "\"...")

	# WARNING: QUIRK: os.path.commonprefix returns the common string of 2 pathnames, not the deepest common directory
	# For example, the common directories between /home/mr_x/pictures/test and /home/mr_x/pictures/t_test is /home/mr_x/pictures/t!
	# Another bad case is that /pictures/test and /pictures/test2 return /pictures/test, which is a valid directory
	# HOW TO DEAL WITH THIS?  ...AND DEAL WITH IT EFFICIENTLY???
	common = os.path.commonprefix([abs1, abs2])
	# oh, duh!  it must end in an os.sep character if there's no directory match
	# delete all after the last os.sep
	cdir = common.rpartition(os.sep)[0] + os.sep

	# wipe out the common portion of each pathname
	rel1 = abs1.replace(cdir, '', 1)
	rel2 = abs2.replace(cdir, '', 1)

	# split link2 into it's separate components
	# we'll just traverse upwards, replacing each dir with a parent directory symbol ".."
	spl = rel2.split(os.sep)
	out = ""
	for s in spl:
		if out != "": out += os.sep
		if s: out += ".."

	# then add our original path to the end
	out += os.sep + rel1
	print ("make_rel_path: path = \"" + out + "\"")
	return os.path.normpath(out)


def calculate_outfile_link(infile, outfile):
	# getting the link to the output file is a surprisingly-complicated formula
	# maybe I've just made it more complicated than it needs to be - TODO: refine and simplify
	# I've decided that a relative link is necessary, so that directories are generally relocatable
	# if the outfile is in a new dir, then it should link to the target relative from that new dir

	link_dir_rel = make_rel_path(os.path.dirname(os.readlink(infile)), os.path.dirname(outfile))
	print ("link_dir_rel = " + link_dir_rel)
	outfile_temp = os.path.normpath(link_dir_rel + os.sep + outfile)
	# print "outfile_temp = " + outfile_temp
	outfile_link =  outfile_temp.replace(infile, os.path.basename(os.readlink(infile)))
	print ("outfile = " + outfile + ", outfile_link = " + outfile_link)
	return outfile_link


def isNameInFileList(name, infile):
	in_file = False
	if os.path.exists(infile):
		f = open(infile)
		try:
			for line in f:
				if line.strip() == name:
					f.close()
					return True
		finally:
			f.close()
	return in_file


def fileTextHexMD5(infile):
	# m = md5.new()
	m = hashlib.md5()
	fin = open(infile, 'r')
	try:
		for line in fin:
			m.update(line.strip() + "\n")
	except:
		return 0
	finally:
		fin.close()
	return m.hexdigest()


def textdiff(file1, file2):
	if not os.path.exists(file1) or not os.path.exists(file2):
		return True
	md51 = fileTextHexMD5(file1)
	md52 = fileTextHexMD5(file2)
	if md51 == md52:
		return False
	else:
		return True


def handle_link(infile, outfile, conf_files=None):
	# print "infile = " + infile + ", infile_link = " + os.readlink(infile)

	# TODO: consider replacing a file with the corresponding link if the linked file is newer
	# TODO: consider linking to conf_file if infile is a link
	# we have a link to our infile
	# if outfile or conf_file don't exist and they *do* exist in the
	#   dir of the infile target, then link conf_file and/or infile
	if os.path.islink(infile):
		if not os.path.exists(outfile):
			# early attempt at making the outfile link
			# outfile_link = os.path.basename(outfile.replace(infile, os.readlink(infile)))

			outfile_link = calculate_outfile_link(infile, outfile)
			# make an absolute path to outfile_link so we can test for it's existence
			# TODO: should use os.join here
			outfile_link_abs = os.path.normpath(os.getcwd() + os.sep + os.path.dirname(outfile) + os.sep + outfile_link)
			if os.path.exists(outfile_link_abs):
				print ("handle_link: " + outfile + " -> " + outfile_link)
				os.symlink(outfile_link, outfile)
			else:
				print ("handle_link: the linked output file, \"" + outfile_link_abs + "\" for \"" + infile + "\" does not exist.")
		else:
			if os.path.islink(outfile):
				# print "handle_link: the output file \"" + outfile + "\" already exists as a link.  Not updating.  TODO: consider if this should be done."
				pass
			else:
				print ("handle_link: the output file \"" + outfile + "\" already exists as a regular file.  Not replacing.  TODO: consider if this should be done if the link is newer.")

		if conf_files:
			for conf_file in conf_files:
				if not os.path.exists(conf_file):
					outfile_link = calculate_outfile_link(infile, conf_file)
					# TODO: should use os.join here
					outfile_link_abs = os.path.normpath(os.getcwd() + os.sep + os.path.dirname(conf_file) + os.sep + outfile_link)
					if os.path.exists(outfile_link_abs):
						print ("handle_link: " + conf_file + " -> " + outfile_link)
						os.symlink(outfile_link, conf_file)
					else:
						print ("handle_link: the linked output file, \"" + outfile_link_abs + "\" for \"" + infile + "\" does not exist.")


# obsolete
def _check_link(infile, outfile, conf_file):
	# TODO: consider replacing a file with the corresponding link if the linked file is newer
	if os.path.islink(infile):
		# we have a link to our infile
		# if outfile or conf_file don't exist and they *do* exist in the
		#   dir of the infile target, then link conf_file and/or infile
		if not os.path.exists(outfile):
			if not os.path.exists(conf_file):
				link_dir = os.path.dirname(os.readlink(infile))
				if os.path.exists(link_dir + os.path.basename(conf_file)):
					os.symlink(link_dir + os.path.basename(conf_file), conf_file)
				if os.path.exists(link_dir + os.path.basename(outfile)):
					os.symlink(link_dir + os.path.basename(outfile), outfile)


# obsolete
def _handle_link(infile, outfile, conf_file):
	# TODO: consider replacing a file with the corresponding link if the linked file is newer
	# TODO: consider linking to conf_file if infile is a link
	# we have a link to our infile
	# if outfile or conf_file don't exist and they *do* exist in the
	#   dir of the infile target, then link conf_file and/or infile

	if os.path.islink(infile):
		link_dir = os.path.dirname(os.readlink(infile))
		if not os.path.exists(outfile):
			if os.path.exists(link_dir + os.path.basename(outfile)):
				os.symlink(link_dir + os.path.basename(outfile), outfile)
		if not os.path.exists(conf_file):
			if os.path.exists(link_dir + os.path.basename(conf_file)):
				os.symlink(link_dir + os.path.basename(conf_file), conf_file)


# from http://www.nabble.com/python-question---how-to-test-if-file-is-executable-in-path--td18547896.html
def find_exe(name):
    try:
        path = os.getenv("PATH")
        if path == None:
            path = os.defpath
        for dir in path.split(os.pathsep):
            file = os.path.join(dir, name)
            if os.access(file, os.X_OK):
                return file
    except:
        pass
    return None
